<?php

namespace IiMedias\StreamBundle\NodeJs;

use \Exception;

class NodeJs
{
    private function getCamelCaseLibName($libName)
    {
        if (in_array($libName, $this->libs())) {
            return lcfirst(str_replace(' ', '', ucwords(str_replace(array(' ', '-', '_', '.'), ' ', $libName))));
        }
        else {
            throw new Exception('Unknown library : ' . $libName);
        }
    }
    
    public static function libs()
    {
        $libs = array();
        
        $libs[] = 'http';
        $libs[] = 'irc';
        $libs[] = 'request';
        $libs[] = 'socket.io';
        $libs[] = 'socket.io-client';
        $libs[] = 'hitbox-chat';
        
        return $libs;
    }
    
    public static function nodeLibs()
    {
        $nodeLibs = array();
        
        $nodeLibs[] = 'child_process';
        
        return $nodeLibs;
    }
    
    public function ircConfig()
    {
        
    }
}

<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class TwitchHostsCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;
    
    protected function configure()
    {
        $this
            ->setName('stream:twitch:hosts')
            ->setDescription('Récupère tous les hosts d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'             => 'ok',
            '_error'              => null,
            'timestampNow'        => null,
            'timestampThisMinute' => null,
            'channelId'           => null,
            'streamId'            => null,
            'hostsCount'          => null,
        ];
        try {
            $now        = new DateTime('now');
            $thisMinute = new DateTime('now -' . intval($now->format('s')) . ' second');
            $response['timestampNow']        = $now->getTimestamp();
            $response['timestampThisMinute'] = $thisMinute->getTimestamp();
            $channelId  = urldecode($input->getArgument('channelId'));
            $site       = SiteQuery::getOneByCode('twitch');
            $channel    = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream    = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId']  = $stream->getId();
            if ($stream->getCanScan() === false || $channel->getCanScan() === false) {
                throw new Exception('Can not scan this stream or this channel');
            }
            $pathHostsJson = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'working');
            $fileHostsJson = 'stream_hosts_' . $channel->getId() . '_' . $thisMinute->getTimestamp() . '.json';
            if (file_exists($pathHostsJson . DIRECTORY_SEPARATOR . $fileHostsJson)) {
                $contentHostsJson = json_decode(file_get_contents($pathHostsJson . DIRECTORY_SEPARATOR . $fileHostsJson), true);
            } else {
                file_put_contents($pathHostsJson . DIRECTORY_SEPARATOR . $fileHostsJson, json_encode([]));
                $contentHostsJson = [];
            }
            $hostsUrl   = 'https://tmi.twitch.tv/hosts?include_logins=1&target=' . $channel->getChannelId();
            $hostsJson  = json_decode(file_get_contents($hostsUrl), true);
            $hostsCount = count($hostsJson['hosts']);
            $channel
                ->setHostsCount($hostsCount)
                ->save()
            ;
            $response['hostsCount'] = $hostsCount;
            foreach ($hostsJson['hosts'] as $hostData) {
                if (!in_array($hostData['host_login'], $contentHostsJson)) {
                    $contentHostsJson[] = $hostData['host_login'];
                }
            }
            sort($contentHostsJson);
            file_put_contents($pathHostsJson . DIRECTORY_SEPARATOR . $fileHostsJson, json_encode($contentHostsJson));
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}
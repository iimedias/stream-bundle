<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class TwitchChattersCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;
    
    protected function configure()
    {
        $this
            ->setName('stream:twitch:chatters')
            ->setDescription('Récupère tous les chatters d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'             => 'ok',
            '_error'              => null,
            'timestampNow'        => null,
            'timestampThisMinute' => null,
            'channelId'           => null,
            'streamId'            => null,
            'chattersCount'       => null,
        ];
        try {
            $now        = new DateTime('now');
            $thisMinute = new DateTime('now -' . intval($now->format('s')) . ' second');
            $response['timestampNow']        = $now->getTimestamp();
            $response['timestampThisMinute'] = $thisMinute->getTimestamp();
            $channelId  = urldecode($input->getArgument('channelId'));
            $site       = SiteQuery::getOneByCode('twitch');
            $channel    = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream    = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId']  = $stream->getId();
            if ($stream->getCanScan() === false || $channel->getCanScan() === false) {
                throw new Exception('Can not scan this stream or this channel');
            }
            $pathChattersJson = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'working');
            $fileChattersJson = 'stream_chatters_' . $channel->getId() . '_' . $thisMinute->getTimestamp() . '.json';
            if (file_exists($pathChattersJson . DIRECTORY_SEPARATOR . $fileChattersJson)) {
                $contentChattersJson = json_decode(file_get_contents($pathChattersJson . DIRECTORY_SEPARATOR . $fileChattersJson), true);
            } else {
                file_put_contents($pathChattersJson . DIRECTORY_SEPARATOR . $fileChattersJson, json_encode([]));
                $contentChattersJson = [];
            }
            $chattersUrl   = 'http://tmi.twitch.tv/group/user/' . $channel->getChannel() . '/chatters';
            $chattersJson  = json_decode(file_get_contents($chattersUrl), true);
            $chattersCount = 0;
            foreach ($chattersJson['chatters'] as $type => $chattersList) {
                $chattersCount += count($chattersList);
            }
            $channel
                ->setChattersCount($chattersCount)
                ->setLiveViewersCount($chattersCount)
                ->save()
            ;
            $response['chattersCount'] = $chattersCount;
            foreach ($chattersJson['chatters'] as $type => $chattersList) {
                $type = ($type !== 'staff') ? substr($type, 0, (strlen($type) - 1)) : $type;
                foreach ($chattersList as $chatter) {
                    if (!isset($contentChattersJson[$chatter])) {
                        $contentChattersJson[$chatter] = ['name' => $chatter, 'type' => $type];
                    }
//                    $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($chatter, $channel->getSite());
//                    ChatterExperience::createNew($channel, $chatUser, $type, $thisMinute);
                }
            }
            ksort($contentChattersJson);
            file_put_contents($pathChattersJson . DIRECTORY_SEPARATOR . $fileChattersJson, json_encode($contentChattersJson));
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}
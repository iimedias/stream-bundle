<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use \DateTime;

class StreamFollowersRefreshCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stream:followers:refresh')
            ->setDescription('Récupère la liste des followers d\'un stream')
            ->addArgument('streamId')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $streamId  = intval(urldecode($input->getArgument('streamId')));
        $channelId = intval(urldecode($input->getArgument('channelId')));

        $channel = ChannelQuery::create()
            ->filterById($channelId)
            ->filterByStreamId($streamId)
            ->findOne()
        ;

        $jsonReturn = array('streamId' => 0, 'channelId' => 0, 'stream' => '', 'site' => '', 'channel' => '', 'new' => array(), 'old' => array(), 'currents' => array());

        if (!is_null($channel)) {
            $jsonReturn['streamId']  = $channel->getStream()->getId();
            $jsonReturn['channelId'] = $channel->getId();
            $jsonReturn['stream']    = $channel->getStream()->getName();
            $jsonReturn['site']      = $channel->getSite();
            $jsonReturn['channel']   = $channel->getChannel();

            switch ($channel->getSite()) {
                case 'twitch':
                    $limit  = 100;
                    $offset = 0;

                    $currentFollowers = ChannelFollowerQuery::create()
                        ->filterByStreamId($channel->getStream()->getId())
                        ->filterbyChannelId($channel->getId())
                        ->find()
                    ;

                    foreach ($currentFollowers as $currentFollower) {
                        $jsonReturn['currents'][$currentFollower->getChatUser()->getUsername()] = $currentFollower->getChatUser()->getUsername();
                    }

                    $jsonFollowers = json_decode(file_get_contents('https://api.twitch.tv/kraken/channels/' . $channel->getChannel() . '/follows?direction=DESC&limit=' . $limit . '&offset=0'));
                    $nextJsonUrl   = $jsonFollowers->_links->next;
                    foreach ($jsonFollowers->follows as $jsonFollower) {
                        $follower = ChannelFollowerQuery::create()
                            ->filterbyStreamId($channel->getStream()->getId())
                            ->filterbyChannelId($channel->getId())
                            ->useChatUserQuery()
                                ->filterBySite('twitch')
                                ->filterByUsername($jsonFollower->user->name)
                            ->endUse()
                            ->findOne()
                        ;

                        if (is_null($follower)) {
                            $chatUser = ChatUserQuery::create()
                                ->filterBySite('twitch')
                                ->filterByUsername($jsonFollower->user->name)
                                ->findOne()
                            ;

                            if (is_null($chatUser)) {
                                $chatUser = new ChatUser();
                                $chatUser
                                    ->setSite('twitch')
                                    ->setUsername($jsonFollower->user->name)
                                    ->setDisplayName($jsonFollower->user->display_name)
                                    ->setRegisteredAt(new DateTime($jsonFollower->user->created_at))
                                    ->save()
                                ;
                            }

                            $follower = new ChannelFollower();
                            $follower
                                ->setStreamId($channel->getStream()->getId())
                                ->setChannelId($channel->getId())
                                ->setChatUser($chatUser)
                                ->setFollowedAt(new DateTime($jsonFollower->created_at))
                                ->save()
                            ;

                            $jsonReturn['new'][$jsonFollower->user->name] = $jsonFollower->user->name;
                        }
                    }

                    if ((count($jsonReturn['new']) + count($jsonReturn['currents'])) < $jsonFollowers->_total) {
                        for ($i = 1, $iMax = ceil($jsonFollowers->_total / $limit); $i < $iMax; $i++) {
                            dump($jsonFollowers->_links->next);
                            $jsonFollowers = json_decode(file_get_contents($nextJsonUrl));
                            $nextJsonUrl   = $jsonFollowers->_links->next;
                            foreach ($jsonFollowers->follows as $jsonFollower) {
                                $follower = ChannelFollowerQuery::create()
                                    ->filterbyStreamId($channel->getStream()->getId())
                                    ->filterbyChannelId($channel->getId())
                                    ->useChatUserQuery()
                                    ->filterBySite('twitch')
                                    ->filterByUsername($jsonFollower->user->name)
                                    ->endUse()
                                    ->findOne()
                                ;

                                if (is_null($follower)) {
                                    $chatUser = ChatUserQuery::create()
                                        ->filterBySite('twitch')
                                        ->filterByUsername($jsonFollower->user->name)
                                        ->findOne()
                                    ;

                                    if (is_null($chatUser)) {
                                        $chatUser = new ChatUser();
                                        $chatUser
                                            ->setSite('twitch')
                                            ->setUsername($jsonFollower->user->name)
                                            ->setDisplayName($jsonFollower->user->display_name)
                                            ->setRegisteredAt(new DateTime($jsonFollower->user->created_at))
                                            ->save()
                                        ;
                                    }

                                    $followerAt = new DateTime($jsonFollower->created_at);

                                    $follower = new ChannelFollower();
                                    $follower
                                        ->setStreamId($channel->getStream()->getId())
                                        ->setChannelId($channel->getId())
                                        ->setChatUser($chatUser)
                                        ->setFollowedAt($followerAt)
                                        ->save()
                                    ;

                                    $jsonReturn['new'][$jsonFollower->user->name] = $jsonFollower->user->name;
                                }
                            }
                        }
                    }

                    dump($jsonFollowers->_total, $currentFollowers->count());
            }
        }

        sort($jsonReturn['old']);
        sort($jsonReturn['new']);
        sort($jsonReturn['currents']);

        $output->writeln(json_encode($jsonReturn));
    }

}

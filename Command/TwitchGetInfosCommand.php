<?php

namespace IiMedias\StreamBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class TwitchGetInfosCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;
    
    protected function configure()
    {
        $this
            ->setName('stream:twitch:getinfos')
            ->setDescription('Récupère toutes les informations d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function throwExceptionIfNull($var, $message)
    {
        if (is_null($var)) {
            throw new Exception($message);
        }
    }
    
    protected function getDateTimeOfTZFormat($dateString)
    {
        preg_match('#^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})Z#', $dateString, $matches);
        $newTimestamp  = (intval($matches[1]) - 1970) * 60 * 60 * 24 * 365;
        $newTimestamp += floor((intval($matches[1]) - 1970 + 2) / 4) * 60 * 60 * 24;
        if (intval($matches[1]) % 4 === 0 && intval($matches[2]) < 3) {
            $newTimestamp -= 60 * 60 * 24;
        }
        switch (intval($matches[2])) {
            case 12:
                $newTimestamp += 60 * 60 * 24 * 30;
            case 11:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 10:
                $newTimestamp += 60 * 60 * 24 * 30;
            case 9:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 8:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 7:
                $newTimestamp += 60 * 60 * 24 * 30;
            case 6:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 5:
                $newTimestamp += 60 * 60 * 24 * 30;
            case 4:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 3:
                $newTimestamp += 60 * 60 * 24 * 28;
            case 2:
                $newTimestamp += 60 * 60 * 24 * 31;
            case 1:
                $newTimestamp += 0;
        }
        $newTimestamp += (intval($matches[3]) - 1) * 60 * 60 * 24;
        $newTimestamp += intval($matches[4]) * 60 * 60;
        $newTimestamp += intval($matches[5]) * 60;
        $newTimestamp += intval($matches[6]);
        $newDate = new DateTime();
        $newDate->setTimestamp($newTimestamp);
        return $newDate;
    }
    
    protected function initiateResponse()
    {
        $this->success = 'ok';
        $this->error   = null;
        $this->site    = null;
        $this->channel = null;
        $this->stream  = null;
        $this->bot     = null;
        $this->events  = array();
    }
    
    protected function printJsonResponse(OutputInterface $output)
    {
        $output->writeln(json_encode(array(
            'success'  => $this->success,
            'error'    => $this->error,
            'site'     => $this->site,
            'channel'  => $this->channel,
            'stream'   => $this->stream,
            'chatUser' => $this->chatUser,
            'bot'      => $this->bot,
            'events'   => $this->events,
        )));
    }
    
    protected function siteObjectTemplate()
    {
        return (object) array(
            'id'   => null,
            'name' => null,
            'code' => null,
        );
    }
    
    protected function streamObjectTemplate()
    {
        return (object) array(
            'id'   => null,
            'name' => null,
        );
    }
    
    protected function channelObjectTemplate()
    {
        return (object) array(
            'id'             => null,
            'channel'        => null,
            'viewsCount'     => null,
            'followersCount' => null,
        );
    }
    
    protected function chatUserObjectTemplate()
    {
        return (object) array(
            'id'          => null,
            'username'    => null,
            'displayName' => null,
        );
    }
    
    protected function getSite()
    {
        $site             = SiteQuery::getOneByCode('twitch');
        $this->throwExceptionIfNull($site, 'Unknown site');
        $this->site       = $this->siteObjectTemplate();
        $this->site->id   = $site->getId();
        $this->site->name = $site->getName();
        $this->site->code = $site->getCode();
        return $site;
    }
    
    protected function getChannel($channelId, Site $site)
    {
        $channel                       = is_int($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
        $this->throwExceptionIfNull($channel, 'Unknown channel');
        $this->channel                 = $this->channelObjectTemplate();
        $this->channel->id             = $channel->getId();
        $this->channel->channel        = $channel->getChannel();
        $this->channel->viewsCount     = $channel->getViewsCount();
        $this->channel->followersCount = $channel->getFollowersCount();
        $this->throwExceptionIfNull($channel->getStream(), 'Unknown stream');
        $this->stream                  = $this->streamObjectTemplate();
        $this->stream->id              = $channel->getStream()->getId();
        $this->stream->name            = $channel->getStream()->getName();
        return $channel;
    }
    
    protected function getStreamChatUser(Channel $channel, Site $site)
    {
        $chatUser                    = ChatUserQuery::getOneOrCreateByUsernameAndSite($channel->getChannel(), $site);
        $this->throwExceptionIfNull($chatUser, 'Unknown chat user');
        $this->chatUser              = $this->chatUserObjectTemplate();
        $this->chatUser->id          = $chatUser->getId();
        $this->chatUser->username    = $chatUser->getUsername();
        $this->chatUser->displayName = $chatUser->getDisplayName();
        return $chatUser;
    }
    
    protected function getBot(Channel $channel)
    {
        $bot = null;
        $this->throwExceptionIfNull($channel->getBot(), 'No bot defined');
        foreach ($channel->getBot()->getChannels() as $channelBot) {
            if ($channelBot->getSite()->getCode() == 'twitch') {
                $bot = $channelBot;
                break;
            }
        }
        $this->throwExceptionIfNull($bot, 'No bot for this site');
        $this->bot          = $this->channelObjectTemplate();
        $this->bot->id      = $bot->getId();
        $this->bot->channel = $bot->getChannel();
        return $bot;
    }
    
    protected function getChannelInfosViaApi(Channel $channel, ChatUser $chatUser, Channel $bot)
    {
        $channelUrl  = 'https://api.twitch.tv/kraken/channels/' . $channel->getChannel() . '?client_id=' . $bot->getClientId();
        $channelJson = json_decode(file_get_contents($channelUrl), true);
        $this->updateViewsCount($channel, $chatUser, $channelJson['views']);
        $this->updateFollowersCount($channel, $chatUser, $bot, $channelJson['followers']);
        $this->updateChattersCount($channel);
        $this->updateHostsCount($channel);
//        $this->updateSubscribersCount($channel, $bot);
        $this->updateStatus($channel, $chatUser, $channelJson['status']);
        $this->updateGame($channel, $chatUser, $channelJson['game']);
        $this->updateLiveStatus($channel, $chatUser, $bot);
        $channel->updateViaTwitchChannelJson($channelJson);
        $chatUser->updateViaTwitchChannelJson($channelJson);
    }
    
    protected function updateViewsCount(Channel $channel, ChatUser $chatUser, $newViewsCount)
    {
        if ($newViewsCount != $channel->getViewsCount()) {
            $diff = $newViewsCount - $channel->getViewsCount();
            for ($i = 0; $i < $diff; $i++) {
//                Experience::createNewView($channel, $chatUser); 
            }
            $this->channel->viewsCount = $newViewsCount;
//            $channel->updateViewsCount($newViewsCount);
        }
        return $channel;
    }
    
    protected function updateFollowersCount(Channel $channel, ChatUser $chatUser, Channel $bot, $newFollowersCount)
    {
        if ($newFollowersCount != $channel->getFollowersCount()) {
            $newViews   = $newFollowersCount - $channel->getFollowersCount();
            if ($newViews < 0) {
                Experience::createNewUnfollows($channel, $chatUser, abs($newViews));
            }
            $lastFollow = ExperienceQuery::getLastFollowOfChannel($channel);
            $lastTs     = !is_null($lastFollow) ? $lastFollow->getAt()->getTimestamp() : 0;
            for ($page = 0; $page < 17; $page++) {
                $followsUrl  = 'https://api.twitch.tv/kraken/channels/' . $channel->getChannel() . '/follows?client_id=' . $bot->getClientId() . '&direction=DESC&limit=100&offset=' . ($page * 100);
                $followsJson = json_decode(file_get_contents($followsUrl), true);
                foreach ($followsJson['follows'] as $followJson) {
                    $followAt = $this->getDateTimeOfTZFormat($followJson['created_at']);
                    if ($followAt->getTimestamp() < $lastTs) {
                        break 2;
                    }
                    $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($followJson['user']['name'], $channel->getSite());
                    Experience::createNewFollow($channel, $chatUser, $followAt);
                }
            }
//            $channel->updateFollowersCount($newFollowersCount);
        }
    }
    
    protected function updateChattersCount(Channel $channel)
    {
        $chattersUrl  = 'http://tmi.twitch.tv/group/user/' . $channel->getChannel() . '/chatters';
        $chattersJson = json_decode(file_get_contents($chattersUrl), true);
        foreach ($chattersJson['chatters'] as $type => $chattersList) {
            foreach ($chattersList as $chatter) {
                $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($chatter, $channel->getSite());
                Experience::createNewConnection($channel, $chatUser);
            }
        }
    }
    
    protected function updateHostsCount(Channel $channel)
    {
        $hostsUrl  = 'https://tmi.twitch.tv/hosts?include_logins=1&target=' . $channel->getChannelId();
        $hostsJson = json_decode(file_get_contents($hostsUrl), true);
        foreach ($hostsJson['hosts'] as $host) {
            $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($host['host_login'], $channel->getSite());
            Experience::createNewHost($channel, $chatUser);
        }
    }
    
    protected function updateSubscribersCount(Channel $channel, Channel $bot)
    {
        $subscriptionsUrl  = 'https://api.twitch.tv/kraken/channels/' . $channel->getChannelId() . '/subscriptions?client_id=' . $bot->getClientId();
        $subscriptionsJson = json_decode(file_get_contents($subscriptionsUrl));
    }
    
    protected function updateStatus(Channel $channel, ChatUser $chatUser, $status)
    {
        try {
            $lastStatus = ExperienceQuery::getLastStatusOfChannel($channel);
            $status = str_replace("\xE2\x98\x95",     '&#9749;',   $status); // Hot Beverage :coffee:
            $status = str_replace("\xE2\x98\xA0",     '&#9760;',   $status); // Skull and Crossbones
            $status = str_replace("\xE2\x9B\x94",     '&#9940;',   $status); // No Entry :no_entry:
            $status = str_replace("\xEF\xB8\x8F",     '&#65039;',  $status); // Hot Beverage :coffee:
            $status = str_replace("\xF0\x9F\x8F\x86", '&#127942;', $status); // Trophy :trophy:
            if (is_null($lastStatus) || $lastStatus->getMessage() != $status) {
                Experience::createNewStatus($channel, $chatUser, $status);
            }
        } catch (Exception $e) {
        }
    }
    
    protected function updateGame(Channel $channel, ChatUser $chatUser, $game)
    {
        try {
            $lastGame = ExperienceQuery::getLastGameOfChannel($channel);
            if (is_null($lastGame) || $lastGame->getMessage() != $game) {
                Experience::createNewGame($channel, $chatUser, $game);
            }
        } catch (Exception $e) {
        }
    }
    
    protected function updateLiveStatus(Channel $channel, ChatUser $chatUser, Channel $bot)
    {
        $liveStUrl  = 'https://api.twitch.tv/kraken/streams?channel=' . $channel->getChannel() . '&client_id=' . $bot->getClientId();
        $liveStJson = json_decode(file_get_contents($liveStUrl), true);
        $liveStatus = (is_array($liveStJson['streams']) && count($liveStJson['streams']) == 1) ? $liveStJson['streams'][0]['stream_type'] : 'offline';
        $lastLiveSt = ExperienceQuery::getLastLiveStatusOfChannel($channel);
        if (is_null($lastLiveSt) || $lastLiveSt->getMessage() != $liveStatus) {
            Experience::createNewLiveStatus($channel, $chatUser, $liveStatus);
        }
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        exit();
        try {
            $this->initiateResponse();
            $site     = $this->getSite();
            $channel  = $this->getChannel(urldecode($input->getArgument('channelId')), $site);
            if ($channel->getCanScan() == true) {
                $chatUser = $this->getStreamChatUser($channel, $site);
                $bot      = $this->getBot($channel);
                $this->getChannelInfosViaApi($channel, $chatUser, $bot);
            }
        } catch (Exception $e) {
            $this->success = 'ko';
            $this->error   = $e->getMessage();
        }
        $this->printJsonResponse($output);
    }
}

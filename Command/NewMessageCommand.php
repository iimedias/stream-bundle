<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\Base\ChannelQuery;
use IiMedias\StreamBundle\Model\Base\ChatUserQuery;
use IiMedias\StreamBundle\Model\Base\TwitchBotQuery;
use IiMedias\StreamBundle\Model\ChatMessage;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\MessageExperienceQuery;
use IiMedias\StreamBundle\Model\StreamQuery;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\MessageExperience;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use \DateTime;

class NewMessageCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stream:message:new')
            ->setDescription('Enregistre le message')
            ->addArgument('site')
            ->addArgument('type')
            ->addArgument('from')
            ->addArgument('to')
            ->addArgument('message')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $site    = urldecode($input->getArgument('site'));
        $type    = urldecode($input->getArgument('type'));
        $from    = urldecode($input->getArgument('from'));
        $to      = urldecode($input->getArgument('to'));
        $message = urldecode($input->getArgument('message'));

        $response = [
            '_status'  => 'ok',
            '_error'   => null,
            'args'     => [
                'site'    => $site,
                'type'    => $type,
                'from'    => $from,
                'to'      => $to,
                'message' => $message,
            ],
            'site'     => [
                'id'   => null,
                'name' => null,
                'code' => null,
            ],
            'stream'   => [
                'id'   => null,
                'name' => null,
            ],
            'channel'  => [
                'id'   => null,
                'name' => null,
            ],
            'chatUser' => [
                'id'          => null,
                'username'    => null,
                'displayName' => null,
                'avatarUrl'   => null,
            ],
            'message'  => [
                'id'        => null,
                'type'      => null,
                'message'   => null,
                'timestamp' => null,
            ],
        ];

        try {
            $site = SiteQuery::create()
                ->filterByCode($site)
                ->findOne()
            ;
            if (is_null($site)) {
                throw new \Exception('Unknown site');
            }
            $response['site'] = [
                'id'   => $site->getId(),
                'name' => $site->getName(),
                'code' => $site->getCode(),
            ];

            $chatUser = ChatUserQuery::create()
                ->filterBySite($site)
                ->filterByUsername($from)
                ->findOne()
            ;
            if (is_null($chatUser)) {
                $chatUser = new ChatUser();
                $chatUser
                    ->setParent(null)
                    ->setSite($site)
                    ->setUsername($from)
                    ->setTimeoutLevel(0)
                    ->setLastTimeoutAt(null)
                    ->save()
                ;
            }
            $response['chatUser'] = [
                'id'          => $chatUser->getId(),
                'username'    => $chatUser->getUsername(),
                'displayName' => $chatUser->getDisplayName(),
                'avatarUrl'   => $chatUser->getLogoUrl(),
            ];

            switch ($site->getCode()) {
                case 'twitch':
                    $channel = ChannelQuery::create()
                        ->filterBySite($site)
                        ->filterByChannel($to)
                        ->findOne()
                    ;
                    break;
                default:
                    exit();
            }
            $response['stream'] = [
                'id'   => $channel->getStream()->getId(),
                'name' => $channel->getStream()->getName(),
            ];
            $response['channel'] = [
                'id'   => $channel->getId(),
                'name' => $channel->getChannel(),
            ];

            $messageExperience = new MessageExperience();
            $messageExperience
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setSite($site)
                ->setChatUser($chatUser)
                ->setType($type)
                ->setMessage($message)
                ->setExp(1)
                ->setAt(new \DateTime())
                ->save()
            ;

            $response['message'] = [
                'id'        => $messageExperience->getId(),
                'type'      => $messageExperience->getType(),
                'message'   => $messageExperience->getMessage(),
                'timestamp' => $messageExperience->getAt()->getTimestamp(),
            ];

            $messageExperienceCount = MessageExperienceQuery::create()
                ->filterByStream($channel->getStream())
                ->filterByChannel($channel)
                ->filterBySite($site)
                ->count()
            ;

            $channel
                ->setMessagesCount($messageExperienceCount)
                ->save()
            ;
        } catch (\Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }

}

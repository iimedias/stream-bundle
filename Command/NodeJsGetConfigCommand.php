<?php

namespace IiMedias\StreamBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

use IiMedias\StreamBundle\Model\StreamQuery;

class NodeJsGetConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nodejs:stream:getconfig')
            ->setDescription('Récupère la configuration du StreamBundle pour le serveur nodejs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $config = array(
            'mixer'      => array(
                'bot'    => array(),
                'socket' => array(
                    'client' => array(),
                ),
            ),
            'twitch'     => array(
                'irc' => array(
                    'host' => 'irc.twitch.tv',
                    'port' => 6667,
                ),
                'bot'    => array(),
                'socket' => array(
                    'irc' => array(),
                ),
                'var'    => array(),
            ),
            'tipeeeStream' => array(),
            'sockets'      => array(),
            'vars'         => array(),
        );

        $bots    = StreamQuery::create()
            ->filterByType('bot')
            ->find();
        $streams = StreamQuery::create()
            ->filterByType('stream')
            ->filterByCanScan(true)
            ->find();

        foreach ($bots as $bot) {
            foreach ($bot->getChannels() as $channel) {
                switch ($channel->getSite()->getCode()) {
                    case 'mixer':
                        $config['mixer']['bot'][$bot->getId()] = array(
                            'botUserId'    => $channel->getClientId(),
                            'botChannelId' => $channel->getSecretId(),
                            'botName'      => $channel->getChannel(),
                            'botAccess'    => $channel->getPasswordOauth(),
                        );
                        $config['mixer']['socket']['client'][$bot->getId()] = null;
                        break;
                    case 'twitch':
                        $config['twitch']['bot'][$bot->getId()] = array(
                            'clientId'    => $channel->getClientId(),
                            'secretId'    => $channel->getSecretId(),
                            'botName'     => $channel->getChannel(),
                            'botPassword' => $channel->getPasswordOauth(),
                            'botNick'     => array($channel->getChannel()),
                            'channels'    => array($channel->getChannel()),
                        );
                        $config['twitch']['socket']['irc'][$bot->getId()] = null;
                        break;
                    default:
//                        dump($channel->getSite());
//                        exit();
                }
            }
        }

        foreach ($streams as $stream) {
            foreach ($stream->getChannels() as $channel) {
                switch ($channel->getSite()->getCode()) {
                    case 'twitch':
                        $config['twitch']['bot'][$channel->getBot()->getId()]['channels'][] = '#' . $channel->getChannel();
                        break;
                    default:
                }
            }
        }

        $output->write(json_encode($config));
    }
}

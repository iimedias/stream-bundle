<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use IiMedias\StreamBundle\Model\FollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class UserScanCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;
    
    protected function configure()
    {
        $this
            ->setName('stream:twitch:userscan')
            ->setDescription('Récupère tous les chatters d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status' => 'ok',
            '_error'  => null,
            'user'    => null,
        ];
        try {
            $bot = ChannelQuery::create()->findOneById(1);
            $chatUser = ChatUserQuery::create()
                ->filterByCanRescan(true)
                ->findOne()
            ;
            try {
                $userInfoUrl  = 'https://api.twitch.tv/kraken/users/' . $chatUser->getUsername() . '?client_id=' . $bot->getClientId();
                $userInfoJson = json_decode(file_get_contents($userInfoUrl), true);
            } catch (Exception $e) {
                $chatUser
                    ->setIsDeletedAccount(true)
                    ->setCanRescan(false)
                    ->save()
                ;
                exit();
            }

            $allowedChannels = ChannelQuery::create()
                ->filterByCanScan(true)
                ->find()
            ;
            foreach ($allowedChannels as $channel) {
                try {
                    $response['user'] = $chatUser->getUsername();
                    $userFollowUrl    = 'https://api.twitch.tv/kraken/users/' . $chatUser->getUsername() . '/follows/channels/' . $channel->getChannel() . '?client_id=' . $bot->getClientId();
                    $userFollowJson   = json_decode(file_get_contents($userFollowUrl), true);
                    preg_match('#^([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})Z$#', $userFollowJson['created_at'], $followAtMatches);
                    $followAt         = new DateTime($followAtMatches[1] . ' ' . $followAtMatches[2] . ' UTC');
                    $follow           = FollowExperienceQuery::create()
                        ->filterByChannel($channel)
                        ->filterByChatUser($chatUser)
                        ->filterByAt($followAt)
                        ->findOne()
                    ;
                    if (is_null($follow)) {
                        $follow = new FollowExperience();
                        $follow
                            ->setStream($channel->getStream())
                            ->setChannel($channel)
                            ->setSite($channel->getSite())
                            ->setChatUser($chatUser)
                            ->setExp(20)
                            ->setAt($followAt)
                            ->save()
                        ;
                    }
                } catch (Exception $e) {
                }
            }
            $chatUser
                ->setDisplayName($userInfoJson['display_name'])
                ->setLogoUrl($userInfoJson['logo'])
                ->setCanRescan(false)
                ->save()
            ;
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}
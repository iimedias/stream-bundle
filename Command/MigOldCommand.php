<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\FollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatMessage;
use IiMedias\StreamBundle\Model\ChatMessageQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery;

class MigOldCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:stream:check:host:double')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $follows = FollowExperienceQuery::create()
            ->find();
        dump(count($follows));
    }
}

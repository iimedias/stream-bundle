<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\FollowDiffData;
use IiMedias\StreamBundle\Model\ViewDiffData;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\GameDataQuery;
use IiMedias\StreamBundle\Model\StatusDataQuery;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery;

class TwitchMainInfosCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;

    protected function configure()
    {
        $this
            ->setName('stream:twitch:maininfos')
            ->setDescription('Récupère toutes les informationd\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function emojiReplace($str)
    {
        $str = str_replace("\xF0\x9F\x87\xAB\xF0\x9F\x87\xB7", '&#x1F1EB;', $str); // Flag France
        $str = str_replace("\xF0\x9F\x87\xAC\xF0\x9F\x87\xA7", '&#xE510;', $str); // Flag Royaume Uni
        return $str;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'          => 'ok',
            '_error'           => null,
            'streamId'         => null,
            'channelId'        => null,
            'status'           => null,
            'game'             => null,
            'followersCount'   => null,
            'followersDiff'    => null,
            'viewsCount'       => null,
            'viewsDiff'        => null,
            'viewersCount'     => null,
            'chattersCount'    => null,
            'hostsCount'       => null,
            'messagesCount'    => null,
            'liveViewersCount' => null,
        ];
        try {
            $channelId = urldecode($input->getArgument('channelId'));
            $site      = SiteQuery::getOneByCode('twitch');
            $channel   = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream    = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId']  = $stream->getId();
            if ($stream->getCanScan() === false || $channel->getCanScan() === false) {
                throw new Exception('Can not scan this stream or this channel');
            }
            $bot = null;
            if (is_null($channel->getBot())) {
                throw new Exception('No bot defined');
            }
            foreach ($channel->getBot()->getChannels() as $channelBot) {
                if ($channelBot->getSite()->getCode() == 'twitch') {
                    $bot = $channelBot;
                    break;
                }
            }
            if (is_null($bot)) {
                throw new Exception('No bot for this site');
            }
            $channelUrl    = 'https://api.twitch.tv/kraken/channels/' . $channel->getChannel() . '?client_id=' . $bot->getClientId();
            $channelJson   = json_decode(file_get_contents($channelUrl), true);
            $followersDiff = intval($channelJson['followers']) - $channel->getFollowersCount();
            $viewsDiff     = ($channel->getViewsCount() > 0) ? intval($channelJson['views']) - $channel->getViewsCount() : 0;
            $twitchGame    = ApiTwitchGameQuery::getOneByNameOrCreate($channelJson['game']);
            $channel
                ->setChannelId($channelJson['_id'])
                ->setStatus($this->emojiReplace($channelJson['status']))
                ->setApiTwitchGame($twitchGame)
                ->setGame($channelJson['game'])
                ->setFollowersCount($channelJson['followers'])
                ->setFollowersDiff($followersDiff)
                ->setViewsCount($channelJson['views'])
                ->setViewsDiff($viewsDiff)
                ->save()
            ;
            if ($viewsDiff !== 0) {
                $viewDiffData = new ViewDiffData();
                $viewDiffData
                    ->setStream($channel->getStream())
                    ->setChannel($channel)
                    ->setSite($site)
                    ->setDiff($viewsDiff)
                    ->setAt(new DateTime('now'))
                    ->save()
                ;
            }
            if ($followersDiff !== 0) {
                $followDiffData = new FollowDiffData();
                $followDiffData
                    ->setStream($channel->getStream())
                    ->setChannel($channel)
                    ->setSite($site)
                    ->setDiff($followersDiff)
                    ->setAt(new DateTime('now'))
                    ->save()
                ;
            }
            GameDataQuery::getLastOfChannelOrCreate($channel, $twitchGame);
            StatusDataQuery::getLastOfChannelOrCreate($channel, $channel->getStatus());
            $response['status']           = $channel->getStatus();
            $response['game']             = $channel->getApiTwitchGame()->getName();
            $response['followersCount']   = $channel->getFollowersCount();
            $response['followersDiff']    = $channel->getFollowersDiff();
            $response['viewsCount']       = $channel->getViewsCount();
            $response['viewsDiff']        = $channel->getViewsDiff();
            $response['viewersCount']     = $channel->getViewersCount();
            $response['chattersCount']    = $channel->getChattersCount();
            $response['hostsCount']       = $channel->getHostsCount();
            $response['messagesCount']    = $channel->getMessagesCount();
            $response['liveViewersCount'] = $channel->getLiveViewersCount();
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}
/*
données intéressantes restantes à intégrer
array:22 [
  "_links" => array:10 [
                "self" => "https://api.twitch.tv/kraken/channels/kanjo"
    "follows" => "https://api.twitch.tv/kraken/channels/kanjo/follows"
    "commercial" => "https://api.twitch.tv/kraken/channels/kanjo/commercial"
    "stream_key" => "https://api.twitch.tv/kraken/channels/kanjo/stream_key"
    "chat" => "https://api.twitch.tv/kraken/chat/kanjo"
    "features" => "https://api.twitch.tv/kraken/channels/kanjo/features"
    "subscriptions" => "https://api.twitch.tv/kraken/channels/kanjo/subscriptions"
    "editors" => "https://api.twitch.tv/kraken/channels/kanjo/editors"
    "teams" => "https://api.twitch.tv/kraken/channels/kanjo/teams"
    "videos" => "https://api.twitch.tv/kraken/channels/kanjo/videos"
  ]
]

#}
*/

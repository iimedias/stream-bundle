<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\Base\ViewerDataQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

use IiMedias\StreamBundle\Model\StreamQuery;
use IiMedias\StreamBundle\Model\UserExperienceQuery;
use IiMedias\StreamBundle\Model\RankQuery;
use IiMedias\StreamBundle\Model\ChatterExperienceQuery;
use IiMedias\StreamBundle\Model\MessageExperienceQuery;
use IiMedias\StreamBundle\Model\FollowExperienceQuery;
use IiMedias\StreamBundle\Model\HostExperienceQuery;
use IiMedias\StreamBundle\Model\AvatarQuery;

class ExpCalcCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stream:expcalc')
            ->setDescription('...')
            ->addArgument('channelId', InputArgument::REQUIRED)
            ->addOption('no-message', null, InputOption::VALUE_NONE)
            ->addOption('no-follow', null, InputOption::VALUE_NONE)
            ->addOption('no-host', null, InputOption::VALUE_NONE)
            ->addOption('no-view', null, InputOption::VALUE_NONE)
        ;
    }

    protected function outputSubtitle(OutputInterface $output, $text)
    {
        $output->writeln('<options=bold> > ' . $text . '</>', OutputInterface::VERBOSITY_VERBOSE);
    }

    protected function outputSuccess(OutputInterface $output, $text)
    {
        $output->writeln('<fg=green>    ' . $text . '</>', OutputInterface::VERBOSITY_VERBOSE);
    }

    protected function outputInfo(OutputInterface $output, $text)
    {
        $output->writeln('<fg=cyan>    ' . $text . '</>', OutputInterface::VERBOSITY_VERBOSE);
    }

    protected function addLap(OutputInterface $output, Stopwatch $stopwatch)
    {
        $stopwatch->lap('ExpCalc');
        $output->writeln('<fg=green>    Ajout d\'un Lap StopWatch</>', OutputInterface::VERBOSITY_VERBOSE);
        $output->writeln('', OutputInterface::VERBOSITY_VERBOSE);
    }

    protected function newProgressBar(OutputInterface $output, $maxIterations, $startMessage)
    {
        if ($output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $progressFormat = '    <bg=cyan> %message% </>' . PHP_EOL .  '    <fg=cyan>%current%/%max% [%bar%] %percent%%</>' . PHP_EOL .  '    <fg=cyan>%remaining%/%elapsed%/%estimated% - %memory%</>' . PHP_EOL;
            $progress       = new ProgressBar($output, $maxIterations);
            $progress->setFormat($progressFormat);
            $progress->setMessage($startMessage);
            $progress->start();
        } else {
            $progress = null;
        }

        return $progress;
    }

    protected function testProgressBar($progress)
    {
        $testReturn = false;
        if (!is_null($progress) && is_object($progress) && $progress instanceof ProgressBar) {
            $testReturn = true;
        }
        return $testReturn;
    }

    protected function endProgressBar($progress, $message)
    {
        if ($this->testProgressBar($progress)) {
            $progressFormat = '<fg=green>    %message%</>' . PHP_EOL;
            $progress->setMessage($message);
            $progress->setFormat($progressFormat);
            $progress->finish();
        }
    }

    protected function setProgressBarMessage($progress, $message, $name = 'message')
    {
        if ($this->testProgressBar($progress)) {
            $progress->setMessage($message, $name);
        }
    }

    protected function advanceProgressBar($progress, $step = 1)
    {
        if ($this->testProgressBar($progress)) {
            $progress->advance($step);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* Titre */
        $output->writeln('<fg=white;bg=blue>                            </>', OutputInterface::VERBOSITY_VERBOSE);
        $output->writeln('<fg=white;bg=blue;options=bold>  Recalcul des Expériences  </>', OutputInterface::VERBOSITY_VERBOSE);
        $output->writeln('<fg=white;bg=blue>                            </>', OutputInterface::VERBOSITY_VERBOSE);
        $output->writeln('', OutputInterface::VERBOSITY_VERBOSE);
        $progressFormat = '    <bg=cyan> %message% </>' . PHP_EOL .  '    <fg=cyan>%current%/%max% [%bar%] %percent%%</>' . PHP_EOL .  '    <fg=cyan>%remaining%/%elapsed%/%estimated% - %memory%</>' . PHP_EOL;

        /* Démarrage de StopWatch */
        $this->outputSubtitle($output, 'Démarrage de StopWatch');
        $stopwatch = new Stopwatch();
        $stopwatch->start('ExpCalc');
        $this->outputSuccess($output, 'StopWatch démarré');
        $output->writeln('', OutputInterface::VERBOSITY_VERBOSE);

        /* Récupération des données de stream */
        $this->outputSubtitle($output, 'Récupération des donnés du Stream');
        $this->outputInfo($output, 'Récupération via la donnée ' . $input->getArgument('channelId'));
        $stream = StreamQuery::findOneByChannelNameId($input->getArgument('channelId'));
        $this->outputSuccess($output, 'Données récupérées (#' . $stream->getId() . ' ' . $stream->getName() . ')');
        $this->addLap($output, $stopwatch);

        /* Tableau d'utilisateurs */
        $this->outputSubtitle($output, 'Tableau d\'utilisateurs');
        $this->outputInfo($output, 'Initialisation du tableau');
        $userExps = [];
        $this->outputInfo($output, 'Création du template');
        $userExpTemp = [
            'userExp'      => null,
            'isFollowing'  => false,
            'messageCount' => 0,
            'followCount'  => 0,
            'hostCount'    => 0,
            'chatterCount' => 0,
        ];
        $this->outputSuccess($output, 'Initialisation du tableau terminée');
        $this->addLap($output, $stopwatch);

        /* Utilisateurs existants */
        $this->outputSubtitle($output, 'Utilisateurs existants');
        $userExperiences = UserExperienceQuery::getAllByStream($stream);
        $output->writeln('<fg=green>    Utilisateurs récupérés</>', OutputInterface::VERBOSITY_VERBOSE);
        $output->writeln('        ' . $userExperiences->count() . ' lignes', OutputInterface::VERBOSITY_VERBOSE);
        $progress = $this->newProgressBar($output, $userExperiences->count(), 'Début du traitement des utilisateurs existants');
        foreach ($userExperiences as $userExperience) {
            $this->setProgressBarMessage($progress, 'Traitement de l\'utilisateur ' . $userExperience->getChatUser()->getUsername());
            $userExpKey = $userExperience->getSite()->getCode() . '-' . $userExperience->getChatUser()->getUsername();
            $userExps[$userExpKey]            = $userExpTemp;
            $userExps[$userExpKey]['userExp'] = $userExperience;
            $this->advanceProgressBar($progress);
        }
        $this->endProgressBar($progress, 'Utilisateurs existants traités');
        $this->addLap($output, $stopwatch);
        unset($userExperiences);
        
        /* Rangs */
        $this->outputSubtitle($output, 'Tableau des rangs');
        $this->outputInfo($output, 'Récupération des rangs');
        $ranks = RankQuery::create()
            ->filterByStream($stream)
            ->find();
        $this->outputSuccess($output, 'Rangs récupérés (' . $ranks->count() .')');
        $this->addLap($output, $stopwatch);
        
        /* Avatars */
        $this->outputSubtitle($output, 'Tableau des avatars');
        $this->outputInfo($output, 'Récupération des avatars');
        $avatars = AvatarQuery::create()
            ->filterByStream($stream)
            ->find();
        $this->outputSuccess($output, 'Avatars récupérés (' . $avatars->count() .')');
        $this->addLap($output, $stopwatch);

        /* Messages */
        $output->writeln('<options=bold> > Messages</>', OutputInterface::VERBOSITY_VERBOSE);
        if ($input->getOption('no-message') === null || $input->getOption('no-message') === false) {
            $messages = MessageExperienceQuery::create()
                ->useChatUserQuery()
                    ->orderByUsername()
                ->endUse()
                ->filterByStream($stream)
                ->find();
            $output->writeln('<fg=green>    Messages récupérés</>', OutputInterface::VERBOSITY_VERBOSE);
            $output->writeln('        ' . $messages->count() . ' lignes', OutputInterface::VERBOSITY_VERBOSE);
            $progress = new ProgressBar($output, $messages->count());
            $progress->setFormat($progressFormat);
            $progress->setMessage('Début du traitement des Messages');
            $progress->start();
            foreach ($messages as $message) {
                $userExpKey = $message->getSite()->getCode() . '-' . $message->getChatUser()->getUsername();
                $progress->setMessage('Traitement des messages de l\'utilisateur ' . $userExpKey);
                if (!isset($userExps[$userExpKey])) {
                    $userExp = UserExperienceQuery::create()
                        ->filterBySite($message->getSite())
                        ->filterByStream($message->getStream())
                        ->filterByChannel($message->getChannel())
                        ->filterByChatUser($message->getChatUser())
                        ->findOneOrCreate()
                    ;
                    if ($userExp->isNew()) {
                        $userExp->save();
                    }
                    $userExps[$userExpKey]            = $userExpTemp;
                    $userExps[$userExpKey]['userExp'] = $userExp;
                }
                if (is_null($message->getUserExperience()) || $message->getUserExperience() !== $userExps[$userExpKey]['userExp']) {
                    $message
                        ->setUserExperience($userExps[$userExpKey]['userExp'])
                        ->save();
                }
                $userExps[$userExpKey]['messageCount']++;
                $progress->advance();
            }
            $progress->setMessage('Messages traités');
            $progress->finish();
            unset($messages);
        } else {
            $output->writeln('<fg=green>    Messages ignorés</>', OutputInterface::VERBOSITY_VERBOSE);
        }
        $this->addLap($output, $stopwatch);

        /* Abonnements */
        $output->writeln('<options=bold> > Abonnements</>', OutputInterface::VERBOSITY_VERBOSE);
        if ($input->getOption('no-follow') === null || $input->getOption('no-follow') === false) {
            $follows = FollowExperienceQuery::create()
                ->useChatUserQuery()
                    ->orderByUsername()
                ->endUse()
                ->filterByStream($stream)
                ->find();
            $output->writeln('<fg=green>    Abonnements récupérés</>', OutputInterface::VERBOSITY_VERBOSE);
            $output->writeln('        ' . $follows->count() . ' lignes', OutputInterface::VERBOSITY_VERBOSE);
            $progress = new ProgressBar($output, $follows->count());
            $progress->setFormat($progressFormat);
            $progress->setMessage('Début du traitement des Abonnements');
            $progress->start();
            foreach ($follows as $follow) {
                $userExpKey = $follow->getSite()->getCode() . '-' . $follow->getChatUser()->getUsername();
                $progress->setMessage('Traitement des abonnements de l\'utilisateur ' . $userExpKey);
                if (!isset($userExps[$userExpKey])) {
                    $userExp = UserExperienceQuery::create()
                        ->filterBySite($follow->getSite())
                        ->filterByStream($follow->getStream())
                        ->filterByChannel($follow->getChannel())
                        ->filterByChatUser($follow->getChatUser())
                        ->findOneOrCreate()
                    ;
                    if ($userExp->isNew()) {
                        $userExp->save();
                    }
                    $userExps[$userExpKey]            = $userExpTemp;
                    $userExps[$userExpKey]['userExp'] = $userExp;
                }
                if (is_null($follow->getUserExperience()) || $follow->getUserExperience() !== $userExps[$userExpKey]['userExp']) {
                    $follow
                        ->setUserExperience($userExps[$userExpKey]['userExp'])
                        ->save();
                }
                $userExps[$userExpKey]['isFollowing'] = true;
                $userExps[$userExpKey]['followCount']++;
                $progress->advance();
            }
            $progress->setMessage('Abonnements traités');
            $progress->finish();
            unset($follows);
        } else {
            $output->writeln('<fg=green>    Abonnements ignorés</>', OutputInterface::VERBOSITY_VERBOSE);
        }
        $this->addLap($output, $stopwatch);

        /* Hebergements */
        $output->writeln('<options=bold> > Hébergements</>', OutputInterface::VERBOSITY_VERBOSE);
        if ($input->getOption('no-host') === null || $input->getOption('no-host') === false) {
            $hosts = HostExperienceQuery::create()
                ->useChatUserQuery()
                    ->orderByUsername()
                ->endUse()
                ->filterByStream($stream)
                ->find();
            $output->writeln('<fg=green>    Hébergements récupérés</>', OutputInterface::VERBOSITY_VERBOSE);
            $output->writeln('        ' . $hosts->count() . ' lignes', OutputInterface::VERBOSITY_VERBOSE);
            $progress = new ProgressBar($output, $hosts->count());
            $progress->setFormat($progressFormat);
            $progress->setMessage('Début du traitement des Hébergements');
            $progress->start();
            foreach ($hosts as $host) {
                $userExpKey = $host->getSite()->getCode() . '-' . $host->getChatUser()->getUsername();
                $progress->setMessage('Traitement des hébergements de l\'utilisateur ' . $userExpKey);
                if (!isset($userExps[$userExpKey])) {
                    $userExp = UserExperienceQuery::create()
                        ->filterBySite($host->getSite())
                        ->filterByStream($host->getStream())
                        ->filterByChannel($host->getChannel())
                        ->filterByChatUser($host->getChatUser())
                        ->findOneOrCreate()
                    ;
                    if ($userExp->isNew()) {
                        $userExp->save();
                    }
                    $userExps[$userExpKey]            = $userExpTemp;
                    $userExps[$userExpKey]['userExp'] = $userExp;
                }
                if (is_null($host->getUserExperience()) || $host->getUserExperience() !== $userExps[$userExpKey]['userExp']) {
                    $host
                        ->setUserExperience($userExps[$userExpKey]['userExp'])
                        ->save();
                }
                $userExps[$userExpKey]['hostCount']++;
                $progress->advance();
            }
            $progress->setMessage('Hébergements traités');
            $progress->finish();
            unset($hosts);
        } else {
            $output->writeln('<fg=green>    Hébergements ignorés</>', OutputInterface::VERBOSITY_VERBOSE);
        }
        $this->addLap($output, $stopwatch);

        /* Vues */
        $output->writeln('<options=bold> > Vues</>', OutputInterface::VERBOSITY_VERBOSE);
        if ($input->getOption('no-view') === null || $input->getOption('no-view') === false) {
            $progress = $this->newProgressBar($output, count($userExps), 'Début du traitement des Vues par utilisateur déjà recensé');
            foreach ($userExps as $userExpKey => $userExp) {
                $progressMessageBase = 'Traitement des vues de ' . $userExp['userExp']->getChatUser()->getUsername();
                $this->setProgressBarMessage($progress, $progressMessageBase);
                $viewsCount = ChatterExperienceQuery::countByDetailedUserExperience($userExp['userExp']);
                $userExps[$userExpKey]['chatterCount'] = $viewsCount;
                if ($viewsCount > 0) {
                    $this->setProgressBarMessage($progress, $progressMessageBase . ' (Vues non associées)');
                    $virginViewsCount = ChatterExperienceQuery::countByDetailedUserExperience($userExp['userExp'], true);
                    if ($virginViewsCount > 0) {
                        $this->setProgressBarMessage($progress, $progressMessageBase . ' (Sauvegarde)');
                        ChatterExperienceQuery::updateByDetailedUserExperience(['UserExperienceId' => $userExp['userExp']->getId()], $userExp['userExp'], true);
                    }
                }
                $this->advanceProgressBar($progress);
            }
            $this->endProgressBar($progress, 'Vues par utilisateur déjà rescencé traitées');
            $chatters = ChatterExperienceQuery::findAllByStreamGroupByChatUser($stream, true);
            dump($chatters->count());
            if ($chatters->count() > 0) {
                $progress = $this->newProgressBar($output, count($chatters), 'Début du traitement des Vues non Associées');
                foreach ($chatters as $chatter) {
                    $userExp = UserExperienceQuery::findOneOrCreateBySiteAndStreamAndChannelAndChatUser($chatter['idSite'], $chatter['idStream'], $chatter['idChannel'], $chatter['ChatUserId']);
                    $progressMessageBase = 'Traitement des vues de ' . $userExp->getChatUser()->getUsername();
                    $this->setProgressBarMessage($progress, $progressMessageBase . ' (Mise à jour)');
                    if ($userExp->isNew()) {
                        $userExp->save();
                    }
                    ChatterExperienceQuery::updateByDetailedUserExperience(['UserExperienceId' => $userExp->getId()], $userExp, true);
                    $this->setProgressBarMessage($progress, $progressMessageBase . ' (Compte)');
                    $userExpKey = $userExp->getSite()->getCode() . '-' . $userExp->getChatUser()->getUsername();
                    $viewsCount = ChatterExperienceQuery::countByDetailedUserExperience($userExp);
                    $userExps[$userExpKey]                 = $userExpTemp;
                    $userExps[$userExpKey]['userExp']      = $userExp;
                    $userExps[$userExpKey]['chatterCount'] = $viewsCount;
                    $this->advanceProgressBar($progress);
                }
                $this->endProgressBar($progress, 'Vues Traitées');
            }
        } else {
            $output->writeln('<fg=green>    Vues ignorés</>', OutputInterface::VERBOSITY_VERBOSE);
        }
        $this->addLap($output, $stopwatch);

        /* Mises à jour des utilisateurs */
        $output->writeln('<options=bold> > Mises à jour des utilisateurs</>', OutputInterface::VERBOSITY_VERBOSE);
        $defaultRank   = null;
        $followRank    = null;
        $defaultAvatar = null;
        foreach ($ranks as $rank) {
            if ($rank->getIsDefault()) {
                $defaultRank = $rank;
            }
            if ($rank->getSetIfFollowEvent()) {
                $followRank = $rank;
            }
        }
        foreach ($avatars as $avatar) {
            if ($avatar->getIsDefault()) {
                $defaultAvatar = $avatar;
            }
        }
        $progress = new ProgressBar($output, count($userExps));
        $progress->setFormat($progressFormat);
        $progress->setMessage('Début du traitement des utilisateurs');
        $progress->start();
        foreach ($userExps as $userExpKey => $userExp) {
            $rank   = $userExp['userExp']->getRank();
            $avatar = $userExp['userExp']->getAvatar();
            if (is_null($rank)) {
                $rank = $defaultRank;
            }
            if (is_null($avatar)) {
                $avatar = $defaultAvatar;
            }
            $progress->setMessage('Traitement de l\'utilisateur ' . $userExpKey);
            if ($input->getOption('no-message') === null || $input->getOption('no-message') === false) {
                $userExp['userExp']->setMessagesCount($userExp['messageCount']);
            }
            if ($input->getOption('no-follow') === null || $input->getOption('no-follow') === false) {
                $userExp['userExp']
                    ->setIsFollowing($userExp['isFollowing'])
                    ->setFollowsCount($userExp['followCount'])
                ;
                if ($userExp['isFollowing'] === true && !is_null($rank) && $rank !== $followRank && !in_array($followRank->getId(), $rank->getDependencyRankIds())) {
                    $rank = $followRank;
                }
            }
            if ($input->getOption('no-host') === null || $input->getOption('no-host') === false) {
                $userExp['userExp']->setHostsCount($userExp['hostCount']);
            }
            if ($input->getOption('no-view') === null || $input->getOption('no-view') === false) {
                $userExp['userExp']->setChattersCount($userExp['chatterCount']);
            }
            if (!is_null($rank)) {
                $userExp['userExp']->setRank($rank);
            }
            if (!is_null($avatar)) {
                $userExp['userExp']->setAvatar($avatar);
            }
            if ($userExp['userExp']->isModified()) {
                $userExp['userExp']->save();
            }
            $progress->advance();
        }
        $progress->setMessage('Utilisateurs traités');
        $progress->finish();
        $this->addLap($output, $stopwatch);
        unset($userExps);

        /* Fin du Stopwatch */
        $output->writeln('<options=bold> > Résumé</>', OutputInterface::VERBOSITY_VERBOSE);
        $event = $stopwatch->stop('ExpCalc');
        $events = [
            'Récupération du Stream                      ',
            'Récupération des Utilisateurs déjà existants',
            'Traitement des Utilisateurs déjà existants  ',
            'Récupération des rangs                      ',
            'Récupération des avatars                    ',
            'Traitement des Messages                     ',
            'Traitement des Abonnements                  ',
            'Traitement des Hébergements                 ',
            'Traitement des Vues                         ',
            'Enregistrement des Utilisateurs             ',
            'Fin                                         ',
        ];
        foreach ($event->getPeriods() as $i => $period) {
            $output->writeln('    ' . $events[$i] . ' ' . number_format(round($period->getDuration() / 1000, 3), 3) . 's (' . number_format(round($period->getStartTime() / 1000, 3), 3) . 's>' . number_format(round($period->getEndTime() / 1000, 3), 3) . 's)', OutputInterface::VERBOSITY_VERBOSE);
        }
    }
}

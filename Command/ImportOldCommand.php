<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use IiMedias\StreamBundle\Model\HostExperience;
use IiMedias\StreamBundle\Model\MessageExperience;
use IiMedias\StreamBundle\Model\MessageExperienceQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;

use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;

class ImportOldCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:stream:import:old')
            ->setDescription('importation depuis l ancien site')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startTimestamp   = 1454976000;
        $appConfig        = AppConfigSingleton::getInstance();
        $currentTimestamp = intval($appConfig->getValue('streamOldTimestamp', $startTimestamp));
        $currentDateTime  = new DateTime();
        $currentDateTime->setTimestamp($currentTimestamp);
        $endDatetime      = new DateTime();
        $endDatetime->setTimestamp($currentDateTime->getTimestamp() + 3600);
        $url = 'http://old.sebii.fr/app_dev.php/json/exp/' . $currentTimestamp;
        dump($url);
        dump($currentDateTime);
        $jsonImport = json_decode(file_get_contents($url), true);

        $site    = SiteQuery::getOneByCode('twitch');
        $channel = ChannelQuery::getOneByChannelAndSite('sebiiheckel', $site);

        /* Gestion des messages */
        foreach ($jsonImport['messages'] as $jsonMessage) {
            $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($jsonMessage['pseudo'], $site);
            $messageDate = new DateTime();
            $messageDate->setTimestamp($jsonMessage['timestamp']);
            $message = new MessageExperience();
            $message
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setSite($site)
                ->setChatUser($chatUser)
                ->setType('message')
                ->setMessage($jsonMessage['message'])
                ->setExp(1)
                ->setAt($messageDate)
                ->save()
            ;
        }

        $countMessages = MessageExperienceQuery::create()
            ->filterByStream($channel->getStream())
            ->filterByChannel($channel)
            ->filterBySite($site)
            ->count();
        $channel
            ->setMessagesCount($countMessages)
            ->save();

        /* Gestion des Chatters */
        foreach ($jsonImport['views'] as $chatterTimestamp => $chattersList) {
            $chatterDateTime = new DateTime();
            $chatterDateTime->setTimestamp($chatterTimestamp);
            foreach ($chattersList as $chatterName) {
                $chatUser          = ChatUserQuery::getOneOrCreateByUsernameAndSite($chatterName, $site);
                $chatterExperience = new ChatterExperience();
                $chatterExperience
                    ->setStream($channel->getStream())
                    ->setChannel($channel)
                    ->setSite($site)
                    ->setChatUser($chatUser)
                    ->setExp(1)
                    ->setAt($chatterDateTime)
                    ->save()
                ;
            }
        }

        /* Gestion des Hosts */
        foreach ($jsonImport['hosts'] as $hostTimestamp => $hostsList) {
            $hostDateTime = new DateTime();
            $hostDateTime->setTimestamp($hostTimestamp);
            foreach ($hostsList as $hostName) {
                $chatUser       = ChatUserQuery::getOneOrCreateByUsernameAndSite($hostName, $site);
                $hostExperience = new HostExperience();
                $hostExperience
                    ->setStream($channel->getStream())
                    ->setChannel($channel)
                    ->setSite($site)
                    ->setChatUser($chatUser)
                    ->setExp(1)
                    ->setAt($hostDateTime)
                    ->save()
                ;
            }
        }

        $appConfig->setValue('streamOldTimestamp', $currentTimestamp + 3600);

        dump($jsonImport['hosts']);
    }
}

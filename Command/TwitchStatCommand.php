<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperienceQuery;
use IiMedias\StreamBundle\Model\FollowDiffDataQuery;
use IiMedias\StreamBundle\Model\FollowExperienceQuery;
use IiMedias\StreamBundle\Model\HostExperienceQuery;
use IiMedias\StreamBundle\Model\Map\ChatterExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\FollowDiffDataTableMap;
use IiMedias\StreamBundle\Model\Map\HostExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\ViewerDataTableMap;
use IiMedias\StreamBundle\Model\MessageExperienceQuery;
use IiMedias\StreamBundle\Model\StatusData;
use IiMedias\StreamBundle\Model\StatusDataQuery;
use IiMedias\StreamBundle\Model\ViewerDataQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Propel\Runtime\ActiveQuery\Criteria;
use \Exception;
use \DateTime;

use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\Stat;
use IiMedias\StreamBundle\Model\StatQuery;
use IiMedias\StreamBundle\Model\TypeDataQuery;
use IiMedias\StreamBundle\Model\ViewDiffDataQuery;
use IiMedias\StreamBundle\Model\Map\ViewDiffDataTableMap;

class TwitchStatCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stream:twitch:stats')
            ->setDescription('Récupère toutes les informationd\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'   => 'ok',
            '_error'    => null,
            'siteId'    => null,
            'channelId' => null,
            'streamId'  => null,
            'duration'  => null,
            'startAt'   => null,
            'endAt'     => null,
        ];
        try {
            /* Données de filtrage de la statistique */
            $channelId         = urldecode($input->getArgument('channelId'));
            $site              = SiteQuery::getOneByCode('twitch');
            $response['siteId'] = $site->getId();
            $channel           = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $response['channelId'] = $channel->getId();
            $stream                = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['streamId'] = $stream->getId();

            /* Données de filtrage temporel */
            $durationsList = ['live', 'vod', 'year', 'month', 'week', 'day', 'hour', 'minute', 'rescan', 'exit'];
            $now           = new DateTime('now UTC -2 minutes');
            $stat          = null;
            foreach ($durationsList as $durationPurpose) {
                $startAt    = null;
                $endAt      = null;
                $findedStat = null;
                switch ($durationPurpose) {
                    case 'live': // 6
                        $lastLive = TypeDataQuery::create()
                            ->filterBySite($site)
                            ->filterByStream($stream)
                            ->filterByChannel($channel)
                            ->filterByStreamType($durationPurpose)
                            ->orderByAt(Criteria::DESC)
                            ->findOne()
                        ;
                        if (is_null($lastLive)) {
                            break;
                        }
                        $nextChange = TypeDataQuery::create()
                            ->filterBySite($site)
                            ->filterByStream($stream)
                            ->filterByChannel($channel)
                            ->filterByStreamType($durationPurpose, Criteria::NOT_EQUAL)
                            ->filterByAt($lastLive->getAt(), Criteria::GREATER_EQUAL)
                            ->findOne()
                        ;
                        if (is_null($nextChange)) {
                            break;
                        }
                        $startAt    = $lastLive->getAt();
                        $endAt      = $nextChange->getAt();
                        break;
                    case 'vod': // 7
                        $lastLive = TypeDataQuery::create()
                            ->filterBySite($site)
                            ->filterByStream($stream)
                            ->filterByChannel($channel)
                            ->filterByStreamType($durationPurpose)
                            ->orderByAt(Criteria::DESC)
                            ->findOne()
                        ;
                        if (is_null($lastLive)) {
                            break;
                        }
                        $nextChange = TypeDataQuery::create()
                            ->filterBySite($site)
                            ->filterByStream($stream)
                            ->filterByChannel($channel)
                            ->filterByStreamType($durationPurpose, Criteria::NOT_EQUAL)
                            ->filterByAt($lastLive->getAt(), Criteria::GREATER_EQUAL)
                            ->findOne()
                        ;
                        if (is_null($nextChange)) {
                            break;
                        }
                        $startAt    = $lastLive->getAt();
                        $endAt      = $nextChange->getAt();
                        break;
                    case 'year': // 5
                        $startAt = new DateTime($now->format('Y') . '-01-01 00:00:00 UTC -1 year');
                        $endAt   = new DateTime($startAt->format('Y') . '-12-31 23:59:59 UTC');
                        break;
                    case 'month': // 4
                        $startAt = new DateTime($now->format('Y-m') . '-01 00:00:00 UTC -1 month');
                        $endAt   = new DateTime($startAt->format('Y-m-t') . ' 23:59:59 UTC');
                        break;
                    case 'week': // 3
                        $startAt = new DateTime($now->format('Y-m-d') . ' 00:00:00 UTC -' . $now->format('w') . ' day -1 week');
                        $endAt   = new DateTime($startAt->format('Y-m-d') . ' 23:59:59 UTC +7 day');
                        break;
                    case 'day': // 2
                        $startAt = new DateTime($now->format('Y-m-d') . ' 00:00:00 UTC -' . $now->format('w') . ' day');
                        $endAt   = new DateTime($startAt->format('Y-m-d') . ' 23:59:59 UTC');
                        break;
                    case 'hour': // 1
                        $startAt = new DateTime($now->format('Y-m-d H') . ':00:00 UTC -1 hour');
                        $endAt   = new DateTime($startAt->format('Y-m-d H') . ':59:59 UTC');
                        break;
                    case 'minute': // 0
                        $startAt = new DateTime($now->format('Y-m-d H:i') . ':00 UTC -1 minute');
                        $endAt   = new DateTime($startAt->format('Y-m-d H:i') . ':59 UTC');
                        break;
                    case 'rescan':
                        throw new Exception('Rescan non implémenté pour le moment');
                        break;
                    case 'exit':
                        throw new Exception('Rien à scanner');
                }
                if (!is_null($startAt) && !is_null($endAt)) {
                    $findedStat = StatQuery::create()
                        ->filterBySite($site)
                        ->filterByStream($stream)
                        ->filterByChannel($channel)
                        ->filterByDuration($durationPurpose)
                        ->filterByStartAt($startAt)
                        ->filterByEndAt($endAt)
                        ->findOne()
                    ;
                }
                if (!is_null($startAt) && !is_null($endAt) && is_null($findedStat)) {
                    $duration = $durationPurpose;
                    dump('stat à créer sur ' . $durationPurpose . ' de ' . $startAt->format('d/m/Y H:i:s') . ' à ' . $endAt->format('d/m/Y H:i:s'));
                    break;
                }
            }
            $response['duration'] = $duration;
            $response['startAt']  = $startAt->format('Y/m/d H:i:s');
            $response['endAt']    = $endAt->format('Y/m/d H:i:s');

            /* Recherche de la Statistique */
            $stat    = StatQuery::create()
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByDuration($duration)
                ->filterByStartAt($startAt)
                ->filterByEndAt($endAt)
                ->findOne()
            ;

            /* Si elle n'existe pas, on la crée */
            if (is_null($stat)) {
                $stat = new Stat();
                $stat
                    ->setSite($site)
                    ->setStream($stream)
                    ->setChannel($channel)
                    ->setDuration($duration)
                    ->setStartAt($startAt)
                    ->setEndAt($endAt)
                    ->save()
                ;
            }

            /* Récupération du type (offline, live ou vod) */
            $streamTypeData = TypeDataQuery::create()
                ->filterByAt($startAt, Criteria::LESS_EQUAL)
                ->orderByAt(Criteria::DESC)
                ->findOne()
            ;
            $stat->setStreamType($streamTypeData->getStreamType());

            /* Récupération du nombre de nouvelles vues */
            $views = ViewDiffDataQuery::create()
                ->select(['total'])
                ->withColumn('SUM(' . ViewDiffDataTableMap::COL_STDDAT_DIFF . ')', 'total')
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->find()
            ;
            $stat->setViews(intval($views->getData()[0]));

            /* Récupération du nombre maximum de spectateurs */
            $maxViewers = ViewerDataQuery::create()
                ->select(['max'])
                ->withColumn('MAX(' . ViewerDataTableMap::COL_STVDAT_COUNT . ')', 'max')
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->find()
            ;
            $stat->setMaximumViewers(intval($maxViewers->getData()[0]));

            /* Récupération du total de vues et le nombre d'occurences */
            $averageViewers = ViewerDataQuery::create()
                ->select(['total', 'count'])
                ->withColumn('SUM(' . ViewerDataTableMap::COL_STVDAT_COUNT . ')', 'total')
                ->withColumn('COUNT(*)', 'count')
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->filterByCount(0, Criteria::GREATER_THAN)
                ->find()
            ;
            $stat->setAverageViewers(intval(floor($averageViewers->getData()[0]['total'] / $averageViewers->getData()[0]['count'])));

            /* Récupération du nombre de chatters uniques */
            $chatters = ChatterExperienceQuery::create()
                ->select(ChatterExperienceTableMap::COL_STCEXP_STCUSR_ID)
                ->distinct()
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->count()
            ;
            $stat->setChatters($chatters);

            /* Récupération du nombre de messages */
            $messages = MessageExperienceQuery::create()
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->count()
            ;
            $stat->setMessages($messages);

            /* Récupération du nombre de hosts uniques */
            $hosts = HostExperienceQuery::create()
                ->select(HostExperienceTableMap::COL_STHEXP_STCUSR_ID)
                ->distinct()
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->count()
            ;
            $stat->setHosts($hosts);

            /* Récupération du nombre de follows */
            $follows = FollowExperienceQuery::create()
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->count()
            ;
            $stat->setFollows($follows);

            /* Récupération du nombre de unfollows */
            $unfollows = FollowDiffDataQuery::create()
                ->select(['total'])
                ->withColumn('SUM(' . FollowDiffDataTableMap::COL_STFDAT_DIFF . ')', 'total')
                ->filterBySite($site)
                ->filterByStream($stream)
                ->filterByChannel($channel)
                ->filterByDiff(0, Criteria::LESS_THAN)
                ->filterByAt($startAt, Criteria::GREATER_EQUAL)
                ->filterByAt($endAt, Criteria::LESS_EQUAL)
                ->find()
            ;
            $stat->setUnfollows(intval($unfollows->getData()[0]));

            $stat->save();
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }

        $output->write(json_encode($response));
    }
}

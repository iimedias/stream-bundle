<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use IiMedias\StreamBundle\Model\HostExperience;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class TwitchHostsFileCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;
    
    protected function configure()
    {
        $this
            ->setName('stream:twitch:hostsfile')
            ->setDescription('Récupère tous les chatters d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'   => 'ok',
            '_error'    => null,
            'channelId' => null,
            'streamId'  => null,
        ];
        try {
            $channelId  = urldecode($input->getArgument('channelId'));
            $site       = SiteQuery::getOneByCode('twitch');
            $channel    = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream    = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId']  = $stream->getId();
            if ($channel->getLockHostsScan() == true) {
                throw new Exception('Already in use');
            }
            $channel
                ->setLockHostsScan(true)
                ->save()
            ;
            $finder             = new Finder();
            $pathHostsJson      = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'working');
            $finder
                ->files()
                ->in($pathHostsJson)
                ->name('stream_hosts_' . $channel->getId() . '_*.json')
                ->sortByName()
            ;
            $fs = new Filesystem();
            if (count($finder) > 1) {
                $iterator = $finder->getIterator();
                $iterator->rewind();
                $file     = $iterator->current();
                preg_match('#^stream_hosts_' . $channel->getId() . '_([0-9]*)\.json$#', $file->getFilename(), $timestampMatches);
                $minuteDate = new \DateTime();
                $minuteDate->setTimestamp($timestampMatches[1]);
                $hostsJson = json_decode(file_get_contents($file->getPathname()), true);
                $fs->remove($file->getPathname());
                foreach ($hostsJson as $hostName) {
                    $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($hostName, $channel->getSite());
                    HostExperience::createNew($channel, $chatUser, $minuteDate);
                }
            }
            $channel
                ->setLockHostsScan(false)
                ->save()
            ;
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}
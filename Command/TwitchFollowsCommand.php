<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\ChatterExperience;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\FollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery;

class TwitchFollowsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stream:twitch:follows')
            ->setDescription('Récupère tous les follows d\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'    => 'ok',
            '_error'     => null,
            'channelId'  => null,
            'streamId'   => null,
            'follows'    => [],
        ];

        try {
            $channelId = urldecode($input->getArgument('channelId'));
            $site = SiteQuery::getOneByCode('twitch');
            $channel = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId'] = $stream->getId();
            if ($stream->getCanScan() === false || $channel->getCanScan() === false) {
                throw new Exception('Can not scan this stream or this channel');
            }
            $bot = null;
            if (is_null($channel->getBot())) {
                throw new Exception('No bot defined');
            }
            foreach ($channel->getBot()->getChannels() as $channelBot) {
                if ($channelBot->getSite()->getCode() == 'twitch') {
                    $bot = $channelBot;
                    break;
                }
            }
            if (is_null($bot)) {
                throw new Exception('No bot for this site');
            }

            $limit = 100;

            for ($i = 0; $i == 0; $i++) {
                $followsUrl  = 'https://api.twitch.tv/kraken/channels/' . $channel->getChannel() . '/follows?direction=DESC&limit=' . $limit . '&offset=' . ($i * 100) . '&client_id=' . $bot->getClientId();
                $followsJson = json_decode(file_get_contents($followsUrl), true);
                foreach ($followsJson['follows'] as $followJson) {
                    preg_match('#^([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2}:[0-9]{2})Z$#', $followJson['created_at'], $followAtMatches);
                    $chatUser = ChatUserQuery::getOneOrCreateByUsernameAndSite($followJson['user']['name'], $channel->getSite());
                    $followAt = new DateTime($followAtMatches[1] . ' ' . $followAtMatches[2] . ' UTC');
                    $follow   = FollowExperienceQuery::create()
                        ->filterByChannel($channel)
                        ->filterByChatUser($chatUser)
                        ->filterByAt($followAt)
                        ->findOne()
                    ;
                    if (is_null($follow)) {
                        $follow = new FollowExperience();
                        $follow
                            ->setStream($channel->getStream())
                            ->setChannel($channel)
                            ->setSite($channel->getSite())
                            ->setChatUser($chatUser)
                            ->setExp(20)
                            ->setAt($followAt)
                            ->save()
                        ;
                        $response['follows'][] = $chatUser->getUsername();
                    }
                }
            }
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}

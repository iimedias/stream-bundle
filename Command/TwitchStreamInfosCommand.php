<?php

namespace IiMedias\StreamBundle\Command;

use IiMedias\StreamBundle\Model\TypeDataQuery;
use IiMedias\StreamBundle\Model\ViewerData;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;
use \Exception;
use \stdClass;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Experience;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;

class TwitchStreamInfosCommand extends ContainerAwareCommand
{
    protected $success;
    protected $error;
    protected $site;
    protected $channel;
    protected $stream;
    protected $chatUser;
    protected $bot;

    protected function configure()
    {
        $this
            ->setName('stream:twitch:streaminfos')
            ->setDescription('Récupère toutes les informationd\'un stream')
            ->addArgument('channelId')
        ;
    }

    protected function emojiReplace($str)
    {
        $str = str_replace("\xF0\x9F\x87\xAB\xF0\x9F\x87\xB7", '&#x1F1EB;', $str); // Flag France
        $str = str_replace("\xEE\x94\x90", '&#xE510;', $str); // Flag Royaume Uni
        return $str;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = [
            '_status'          => 'ok',
            '_error'           => null,
            'streamId'         => null,
            'channelId'        => null,
        ];
        try {
            $channelId = urldecode($input->getArgument('channelId'));
            $site      = SiteQuery::getOneByCode('twitch');
            $channel   = is_numeric($channelId) ? ChannelQuery::getOneByIdAndSite($channelId, $site) : ChannelQuery::getOneByChannelAndSite($channelId, $site);
            if (is_null($channel)) {
                throw new Exception('Unknown Channel');
            }
            $stream    = $channel->getStream();
            if (is_null($stream)) {
                throw new Exception('Unknown Stream');
            }
            $response['channelId'] = $channel->getId();
            $response['streamId']  = $stream->getId();
            if ($stream->getCanScan() === false || $channel->getCanScan() === false) {
                throw new Exception('Can not scan this stream or this channel');
            }
            $bot = null;
            if (is_null($channel->getBot())) {
                throw new Exception('No bot defined');
            }
            foreach ($channel->getBot()->getChannels() as $channelBot) {
                if ($channelBot->getSite()->getCode() == 'twitch') {
                    $bot = $channelBot;
                    break;
                }
            }
            if (is_null($bot)) {
                throw new Exception('No bot for this site');
            }
            $streamUrl    = 'https://api.twitch.tv/kraken/streams/' . $channel->getChannel() . '?client_id=' . $bot->getClientId();
            $streamJson   = json_decode(file_get_contents($streamUrl), true);
            $streamType   = is_null($streamJson['stream']) ? 'offline' : (($streamJson['stream']['stream_type'] === "watch_party") ? 'vod' : 'live');
            $viewersCount = is_null($streamJson['stream']) ? 0 : intval($streamJson['stream']['viewers']);
            $channel
                ->setStreamType($streamType)
                ->setViewersCount($viewersCount)
                ->save()
            ;
            $viewerData = new ViewerData();
            $viewerData
                ->setSite($site)
                ->setStream($stream)
                ->setChannel($channel)
                ->setCount($viewersCount)
                ->setAt(new DateTime())
                ->save()
            ;
            TypeDataQuery::getLastOfChannelOrCreate($channel, $streamType);
        } catch (Exception $e) {
            $response['_status'] = 'ko';
            $response['_error']  = $e->getMessage();
        }
        $output->write(json_encode($response));
    }
}


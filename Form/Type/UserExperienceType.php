<?php

namespace IiMedias\StreamBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IiMedias\StreamBundle\Model\TwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery;
use IiMedias\StreamBundle\Model\RankQuery;
use IiMedias\StreamBundle\Model\AvatarQuery;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class UserExperienceType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['formMode'] == 'add') {
            $notViewers = \IiMedias\StreamBundle\Model\UserExperienceQuery::create()
                ->select([\IiMedias\StreamBundle\Model\Map\UserExperienceTableMap::COL_STUEXP_STCUSR_ID])
                ->filterByStreamId(2)
                ->find()
            ;
            
            $viewers = ChatUserQuery::create()
                ->filterById($notViewers->getData(), \Propel\Runtime\ActiveQuery\Criteria::NOT_IN)
                ->select([ChatUserTableMap::COL_STCUSR_ID, ChatUserTableMap::COL_STCUSR_USERNAME])
                ->orderByUsername()
                ->find();
                
            $viewerChoices = [];
            foreach ($viewers->getData() as $viewer) {
                $viewerChoices[$viewer[ChatUserTableMap::COL_STCUSR_USERNAME]] = $viewer[ChatUserTableMap::COL_STCUSR_ID];
            }
            $builder
                ->add('chatUserId', ChoiceType::class, [
                    'attr'     => [
                        'placeholder' => 'Utilisateur',
                    ],
                    'choices'  => $viewerChoices,
                    'label'    => 'Utilisateur',
                    'required' => true,
                ]);
        }
        
        $rankChoices = [];
        $avatarChoices = [];
        $ranks = RankQuery::create()
            ->filterByStream($options['stream'])
            ->orderByName()
            ->find();
        $avatars = AvatarQuery::create()
            ->filterByStream($options['stream'])
            ->orderByName()
            ->find();
        foreach ($ranks as $rank) {
            $rankChoices[$rank->getName()] = $rank->getId();
        }
        foreach ($avatars as $avatar) {
            $rankName = ($avatar->getAssociatedRank()) ? $avatar->getAssociatedRank()->getName() : '## NO ASSOCIATED RANK';
            $avatarChoices[$rankName][$avatar->getName()] = $avatar->getId();
        }
        ksort($avatarChoices);
        
        $builder
            ->add('rankId', ChoiceType::class, [
                'attr'     => [
                    'placeholder' => 'Rang',
                ],
                'choices'  => $rankChoices,
                'label'    => 'Rang',
                'required' => false,
            ])
            ->add('avatarId', ChoiceType::class, [
                'attr'     => [
                    'placeholder' => 'Avatar',
                ],
                'choices'  => $avatarChoices,
                'label'    => 'Avatar',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'attr'  => [
                    'placeholder' => 'Enregistrer',
                ],
                'label' => 'Enregistrer',
            ])
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StreamBundle\Model\UserExperience',
                'name'       => 'user_experience',
                'stream'     => null,
                'formMode'   => 'add',
        ));
    }
}

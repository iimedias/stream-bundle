<?php

namespace IiMedias\StreamBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IiMedias\StreamBundle\Model\TwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery;
use IiMedias\StreamBundle\Model\RankQuery;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class RankType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rankChoices = [];
        $ranks = RankQuery::create()
            ->filterByStream($options['stream'])
            ->find();
        foreach ($ranks as $rank) {
            $rankChoices[$rank->getName()] = $rank->getId();
        }
        
        $builder
            ->add('name', TextType::class, [
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
                'label'    => 'Nom',
                'required' => true,
            ])
            ->add('isDefault', CheckboxType::class, [
                'label'    => 'Rang par défaut',
                'required' => false,
            ])
            ->add('setIfFollowEvent', CheckboxType::class, [
                'label'    => 'Rang si follow',
                'required' => false,
            ])
            ->add('setIfModerator', CheckboxType::class, [
                'label'    => 'Rang si modérateur',
                'required' => false,
            ])
            ->add('setIfStreamer', CheckboxType::class, [
                'label'    => 'Rang si streamer',
                'required' => false,
            ])
            ->add('dependencyRankIds', ChoiceType::class, [
                'choices'  => $rankChoices,
                'multiple' => true,
                'label'    => 'Dépend du rang',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'attr'  => [
                    'placeholder' => 'Enregistrer',
                ],
                'label' => 'Enregistrer',
            ])
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StreamBundle\Model\Rank',
                'name'       => 'rank',
                'stream'     => null,
        ));
    }
}

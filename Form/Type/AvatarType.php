<?php

namespace IiMedias\StreamBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IiMedias\StreamBundle\Model\TwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery;
use IiMedias\StreamBundle\Model\RankQuery;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AvatarType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rankChoices = [];
        $ranks = RankQuery::create()
            ->filterByStream($options['stream'])
            ->find();
        foreach ($ranks as $rank) {
            $rankChoices[$rank->getName()] = $rank->getId();
        }
        
        $builder
            ->add('name', TextType::class, [
                'attr'     => [
                    'placeholder' => 'Nom',
                ],
                'label'    => 'Nom',
                'required' => true,
            ])
            ->add('code', TextType::class, [
                'attr'     => [
                    'placeholder' => 'Code',
                ],
                'label'    => 'Code',
                'required' => false,
            ])
            ->add('avatar', FileType::class, [
                'label'    => 'Avatar',
                'required' => true,
            ])
            ->add('mainColor', ChoiceType::class, [
                'choices'  => [
                    'Sans couleur'   => 'colorless',
                    'Rouge'          => 'red',
                    'Bleu'           => 'blue',
                    'Jaune'          => 'yellow',
                    'Vert'           => 'green',
                    'Rose'           => 'pink',
                    'Noir'           => 'black',
                    'Blanc'          => 'white',
                    'Or'             => 'gold',
                    'Argent'         => 'silver',
                    'Orange'         => 'orange',
                    'Bleu Marine'    => 'navy',
                    'Rouge Bordeaux' => 'fire',
                    'Violet'         => 'violet',
                    'Cyan'           => 'cyan',
                    'Gris'           => 'grey',
                ],
                'multiple' => false,
                'label'    => 'Couleur',
                'required' => true,
            ])
            ->add('isDefault', CheckboxType::class, [
                'label'    => 'Rang par défaut',
                'required' => false,
            ])
            ->add('canBeOwned', CheckboxType::class, [
                'label'    => 'Utilisateur unique',
                'required' => false,
            ])
            ->add('minRankId', ChoiceType::class, [
                'choices'  => $rankChoices,
                'multiple' => false,
                'label'    => 'Rang Minimum',
                'required' => false,
            ])
            ->add('associatedRankId', ChoiceType::class, [
                'choices'  => $rankChoices,
                'multiple' => false,
                'label'    => 'Rang Associé',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'attr'  => [
                    'placeholder' => 'Enregistrer',
                ],
                'label' => 'Enregistrer',
            ])
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StreamBundle\Model\Avatar',
                'name'       => 'avatar',
                'stream'     => null,
        ));
    }
}

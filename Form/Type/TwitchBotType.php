<?php

namespace IiMedias\StreamBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class TwitchBotType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'UserName',
                    ),
                    'label'      => 'UserName',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('password', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Mot de passe',
                    ),
                    'label'      => 'Mot de passe',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('submit', SubmitType::class, array(
                    'attr'  => array(
                        'class' => 'btn btn-primary',
                        'placeholder' => 'Enregistrer',
                    ),
                    'label' => 'Enregistrer',
            ))
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StreamBundle\Model\TwitchBot',
                'name'       => 'twitchBot',
        ));
    }
}

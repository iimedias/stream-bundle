<?php

namespace IiMedias\StreamBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IiMedias\StreamBundle\Model\TwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class ChannelType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('channel', TextType::class, [
                'attr'     => [
                    'placeholder' => 'Canal',
                ],
                'label'    => 'Canal',
                'required' => true,
            ])
            ->add('clientId', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'Client ID',
                ],
                'label' => 'Client ID',
                'required' => false,
            ])
            ->add('passwordOauth', PasswordType::class, [
                'attr'     => [
                    'placeholder' => 'Mot de Passe OAuth',
                ],
                'label'    => 'Mot de Passe OAuth',
                'required' => false,
            ])
            ->add('secretId', PasswordType::class, [
                'attr'     => [
                    'placeholder' => 'Secret ID',
                ],
                'label'    => 'Secret ID',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'attr'  => [
                    'placeholder' => 'Enregistrer',
                ],
                'label' => 'Enregistrer',
            ])
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\StreamBundle\Model\Channel',
                'name'       => 'channel',
                'type'       => 'stream',
        ));
    }
}

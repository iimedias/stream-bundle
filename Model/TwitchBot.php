<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\TwitchBot as BaseTwitchBot;

/**
 * Skeleton subclass for representing a row from the 'stream_twitch_bot_sttwbt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TwitchBot extends BaseTwitchBot
{
    public function __toString()
    {
        return $this->getUsername();
    }
}

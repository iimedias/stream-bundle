<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ExperienceQuery as BaseExperienceQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_experience_stexpr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ExperienceQuery extends BaseExperienceQuery
{
    public static function getAllByStreamAndPeriod(Stream $stream, DateTime $begin, DateTime $end)
    {
        $results = self::create()
            ->filterByStream($stream)
            ->filterByAt($begin, Criteria::GREATER_EQUAL)
            ->filterByAt($end, Criteria::LESS_THAN)
            ->orderByAt(Criteria::ASC)
            ->find();
        return $results;
    }
    
    public static function getLastFollowOfChannel(Channel $channel)
    {
        $result = self::create()
            ->filterByType('follow')
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne();
        return $result;
    }
    
    public static function getLastStatusOfChannel(Channel $channel)
    {
        $result = self::create()
            ->filterByType('status')
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne();
        return $result;
    }
    
    public static function getLastLiveStatusOfChannel(Channel $channel)
    {
        $result = self::create()
            ->filterByType('live')
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne();
        return $result;
    }
    
    public static function getLastGameOfChannel(Channel $channel)
    {
        $result = self::create()
            ->filterByType('game')
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne();
        return $result;
    }
}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\UserExperienceQuery as BaseUserExperienceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_user_experience_stuexp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserExperienceQuery extends BaseUserExperienceQuery
{
    public static function getAllByStream(Stream $stream)
    {
        $userExperiences = self::create()
            ->useChatUserQuery()
                ->orderByUsername()
            ->endUse()
            ->filterByStream($stream)
            ->find()
        ;
        return $userExperiences;
    }

    public static function findOneOrCreateBySiteAndStreamAndChannelAndChatUser($site, $stream, $channel, $chatUser)
    {
        $userExperience = self::create()
            ->_if(is_object($site) && $site instanceof Site)
                ->filterBySite($site)
            ->_else()
                ->filterBySiteId($site)
            ->_endif()
            ->_if(is_object($stream) && $stream instanceof Stream)
                ->filterByStream($stream)
            ->_else()
                ->filterByStreamId($stream)
            ->_endif()
            ->_if(is_object($channel) && $stream instanceof Channel)
                ->filterByChannel($channel)
            ->_else()
                ->filterByChannelId($channel)
            ->_endif()
            ->_if(is_object($channel) && $stream instanceof ChatUser)
                ->filterByChatUser($chatUser)
            ->_else()
                ->filterByChatUserId($chatUser)
            ->_endif()
            ->findOneOrCreate()
        ;
        return $userExperience;
    }
}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\TwitchBotQuery as BaseTwitchBotQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_twitch_bot_sttwbt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TwitchBotQuery extends BaseTwitchBotQuery
{

}

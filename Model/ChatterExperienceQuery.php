<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ChatterExperienceQuery as BaseChatterExperienceQuery;
use IiMedias\StreamBundle\Model\Map\ChatterExperienceTableMap;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_chatter_experience_stcexp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ChatterExperienceQuery extends BaseChatterExperienceQuery
{
    protected function filterByDetailedUserExperience(UserExperience $userExperience)
    {
        $chatterExperience = $this
            ->filterByStream($userExperience->getStream())
            ->filterByChannel($userExperience->getChannel())
            ->filterBySite($userExperience->getSite())
            ->filterByChatUser($userExperience->getChatUser())
        ;
        return $chatterExperience;
    }

    public static function countByDetailedUserExperience(UserExperience $userExperience, $isUserExperienceNull = false)
    {
        $chatterExperienceCount = self::create()
            ->filterByDetailedUserExperience($userExperience)
            ->_if($isUserExperienceNull === true)
                ->filterByUserExperienceId(null)
            ->_endif()
            ->count()
        ;
        return $chatterExperienceCount;
    }

    public static function updateByDetailedUserExperience(array $updateValues, UserExperience $userExperience, $isUserExperienceNull = false)
    {
        $chatterExperienceCount = self::create()
            ->filterByDetailedUserExperience($userExperience)
            ->_if($isUserExperienceNull === true)
                ->filterByUserExperienceId(null)
            ->_endif()
            ->update($updateValues)
        ;
    }

    public static function findAllByStream(Stream $stream, $isUserExperienceNull = false)
    {
        $chatterExperience = self::create()
            ->_if($isUserExperienceNull === true)
                ->filterByUserExperienceId(null)
            ->_endif()
            ->filterByStream($stream)
            ->useChatUserQuery()
                ->orderByUsername()
            ->endUse()
            ->find();
        return $chatterExperience;
    }

    public static function findAllByStreamGroupByChatUser(Stream $stream, $isUserExperienceNull = false)
    {
        $chatterExperience = self::create()
            ->_if($isUserExperienceNull === true)
                ->filterByUserExperienceId(null)
            ->_endif()
            ->filterByStream($stream)
            ->withColumn('count(*)', 'nbChatterExperience')
            ->withColumn(ChatterExperienceTableMap::COL_STCEXP_STCHAN_ID, 'idChannel')
            ->withColumn(ChatterExperienceTableMap::COL_STCEXP_STSITE_ID, 'idSite')
            ->withColumn(ChatterExperienceTableMap::COL_STCEXP_STSTRM_ID, 'idStream')
            ->groupByChatUserId()
            ->groupByStreamId()
            ->groupByChannelId()
            ->groupBySiteId()
            ->select('ChatUserId', 'ChannelId', 'StreamId', 'SiteId', 'nbChatterExperience')
            ->find()
        ;
        return $chatterExperience;
    }

    public static function countAllByStream(Stream $stream, $isUserExperienceNull = false)
    {
        $chatterExperience = self::create()
            ->_if($isUserExperienceNull === true)
                ->filterByUserExperienceId(null)
            ->_endif()
            ->filterByStream($stream)
            ->count();
        return $chatterExperience;
    }
}

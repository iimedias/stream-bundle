<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\GameDataQuery as BaseGameDataQuery;
use IiMedias\StreamBundle\Model\GameData;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;
use \DateTime;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_game_data_stgdat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GameDataQuery extends BaseGameDataQuery
{
    public function getLastOfChannelOrCreate(Channel $channel, ApiTwitchGame $twitchGame)
    {
        $gameData = self::create()
            ->filterBySite($channel->getSite())
            ->filterByStream($channel->getStream())
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne()
        ;

        if (is_null($gameData) || $gameData->getApiTwitchGame() != $twitchGame) {
            $gameData = new GameData();
            $gameData
                ->setSite($channel->getSite())
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setApiTwitchGame($twitchGame)
                ->setAt(new DateTime())
                ->save()
            ;
        }

        return $gameData;
    }
}

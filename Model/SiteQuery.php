<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\SiteQuery as BaseSiteQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_site_stsite' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteQuery extends BaseSiteQuery
{
    public static function getOneByCode($code)
    {
        $result = self::create()
            ->filterByCode($code)
            ->findOne();
        return $result;
    }

    public static function getAll()
    {
        $sites = self::create()
            ->orderByName(Criteria::ASC)
            ->find()
        ;
        return $sites;
    }
}

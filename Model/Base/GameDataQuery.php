<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\GameData as ChildGameData;
use IiMedias\StreamBundle\Model\GameDataQuery as ChildGameDataQuery;
use IiMedias\StreamBundle\Model\Map\GameDataTableMap;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_game_data_stgdat' table.
 *
 *
 *
 * @method     ChildGameDataQuery orderById($order = Criteria::ASC) Order by the stgdat_id column
 * @method     ChildGameDataQuery orderByStreamId($order = Criteria::ASC) Order by the stgdat_ststrm_id column
 * @method     ChildGameDataQuery orderByChannelId($order = Criteria::ASC) Order by the stgdat_stchan_id column
 * @method     ChildGameDataQuery orderBySiteId($order = Criteria::ASC) Order by the stgdat_stsite_id column
 * @method     ChildGameDataQuery orderByTwitchGameId($order = Criteria::ASC) Order by the stchan_vgatga_id column
 * @method     ChildGameDataQuery orderByAt($order = Criteria::ASC) Order by the stgdat_at column
 *
 * @method     ChildGameDataQuery groupById() Group by the stgdat_id column
 * @method     ChildGameDataQuery groupByStreamId() Group by the stgdat_ststrm_id column
 * @method     ChildGameDataQuery groupByChannelId() Group by the stgdat_stchan_id column
 * @method     ChildGameDataQuery groupBySiteId() Group by the stgdat_stsite_id column
 * @method     ChildGameDataQuery groupByTwitchGameId() Group by the stchan_vgatga_id column
 * @method     ChildGameDataQuery groupByAt() Group by the stgdat_at column
 *
 * @method     ChildGameDataQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGameDataQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGameDataQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGameDataQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGameDataQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGameDataQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGameDataQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildGameDataQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildGameDataQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildGameDataQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildGameDataQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildGameDataQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildGameDataQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildGameDataQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildGameDataQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildGameDataQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildGameDataQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildGameDataQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildGameDataQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildGameDataQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildGameDataQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildGameDataQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildGameDataQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildGameDataQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildGameDataQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildGameDataQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildGameDataQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildGameDataQuery leftJoinApiTwitchGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildGameDataQuery rightJoinApiTwitchGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildGameDataQuery innerJoinApiTwitchGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiTwitchGame relation
 *
 * @method     ChildGameDataQuery joinWithApiTwitchGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiTwitchGame relation
 *
 * @method     ChildGameDataQuery leftJoinWithApiTwitchGame() Adds a LEFT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildGameDataQuery rightJoinWithApiTwitchGame() Adds a RIGHT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildGameDataQuery innerJoinWithApiTwitchGame() Adds a INNER JOIN clause and with to the query using the ApiTwitchGame relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildGameData findOne(ConnectionInterface $con = null) Return the first ChildGameData matching the query
 * @method     ChildGameData findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGameData matching the query, or a new ChildGameData object populated from the query conditions when no match is found
 *
 * @method     ChildGameData findOneById(int $stgdat_id) Return the first ChildGameData filtered by the stgdat_id column
 * @method     ChildGameData findOneByStreamId(int $stgdat_ststrm_id) Return the first ChildGameData filtered by the stgdat_ststrm_id column
 * @method     ChildGameData findOneByChannelId(int $stgdat_stchan_id) Return the first ChildGameData filtered by the stgdat_stchan_id column
 * @method     ChildGameData findOneBySiteId(int $stgdat_stsite_id) Return the first ChildGameData filtered by the stgdat_stsite_id column
 * @method     ChildGameData findOneByTwitchGameId(int $stchan_vgatga_id) Return the first ChildGameData filtered by the stchan_vgatga_id column
 * @method     ChildGameData findOneByAt(string $stgdat_at) Return the first ChildGameData filtered by the stgdat_at column *

 * @method     ChildGameData requirePk($key, ConnectionInterface $con = null) Return the ChildGameData by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOne(ConnectionInterface $con = null) Return the first ChildGameData matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGameData requireOneById(int $stgdat_id) Return the first ChildGameData filtered by the stgdat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOneByStreamId(int $stgdat_ststrm_id) Return the first ChildGameData filtered by the stgdat_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOneByChannelId(int $stgdat_stchan_id) Return the first ChildGameData filtered by the stgdat_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOneBySiteId(int $stgdat_stsite_id) Return the first ChildGameData filtered by the stgdat_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOneByTwitchGameId(int $stchan_vgatga_id) Return the first ChildGameData filtered by the stchan_vgatga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGameData requireOneByAt(string $stgdat_at) Return the first ChildGameData filtered by the stgdat_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGameData[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGameData objects based on current ModelCriteria
 * @method     ChildGameData[]|ObjectCollection findById(int $stgdat_id) Return ChildGameData objects filtered by the stgdat_id column
 * @method     ChildGameData[]|ObjectCollection findByStreamId(int $stgdat_ststrm_id) Return ChildGameData objects filtered by the stgdat_ststrm_id column
 * @method     ChildGameData[]|ObjectCollection findByChannelId(int $stgdat_stchan_id) Return ChildGameData objects filtered by the stgdat_stchan_id column
 * @method     ChildGameData[]|ObjectCollection findBySiteId(int $stgdat_stsite_id) Return ChildGameData objects filtered by the stgdat_stsite_id column
 * @method     ChildGameData[]|ObjectCollection findByTwitchGameId(int $stchan_vgatga_id) Return ChildGameData objects filtered by the stchan_vgatga_id column
 * @method     ChildGameData[]|ObjectCollection findByAt(string $stgdat_at) Return ChildGameData objects filtered by the stgdat_at column
 * @method     ChildGameData[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GameDataQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\GameDataQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\GameData', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGameDataQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGameDataQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGameDataQuery) {
            return $criteria;
        }
        $query = new ChildGameDataQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGameData|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GameDataTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GameDataTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGameData A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stgdat_id, stgdat_ststrm_id, stgdat_stchan_id, stgdat_stsite_id, stchan_vgatga_id, stgdat_at FROM stream_game_data_stgdat WHERE stgdat_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGameData $obj */
            $obj = new ChildGameData();
            $obj->hydrate($row);
            GameDataTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGameData|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stgdat_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stgdat_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stgdat_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stgdat_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stgdat_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stgdat_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stgdat_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stgdat_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stgdat_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stgdat_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stgdat_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stgdat_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stgdat_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stgdat_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stgdat_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stgdat_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stchan_vgatga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTwitchGameId(1234); // WHERE stchan_vgatga_id = 1234
     * $query->filterByTwitchGameId(array(12, 34)); // WHERE stchan_vgatga_id IN (12, 34)
     * $query->filterByTwitchGameId(array('min' => 12)); // WHERE stchan_vgatga_id > 12
     * </code>
     *
     * @see       filterByApiTwitchGame()
     *
     * @param     mixed $twitchGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByTwitchGameId($twitchGameId = null, $comparison = null)
    {
        if (is_array($twitchGameId)) {
            $useMinMax = false;
            if (isset($twitchGameId['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($twitchGameId['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId, $comparison);
    }

    /**
     * Filter the query on the stgdat_at column
     *
     * Example usage:
     * <code>
     * $query->filterByAt('2011-03-14'); // WHERE stgdat_at = '2011-03-14'
     * $query->filterByAt('now'); // WHERE stgdat_at = '2011-03-14'
     * $query->filterByAt(array('max' => 'yesterday')); // WHERE stgdat_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $at The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByAt($at = null, $comparison = null)
    {
        if (is_array($at)) {
            $useMinMax = false;
            if (isset($at['min'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_AT, $at['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($at['max'])) {
                $this->addUsingAlias(GameDataTableMap::COL_STGDAT_AT, $at['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameDataTableMap::COL_STGDAT_AT, $at, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGameDataQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GameDataTableMap::COL_STGDAT_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiTwitchGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiTwitchGame|ObjectCollection $apiTwitchGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGameDataQuery The current query, for fluid interface
     */
    public function filterByApiTwitchGame($apiTwitchGame, $comparison = null)
    {
        if ($apiTwitchGame instanceof \IiMedias\VideoGamesBundle\Model\ApiTwitchGame) {
            return $this
                ->addUsingAlias(GameDataTableMap::COL_STCHAN_VGATGA_ID, $apiTwitchGame->getId(), $comparison);
        } elseif ($apiTwitchGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GameDataTableMap::COL_STCHAN_VGATGA_ID, $apiTwitchGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiTwitchGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiTwitchGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiTwitchGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function joinApiTwitchGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiTwitchGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiTwitchGame');
        }

        return $this;
    }

    /**
     * Use the ApiTwitchGame relation ApiTwitchGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery A secondary query class using the current class as primary query
     */
    public function useApiTwitchGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiTwitchGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiTwitchGame', '\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGameData $gameData Object to remove from the list of results
     *
     * @return $this|ChildGameDataQuery The current query, for fluid interface
     */
    public function prune($gameData = null)
    {
        if ($gameData) {
            $this->addUsingAlias(GameDataTableMap::COL_STGDAT_ID, $gameData->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_game_data_stgdat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameDataTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GameDataTableMap::clearInstancePool();
            GameDataTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameDataTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GameDataTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GameDataTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GameDataTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GameDataQuery

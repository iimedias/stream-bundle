<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\ChatUser as ChildChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery as ChildChatUserQuery;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_chat_user_stcusr' table.
 *
 *
 *
 * @method     ChildChatUserQuery orderById($order = Criteria::ASC) Order by the stcusr_id column
 * @method     ChildChatUserQuery orderByParentId($order = Criteria::ASC) Order by the stcusr_parent_id column
 * @method     ChildChatUserQuery orderBySiteId($order = Criteria::ASC) Order by the stcusr_stsite_id column
 * @method     ChildChatUserQuery orderByUsername($order = Criteria::ASC) Order by the stcusr_username column
 * @method     ChildChatUserQuery orderByDisplayName($order = Criteria::ASC) Order by the stcusr_display_name column
 * @method     ChildChatUserQuery orderByLogoUrl($order = Criteria::ASC) Order by the stcusr_logo_url column
 * @method     ChildChatUserQuery orderByTimeoutLevel($order = Criteria::ASC) Order by the stcusr_timeout_level column
 * @method     ChildChatUserQuery orderByLastTimeoutAt($order = Criteria::ASC) Order by the stcusr_last_timeout_at column
 * @method     ChildChatUserQuery orderByRegisteredAt($order = Criteria::ASC) Order by the stcusr_registered_at column
 * @method     ChildChatUserQuery orderByIsDeletedAccount($order = Criteria::ASC) Order by the stcusr_is_deleted_account column
 * @method     ChildChatUserQuery orderByCanRescan($order = Criteria::ASC) Order by the stcusr_can_rescan column
 * @method     ChildChatUserQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the stcusr_created_by_user_id column
 * @method     ChildChatUserQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the stcusr_updated_by_user_id column
 * @method     ChildChatUserQuery orderByCreatedAt($order = Criteria::ASC) Order by the stcusr_created_at column
 * @method     ChildChatUserQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stcusr_updated_at column
 *
 * @method     ChildChatUserQuery groupById() Group by the stcusr_id column
 * @method     ChildChatUserQuery groupByParentId() Group by the stcusr_parent_id column
 * @method     ChildChatUserQuery groupBySiteId() Group by the stcusr_stsite_id column
 * @method     ChildChatUserQuery groupByUsername() Group by the stcusr_username column
 * @method     ChildChatUserQuery groupByDisplayName() Group by the stcusr_display_name column
 * @method     ChildChatUserQuery groupByLogoUrl() Group by the stcusr_logo_url column
 * @method     ChildChatUserQuery groupByTimeoutLevel() Group by the stcusr_timeout_level column
 * @method     ChildChatUserQuery groupByLastTimeoutAt() Group by the stcusr_last_timeout_at column
 * @method     ChildChatUserQuery groupByRegisteredAt() Group by the stcusr_registered_at column
 * @method     ChildChatUserQuery groupByIsDeletedAccount() Group by the stcusr_is_deleted_account column
 * @method     ChildChatUserQuery groupByCanRescan() Group by the stcusr_can_rescan column
 * @method     ChildChatUserQuery groupByCreatedByUserId() Group by the stcusr_created_by_user_id column
 * @method     ChildChatUserQuery groupByUpdatedByUserId() Group by the stcusr_updated_by_user_id column
 * @method     ChildChatUserQuery groupByCreatedAt() Group by the stcusr_created_at column
 * @method     ChildChatUserQuery groupByUpdatedAt() Group by the stcusr_updated_at column
 *
 * @method     ChildChatUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatUserQuery leftJoinParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the Parent relation
 * @method     ChildChatUserQuery rightJoinParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Parent relation
 * @method     ChildChatUserQuery innerJoinParent($relationAlias = null) Adds a INNER JOIN clause to the query using the Parent relation
 *
 * @method     ChildChatUserQuery joinWithParent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Parent relation
 *
 * @method     ChildChatUserQuery leftJoinWithParent() Adds a LEFT JOIN clause and with to the query using the Parent relation
 * @method     ChildChatUserQuery rightJoinWithParent() Adds a RIGHT JOIN clause and with to the query using the Parent relation
 * @method     ChildChatUserQuery innerJoinWithParent() Adds a INNER JOIN clause and with to the query using the Parent relation
 *
 * @method     ChildChatUserQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildChatUserQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildChatUserQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildChatUserQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildChatUserQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildChatUserQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildChatUserQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildChatUserQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildChatUserQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildChatUserQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildChatUserQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildChatUserQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildChatUserQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildChatUserQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildChatUserQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildChatUserQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildChatUserQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildChatUserQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildChatUserQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildChatUserQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildChatUserQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildChatUserQuery leftJoinChild($relationAlias = null) Adds a LEFT JOIN clause to the query using the Child relation
 * @method     ChildChatUserQuery rightJoinChild($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Child relation
 * @method     ChildChatUserQuery innerJoinChild($relationAlias = null) Adds a INNER JOIN clause to the query using the Child relation
 *
 * @method     ChildChatUserQuery joinWithChild($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Child relation
 *
 * @method     ChildChatUserQuery leftJoinWithChild() Adds a LEFT JOIN clause and with to the query using the Child relation
 * @method     ChildChatUserQuery rightJoinWithChild() Adds a RIGHT JOIN clause and with to the query using the Child relation
 * @method     ChildChatUserQuery innerJoinWithChild() Adds a INNER JOIN clause and with to the query using the Child relation
 *
 * @method     ChildChatUserQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildChatUserQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildChatUserQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildChatUserQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildChatUserQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildChatUserQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     ChildChatUserQuery leftJoinDeepBotImportExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildChatUserQuery rightJoinDeepBotImportExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildChatUserQuery innerJoinDeepBotImportExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChatUserQuery joinWithDeepBotImportExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithDeepBotImportExperience() Adds a LEFT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildChatUserQuery rightJoinWithDeepBotImportExperience() Adds a RIGHT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildChatUserQuery innerJoinWithDeepBotImportExperience() Adds a INNER JOIN clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChatUserQuery leftJoinMessageExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageExperience relation
 * @method     ChildChatUserQuery rightJoinMessageExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageExperience relation
 * @method     ChildChatUserQuery innerJoinMessageExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageExperience relation
 *
 * @method     ChildChatUserQuery joinWithMessageExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithMessageExperience() Adds a LEFT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildChatUserQuery rightJoinWithMessageExperience() Adds a RIGHT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildChatUserQuery innerJoinWithMessageExperience() Adds a INNER JOIN clause and with to the query using the MessageExperience relation
 *
 * @method     ChildChatUserQuery leftJoinChatterExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildChatUserQuery rightJoinChatterExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildChatUserQuery innerJoinChatterExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatterExperience relation
 *
 * @method     ChildChatUserQuery joinWithChatterExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithChatterExperience() Adds a LEFT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildChatUserQuery rightJoinWithChatterExperience() Adds a RIGHT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildChatUserQuery innerJoinWithChatterExperience() Adds a INNER JOIN clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildChatUserQuery leftJoinFollowExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowExperience relation
 * @method     ChildChatUserQuery rightJoinFollowExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowExperience relation
 * @method     ChildChatUserQuery innerJoinFollowExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowExperience relation
 *
 * @method     ChildChatUserQuery joinWithFollowExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithFollowExperience() Adds a LEFT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildChatUserQuery rightJoinWithFollowExperience() Adds a RIGHT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildChatUserQuery innerJoinWithFollowExperience() Adds a INNER JOIN clause and with to the query using the FollowExperience relation
 *
 * @method     ChildChatUserQuery leftJoinHostExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the HostExperience relation
 * @method     ChildChatUserQuery rightJoinHostExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HostExperience relation
 * @method     ChildChatUserQuery innerJoinHostExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the HostExperience relation
 *
 * @method     ChildChatUserQuery joinWithHostExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HostExperience relation
 *
 * @method     ChildChatUserQuery leftJoinWithHostExperience() Adds a LEFT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildChatUserQuery rightJoinWithHostExperience() Adds a RIGHT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildChatUserQuery innerJoinWithHostExperience() Adds a INNER JOIN clause and with to the query using the HostExperience relation
 *
 * @method     ChildChatUserQuery leftJoinExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Experience relation
 * @method     ChildChatUserQuery rightJoinExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Experience relation
 * @method     ChildChatUserQuery innerJoinExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the Experience relation
 *
 * @method     ChildChatUserQuery joinWithExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Experience relation
 *
 * @method     ChildChatUserQuery leftJoinWithExperience() Adds a LEFT JOIN clause and with to the query using the Experience relation
 * @method     ChildChatUserQuery rightJoinWithExperience() Adds a RIGHT JOIN clause and with to the query using the Experience relation
 * @method     ChildChatUserQuery innerJoinWithExperience() Adds a INNER JOIN clause and with to the query using the Experience relation
 *
 * @method     \IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery|\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery|\IiMedias\StreamBundle\Model\MessageExperienceQuery|\IiMedias\StreamBundle\Model\ChatterExperienceQuery|\IiMedias\StreamBundle\Model\FollowExperienceQuery|\IiMedias\StreamBundle\Model\HostExperienceQuery|\IiMedias\StreamBundle\Model\ExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChatUser findOne(ConnectionInterface $con = null) Return the first ChildChatUser matching the query
 * @method     ChildChatUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatUser matching the query, or a new ChildChatUser object populated from the query conditions when no match is found
 *
 * @method     ChildChatUser findOneById(int $stcusr_id) Return the first ChildChatUser filtered by the stcusr_id column
 * @method     ChildChatUser findOneByParentId(int $stcusr_parent_id) Return the first ChildChatUser filtered by the stcusr_parent_id column
 * @method     ChildChatUser findOneBySiteId(int $stcusr_stsite_id) Return the first ChildChatUser filtered by the stcusr_stsite_id column
 * @method     ChildChatUser findOneByUsername(string $stcusr_username) Return the first ChildChatUser filtered by the stcusr_username column
 * @method     ChildChatUser findOneByDisplayName(string $stcusr_display_name) Return the first ChildChatUser filtered by the stcusr_display_name column
 * @method     ChildChatUser findOneByLogoUrl(string $stcusr_logo_url) Return the first ChildChatUser filtered by the stcusr_logo_url column
 * @method     ChildChatUser findOneByTimeoutLevel(int $stcusr_timeout_level) Return the first ChildChatUser filtered by the stcusr_timeout_level column
 * @method     ChildChatUser findOneByLastTimeoutAt(string $stcusr_last_timeout_at) Return the first ChildChatUser filtered by the stcusr_last_timeout_at column
 * @method     ChildChatUser findOneByRegisteredAt(string $stcusr_registered_at) Return the first ChildChatUser filtered by the stcusr_registered_at column
 * @method     ChildChatUser findOneByIsDeletedAccount(boolean $stcusr_is_deleted_account) Return the first ChildChatUser filtered by the stcusr_is_deleted_account column
 * @method     ChildChatUser findOneByCanRescan(boolean $stcusr_can_rescan) Return the first ChildChatUser filtered by the stcusr_can_rescan column
 * @method     ChildChatUser findOneByCreatedByUserId(int $stcusr_created_by_user_id) Return the first ChildChatUser filtered by the stcusr_created_by_user_id column
 * @method     ChildChatUser findOneByUpdatedByUserId(int $stcusr_updated_by_user_id) Return the first ChildChatUser filtered by the stcusr_updated_by_user_id column
 * @method     ChildChatUser findOneByCreatedAt(string $stcusr_created_at) Return the first ChildChatUser filtered by the stcusr_created_at column
 * @method     ChildChatUser findOneByUpdatedAt(string $stcusr_updated_at) Return the first ChildChatUser filtered by the stcusr_updated_at column *

 * @method     ChildChatUser requirePk($key, ConnectionInterface $con = null) Return the ChildChatUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOne(ConnectionInterface $con = null) Return the first ChildChatUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatUser requireOneById(int $stcusr_id) Return the first ChildChatUser filtered by the stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByParentId(int $stcusr_parent_id) Return the first ChildChatUser filtered by the stcusr_parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneBySiteId(int $stcusr_stsite_id) Return the first ChildChatUser filtered by the stcusr_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByUsername(string $stcusr_username) Return the first ChildChatUser filtered by the stcusr_username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByDisplayName(string $stcusr_display_name) Return the first ChildChatUser filtered by the stcusr_display_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByLogoUrl(string $stcusr_logo_url) Return the first ChildChatUser filtered by the stcusr_logo_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByTimeoutLevel(int $stcusr_timeout_level) Return the first ChildChatUser filtered by the stcusr_timeout_level column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByLastTimeoutAt(string $stcusr_last_timeout_at) Return the first ChildChatUser filtered by the stcusr_last_timeout_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByRegisteredAt(string $stcusr_registered_at) Return the first ChildChatUser filtered by the stcusr_registered_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByIsDeletedAccount(boolean $stcusr_is_deleted_account) Return the first ChildChatUser filtered by the stcusr_is_deleted_account column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByCanRescan(boolean $stcusr_can_rescan) Return the first ChildChatUser filtered by the stcusr_can_rescan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByCreatedByUserId(int $stcusr_created_by_user_id) Return the first ChildChatUser filtered by the stcusr_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByUpdatedByUserId(int $stcusr_updated_by_user_id) Return the first ChildChatUser filtered by the stcusr_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByCreatedAt(string $stcusr_created_at) Return the first ChildChatUser filtered by the stcusr_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByUpdatedAt(string $stcusr_updated_at) Return the first ChildChatUser filtered by the stcusr_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatUser objects based on current ModelCriteria
 * @method     ChildChatUser[]|ObjectCollection findById(int $stcusr_id) Return ChildChatUser objects filtered by the stcusr_id column
 * @method     ChildChatUser[]|ObjectCollection findByParentId(int $stcusr_parent_id) Return ChildChatUser objects filtered by the stcusr_parent_id column
 * @method     ChildChatUser[]|ObjectCollection findBySiteId(int $stcusr_stsite_id) Return ChildChatUser objects filtered by the stcusr_stsite_id column
 * @method     ChildChatUser[]|ObjectCollection findByUsername(string $stcusr_username) Return ChildChatUser objects filtered by the stcusr_username column
 * @method     ChildChatUser[]|ObjectCollection findByDisplayName(string $stcusr_display_name) Return ChildChatUser objects filtered by the stcusr_display_name column
 * @method     ChildChatUser[]|ObjectCollection findByLogoUrl(string $stcusr_logo_url) Return ChildChatUser objects filtered by the stcusr_logo_url column
 * @method     ChildChatUser[]|ObjectCollection findByTimeoutLevel(int $stcusr_timeout_level) Return ChildChatUser objects filtered by the stcusr_timeout_level column
 * @method     ChildChatUser[]|ObjectCollection findByLastTimeoutAt(string $stcusr_last_timeout_at) Return ChildChatUser objects filtered by the stcusr_last_timeout_at column
 * @method     ChildChatUser[]|ObjectCollection findByRegisteredAt(string $stcusr_registered_at) Return ChildChatUser objects filtered by the stcusr_registered_at column
 * @method     ChildChatUser[]|ObjectCollection findByIsDeletedAccount(boolean $stcusr_is_deleted_account) Return ChildChatUser objects filtered by the stcusr_is_deleted_account column
 * @method     ChildChatUser[]|ObjectCollection findByCanRescan(boolean $stcusr_can_rescan) Return ChildChatUser objects filtered by the stcusr_can_rescan column
 * @method     ChildChatUser[]|ObjectCollection findByCreatedByUserId(int $stcusr_created_by_user_id) Return ChildChatUser objects filtered by the stcusr_created_by_user_id column
 * @method     ChildChatUser[]|ObjectCollection findByUpdatedByUserId(int $stcusr_updated_by_user_id) Return ChildChatUser objects filtered by the stcusr_updated_by_user_id column
 * @method     ChildChatUser[]|ObjectCollection findByCreatedAt(string $stcusr_created_at) Return ChildChatUser objects filtered by the stcusr_created_at column
 * @method     ChildChatUser[]|ObjectCollection findByUpdatedAt(string $stcusr_updated_at) Return ChildChatUser objects filtered by the stcusr_updated_at column
 * @method     ChildChatUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatUserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ChatUserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\ChatUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatUserQuery) {
            return $criteria;
        }
        $query = new ChildChatUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatUserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChatUserTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stcusr_id, stcusr_parent_id, stcusr_stsite_id, stcusr_username, stcusr_display_name, stcusr_logo_url, stcusr_timeout_level, stcusr_last_timeout_at, stcusr_registered_at, stcusr_is_deleted_account, stcusr_can_rescan, stcusr_created_by_user_id, stcusr_updated_by_user_id, stcusr_created_at, stcusr_updated_at FROM stream_chat_user_stcusr WHERE stcusr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChatUser $obj */
            $obj = new ChildChatUser();
            $obj->hydrate($row);
            ChatUserTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChatUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stcusr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stcusr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stcusr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stcusr_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE stcusr_parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE stcusr_parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE stcusr_parent_id > 12
     * </code>
     *
     * @see       filterByParent()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the stcusr_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stcusr_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stcusr_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stcusr_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stcusr_username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE stcusr_username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE stcusr_username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the stcusr_display_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayName('fooValue');   // WHERE stcusr_display_name = 'fooValue'
     * $query->filterByDisplayName('%fooValue%', Criteria::LIKE); // WHERE stcusr_display_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $displayName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByDisplayName($displayName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($displayName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_DISPLAY_NAME, $displayName, $comparison);
    }

    /**
     * Filter the query on the stcusr_logo_url column
     *
     * Example usage:
     * <code>
     * $query->filterByLogoUrl('fooValue');   // WHERE stcusr_logo_url = 'fooValue'
     * $query->filterByLogoUrl('%fooValue%', Criteria::LIKE); // WHERE stcusr_logo_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logoUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByLogoUrl($logoUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logoUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_LOGO_URL, $logoUrl, $comparison);
    }

    /**
     * Filter the query on the stcusr_timeout_level column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeoutLevel(1234); // WHERE stcusr_timeout_level = 1234
     * $query->filterByTimeoutLevel(array(12, 34)); // WHERE stcusr_timeout_level IN (12, 34)
     * $query->filterByTimeoutLevel(array('min' => 12)); // WHERE stcusr_timeout_level > 12
     * </code>
     *
     * @param     mixed $timeoutLevel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByTimeoutLevel($timeoutLevel = null, $comparison = null)
    {
        if (is_array($timeoutLevel)) {
            $useMinMax = false;
            if (isset($timeoutLevel['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL, $timeoutLevel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timeoutLevel['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL, $timeoutLevel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL, $timeoutLevel, $comparison);
    }

    /**
     * Filter the query on the stcusr_last_timeout_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLastTimeoutAt('2011-03-14'); // WHERE stcusr_last_timeout_at = '2011-03-14'
     * $query->filterByLastTimeoutAt('now'); // WHERE stcusr_last_timeout_at = '2011-03-14'
     * $query->filterByLastTimeoutAt(array('max' => 'yesterday')); // WHERE stcusr_last_timeout_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastTimeoutAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByLastTimeoutAt($lastTimeoutAt = null, $comparison = null)
    {
        if (is_array($lastTimeoutAt)) {
            $useMinMax = false;
            if (isset($lastTimeoutAt['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT, $lastTimeoutAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastTimeoutAt['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT, $lastTimeoutAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT, $lastTimeoutAt, $comparison);
    }

    /**
     * Filter the query on the stcusr_registered_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRegisteredAt('2011-03-14'); // WHERE stcusr_registered_at = '2011-03-14'
     * $query->filterByRegisteredAt('now'); // WHERE stcusr_registered_at = '2011-03-14'
     * $query->filterByRegisteredAt(array('max' => 'yesterday')); // WHERE stcusr_registered_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $registeredAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByRegisteredAt($registeredAt = null, $comparison = null)
    {
        if (is_array($registeredAt)) {
            $useMinMax = false;
            if (isset($registeredAt['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_REGISTERED_AT, $registeredAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($registeredAt['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_REGISTERED_AT, $registeredAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_REGISTERED_AT, $registeredAt, $comparison);
    }

    /**
     * Filter the query on the stcusr_is_deleted_account column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeletedAccount(true); // WHERE stcusr_is_deleted_account = true
     * $query->filterByIsDeletedAccount('yes'); // WHERE stcusr_is_deleted_account = true
     * </code>
     *
     * @param     boolean|string $isDeletedAccount The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByIsDeletedAccount($isDeletedAccount = null, $comparison = null)
    {
        if (is_string($isDeletedAccount)) {
            $isDeletedAccount = in_array(strtolower($isDeletedAccount), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT, $isDeletedAccount, $comparison);
    }

    /**
     * Filter the query on the stcusr_can_rescan column
     *
     * Example usage:
     * <code>
     * $query->filterByCanRescan(true); // WHERE stcusr_can_rescan = true
     * $query->filterByCanRescan('yes'); // WHERE stcusr_can_rescan = true
     * </code>
     *
     * @param     boolean|string $canRescan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByCanRescan($canRescan = null, $comparison = null)
    {
        if (is_string($canRescan)) {
            $canRescan = in_array(strtolower($canRescan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CAN_RESCAN, $canRescan, $comparison);
    }

    /**
     * Filter the query on the stcusr_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE stcusr_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE stcusr_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE stcusr_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the stcusr_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE stcusr_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE stcusr_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE stcusr_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the stcusr_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stcusr_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stcusr_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stcusr_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stcusr_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stcusr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stcusr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stcusr_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByParent($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_PARENT_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_PARENT_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByParent() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Parent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Parent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Parent');
        }

        return $this;
    }

    /**
     * Use the Parent relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Parent', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByChild($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $chatUser->getParentId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            return $this
                ->useChildQuery()
                ->filterByPrimaryKeys($chatUser->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChild() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Child relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinChild($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Child');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Child');
        }

        return $this;
    }

    /**
     * Use the Child relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChildQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChild($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Child', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $userExperience->getChatUserId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\DeepBotImportExperience object
     *
     * @param \IiMedias\StreamBundle\Model\DeepBotImportExperience|ObjectCollection $deepBotImportExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByDeepBotImportExperience($deepBotImportExperience, $comparison = null)
    {
        if ($deepBotImportExperience instanceof \IiMedias\StreamBundle\Model\DeepBotImportExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $deepBotImportExperience->getChatUserId(), $comparison);
        } elseif ($deepBotImportExperience instanceof ObjectCollection) {
            return $this
                ->useDeepBotImportExperienceQuery()
                ->filterByPrimaryKeys($deepBotImportExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDeepBotImportExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\DeepBotImportExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeepBotImportExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinDeepBotImportExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeepBotImportExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeepBotImportExperience');
        }

        return $this;
    }

    /**
     * Use the DeepBotImportExperience relation DeepBotImportExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery A secondary query class using the current class as primary query
     */
    public function useDeepBotImportExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDeepBotImportExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeepBotImportExperience', '\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\MessageExperience object
     *
     * @param \IiMedias\StreamBundle\Model\MessageExperience|ObjectCollection $messageExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByMessageExperience($messageExperience, $comparison = null)
    {
        if ($messageExperience instanceof \IiMedias\StreamBundle\Model\MessageExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $messageExperience->getChatUserId(), $comparison);
        } elseif ($messageExperience instanceof ObjectCollection) {
            return $this
                ->useMessageExperienceQuery()
                ->filterByPrimaryKeys($messageExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\MessageExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinMessageExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageExperience');
        }

        return $this;
    }

    /**
     * Use the MessageExperience relation MessageExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\MessageExperienceQuery A secondary query class using the current class as primary query
     */
    public function useMessageExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageExperience', '\IiMedias\StreamBundle\Model\MessageExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatterExperience object
     *
     * @param \IiMedias\StreamBundle\Model\ChatterExperience|ObjectCollection $chatterExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByChatterExperience($chatterExperience, $comparison = null)
    {
        if ($chatterExperience instanceof \IiMedias\StreamBundle\Model\ChatterExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $chatterExperience->getChatUserId(), $comparison);
        } elseif ($chatterExperience instanceof ObjectCollection) {
            return $this
                ->useChatterExperienceQuery()
                ->filterByPrimaryKeys($chatterExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatterExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatterExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatterExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinChatterExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatterExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatterExperience');
        }

        return $this;
    }

    /**
     * Use the ChatterExperience relation ChatterExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatterExperienceQuery A secondary query class using the current class as primary query
     */
    public function useChatterExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatterExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatterExperience', '\IiMedias\StreamBundle\Model\ChatterExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowExperience object
     *
     * @param \IiMedias\StreamBundle\Model\FollowExperience|ObjectCollection $followExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByFollowExperience($followExperience, $comparison = null)
    {
        if ($followExperience instanceof \IiMedias\StreamBundle\Model\FollowExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $followExperience->getChatUserId(), $comparison);
        } elseif ($followExperience instanceof ObjectCollection) {
            return $this
                ->useFollowExperienceQuery()
                ->filterByPrimaryKeys($followExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinFollowExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowExperience');
        }

        return $this;
    }

    /**
     * Use the FollowExperience relation FollowExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowExperienceQuery A secondary query class using the current class as primary query
     */
    public function useFollowExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowExperience', '\IiMedias\StreamBundle\Model\FollowExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\HostExperience object
     *
     * @param \IiMedias\StreamBundle\Model\HostExperience|ObjectCollection $hostExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByHostExperience($hostExperience, $comparison = null)
    {
        if ($hostExperience instanceof \IiMedias\StreamBundle\Model\HostExperience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $hostExperience->getChatUserId(), $comparison);
        } elseif ($hostExperience instanceof ObjectCollection) {
            return $this
                ->useHostExperienceQuery()
                ->filterByPrimaryKeys($hostExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHostExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\HostExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HostExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinHostExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HostExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HostExperience');
        }

        return $this;
    }

    /**
     * Use the HostExperience relation HostExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\HostExperienceQuery A secondary query class using the current class as primary query
     */
    public function useHostExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinHostExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HostExperience', '\IiMedias\StreamBundle\Model\HostExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Experience object
     *
     * @param \IiMedias\StreamBundle\Model\Experience|ObjectCollection $experience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByExperience($experience, $comparison = null)
    {
        if ($experience instanceof \IiMedias\StreamBundle\Model\Experience) {
            return $this
                ->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $experience->getChatUserId(), $comparison);
        } elseif ($experience instanceof ObjectCollection) {
            return $this
                ->useExperienceQuery()
                ->filterByPrimaryKeys($experience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\Experience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Experience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function joinExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Experience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Experience');
        }

        return $this;
    }

    /**
     * Use the Experience relation Experience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ExperienceQuery A secondary query class using the current class as primary query
     */
    public function useExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Experience', '\IiMedias\StreamBundle\Model\ExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatUser $chatUser Object to remove from the list of results
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function prune($chatUser = null)
    {
        if ($chatUser) {
            $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_ID, $chatUser->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_chat_user_stcusr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatUserTableMap::clearInstancePool();
            ChatUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatUserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatUserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChatUserTableMap::COL_STCUSR_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChatUserTableMap::COL_STCUSR_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChatUserTableMap::COL_STCUSR_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ChatUserTableMap::COL_STCUSR_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChatUserTableMap::COL_STCUSR_CREATED_AT);
    }

} // ChatUserQuery

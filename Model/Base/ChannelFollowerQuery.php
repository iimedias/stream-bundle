<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\ChannelFollower as ChildChannelFollower;
use IiMedias\StreamBundle\Model\ChannelFollowerQuery as ChildChannelFollowerQuery;
use IiMedias\StreamBundle\Model\Map\ChannelFollowerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_channel_follower_stchfo' table.
 *
 *
 *
 * @method     ChildChannelFollowerQuery orderById($order = Criteria::ASC) Order by the stchfo_id column
 * @method     ChildChannelFollowerQuery orderByStreamId($order = Criteria::ASC) Order by the stchfo_ststrm_id column
 * @method     ChildChannelFollowerQuery orderByChannelId($order = Criteria::ASC) Order by the stchfo_stchan_id column
 * @method     ChildChannelFollowerQuery orderBySiteId($order = Criteria::ASC) Order by the stchfo_stsite_id column
 * @method     ChildChannelFollowerQuery orderByChatUserId($order = Criteria::ASC) Order by the stchfo_stcusr_id column
 * @method     ChildChannelFollowerQuery orderByIsFollowing($order = Criteria::ASC) Order by the stchfo_is_following column
 * @method     ChildChannelFollowerQuery orderByFollowedAt($order = Criteria::ASC) Order by the stchfo_followed_at column
 * @method     ChildChannelFollowerQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the stchfo_created_by_user_id column
 * @method     ChildChannelFollowerQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the stchfo_updated_by_user_id column
 * @method     ChildChannelFollowerQuery orderByCreatedAt($order = Criteria::ASC) Order by the stchfo_created_at column
 * @method     ChildChannelFollowerQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stchfo_updated_at column
 *
 * @method     ChildChannelFollowerQuery groupById() Group by the stchfo_id column
 * @method     ChildChannelFollowerQuery groupByStreamId() Group by the stchfo_ststrm_id column
 * @method     ChildChannelFollowerQuery groupByChannelId() Group by the stchfo_stchan_id column
 * @method     ChildChannelFollowerQuery groupBySiteId() Group by the stchfo_stsite_id column
 * @method     ChildChannelFollowerQuery groupByChatUserId() Group by the stchfo_stcusr_id column
 * @method     ChildChannelFollowerQuery groupByIsFollowing() Group by the stchfo_is_following column
 * @method     ChildChannelFollowerQuery groupByFollowedAt() Group by the stchfo_followed_at column
 * @method     ChildChannelFollowerQuery groupByCreatedByUserId() Group by the stchfo_created_by_user_id column
 * @method     ChildChannelFollowerQuery groupByUpdatedByUserId() Group by the stchfo_updated_by_user_id column
 * @method     ChildChannelFollowerQuery groupByCreatedAt() Group by the stchfo_created_at column
 * @method     ChildChannelFollowerQuery groupByUpdatedAt() Group by the stchfo_updated_at column
 *
 * @method     ChildChannelFollowerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChannelFollowerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChannelFollowerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChannelFollowerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChannelFollowerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChannelFollowerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChannelFollowerQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildChannelFollowerQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildChannelFollowerQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildChannelFollowerQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildChannelFollowerQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildChannelFollowerQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildChannelFollowerQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildChannelFollowerQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildChannelFollowerQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildChannelFollowerQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildChannelFollowerQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildChannelFollowerQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildChannelFollowerQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildChannelFollowerQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildChannelFollowerQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildChannelFollowerQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildChannelFollowerQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildChannelFollowerQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildChannelFollowerQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildChannelFollowerQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildChannelFollowerQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildChannelFollowerQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildChannelFollowerQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildChannelFollowerQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildChannelFollowerQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildChannelFollowerQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChannelFollowerQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChannelFollowerQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChannelFollower findOne(ConnectionInterface $con = null) Return the first ChildChannelFollower matching the query
 * @method     ChildChannelFollower findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChannelFollower matching the query, or a new ChildChannelFollower object populated from the query conditions when no match is found
 *
 * @method     ChildChannelFollower findOneById(int $stchfo_id) Return the first ChildChannelFollower filtered by the stchfo_id column
 * @method     ChildChannelFollower findOneByStreamId(int $stchfo_ststrm_id) Return the first ChildChannelFollower filtered by the stchfo_ststrm_id column
 * @method     ChildChannelFollower findOneByChannelId(int $stchfo_stchan_id) Return the first ChildChannelFollower filtered by the stchfo_stchan_id column
 * @method     ChildChannelFollower findOneBySiteId(int $stchfo_stsite_id) Return the first ChildChannelFollower filtered by the stchfo_stsite_id column
 * @method     ChildChannelFollower findOneByChatUserId(int $stchfo_stcusr_id) Return the first ChildChannelFollower filtered by the stchfo_stcusr_id column
 * @method     ChildChannelFollower findOneByIsFollowing(boolean $stchfo_is_following) Return the first ChildChannelFollower filtered by the stchfo_is_following column
 * @method     ChildChannelFollower findOneByFollowedAt(string $stchfo_followed_at) Return the first ChildChannelFollower filtered by the stchfo_followed_at column
 * @method     ChildChannelFollower findOneByCreatedByUserId(int $stchfo_created_by_user_id) Return the first ChildChannelFollower filtered by the stchfo_created_by_user_id column
 * @method     ChildChannelFollower findOneByUpdatedByUserId(int $stchfo_updated_by_user_id) Return the first ChildChannelFollower filtered by the stchfo_updated_by_user_id column
 * @method     ChildChannelFollower findOneByCreatedAt(string $stchfo_created_at) Return the first ChildChannelFollower filtered by the stchfo_created_at column
 * @method     ChildChannelFollower findOneByUpdatedAt(string $stchfo_updated_at) Return the first ChildChannelFollower filtered by the stchfo_updated_at column *

 * @method     ChildChannelFollower requirePk($key, ConnectionInterface $con = null) Return the ChildChannelFollower by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOne(ConnectionInterface $con = null) Return the first ChildChannelFollower matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChannelFollower requireOneById(int $stchfo_id) Return the first ChildChannelFollower filtered by the stchfo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByStreamId(int $stchfo_ststrm_id) Return the first ChildChannelFollower filtered by the stchfo_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByChannelId(int $stchfo_stchan_id) Return the first ChildChannelFollower filtered by the stchfo_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneBySiteId(int $stchfo_stsite_id) Return the first ChildChannelFollower filtered by the stchfo_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByChatUserId(int $stchfo_stcusr_id) Return the first ChildChannelFollower filtered by the stchfo_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByIsFollowing(boolean $stchfo_is_following) Return the first ChildChannelFollower filtered by the stchfo_is_following column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByFollowedAt(string $stchfo_followed_at) Return the first ChildChannelFollower filtered by the stchfo_followed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByCreatedByUserId(int $stchfo_created_by_user_id) Return the first ChildChannelFollower filtered by the stchfo_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByUpdatedByUserId(int $stchfo_updated_by_user_id) Return the first ChildChannelFollower filtered by the stchfo_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByCreatedAt(string $stchfo_created_at) Return the first ChildChannelFollower filtered by the stchfo_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannelFollower requireOneByUpdatedAt(string $stchfo_updated_at) Return the first ChildChannelFollower filtered by the stchfo_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChannelFollower[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChannelFollower objects based on current ModelCriteria
 * @method     ChildChannelFollower[]|ObjectCollection findById(int $stchfo_id) Return ChildChannelFollower objects filtered by the stchfo_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByStreamId(int $stchfo_ststrm_id) Return ChildChannelFollower objects filtered by the stchfo_ststrm_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByChannelId(int $stchfo_stchan_id) Return ChildChannelFollower objects filtered by the stchfo_stchan_id column
 * @method     ChildChannelFollower[]|ObjectCollection findBySiteId(int $stchfo_stsite_id) Return ChildChannelFollower objects filtered by the stchfo_stsite_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByChatUserId(int $stchfo_stcusr_id) Return ChildChannelFollower objects filtered by the stchfo_stcusr_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByIsFollowing(boolean $stchfo_is_following) Return ChildChannelFollower objects filtered by the stchfo_is_following column
 * @method     ChildChannelFollower[]|ObjectCollection findByFollowedAt(string $stchfo_followed_at) Return ChildChannelFollower objects filtered by the stchfo_followed_at column
 * @method     ChildChannelFollower[]|ObjectCollection findByCreatedByUserId(int $stchfo_created_by_user_id) Return ChildChannelFollower objects filtered by the stchfo_created_by_user_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByUpdatedByUserId(int $stchfo_updated_by_user_id) Return ChildChannelFollower objects filtered by the stchfo_updated_by_user_id column
 * @method     ChildChannelFollower[]|ObjectCollection findByCreatedAt(string $stchfo_created_at) Return ChildChannelFollower objects filtered by the stchfo_created_at column
 * @method     ChildChannelFollower[]|ObjectCollection findByUpdatedAt(string $stchfo_updated_at) Return ChildChannelFollower objects filtered by the stchfo_updated_at column
 * @method     ChildChannelFollower[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChannelFollowerQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ChannelFollowerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\ChannelFollower', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChannelFollowerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChannelFollowerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChannelFollowerQuery) {
            return $criteria;
        }
        $query = new ChildChannelFollowerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChannelFollower|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChannelFollowerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChannelFollowerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelFollower A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stchfo_id, stchfo_ststrm_id, stchfo_stchan_id, stchfo_stsite_id, stchfo_stcusr_id, stchfo_is_following, stchfo_followed_at, stchfo_created_by_user_id, stchfo_updated_by_user_id, stchfo_created_at, stchfo_updated_at FROM stream_channel_follower_stchfo WHERE stchfo_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChannelFollower $obj */
            $obj = new ChildChannelFollower();
            $obj->hydrate($row);
            ChannelFollowerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChannelFollower|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stchfo_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stchfo_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stchfo_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stchfo_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stchfo_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stchfo_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stchfo_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stchfo_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stchfo_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stchfo_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stchfo_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stchfo_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stchfo_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stchfo_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stchfo_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stchfo_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stchfo_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stchfo_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stchfo_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stchfo_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stchfo_is_following column
     *
     * Example usage:
     * <code>
     * $query->filterByIsFollowing(true); // WHERE stchfo_is_following = true
     * $query->filterByIsFollowing('yes'); // WHERE stchfo_is_following = true
     * </code>
     *
     * @param     boolean|string $isFollowing The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByIsFollowing($isFollowing = null, $comparison = null)
    {
        if (is_string($isFollowing)) {
            $isFollowing = in_array(strtolower($isFollowing), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_IS_FOLLOWING, $isFollowing, $comparison);
    }

    /**
     * Filter the query on the stchfo_followed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByFollowedAt('2011-03-14'); // WHERE stchfo_followed_at = '2011-03-14'
     * $query->filterByFollowedAt('now'); // WHERE stchfo_followed_at = '2011-03-14'
     * $query->filterByFollowedAt(array('max' => 'yesterday')); // WHERE stchfo_followed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $followedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByFollowedAt($followedAt = null, $comparison = null)
    {
        if (is_array($followedAt)) {
            $useMinMax = false;
            if (isset($followedAt['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_FOLLOWED_AT, $followedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($followedAt['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_FOLLOWED_AT, $followedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_FOLLOWED_AT, $followedAt, $comparison);
    }

    /**
     * Filter the query on the stchfo_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE stchfo_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE stchfo_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE stchfo_created_by_user_id > 12
     * </code>
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the stchfo_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE stchfo_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE stchfo_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE stchfo_updated_by_user_id > 12
     * </code>
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the stchfo_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stchfo_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stchfo_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stchfo_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stchfo_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stchfo_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stchfo_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stchfo_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChannelFollower $channelFollower Object to remove from the list of results
     *
     * @return $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function prune($channelFollower = null)
    {
        if ($channelFollower) {
            $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_ID, $channelFollower->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_channel_follower_stchfo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelFollowerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChannelFollowerTableMap::clearInstancePool();
            ChannelFollowerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelFollowerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChannelFollowerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChannelFollowerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChannelFollowerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChannelFollowerTableMap::COL_STCHFO_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildChannelFollowerQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChannelFollowerTableMap::COL_STCHFO_CREATED_AT);
    }

} // ChannelFollowerQuery

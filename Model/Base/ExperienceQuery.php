<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\Experience as ChildExperience;
use IiMedias\StreamBundle\Model\ExperienceQuery as ChildExperienceQuery;
use IiMedias\StreamBundle\Model\Map\ExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_experience_stexpr' table.
 *
 *
 *
 * @method     ChildExperienceQuery orderById($order = Criteria::ASC) Order by the stexpr_id column
 * @method     ChildExperienceQuery orderByStreamId($order = Criteria::ASC) Order by the stexpr_ststrm_id column
 * @method     ChildExperienceQuery orderByChannelId($order = Criteria::ASC) Order by the stexpr_stchan_id column
 * @method     ChildExperienceQuery orderBySiteId($order = Criteria::ASC) Order by the stexpr_stsite_id column
 * @method     ChildExperienceQuery orderByChatUserId($order = Criteria::ASC) Order by the stexpr_stcusr_id column
 * @method     ChildExperienceQuery orderByType($order = Criteria::ASC) Order by the stexpr_type column
 * @method     ChildExperienceQuery orderByMessage($order = Criteria::ASC) Order by the stexpr_message column
 * @method     ChildExperienceQuery orderByExp($order = Criteria::ASC) Order by the stexpr_exp column
 * @method     ChildExperienceQuery orderByAt($order = Criteria::ASC) Order by the stexpr_at column
 *
 * @method     ChildExperienceQuery groupById() Group by the stexpr_id column
 * @method     ChildExperienceQuery groupByStreamId() Group by the stexpr_ststrm_id column
 * @method     ChildExperienceQuery groupByChannelId() Group by the stexpr_stchan_id column
 * @method     ChildExperienceQuery groupBySiteId() Group by the stexpr_stsite_id column
 * @method     ChildExperienceQuery groupByChatUserId() Group by the stexpr_stcusr_id column
 * @method     ChildExperienceQuery groupByType() Group by the stexpr_type column
 * @method     ChildExperienceQuery groupByMessage() Group by the stexpr_message column
 * @method     ChildExperienceQuery groupByExp() Group by the stexpr_exp column
 * @method     ChildExperienceQuery groupByAt() Group by the stexpr_at column
 *
 * @method     ChildExperienceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildExperienceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildExperienceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildExperienceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildExperienceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildExperienceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildExperienceQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildExperienceQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildExperienceQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildExperienceQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildExperienceQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildExperienceQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildExperienceQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildExperienceQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildExperienceQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildExperienceQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildExperienceQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildExperienceQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildExperienceQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildExperienceQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildExperienceQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildExperienceQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildExperienceQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildExperienceQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildExperienceQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildExperienceQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildExperienceQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildExperienceQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildExperienceQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildExperienceQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildExperienceQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildExperienceQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildExperienceQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildExperienceQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildExperience findOne(ConnectionInterface $con = null) Return the first ChildExperience matching the query
 * @method     ChildExperience findOneOrCreate(ConnectionInterface $con = null) Return the first ChildExperience matching the query, or a new ChildExperience object populated from the query conditions when no match is found
 *
 * @method     ChildExperience findOneById(int $stexpr_id) Return the first ChildExperience filtered by the stexpr_id column
 * @method     ChildExperience findOneByStreamId(int $stexpr_ststrm_id) Return the first ChildExperience filtered by the stexpr_ststrm_id column
 * @method     ChildExperience findOneByChannelId(int $stexpr_stchan_id) Return the first ChildExperience filtered by the stexpr_stchan_id column
 * @method     ChildExperience findOneBySiteId(int $stexpr_stsite_id) Return the first ChildExperience filtered by the stexpr_stsite_id column
 * @method     ChildExperience findOneByChatUserId(int $stexpr_stcusr_id) Return the first ChildExperience filtered by the stexpr_stcusr_id column
 * @method     ChildExperience findOneByType(int $stexpr_type) Return the first ChildExperience filtered by the stexpr_type column
 * @method     ChildExperience findOneByMessage(string $stexpr_message) Return the first ChildExperience filtered by the stexpr_message column
 * @method     ChildExperience findOneByExp(int $stexpr_exp) Return the first ChildExperience filtered by the stexpr_exp column
 * @method     ChildExperience findOneByAt(string $stexpr_at) Return the first ChildExperience filtered by the stexpr_at column *

 * @method     ChildExperience requirePk($key, ConnectionInterface $con = null) Return the ChildExperience by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOne(ConnectionInterface $con = null) Return the first ChildExperience matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildExperience requireOneById(int $stexpr_id) Return the first ChildExperience filtered by the stexpr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByStreamId(int $stexpr_ststrm_id) Return the first ChildExperience filtered by the stexpr_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByChannelId(int $stexpr_stchan_id) Return the first ChildExperience filtered by the stexpr_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneBySiteId(int $stexpr_stsite_id) Return the first ChildExperience filtered by the stexpr_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByChatUserId(int $stexpr_stcusr_id) Return the first ChildExperience filtered by the stexpr_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByType(int $stexpr_type) Return the first ChildExperience filtered by the stexpr_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByMessage(string $stexpr_message) Return the first ChildExperience filtered by the stexpr_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByExp(int $stexpr_exp) Return the first ChildExperience filtered by the stexpr_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExperience requireOneByAt(string $stexpr_at) Return the first ChildExperience filtered by the stexpr_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildExperience[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildExperience objects based on current ModelCriteria
 * @method     ChildExperience[]|ObjectCollection findById(int $stexpr_id) Return ChildExperience objects filtered by the stexpr_id column
 * @method     ChildExperience[]|ObjectCollection findByStreamId(int $stexpr_ststrm_id) Return ChildExperience objects filtered by the stexpr_ststrm_id column
 * @method     ChildExperience[]|ObjectCollection findByChannelId(int $stexpr_stchan_id) Return ChildExperience objects filtered by the stexpr_stchan_id column
 * @method     ChildExperience[]|ObjectCollection findBySiteId(int $stexpr_stsite_id) Return ChildExperience objects filtered by the stexpr_stsite_id column
 * @method     ChildExperience[]|ObjectCollection findByChatUserId(int $stexpr_stcusr_id) Return ChildExperience objects filtered by the stexpr_stcusr_id column
 * @method     ChildExperience[]|ObjectCollection findByType(int $stexpr_type) Return ChildExperience objects filtered by the stexpr_type column
 * @method     ChildExperience[]|ObjectCollection findByMessage(string $stexpr_message) Return ChildExperience objects filtered by the stexpr_message column
 * @method     ChildExperience[]|ObjectCollection findByExp(int $stexpr_exp) Return ChildExperience objects filtered by the stexpr_exp column
 * @method     ChildExperience[]|ObjectCollection findByAt(string $stexpr_at) Return ChildExperience objects filtered by the stexpr_at column
 * @method     ChildExperience[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ExperienceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ExperienceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Experience', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildExperienceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildExperienceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildExperienceQuery) {
            return $criteria;
        }
        $query = new ChildExperienceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildExperience|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ExperienceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ExperienceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExperience A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stexpr_id, stexpr_ststrm_id, stexpr_stchan_id, stexpr_stsite_id, stexpr_stcusr_id, stexpr_type, stexpr_message, stexpr_exp, stexpr_at FROM stream_experience_stexpr WHERE stexpr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildExperience $obj */
            $obj = new ChildExperience();
            $obj->hydrate($row);
            ExperienceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildExperience|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stexpr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stexpr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stexpr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stexpr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stexpr_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stexpr_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stexpr_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stexpr_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stexpr_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stexpr_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stexpr_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stexpr_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stexpr_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stexpr_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stexpr_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stexpr_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stexpr_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stexpr_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stexpr_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stexpr_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stexpr_type column
     *
     * @param     mixed $type The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        $valueSet = ExperienceTableMap::getValueSet(ExperienceTableMap::COL_STEXPR_TYPE);
        if (is_scalar($type)) {
            if (!in_array($type, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $type));
            }
            $type = array_search($type, $valueSet);
        } elseif (is_array($type)) {
            $convertedValues = array();
            foreach ($type as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $type = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the stexpr_message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE stexpr_message = 'fooValue'
     * $query->filterByMessage('%fooValue%', Criteria::LIKE); // WHERE stexpr_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the stexpr_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByExp(1234); // WHERE stexpr_exp = 1234
     * $query->filterByExp(array(12, 34)); // WHERE stexpr_exp IN (12, 34)
     * $query->filterByExp(array('min' => 12)); // WHERE stexpr_exp > 12
     * </code>
     *
     * @param     mixed $exp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByExp($exp = null, $comparison = null)
    {
        if (is_array($exp)) {
            $useMinMax = false;
            if (isset($exp['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_EXP, $exp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exp['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_EXP, $exp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_EXP, $exp, $comparison);
    }

    /**
     * Filter the query on the stexpr_at column
     *
     * Example usage:
     * <code>
     * $query->filterByAt('2011-03-14'); // WHERE stexpr_at = '2011-03-14'
     * $query->filterByAt('now'); // WHERE stexpr_at = '2011-03-14'
     * $query->filterByAt(array('max' => 'yesterday')); // WHERE stexpr_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $at The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByAt($at = null, $comparison = null)
    {
        if (is_array($at)) {
            $useMinMax = false;
            if (isset($at['min'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_AT, $at['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($at['max'])) {
                $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_AT, $at['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_AT, $at, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExperienceQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ExperienceTableMap::COL_STEXPR_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildExperience $experience Object to remove from the list of results
     *
     * @return $this|ChildExperienceQuery The current query, for fluid interface
     */
    public function prune($experience = null)
    {
        if ($experience) {
            $this->addUsingAlias(ExperienceTableMap::COL_STEXPR_ID, $experience->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_experience_stexpr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ExperienceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ExperienceTableMap::clearInstancePool();
            ExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ExperienceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ExperienceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ExperienceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ExperienceQuery

<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\ChatMessage as ChildChatMessage;
use IiMedias\StreamBundle\Model\ChatMessageQuery as ChildChatMessageQuery;
use IiMedias\StreamBundle\Model\Map\ChatMessageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_chat_message_stcmsg' table.
 *
 *
 *
 * @method     ChildChatMessageQuery orderById($order = Criteria::ASC) Order by the stcmsg_id column
 * @method     ChildChatMessageQuery orderByStreamId($order = Criteria::ASC) Order by the stcmsg_ststrm_id column
 * @method     ChildChatMessageQuery orderByChannelId($order = Criteria::ASC) Order by the stcmsg_stchan_id column
 * @method     ChildChatMessageQuery orderByChatUserId($order = Criteria::ASC) Order by the stcmsg_stcusr_id column
 * @method     ChildChatMessageQuery orderByType($order = Criteria::ASC) Order by the stcmsg_type column
 * @method     ChildChatMessageQuery orderByMessage($order = Criteria::ASC) Order by the stcusr_message column
 * @method     ChildChatMessageQuery orderByShow($order = Criteria::ASC) Order by the stcusr_show column
 * @method     ChildChatMessageQuery orderBySendAt($order = Criteria::ASC) Order by the send_at column
 * @method     ChildChatMessageQuery orderByHide($order = Criteria::ASC) Order by the hide column
 *
 * @method     ChildChatMessageQuery groupById() Group by the stcmsg_id column
 * @method     ChildChatMessageQuery groupByStreamId() Group by the stcmsg_ststrm_id column
 * @method     ChildChatMessageQuery groupByChannelId() Group by the stcmsg_stchan_id column
 * @method     ChildChatMessageQuery groupByChatUserId() Group by the stcmsg_stcusr_id column
 * @method     ChildChatMessageQuery groupByType() Group by the stcmsg_type column
 * @method     ChildChatMessageQuery groupByMessage() Group by the stcusr_message column
 * @method     ChildChatMessageQuery groupByShow() Group by the stcusr_show column
 * @method     ChildChatMessageQuery groupBySendAt() Group by the send_at column
 * @method     ChildChatMessageQuery groupByHide() Group by the hide column
 *
 * @method     ChildChatMessageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatMessageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatMessageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatMessageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatMessageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatMessageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatMessageQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildChatMessageQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildChatMessageQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildChatMessageQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildChatMessageQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildChatMessageQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildChatMessageQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildChatMessageQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildChatMessageQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildChatMessageQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildChatMessageQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildChatMessageQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildChatMessageQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildChatMessageQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildChatMessageQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildChatMessageQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildChatMessageQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildChatMessageQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildChatMessageQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChatMessageQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChatMessageQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChatMessage findOne(ConnectionInterface $con = null) Return the first ChildChatMessage matching the query
 * @method     ChildChatMessage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatMessage matching the query, or a new ChildChatMessage object populated from the query conditions when no match is found
 *
 * @method     ChildChatMessage findOneById(int $stcmsg_id) Return the first ChildChatMessage filtered by the stcmsg_id column
 * @method     ChildChatMessage findOneByStreamId(int $stcmsg_ststrm_id) Return the first ChildChatMessage filtered by the stcmsg_ststrm_id column
 * @method     ChildChatMessage findOneByChannelId(int $stcmsg_stchan_id) Return the first ChildChatMessage filtered by the stcmsg_stchan_id column
 * @method     ChildChatMessage findOneByChatUserId(int $stcmsg_stcusr_id) Return the first ChildChatMessage filtered by the stcmsg_stcusr_id column
 * @method     ChildChatMessage findOneByType(string $stcmsg_type) Return the first ChildChatMessage filtered by the stcmsg_type column
 * @method     ChildChatMessage findOneByMessage(string $stcusr_message) Return the first ChildChatMessage filtered by the stcusr_message column
 * @method     ChildChatMessage findOneByShow(boolean $stcusr_show) Return the first ChildChatMessage filtered by the stcusr_show column
 * @method     ChildChatMessage findOneBySendAt(string $send_at) Return the first ChildChatMessage filtered by the send_at column
 * @method     ChildChatMessage findOneByHide(boolean $hide) Return the first ChildChatMessage filtered by the hide column *

 * @method     ChildChatMessage requirePk($key, ConnectionInterface $con = null) Return the ChildChatMessage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOne(ConnectionInterface $con = null) Return the first ChildChatMessage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatMessage requireOneById(int $stcmsg_id) Return the first ChildChatMessage filtered by the stcmsg_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByStreamId(int $stcmsg_ststrm_id) Return the first ChildChatMessage filtered by the stcmsg_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByChannelId(int $stcmsg_stchan_id) Return the first ChildChatMessage filtered by the stcmsg_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByChatUserId(int $stcmsg_stcusr_id) Return the first ChildChatMessage filtered by the stcmsg_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByType(string $stcmsg_type) Return the first ChildChatMessage filtered by the stcmsg_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByMessage(string $stcusr_message) Return the first ChildChatMessage filtered by the stcusr_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByShow(boolean $stcusr_show) Return the first ChildChatMessage filtered by the stcusr_show column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneBySendAt(string $send_at) Return the first ChildChatMessage filtered by the send_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatMessage requireOneByHide(boolean $hide) Return the first ChildChatMessage filtered by the hide column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatMessage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatMessage objects based on current ModelCriteria
 * @method     ChildChatMessage[]|ObjectCollection findById(int $stcmsg_id) Return ChildChatMessage objects filtered by the stcmsg_id column
 * @method     ChildChatMessage[]|ObjectCollection findByStreamId(int $stcmsg_ststrm_id) Return ChildChatMessage objects filtered by the stcmsg_ststrm_id column
 * @method     ChildChatMessage[]|ObjectCollection findByChannelId(int $stcmsg_stchan_id) Return ChildChatMessage objects filtered by the stcmsg_stchan_id column
 * @method     ChildChatMessage[]|ObjectCollection findByChatUserId(int $stcmsg_stcusr_id) Return ChildChatMessage objects filtered by the stcmsg_stcusr_id column
 * @method     ChildChatMessage[]|ObjectCollection findByType(string $stcmsg_type) Return ChildChatMessage objects filtered by the stcmsg_type column
 * @method     ChildChatMessage[]|ObjectCollection findByMessage(string $stcusr_message) Return ChildChatMessage objects filtered by the stcusr_message column
 * @method     ChildChatMessage[]|ObjectCollection findByShow(boolean $stcusr_show) Return ChildChatMessage objects filtered by the stcusr_show column
 * @method     ChildChatMessage[]|ObjectCollection findBySendAt(string $send_at) Return ChildChatMessage objects filtered by the send_at column
 * @method     ChildChatMessage[]|ObjectCollection findByHide(boolean $hide) Return ChildChatMessage objects filtered by the hide column
 * @method     ChildChatMessage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatMessageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ChatMessageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\ChatMessage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatMessageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatMessageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatMessageQuery) {
            return $criteria;
        }
        $query = new ChildChatMessageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatMessage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatMessageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChatMessageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatMessage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stcmsg_id, stcmsg_ststrm_id, stcmsg_stchan_id, stcmsg_stcusr_id, stcmsg_type, stcusr_message, stcusr_show, send_at, hide FROM stream_chat_message_stcmsg WHERE stcmsg_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChatMessage $obj */
            $obj = new ChildChatMessage();
            $obj->hydrate($row);
            ChatMessageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChatMessage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stcmsg_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stcmsg_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stcmsg_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stcmsg_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stcmsg_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stcmsg_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stcmsg_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stcmsg_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stcmsg_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stcmsg_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stcmsg_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stcmsg_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stcmsg_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stcmsg_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stcmsg_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stcmsg_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stcmsg_type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE stcmsg_type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE stcmsg_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the stcusr_message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE stcusr_message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE stcusr_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCUSR_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the stcusr_show column
     *
     * Example usage:
     * <code>
     * $query->filterByShow(true); // WHERE stcusr_show = true
     * $query->filterByShow('yes'); // WHERE stcusr_show = true
     * </code>
     *
     * @param     boolean|string $show The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByShow($show = null, $comparison = null)
    {
        if (is_string($show)) {
            $show = in_array(strtolower($show), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_STCUSR_SHOW, $show, $comparison);
    }

    /**
     * Filter the query on the send_at column
     *
     * Example usage:
     * <code>
     * $query->filterBySendAt('2011-03-14'); // WHERE send_at = '2011-03-14'
     * $query->filterBySendAt('now'); // WHERE send_at = '2011-03-14'
     * $query->filterBySendAt(array('max' => 'yesterday')); // WHERE send_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $sendAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterBySendAt($sendAt = null, $comparison = null)
    {
        if (is_array($sendAt)) {
            $useMinMax = false;
            if (isset($sendAt['min'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_SEND_AT, $sendAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sendAt['max'])) {
                $this->addUsingAlias(ChatMessageTableMap::COL_SEND_AT, $sendAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_SEND_AT, $sendAt, $comparison);
    }

    /**
     * Filter the query on the hide column
     *
     * Example usage:
     * <code>
     * $query->filterByHide(true); // WHERE hide = true
     * $query->filterByHide('yes'); // WHERE hide = true
     * </code>
     *
     * @param     boolean|string $hide The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByHide($hide = null, $comparison = null)
    {
        if (is_string($hide)) {
            $hide = in_array(strtolower($hide), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChatMessageTableMap::COL_HIDE, $hide, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatMessageQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatMessageTableMap::COL_STCMSG_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatMessage $chatMessage Object to remove from the list of results
     *
     * @return $this|ChildChatMessageQuery The current query, for fluid interface
     */
    public function prune($chatMessage = null)
    {
        if ($chatMessage) {
            $this->addUsingAlias(ChatMessageTableMap::COL_STCMSG_ID, $chatMessage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_chat_message_stcmsg table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatMessageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatMessageTableMap::clearInstancePool();
            ChatMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatMessageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatMessageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatMessageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChatMessageQuery

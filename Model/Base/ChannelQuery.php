<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\Channel as ChildChannel;
use IiMedias\StreamBundle\Model\ChannelQuery as ChildChannelQuery;
use IiMedias\StreamBundle\Model\Map\ChannelTableMap;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_channel_stchan' table.
 *
 *
 *
 * @method     ChildChannelQuery orderById($order = Criteria::ASC) Order by the stchan_id column
 * @method     ChildChannelQuery orderByStreamId($order = Criteria::ASC) Order by the stchan_ststrm_id column
 * @method     ChildChannelQuery orderBySiteId($order = Criteria::ASC) Order by the stchan_stsite_id column
 * @method     ChildChannelQuery orderByChannel($order = Criteria::ASC) Order by the stchan_channel column
 * @method     ChildChannelQuery orderByBotId($order = Criteria::ASC) Order by the stchan_ststrm_bot_id column
 * @method     ChildChannelQuery orderByCanScan($order = Criteria::ASC) Order by the stchan_can_scan column
 * @method     ChildChannelQuery orderByPasswordOauth($order = Criteria::ASC) Order by the stchan_password_oauth column
 * @method     ChildChannelQuery orderByChannelId($order = Criteria::ASC) Order by the stchan_channel_id column
 * @method     ChildChannelQuery orderByClientId($order = Criteria::ASC) Order by the stchan_client_id column
 * @method     ChildChannelQuery orderBySecretId($order = Criteria::ASC) Order by the stchan_secred_id column
 * @method     ChildChannelQuery orderByChannelPartner($order = Criteria::ASC) Order by the stchan_channel_partner column
 * @method     ChildChannelQuery orderByLiveMode($order = Criteria::ASC) Order by the stchan_live_mode column
 * @method     ChildChannelQuery orderByStatus($order = Criteria::ASC) Order by the stchan_status column
 * @method     ChildChannelQuery orderByTwitchGameId($order = Criteria::ASC) Order by the stchan_vgatga_id column
 * @method     ChildChannelQuery orderByGame($order = Criteria::ASC) Order by the stchan_game column
 * @method     ChildChannelQuery orderByStreamType($order = Criteria::ASC) Order by the stchan_stream_type column
 * @method     ChildChannelQuery orderByFollowersCount($order = Criteria::ASC) Order by the stchan_followers_count column
 * @method     ChildChannelQuery orderByFollowersDiff($order = Criteria::ASC) Order by the stchan_followers_diff column
 * @method     ChildChannelQuery orderByViewsCount($order = Criteria::ASC) Order by the stchan_views_count column
 * @method     ChildChannelQuery orderByViewsDiff($order = Criteria::ASC) Order by the stchan_views_diff column
 * @method     ChildChannelQuery orderByViewersCount($order = Criteria::ASC) Order by the stchan_viewers_count column
 * @method     ChildChannelQuery orderByChattersCount($order = Criteria::ASC) Order by the stchan_chatters_count column
 * @method     ChildChannelQuery orderByHostsCount($order = Criteria::ASC) Order by the stchan_hosts_count column
 * @method     ChildChannelQuery orderByLiveViewersCount($order = Criteria::ASC) Order by the stchan_live_viewers_count column
 * @method     ChildChannelQuery orderByMessagesCount($order = Criteria::ASC) Order by the stchan_messages_count column
 * @method     ChildChannelQuery orderByLockChattersScan($order = Criteria::ASC) Order by the stchan_lock_chatters_scan column
 * @method     ChildChannelQuery orderByLockHostsScan($order = Criteria::ASC) Order by the stchan_lock_hosts_scan column
 * @method     ChildChannelQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the stchan_created_by_user_id column
 * @method     ChildChannelQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the stchan_updated_by_user_id column
 * @method     ChildChannelQuery orderByCreatedAt($order = Criteria::ASC) Order by the stchan_created_at column
 * @method     ChildChannelQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stchan_updated_at column
 *
 * @method     ChildChannelQuery groupById() Group by the stchan_id column
 * @method     ChildChannelQuery groupByStreamId() Group by the stchan_ststrm_id column
 * @method     ChildChannelQuery groupBySiteId() Group by the stchan_stsite_id column
 * @method     ChildChannelQuery groupByChannel() Group by the stchan_channel column
 * @method     ChildChannelQuery groupByBotId() Group by the stchan_ststrm_bot_id column
 * @method     ChildChannelQuery groupByCanScan() Group by the stchan_can_scan column
 * @method     ChildChannelQuery groupByPasswordOauth() Group by the stchan_password_oauth column
 * @method     ChildChannelQuery groupByChannelId() Group by the stchan_channel_id column
 * @method     ChildChannelQuery groupByClientId() Group by the stchan_client_id column
 * @method     ChildChannelQuery groupBySecretId() Group by the stchan_secred_id column
 * @method     ChildChannelQuery groupByChannelPartner() Group by the stchan_channel_partner column
 * @method     ChildChannelQuery groupByLiveMode() Group by the stchan_live_mode column
 * @method     ChildChannelQuery groupByStatus() Group by the stchan_status column
 * @method     ChildChannelQuery groupByTwitchGameId() Group by the stchan_vgatga_id column
 * @method     ChildChannelQuery groupByGame() Group by the stchan_game column
 * @method     ChildChannelQuery groupByStreamType() Group by the stchan_stream_type column
 * @method     ChildChannelQuery groupByFollowersCount() Group by the stchan_followers_count column
 * @method     ChildChannelQuery groupByFollowersDiff() Group by the stchan_followers_diff column
 * @method     ChildChannelQuery groupByViewsCount() Group by the stchan_views_count column
 * @method     ChildChannelQuery groupByViewsDiff() Group by the stchan_views_diff column
 * @method     ChildChannelQuery groupByViewersCount() Group by the stchan_viewers_count column
 * @method     ChildChannelQuery groupByChattersCount() Group by the stchan_chatters_count column
 * @method     ChildChannelQuery groupByHostsCount() Group by the stchan_hosts_count column
 * @method     ChildChannelQuery groupByLiveViewersCount() Group by the stchan_live_viewers_count column
 * @method     ChildChannelQuery groupByMessagesCount() Group by the stchan_messages_count column
 * @method     ChildChannelQuery groupByLockChattersScan() Group by the stchan_lock_chatters_scan column
 * @method     ChildChannelQuery groupByLockHostsScan() Group by the stchan_lock_hosts_scan column
 * @method     ChildChannelQuery groupByCreatedByUserId() Group by the stchan_created_by_user_id column
 * @method     ChildChannelQuery groupByUpdatedByUserId() Group by the stchan_updated_by_user_id column
 * @method     ChildChannelQuery groupByCreatedAt() Group by the stchan_created_at column
 * @method     ChildChannelQuery groupByUpdatedAt() Group by the stchan_updated_at column
 *
 * @method     ChildChannelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChannelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChannelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChannelQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChannelQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChannelQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChannelQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildChannelQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildChannelQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildChannelQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildChannelQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildChannelQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildChannelQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildChannelQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildChannelQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildChannelQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildChannelQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildChannelQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildChannelQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildChannelQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildChannelQuery leftJoinBot($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bot relation
 * @method     ChildChannelQuery rightJoinBot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bot relation
 * @method     ChildChannelQuery innerJoinBot($relationAlias = null) Adds a INNER JOIN clause to the query using the Bot relation
 *
 * @method     ChildChannelQuery joinWithBot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bot relation
 *
 * @method     ChildChannelQuery leftJoinWithBot() Adds a LEFT JOIN clause and with to the query using the Bot relation
 * @method     ChildChannelQuery rightJoinWithBot() Adds a RIGHT JOIN clause and with to the query using the Bot relation
 * @method     ChildChannelQuery innerJoinWithBot() Adds a INNER JOIN clause and with to the query using the Bot relation
 *
 * @method     ChildChannelQuery leftJoinApiTwitchGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildChannelQuery rightJoinApiTwitchGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildChannelQuery innerJoinApiTwitchGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiTwitchGame relation
 *
 * @method     ChildChannelQuery joinWithApiTwitchGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiTwitchGame relation
 *
 * @method     ChildChannelQuery leftJoinWithApiTwitchGame() Adds a LEFT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildChannelQuery rightJoinWithApiTwitchGame() Adds a RIGHT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildChannelQuery innerJoinWithApiTwitchGame() Adds a INNER JOIN clause and with to the query using the ApiTwitchGame relation
 *
 * @method     ChildChannelQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildChannelQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildChannelQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildChannelQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildChannelQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildChannelQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildChannelQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildChannelQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildChannelQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildChannelQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildChannelQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildChannelQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildChannelQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildChannelQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildChannelQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildChannelQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildChannelQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildChannelQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildChannelQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildChannelQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     ChildChannelQuery leftJoinDeepBotImportExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildChannelQuery rightJoinDeepBotImportExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildChannelQuery innerJoinDeepBotImportExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChannelQuery joinWithDeepBotImportExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithDeepBotImportExperience() Adds a LEFT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildChannelQuery rightJoinWithDeepBotImportExperience() Adds a RIGHT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildChannelQuery innerJoinWithDeepBotImportExperience() Adds a INNER JOIN clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildChannelQuery leftJoinMessageExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageExperience relation
 * @method     ChildChannelQuery rightJoinMessageExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageExperience relation
 * @method     ChildChannelQuery innerJoinMessageExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageExperience relation
 *
 * @method     ChildChannelQuery joinWithMessageExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithMessageExperience() Adds a LEFT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildChannelQuery rightJoinWithMessageExperience() Adds a RIGHT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildChannelQuery innerJoinWithMessageExperience() Adds a INNER JOIN clause and with to the query using the MessageExperience relation
 *
 * @method     ChildChannelQuery leftJoinChatterExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildChannelQuery rightJoinChatterExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildChannelQuery innerJoinChatterExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatterExperience relation
 *
 * @method     ChildChannelQuery joinWithChatterExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithChatterExperience() Adds a LEFT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildChannelQuery rightJoinWithChatterExperience() Adds a RIGHT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildChannelQuery innerJoinWithChatterExperience() Adds a INNER JOIN clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildChannelQuery leftJoinFollowExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowExperience relation
 * @method     ChildChannelQuery rightJoinFollowExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowExperience relation
 * @method     ChildChannelQuery innerJoinFollowExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowExperience relation
 *
 * @method     ChildChannelQuery joinWithFollowExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithFollowExperience() Adds a LEFT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildChannelQuery rightJoinWithFollowExperience() Adds a RIGHT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildChannelQuery innerJoinWithFollowExperience() Adds a INNER JOIN clause and with to the query using the FollowExperience relation
 *
 * @method     ChildChannelQuery leftJoinHostExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the HostExperience relation
 * @method     ChildChannelQuery rightJoinHostExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HostExperience relation
 * @method     ChildChannelQuery innerJoinHostExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the HostExperience relation
 *
 * @method     ChildChannelQuery joinWithHostExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HostExperience relation
 *
 * @method     ChildChannelQuery leftJoinWithHostExperience() Adds a LEFT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildChannelQuery rightJoinWithHostExperience() Adds a RIGHT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildChannelQuery innerJoinWithHostExperience() Adds a INNER JOIN clause and with to the query using the HostExperience relation
 *
 * @method     ChildChannelQuery leftJoinViewDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildChannelQuery rightJoinViewDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildChannelQuery innerJoinViewDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewDiffData relation
 *
 * @method     ChildChannelQuery joinWithViewDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildChannelQuery leftJoinWithViewDiffData() Adds a LEFT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildChannelQuery rightJoinWithViewDiffData() Adds a RIGHT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildChannelQuery innerJoinWithViewDiffData() Adds a INNER JOIN clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildChannelQuery leftJoinViewerData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewerData relation
 * @method     ChildChannelQuery rightJoinViewerData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewerData relation
 * @method     ChildChannelQuery innerJoinViewerData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewerData relation
 *
 * @method     ChildChannelQuery joinWithViewerData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewerData relation
 *
 * @method     ChildChannelQuery leftJoinWithViewerData() Adds a LEFT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildChannelQuery rightJoinWithViewerData() Adds a RIGHT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildChannelQuery innerJoinWithViewerData() Adds a INNER JOIN clause and with to the query using the ViewerData relation
 *
 * @method     ChildChannelQuery leftJoinFollowDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildChannelQuery rightJoinFollowDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildChannelQuery innerJoinFollowDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowDiffData relation
 *
 * @method     ChildChannelQuery joinWithFollowDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildChannelQuery leftJoinWithFollowDiffData() Adds a LEFT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildChannelQuery rightJoinWithFollowDiffData() Adds a RIGHT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildChannelQuery innerJoinWithFollowDiffData() Adds a INNER JOIN clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildChannelQuery leftJoinStatusData($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusData relation
 * @method     ChildChannelQuery rightJoinStatusData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusData relation
 * @method     ChildChannelQuery innerJoinStatusData($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusData relation
 *
 * @method     ChildChannelQuery joinWithStatusData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StatusData relation
 *
 * @method     ChildChannelQuery leftJoinWithStatusData() Adds a LEFT JOIN clause and with to the query using the StatusData relation
 * @method     ChildChannelQuery rightJoinWithStatusData() Adds a RIGHT JOIN clause and with to the query using the StatusData relation
 * @method     ChildChannelQuery innerJoinWithStatusData() Adds a INNER JOIN clause and with to the query using the StatusData relation
 *
 * @method     ChildChannelQuery leftJoinTypeData($relationAlias = null) Adds a LEFT JOIN clause to the query using the TypeData relation
 * @method     ChildChannelQuery rightJoinTypeData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TypeData relation
 * @method     ChildChannelQuery innerJoinTypeData($relationAlias = null) Adds a INNER JOIN clause to the query using the TypeData relation
 *
 * @method     ChildChannelQuery joinWithTypeData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TypeData relation
 *
 * @method     ChildChannelQuery leftJoinWithTypeData() Adds a LEFT JOIN clause and with to the query using the TypeData relation
 * @method     ChildChannelQuery rightJoinWithTypeData() Adds a RIGHT JOIN clause and with to the query using the TypeData relation
 * @method     ChildChannelQuery innerJoinWithTypeData() Adds a INNER JOIN clause and with to the query using the TypeData relation
 *
 * @method     ChildChannelQuery leftJoinGameData($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameData relation
 * @method     ChildChannelQuery rightJoinGameData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameData relation
 * @method     ChildChannelQuery innerJoinGameData($relationAlias = null) Adds a INNER JOIN clause to the query using the GameData relation
 *
 * @method     ChildChannelQuery joinWithGameData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameData relation
 *
 * @method     ChildChannelQuery leftJoinWithGameData() Adds a LEFT JOIN clause and with to the query using the GameData relation
 * @method     ChildChannelQuery rightJoinWithGameData() Adds a RIGHT JOIN clause and with to the query using the GameData relation
 * @method     ChildChannelQuery innerJoinWithGameData() Adds a INNER JOIN clause and with to the query using the GameData relation
 *
 * @method     ChildChannelQuery leftJoinStat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stat relation
 * @method     ChildChannelQuery rightJoinStat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stat relation
 * @method     ChildChannelQuery innerJoinStat($relationAlias = null) Adds a INNER JOIN clause to the query using the Stat relation
 *
 * @method     ChildChannelQuery joinWithStat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stat relation
 *
 * @method     ChildChannelQuery leftJoinWithStat() Adds a LEFT JOIN clause and with to the query using the Stat relation
 * @method     ChildChannelQuery rightJoinWithStat() Adds a RIGHT JOIN clause and with to the query using the Stat relation
 * @method     ChildChannelQuery innerJoinWithStat() Adds a INNER JOIN clause and with to the query using the Stat relation
 *
 * @method     ChildChannelQuery leftJoinExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Experience relation
 * @method     ChildChannelQuery rightJoinExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Experience relation
 * @method     ChildChannelQuery innerJoinExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the Experience relation
 *
 * @method     ChildChannelQuery joinWithExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Experience relation
 *
 * @method     ChildChannelQuery leftJoinWithExperience() Adds a LEFT JOIN clause and with to the query using the Experience relation
 * @method     ChildChannelQuery rightJoinWithExperience() Adds a RIGHT JOIN clause and with to the query using the Experience relation
 * @method     ChildChannelQuery innerJoinWithExperience() Adds a INNER JOIN clause and with to the query using the Experience relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery|\IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery|\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery|\IiMedias\StreamBundle\Model\MessageExperienceQuery|\IiMedias\StreamBundle\Model\ChatterExperienceQuery|\IiMedias\StreamBundle\Model\FollowExperienceQuery|\IiMedias\StreamBundle\Model\HostExperienceQuery|\IiMedias\StreamBundle\Model\ViewDiffDataQuery|\IiMedias\StreamBundle\Model\ViewerDataQuery|\IiMedias\StreamBundle\Model\FollowDiffDataQuery|\IiMedias\StreamBundle\Model\StatusDataQuery|\IiMedias\StreamBundle\Model\TypeDataQuery|\IiMedias\StreamBundle\Model\GameDataQuery|\IiMedias\StreamBundle\Model\StatQuery|\IiMedias\StreamBundle\Model\ExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChannel findOne(ConnectionInterface $con = null) Return the first ChildChannel matching the query
 * @method     ChildChannel findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChannel matching the query, or a new ChildChannel object populated from the query conditions when no match is found
 *
 * @method     ChildChannel findOneById(int $stchan_id) Return the first ChildChannel filtered by the stchan_id column
 * @method     ChildChannel findOneByStreamId(int $stchan_ststrm_id) Return the first ChildChannel filtered by the stchan_ststrm_id column
 * @method     ChildChannel findOneBySiteId(int $stchan_stsite_id) Return the first ChildChannel filtered by the stchan_stsite_id column
 * @method     ChildChannel findOneByChannel(string $stchan_channel) Return the first ChildChannel filtered by the stchan_channel column
 * @method     ChildChannel findOneByBotId(int $stchan_ststrm_bot_id) Return the first ChildChannel filtered by the stchan_ststrm_bot_id column
 * @method     ChildChannel findOneByCanScan(boolean $stchan_can_scan) Return the first ChildChannel filtered by the stchan_can_scan column
 * @method     ChildChannel findOneByPasswordOauth(string $stchan_password_oauth) Return the first ChildChannel filtered by the stchan_password_oauth column
 * @method     ChildChannel findOneByChannelId(int $stchan_channel_id) Return the first ChildChannel filtered by the stchan_channel_id column
 * @method     ChildChannel findOneByClientId(string $stchan_client_id) Return the first ChildChannel filtered by the stchan_client_id column
 * @method     ChildChannel findOneBySecretId(string $stchan_secred_id) Return the first ChildChannel filtered by the stchan_secred_id column
 * @method     ChildChannel findOneByChannelPartner(boolean $stchan_channel_partner) Return the first ChildChannel filtered by the stchan_channel_partner column
 * @method     ChildChannel findOneByLiveMode(int $stchan_live_mode) Return the first ChildChannel filtered by the stchan_live_mode column
 * @method     ChildChannel findOneByStatus(string $stchan_status) Return the first ChildChannel filtered by the stchan_status column
 * @method     ChildChannel findOneByTwitchGameId(int $stchan_vgatga_id) Return the first ChildChannel filtered by the stchan_vgatga_id column
 * @method     ChildChannel findOneByGame(string $stchan_game) Return the first ChildChannel filtered by the stchan_game column
 * @method     ChildChannel findOneByStreamType(int $stchan_stream_type) Return the first ChildChannel filtered by the stchan_stream_type column
 * @method     ChildChannel findOneByFollowersCount(int $stchan_followers_count) Return the first ChildChannel filtered by the stchan_followers_count column
 * @method     ChildChannel findOneByFollowersDiff(int $stchan_followers_diff) Return the first ChildChannel filtered by the stchan_followers_diff column
 * @method     ChildChannel findOneByViewsCount(int $stchan_views_count) Return the first ChildChannel filtered by the stchan_views_count column
 * @method     ChildChannel findOneByViewsDiff(int $stchan_views_diff) Return the first ChildChannel filtered by the stchan_views_diff column
 * @method     ChildChannel findOneByViewersCount(int $stchan_viewers_count) Return the first ChildChannel filtered by the stchan_viewers_count column
 * @method     ChildChannel findOneByChattersCount(int $stchan_chatters_count) Return the first ChildChannel filtered by the stchan_chatters_count column
 * @method     ChildChannel findOneByHostsCount(int $stchan_hosts_count) Return the first ChildChannel filtered by the stchan_hosts_count column
 * @method     ChildChannel findOneByLiveViewersCount(int $stchan_live_viewers_count) Return the first ChildChannel filtered by the stchan_live_viewers_count column
 * @method     ChildChannel findOneByMessagesCount(int $stchan_messages_count) Return the first ChildChannel filtered by the stchan_messages_count column
 * @method     ChildChannel findOneByLockChattersScan(boolean $stchan_lock_chatters_scan) Return the first ChildChannel filtered by the stchan_lock_chatters_scan column
 * @method     ChildChannel findOneByLockHostsScan(boolean $stchan_lock_hosts_scan) Return the first ChildChannel filtered by the stchan_lock_hosts_scan column
 * @method     ChildChannel findOneByCreatedByUserId(int $stchan_created_by_user_id) Return the first ChildChannel filtered by the stchan_created_by_user_id column
 * @method     ChildChannel findOneByUpdatedByUserId(int $stchan_updated_by_user_id) Return the first ChildChannel filtered by the stchan_updated_by_user_id column
 * @method     ChildChannel findOneByCreatedAt(string $stchan_created_at) Return the first ChildChannel filtered by the stchan_created_at column
 * @method     ChildChannel findOneByUpdatedAt(string $stchan_updated_at) Return the first ChildChannel filtered by the stchan_updated_at column *

 * @method     ChildChannel requirePk($key, ConnectionInterface $con = null) Return the ChildChannel by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOne(ConnectionInterface $con = null) Return the first ChildChannel matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChannel requireOneById(int $stchan_id) Return the first ChildChannel filtered by the stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByStreamId(int $stchan_ststrm_id) Return the first ChildChannel filtered by the stchan_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneBySiteId(int $stchan_stsite_id) Return the first ChildChannel filtered by the stchan_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByChannel(string $stchan_channel) Return the first ChildChannel filtered by the stchan_channel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByBotId(int $stchan_ststrm_bot_id) Return the first ChildChannel filtered by the stchan_ststrm_bot_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByCanScan(boolean $stchan_can_scan) Return the first ChildChannel filtered by the stchan_can_scan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByPasswordOauth(string $stchan_password_oauth) Return the first ChildChannel filtered by the stchan_password_oauth column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByChannelId(int $stchan_channel_id) Return the first ChildChannel filtered by the stchan_channel_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByClientId(string $stchan_client_id) Return the first ChildChannel filtered by the stchan_client_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneBySecretId(string $stchan_secred_id) Return the first ChildChannel filtered by the stchan_secred_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByChannelPartner(boolean $stchan_channel_partner) Return the first ChildChannel filtered by the stchan_channel_partner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByLiveMode(int $stchan_live_mode) Return the first ChildChannel filtered by the stchan_live_mode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByStatus(string $stchan_status) Return the first ChildChannel filtered by the stchan_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByTwitchGameId(int $stchan_vgatga_id) Return the first ChildChannel filtered by the stchan_vgatga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByGame(string $stchan_game) Return the first ChildChannel filtered by the stchan_game column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByStreamType(int $stchan_stream_type) Return the first ChildChannel filtered by the stchan_stream_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByFollowersCount(int $stchan_followers_count) Return the first ChildChannel filtered by the stchan_followers_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByFollowersDiff(int $stchan_followers_diff) Return the first ChildChannel filtered by the stchan_followers_diff column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByViewsCount(int $stchan_views_count) Return the first ChildChannel filtered by the stchan_views_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByViewsDiff(int $stchan_views_diff) Return the first ChildChannel filtered by the stchan_views_diff column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByViewersCount(int $stchan_viewers_count) Return the first ChildChannel filtered by the stchan_viewers_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByChattersCount(int $stchan_chatters_count) Return the first ChildChannel filtered by the stchan_chatters_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByHostsCount(int $stchan_hosts_count) Return the first ChildChannel filtered by the stchan_hosts_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByLiveViewersCount(int $stchan_live_viewers_count) Return the first ChildChannel filtered by the stchan_live_viewers_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByMessagesCount(int $stchan_messages_count) Return the first ChildChannel filtered by the stchan_messages_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByLockChattersScan(boolean $stchan_lock_chatters_scan) Return the first ChildChannel filtered by the stchan_lock_chatters_scan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByLockHostsScan(boolean $stchan_lock_hosts_scan) Return the first ChildChannel filtered by the stchan_lock_hosts_scan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByCreatedByUserId(int $stchan_created_by_user_id) Return the first ChildChannel filtered by the stchan_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByUpdatedByUserId(int $stchan_updated_by_user_id) Return the first ChildChannel filtered by the stchan_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByCreatedAt(string $stchan_created_at) Return the first ChildChannel filtered by the stchan_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChannel requireOneByUpdatedAt(string $stchan_updated_at) Return the first ChildChannel filtered by the stchan_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChannel[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChannel objects based on current ModelCriteria
 * @method     ChildChannel[]|ObjectCollection findById(int $stchan_id) Return ChildChannel objects filtered by the stchan_id column
 * @method     ChildChannel[]|ObjectCollection findByStreamId(int $stchan_ststrm_id) Return ChildChannel objects filtered by the stchan_ststrm_id column
 * @method     ChildChannel[]|ObjectCollection findBySiteId(int $stchan_stsite_id) Return ChildChannel objects filtered by the stchan_stsite_id column
 * @method     ChildChannel[]|ObjectCollection findByChannel(string $stchan_channel) Return ChildChannel objects filtered by the stchan_channel column
 * @method     ChildChannel[]|ObjectCollection findByBotId(int $stchan_ststrm_bot_id) Return ChildChannel objects filtered by the stchan_ststrm_bot_id column
 * @method     ChildChannel[]|ObjectCollection findByCanScan(boolean $stchan_can_scan) Return ChildChannel objects filtered by the stchan_can_scan column
 * @method     ChildChannel[]|ObjectCollection findByPasswordOauth(string $stchan_password_oauth) Return ChildChannel objects filtered by the stchan_password_oauth column
 * @method     ChildChannel[]|ObjectCollection findByChannelId(int $stchan_channel_id) Return ChildChannel objects filtered by the stchan_channel_id column
 * @method     ChildChannel[]|ObjectCollection findByClientId(string $stchan_client_id) Return ChildChannel objects filtered by the stchan_client_id column
 * @method     ChildChannel[]|ObjectCollection findBySecretId(string $stchan_secred_id) Return ChildChannel objects filtered by the stchan_secred_id column
 * @method     ChildChannel[]|ObjectCollection findByChannelPartner(boolean $stchan_channel_partner) Return ChildChannel objects filtered by the stchan_channel_partner column
 * @method     ChildChannel[]|ObjectCollection findByLiveMode(int $stchan_live_mode) Return ChildChannel objects filtered by the stchan_live_mode column
 * @method     ChildChannel[]|ObjectCollection findByStatus(string $stchan_status) Return ChildChannel objects filtered by the stchan_status column
 * @method     ChildChannel[]|ObjectCollection findByTwitchGameId(int $stchan_vgatga_id) Return ChildChannel objects filtered by the stchan_vgatga_id column
 * @method     ChildChannel[]|ObjectCollection findByGame(string $stchan_game) Return ChildChannel objects filtered by the stchan_game column
 * @method     ChildChannel[]|ObjectCollection findByStreamType(int $stchan_stream_type) Return ChildChannel objects filtered by the stchan_stream_type column
 * @method     ChildChannel[]|ObjectCollection findByFollowersCount(int $stchan_followers_count) Return ChildChannel objects filtered by the stchan_followers_count column
 * @method     ChildChannel[]|ObjectCollection findByFollowersDiff(int $stchan_followers_diff) Return ChildChannel objects filtered by the stchan_followers_diff column
 * @method     ChildChannel[]|ObjectCollection findByViewsCount(int $stchan_views_count) Return ChildChannel objects filtered by the stchan_views_count column
 * @method     ChildChannel[]|ObjectCollection findByViewsDiff(int $stchan_views_diff) Return ChildChannel objects filtered by the stchan_views_diff column
 * @method     ChildChannel[]|ObjectCollection findByViewersCount(int $stchan_viewers_count) Return ChildChannel objects filtered by the stchan_viewers_count column
 * @method     ChildChannel[]|ObjectCollection findByChattersCount(int $stchan_chatters_count) Return ChildChannel objects filtered by the stchan_chatters_count column
 * @method     ChildChannel[]|ObjectCollection findByHostsCount(int $stchan_hosts_count) Return ChildChannel objects filtered by the stchan_hosts_count column
 * @method     ChildChannel[]|ObjectCollection findByLiveViewersCount(int $stchan_live_viewers_count) Return ChildChannel objects filtered by the stchan_live_viewers_count column
 * @method     ChildChannel[]|ObjectCollection findByMessagesCount(int $stchan_messages_count) Return ChildChannel objects filtered by the stchan_messages_count column
 * @method     ChildChannel[]|ObjectCollection findByLockChattersScan(boolean $stchan_lock_chatters_scan) Return ChildChannel objects filtered by the stchan_lock_chatters_scan column
 * @method     ChildChannel[]|ObjectCollection findByLockHostsScan(boolean $stchan_lock_hosts_scan) Return ChildChannel objects filtered by the stchan_lock_hosts_scan column
 * @method     ChildChannel[]|ObjectCollection findByCreatedByUserId(int $stchan_created_by_user_id) Return ChildChannel objects filtered by the stchan_created_by_user_id column
 * @method     ChildChannel[]|ObjectCollection findByUpdatedByUserId(int $stchan_updated_by_user_id) Return ChildChannel objects filtered by the stchan_updated_by_user_id column
 * @method     ChildChannel[]|ObjectCollection findByCreatedAt(string $stchan_created_at) Return ChildChannel objects filtered by the stchan_created_at column
 * @method     ChildChannel[]|ObjectCollection findByUpdatedAt(string $stchan_updated_at) Return ChildChannel objects filtered by the stchan_updated_at column
 * @method     ChildChannel[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChannelQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ChannelQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Channel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChannelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChannelQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChannelQuery) {
            return $criteria;
        }
        $query = new ChildChannelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChannel|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChannelTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChannelTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannel A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stchan_id, stchan_ststrm_id, stchan_stsite_id, stchan_channel, stchan_ststrm_bot_id, stchan_can_scan, stchan_password_oauth, stchan_channel_id, stchan_client_id, stchan_secred_id, stchan_channel_partner, stchan_live_mode, stchan_status, stchan_vgatga_id, stchan_game, stchan_stream_type, stchan_followers_count, stchan_followers_diff, stchan_views_count, stchan_views_diff, stchan_viewers_count, stchan_chatters_count, stchan_hosts_count, stchan_live_viewers_count, stchan_messages_count, stchan_lock_chatters_scan, stchan_lock_hosts_scan, stchan_created_by_user_id, stchan_updated_by_user_id, stchan_created_at, stchan_updated_at FROM stream_channel_stchan WHERE stchan_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChannel $obj */
            $obj = new ChildChannel();
            $obj->hydrate($row);
            ChannelTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChannel|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stchan_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stchan_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stchan_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stchan_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stchan_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stchan_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stchan_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stchan_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stchan_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stchan_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stchan_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stchan_channel column
     *
     * Example usage:
     * <code>
     * $query->filterByChannel('fooValue');   // WHERE stchan_channel = 'fooValue'
     * $query->filterByChannel('%fooValue%', Criteria::LIKE); // WHERE stchan_channel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $channel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByChannel($channel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($channel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHANNEL, $channel, $comparison);
    }

    /**
     * Filter the query on the stchan_ststrm_bot_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBotId(1234); // WHERE stchan_ststrm_bot_id = 1234
     * $query->filterByBotId(array(12, 34)); // WHERE stchan_ststrm_bot_id IN (12, 34)
     * $query->filterByBotId(array('min' => 12)); // WHERE stchan_ststrm_bot_id > 12
     * </code>
     *
     * @see       filterByBot()
     *
     * @param     mixed $botId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByBotId($botId = null, $comparison = null)
    {
        if (is_array($botId)) {
            $useMinMax = false;
            if (isset($botId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $botId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($botId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $botId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $botId, $comparison);
    }

    /**
     * Filter the query on the stchan_can_scan column
     *
     * Example usage:
     * <code>
     * $query->filterByCanScan(true); // WHERE stchan_can_scan = true
     * $query->filterByCanScan('yes'); // WHERE stchan_can_scan = true
     * </code>
     *
     * @param     boolean|string $canScan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByCanScan($canScan = null, $comparison = null)
    {
        if (is_string($canScan)) {
            $canScan = in_array(strtolower($canScan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CAN_SCAN, $canScan, $comparison);
    }

    /**
     * Filter the query on the stchan_password_oauth column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordOauth('fooValue');   // WHERE stchan_password_oauth = 'fooValue'
     * $query->filterByPasswordOauth('%fooValue%', Criteria::LIKE); // WHERE stchan_password_oauth LIKE '%fooValue%'
     * </code>
     *
     * @param     string $passwordOauth The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByPasswordOauth($passwordOauth = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($passwordOauth)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH, $passwordOauth, $comparison);
    }

    /**
     * Filter the query on the stchan_channel_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stchan_channel_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stchan_channel_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stchan_channel_id > 12
     * </code>
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHANNEL_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHANNEL_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHANNEL_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stchan_client_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClientId('fooValue');   // WHERE stchan_client_id = 'fooValue'
     * $query->filterByClientId('%fooValue%', Criteria::LIKE); // WHERE stchan_client_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $clientId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByClientId($clientId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($clientId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CLIENT_ID, $clientId, $comparison);
    }

    /**
     * Filter the query on the stchan_secred_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySecretId('fooValue');   // WHERE stchan_secred_id = 'fooValue'
     * $query->filterBySecretId('%fooValue%', Criteria::LIKE); // WHERE stchan_secred_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $secretId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterBySecretId($secretId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secretId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_SECRED_ID, $secretId, $comparison);
    }

    /**
     * Filter the query on the stchan_channel_partner column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelPartner(true); // WHERE stchan_channel_partner = true
     * $query->filterByChannelPartner('yes'); // WHERE stchan_channel_partner = true
     * </code>
     *
     * @param     boolean|string $channelPartner The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByChannelPartner($channelPartner = null, $comparison = null)
    {
        if (is_string($channelPartner)) {
            $channelPartner = in_array(strtolower($channelPartner), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER, $channelPartner, $comparison);
    }

    /**
     * Filter the query on the stchan_live_mode column
     *
     * @param     mixed $liveMode The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByLiveMode($liveMode = null, $comparison = null)
    {
        $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_LIVE_MODE);
        if (is_scalar($liveMode)) {
            if (!in_array($liveMode, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $liveMode));
            }
            $liveMode = array_search($liveMode, $valueSet);
        } elseif (is_array($liveMode)) {
            $convertedValues = array();
            foreach ($liveMode as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $liveMode = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LIVE_MODE, $liveMode, $comparison);
    }

    /**
     * Filter the query on the stchan_status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE stchan_status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE stchan_status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the stchan_vgatga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTwitchGameId(1234); // WHERE stchan_vgatga_id = 1234
     * $query->filterByTwitchGameId(array(12, 34)); // WHERE stchan_vgatga_id IN (12, 34)
     * $query->filterByTwitchGameId(array('min' => 12)); // WHERE stchan_vgatga_id > 12
     * </code>
     *
     * @see       filterByApiTwitchGame()
     *
     * @param     mixed $twitchGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByTwitchGameId($twitchGameId = null, $comparison = null)
    {
        if (is_array($twitchGameId)) {
            $useMinMax = false;
            if (isset($twitchGameId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($twitchGameId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VGATGA_ID, $twitchGameId, $comparison);
    }

    /**
     * Filter the query on the stchan_game column
     *
     * Example usage:
     * <code>
     * $query->filterByGame('fooValue');   // WHERE stchan_game = 'fooValue'
     * $query->filterByGame('%fooValue%', Criteria::LIKE); // WHERE stchan_game LIKE '%fooValue%'
     * </code>
     *
     * @param     string $game The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByGame($game = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($game)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_GAME, $game, $comparison);
    }

    /**
     * Filter the query on the stchan_stream_type column
     *
     * @param     mixed $streamType The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStreamType($streamType = null, $comparison = null)
    {
        $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_STREAM_TYPE);
        if (is_scalar($streamType)) {
            if (!in_array($streamType, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $streamType));
            }
            $streamType = array_search($streamType, $valueSet);
        } elseif (is_array($streamType)) {
            $convertedValues = array();
            foreach ($streamType as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $streamType = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_STREAM_TYPE, $streamType, $comparison);
    }

    /**
     * Filter the query on the stchan_followers_count column
     *
     * Example usage:
     * <code>
     * $query->filterByFollowersCount(1234); // WHERE stchan_followers_count = 1234
     * $query->filterByFollowersCount(array(12, 34)); // WHERE stchan_followers_count IN (12, 34)
     * $query->filterByFollowersCount(array('min' => 12)); // WHERE stchan_followers_count > 12
     * </code>
     *
     * @param     mixed $followersCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByFollowersCount($followersCount = null, $comparison = null)
    {
        if (is_array($followersCount)) {
            $useMinMax = false;
            if (isset($followersCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT, $followersCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($followersCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT, $followersCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT, $followersCount, $comparison);
    }

    /**
     * Filter the query on the stchan_followers_diff column
     *
     * Example usage:
     * <code>
     * $query->filterByFollowersDiff(1234); // WHERE stchan_followers_diff = 1234
     * $query->filterByFollowersDiff(array(12, 34)); // WHERE stchan_followers_diff IN (12, 34)
     * $query->filterByFollowersDiff(array('min' => 12)); // WHERE stchan_followers_diff > 12
     * </code>
     *
     * @param     mixed $followersDiff The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByFollowersDiff($followersDiff = null, $comparison = null)
    {
        if (is_array($followersDiff)) {
            $useMinMax = false;
            if (isset($followersDiff['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF, $followersDiff['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($followersDiff['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF, $followersDiff['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF, $followersDiff, $comparison);
    }

    /**
     * Filter the query on the stchan_views_count column
     *
     * Example usage:
     * <code>
     * $query->filterByViewsCount(1234); // WHERE stchan_views_count = 1234
     * $query->filterByViewsCount(array(12, 34)); // WHERE stchan_views_count IN (12, 34)
     * $query->filterByViewsCount(array('min' => 12)); // WHERE stchan_views_count > 12
     * </code>
     *
     * @param     mixed $viewsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByViewsCount($viewsCount = null, $comparison = null)
    {
        if (is_array($viewsCount)) {
            $useMinMax = false;
            if (isset($viewsCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_COUNT, $viewsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($viewsCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_COUNT, $viewsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_COUNT, $viewsCount, $comparison);
    }

    /**
     * Filter the query on the stchan_views_diff column
     *
     * Example usage:
     * <code>
     * $query->filterByViewsDiff(1234); // WHERE stchan_views_diff = 1234
     * $query->filterByViewsDiff(array(12, 34)); // WHERE stchan_views_diff IN (12, 34)
     * $query->filterByViewsDiff(array('min' => 12)); // WHERE stchan_views_diff > 12
     * </code>
     *
     * @param     mixed $viewsDiff The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByViewsDiff($viewsDiff = null, $comparison = null)
    {
        if (is_array($viewsDiff)) {
            $useMinMax = false;
            if (isset($viewsDiff['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_DIFF, $viewsDiff['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($viewsDiff['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_DIFF, $viewsDiff['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWS_DIFF, $viewsDiff, $comparison);
    }

    /**
     * Filter the query on the stchan_viewers_count column
     *
     * Example usage:
     * <code>
     * $query->filterByViewersCount(1234); // WHERE stchan_viewers_count = 1234
     * $query->filterByViewersCount(array(12, 34)); // WHERE stchan_viewers_count IN (12, 34)
     * $query->filterByViewersCount(array('min' => 12)); // WHERE stchan_viewers_count > 12
     * </code>
     *
     * @param     mixed $viewersCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByViewersCount($viewersCount = null, $comparison = null)
    {
        if (is_array($viewersCount)) {
            $useMinMax = false;
            if (isset($viewersCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT, $viewersCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($viewersCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT, $viewersCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT, $viewersCount, $comparison);
    }

    /**
     * Filter the query on the stchan_chatters_count column
     *
     * Example usage:
     * <code>
     * $query->filterByChattersCount(1234); // WHERE stchan_chatters_count = 1234
     * $query->filterByChattersCount(array(12, 34)); // WHERE stchan_chatters_count IN (12, 34)
     * $query->filterByChattersCount(array('min' => 12)); // WHERE stchan_chatters_count > 12
     * </code>
     *
     * @param     mixed $chattersCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByChattersCount($chattersCount = null, $comparison = null)
    {
        if (is_array($chattersCount)) {
            $useMinMax = false;
            if (isset($chattersCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT, $chattersCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chattersCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT, $chattersCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT, $chattersCount, $comparison);
    }

    /**
     * Filter the query on the stchan_hosts_count column
     *
     * Example usage:
     * <code>
     * $query->filterByHostsCount(1234); // WHERE stchan_hosts_count = 1234
     * $query->filterByHostsCount(array(12, 34)); // WHERE stchan_hosts_count IN (12, 34)
     * $query->filterByHostsCount(array('min' => 12)); // WHERE stchan_hosts_count > 12
     * </code>
     *
     * @param     mixed $hostsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByHostsCount($hostsCount = null, $comparison = null)
    {
        if (is_array($hostsCount)) {
            $useMinMax = false;
            if (isset($hostsCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_HOSTS_COUNT, $hostsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hostsCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_HOSTS_COUNT, $hostsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_HOSTS_COUNT, $hostsCount, $comparison);
    }

    /**
     * Filter the query on the stchan_live_viewers_count column
     *
     * Example usage:
     * <code>
     * $query->filterByLiveViewersCount(1234); // WHERE stchan_live_viewers_count = 1234
     * $query->filterByLiveViewersCount(array(12, 34)); // WHERE stchan_live_viewers_count IN (12, 34)
     * $query->filterByLiveViewersCount(array('min' => 12)); // WHERE stchan_live_viewers_count > 12
     * </code>
     *
     * @param     mixed $liveViewersCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByLiveViewersCount($liveViewersCount = null, $comparison = null)
    {
        if (is_array($liveViewersCount)) {
            $useMinMax = false;
            if (isset($liveViewersCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT, $liveViewersCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($liveViewersCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT, $liveViewersCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT, $liveViewersCount, $comparison);
    }

    /**
     * Filter the query on the stchan_messages_count column
     *
     * Example usage:
     * <code>
     * $query->filterByMessagesCount(1234); // WHERE stchan_messages_count = 1234
     * $query->filterByMessagesCount(array(12, 34)); // WHERE stchan_messages_count IN (12, 34)
     * $query->filterByMessagesCount(array('min' => 12)); // WHERE stchan_messages_count > 12
     * </code>
     *
     * @param     mixed $messagesCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByMessagesCount($messagesCount = null, $comparison = null)
    {
        if (is_array($messagesCount)) {
            $useMinMax = false;
            if (isset($messagesCount['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT, $messagesCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($messagesCount['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT, $messagesCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT, $messagesCount, $comparison);
    }

    /**
     * Filter the query on the stchan_lock_chatters_scan column
     *
     * Example usage:
     * <code>
     * $query->filterByLockChattersScan(true); // WHERE stchan_lock_chatters_scan = true
     * $query->filterByLockChattersScan('yes'); // WHERE stchan_lock_chatters_scan = true
     * </code>
     *
     * @param     boolean|string $lockChattersScan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByLockChattersScan($lockChattersScan = null, $comparison = null)
    {
        if (is_string($lockChattersScan)) {
            $lockChattersScan = in_array(strtolower($lockChattersScan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN, $lockChattersScan, $comparison);
    }

    /**
     * Filter the query on the stchan_lock_hosts_scan column
     *
     * Example usage:
     * <code>
     * $query->filterByLockHostsScan(true); // WHERE stchan_lock_hosts_scan = true
     * $query->filterByLockHostsScan('yes'); // WHERE stchan_lock_hosts_scan = true
     * </code>
     *
     * @param     boolean|string $lockHostsScan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByLockHostsScan($lockHostsScan = null, $comparison = null)
    {
        if (is_string($lockHostsScan)) {
            $lockHostsScan = in_array(strtolower($lockHostsScan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN, $lockHostsScan, $comparison);
    }

    /**
     * Filter the query on the stchan_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE stchan_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE stchan_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE stchan_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the stchan_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE stchan_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE stchan_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE stchan_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the stchan_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stchan_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stchan_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stchan_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stchan_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stchan_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stchan_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stchan_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByBot($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBot() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinBot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bot');
        }

        return $this;
    }

    /**
     * Use the Bot relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useBotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bot', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiTwitchGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiTwitchGame|ObjectCollection $apiTwitchGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByApiTwitchGame($apiTwitchGame, $comparison = null)
    {
        if ($apiTwitchGame instanceof \IiMedias\VideoGamesBundle\Model\ApiTwitchGame) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_VGATGA_ID, $apiTwitchGame->getId(), $comparison);
        } elseif ($apiTwitchGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_VGATGA_ID, $apiTwitchGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiTwitchGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiTwitchGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiTwitchGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinApiTwitchGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiTwitchGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiTwitchGame');
        }

        return $this;
    }

    /**
     * Use the ApiTwitchGame relation ApiTwitchGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery A secondary query class using the current class as primary query
     */
    public function useApiTwitchGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiTwitchGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiTwitchGame', '\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $userExperience->getChannelId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\DeepBotImportExperience object
     *
     * @param \IiMedias\StreamBundle\Model\DeepBotImportExperience|ObjectCollection $deepBotImportExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByDeepBotImportExperience($deepBotImportExperience, $comparison = null)
    {
        if ($deepBotImportExperience instanceof \IiMedias\StreamBundle\Model\DeepBotImportExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $deepBotImportExperience->getChannelId(), $comparison);
        } elseif ($deepBotImportExperience instanceof ObjectCollection) {
            return $this
                ->useDeepBotImportExperienceQuery()
                ->filterByPrimaryKeys($deepBotImportExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDeepBotImportExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\DeepBotImportExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeepBotImportExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinDeepBotImportExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeepBotImportExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeepBotImportExperience');
        }

        return $this;
    }

    /**
     * Use the DeepBotImportExperience relation DeepBotImportExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery A secondary query class using the current class as primary query
     */
    public function useDeepBotImportExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDeepBotImportExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeepBotImportExperience', '\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\MessageExperience object
     *
     * @param \IiMedias\StreamBundle\Model\MessageExperience|ObjectCollection $messageExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByMessageExperience($messageExperience, $comparison = null)
    {
        if ($messageExperience instanceof \IiMedias\StreamBundle\Model\MessageExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $messageExperience->getChannelId(), $comparison);
        } elseif ($messageExperience instanceof ObjectCollection) {
            return $this
                ->useMessageExperienceQuery()
                ->filterByPrimaryKeys($messageExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\MessageExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinMessageExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageExperience');
        }

        return $this;
    }

    /**
     * Use the MessageExperience relation MessageExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\MessageExperienceQuery A secondary query class using the current class as primary query
     */
    public function useMessageExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageExperience', '\IiMedias\StreamBundle\Model\MessageExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatterExperience object
     *
     * @param \IiMedias\StreamBundle\Model\ChatterExperience|ObjectCollection $chatterExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByChatterExperience($chatterExperience, $comparison = null)
    {
        if ($chatterExperience instanceof \IiMedias\StreamBundle\Model\ChatterExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $chatterExperience->getChannelId(), $comparison);
        } elseif ($chatterExperience instanceof ObjectCollection) {
            return $this
                ->useChatterExperienceQuery()
                ->filterByPrimaryKeys($chatterExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatterExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatterExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatterExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinChatterExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatterExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatterExperience');
        }

        return $this;
    }

    /**
     * Use the ChatterExperience relation ChatterExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatterExperienceQuery A secondary query class using the current class as primary query
     */
    public function useChatterExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatterExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatterExperience', '\IiMedias\StreamBundle\Model\ChatterExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowExperience object
     *
     * @param \IiMedias\StreamBundle\Model\FollowExperience|ObjectCollection $followExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByFollowExperience($followExperience, $comparison = null)
    {
        if ($followExperience instanceof \IiMedias\StreamBundle\Model\FollowExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $followExperience->getChannelId(), $comparison);
        } elseif ($followExperience instanceof ObjectCollection) {
            return $this
                ->useFollowExperienceQuery()
                ->filterByPrimaryKeys($followExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinFollowExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowExperience');
        }

        return $this;
    }

    /**
     * Use the FollowExperience relation FollowExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowExperienceQuery A secondary query class using the current class as primary query
     */
    public function useFollowExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowExperience', '\IiMedias\StreamBundle\Model\FollowExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\HostExperience object
     *
     * @param \IiMedias\StreamBundle\Model\HostExperience|ObjectCollection $hostExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByHostExperience($hostExperience, $comparison = null)
    {
        if ($hostExperience instanceof \IiMedias\StreamBundle\Model\HostExperience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $hostExperience->getChannelId(), $comparison);
        } elseif ($hostExperience instanceof ObjectCollection) {
            return $this
                ->useHostExperienceQuery()
                ->filterByPrimaryKeys($hostExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHostExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\HostExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HostExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinHostExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HostExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HostExperience');
        }

        return $this;
    }

    /**
     * Use the HostExperience relation HostExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\HostExperienceQuery A secondary query class using the current class as primary query
     */
    public function useHostExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinHostExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HostExperience', '\IiMedias\StreamBundle\Model\HostExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewDiffData|ObjectCollection $viewDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByViewDiffData($viewDiffData, $comparison = null)
    {
        if ($viewDiffData instanceof \IiMedias\StreamBundle\Model\ViewDiffData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $viewDiffData->getChannelId(), $comparison);
        } elseif ($viewDiffData instanceof ObjectCollection) {
            return $this
                ->useViewDiffDataQuery()
                ->filterByPrimaryKeys($viewDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinViewDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewDiffData');
        }

        return $this;
    }

    /**
     * Use the ViewDiffData relation ViewDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useViewDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewDiffData', '\IiMedias\StreamBundle\Model\ViewDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewerData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewerData|ObjectCollection $viewerData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByViewerData($viewerData, $comparison = null)
    {
        if ($viewerData instanceof \IiMedias\StreamBundle\Model\ViewerData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $viewerData->getChannelId(), $comparison);
        } elseif ($viewerData instanceof ObjectCollection) {
            return $this
                ->useViewerDataQuery()
                ->filterByPrimaryKeys($viewerData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewerData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewerData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewerData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinViewerData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewerData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewerData');
        }

        return $this;
    }

    /**
     * Use the ViewerData relation ViewerData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewerDataQuery A secondary query class using the current class as primary query
     */
    public function useViewerDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewerData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewerData', '\IiMedias\StreamBundle\Model\ViewerDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\FollowDiffData|ObjectCollection $followDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByFollowDiffData($followDiffData, $comparison = null)
    {
        if ($followDiffData instanceof \IiMedias\StreamBundle\Model\FollowDiffData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $followDiffData->getChannelId(), $comparison);
        } elseif ($followDiffData instanceof ObjectCollection) {
            return $this
                ->useFollowDiffDataQuery()
                ->filterByPrimaryKeys($followDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinFollowDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowDiffData');
        }

        return $this;
    }

    /**
     * Use the FollowDiffData relation FollowDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useFollowDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowDiffData', '\IiMedias\StreamBundle\Model\FollowDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\StatusData object
     *
     * @param \IiMedias\StreamBundle\Model\StatusData|ObjectCollection $statusData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStatusData($statusData, $comparison = null)
    {
        if ($statusData instanceof \IiMedias\StreamBundle\Model\StatusData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $statusData->getChannelId(), $comparison);
        } elseif ($statusData instanceof ObjectCollection) {
            return $this
                ->useStatusDataQuery()
                ->filterByPrimaryKeys($statusData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStatusData() only accepts arguments of type \IiMedias\StreamBundle\Model\StatusData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinStatusData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusData');
        }

        return $this;
    }

    /**
     * Use the StatusData relation StatusData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatusDataQuery A secondary query class using the current class as primary query
     */
    public function useStatusDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusData', '\IiMedias\StreamBundle\Model\StatusDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\TypeData object
     *
     * @param \IiMedias\StreamBundle\Model\TypeData|ObjectCollection $typeData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByTypeData($typeData, $comparison = null)
    {
        if ($typeData instanceof \IiMedias\StreamBundle\Model\TypeData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $typeData->getChannelId(), $comparison);
        } elseif ($typeData instanceof ObjectCollection) {
            return $this
                ->useTypeDataQuery()
                ->filterByPrimaryKeys($typeData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTypeData() only accepts arguments of type \IiMedias\StreamBundle\Model\TypeData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TypeData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinTypeData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TypeData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TypeData');
        }

        return $this;
    }

    /**
     * Use the TypeData relation TypeData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\TypeDataQuery A secondary query class using the current class as primary query
     */
    public function useTypeDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTypeData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TypeData', '\IiMedias\StreamBundle\Model\TypeDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\GameData object
     *
     * @param \IiMedias\StreamBundle\Model\GameData|ObjectCollection $gameData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByGameData($gameData, $comparison = null)
    {
        if ($gameData instanceof \IiMedias\StreamBundle\Model\GameData) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $gameData->getChannelId(), $comparison);
        } elseif ($gameData instanceof ObjectCollection) {
            return $this
                ->useGameDataQuery()
                ->filterByPrimaryKeys($gameData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameData() only accepts arguments of type \IiMedias\StreamBundle\Model\GameData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinGameData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameData');
        }

        return $this;
    }

    /**
     * Use the GameData relation GameData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\GameDataQuery A secondary query class using the current class as primary query
     */
    public function useGameDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameData', '\IiMedias\StreamBundle\Model\GameDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stat object
     *
     * @param \IiMedias\StreamBundle\Model\Stat|ObjectCollection $stat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByStat($stat, $comparison = null)
    {
        if ($stat instanceof \IiMedias\StreamBundle\Model\Stat) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $stat->getChannelId(), $comparison);
        } elseif ($stat instanceof ObjectCollection) {
            return $this
                ->useStatQuery()
                ->filterByPrimaryKeys($stat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStat() only accepts arguments of type \IiMedias\StreamBundle\Model\Stat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinStat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stat');
        }

        return $this;
    }

    /**
     * Use the Stat relation Stat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatQuery A secondary query class using the current class as primary query
     */
    public function useStatQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stat', '\IiMedias\StreamBundle\Model\StatQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Experience object
     *
     * @param \IiMedias\StreamBundle\Model\Experience|ObjectCollection $experience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChannelQuery The current query, for fluid interface
     */
    public function filterByExperience($experience, $comparison = null)
    {
        if ($experience instanceof \IiMedias\StreamBundle\Model\Experience) {
            return $this
                ->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $experience->getChannelId(), $comparison);
        } elseif ($experience instanceof ObjectCollection) {
            return $this
                ->useExperienceQuery()
                ->filterByPrimaryKeys($experience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\Experience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Experience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function joinExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Experience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Experience');
        }

        return $this;
    }

    /**
     * Use the Experience relation Experience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ExperienceQuery A secondary query class using the current class as primary query
     */
    public function useExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Experience', '\IiMedias\StreamBundle\Model\ExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChannel $channel Object to remove from the list of results
     *
     * @return $this|ChildChannelQuery The current query, for fluid interface
     */
    public function prune($channel = null)
    {
        if ($channel) {
            $this->addUsingAlias(ChannelTableMap::COL_STCHAN_ID, $channel->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_channel_stchan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChannelTableMap::clearInstancePool();
            ChannelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChannelTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChannelTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChannelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChannelTableMap::COL_STCHAN_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChannelTableMap::COL_STCHAN_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ChannelTableMap::COL_STCHAN_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ChannelTableMap::COL_STCHAN_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildChannelQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ChannelTableMap::COL_STCHAN_CREATED_AT);
    }

} // ChannelQuery

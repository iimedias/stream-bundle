<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\DeepBotImportExperience as ChildDeepBotImportExperience;
use IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery as ChildDeepBotImportExperienceQuery;
use IiMedias\StreamBundle\Model\Map\DeepBotImportExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_deepbot_import_experience_stdbie' table.
 *
 *
 *
 * @method     ChildDeepBotImportExperienceQuery orderById($order = Criteria::ASC) Order by the stdbie_id column
 * @method     ChildDeepBotImportExperienceQuery orderByStreamId($order = Criteria::ASC) Order by the stdbie_ststrm_id column
 * @method     ChildDeepBotImportExperienceQuery orderByChannelId($order = Criteria::ASC) Order by the stdbie_stchan_id column
 * @method     ChildDeepBotImportExperienceQuery orderBySiteId($order = Criteria::ASC) Order by the stdbie_stsite_id column
 * @method     ChildDeepBotImportExperienceQuery orderByChatUserId($order = Criteria::ASC) Order by the stdbie_stcusr_id column
 * @method     ChildDeepBotImportExperienceQuery orderByExp($order = Criteria::ASC) Order by the stdbie_exp column
 * @method     ChildDeepBotImportExperienceQuery orderByTime($order = Criteria::ASC) Order by the stdbie_time column
 * @method     ChildDeepBotImportExperienceQuery orderByJoinAt($order = Criteria::ASC) Order by the stdbie_join_at column
 * @method     ChildDeepBotImportExperienceQuery orderByLastSeenAt($order = Criteria::ASC) Order by the stdbie_last_seen_at column
 *
 * @method     ChildDeepBotImportExperienceQuery groupById() Group by the stdbie_id column
 * @method     ChildDeepBotImportExperienceQuery groupByStreamId() Group by the stdbie_ststrm_id column
 * @method     ChildDeepBotImportExperienceQuery groupByChannelId() Group by the stdbie_stchan_id column
 * @method     ChildDeepBotImportExperienceQuery groupBySiteId() Group by the stdbie_stsite_id column
 * @method     ChildDeepBotImportExperienceQuery groupByChatUserId() Group by the stdbie_stcusr_id column
 * @method     ChildDeepBotImportExperienceQuery groupByExp() Group by the stdbie_exp column
 * @method     ChildDeepBotImportExperienceQuery groupByTime() Group by the stdbie_time column
 * @method     ChildDeepBotImportExperienceQuery groupByJoinAt() Group by the stdbie_join_at column
 * @method     ChildDeepBotImportExperienceQuery groupByLastSeenAt() Group by the stdbie_last_seen_at column
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDeepBotImportExperienceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDeepBotImportExperienceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDeepBotImportExperienceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDeepBotImportExperienceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildDeepBotImportExperienceQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildDeepBotImportExperienceQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildDeepBotImportExperienceQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildDeepBotImportExperienceQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildDeepBotImportExperienceQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildDeepBotImportExperienceQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildDeepBotImportExperienceQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDeepBotImportExperience findOne(ConnectionInterface $con = null) Return the first ChildDeepBotImportExperience matching the query
 * @method     ChildDeepBotImportExperience findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDeepBotImportExperience matching the query, or a new ChildDeepBotImportExperience object populated from the query conditions when no match is found
 *
 * @method     ChildDeepBotImportExperience findOneById(int $stdbie_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_id column
 * @method     ChildDeepBotImportExperience findOneByStreamId(int $stdbie_ststrm_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_ststrm_id column
 * @method     ChildDeepBotImportExperience findOneByChannelId(int $stdbie_stchan_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stchan_id column
 * @method     ChildDeepBotImportExperience findOneBySiteId(int $stdbie_stsite_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stsite_id column
 * @method     ChildDeepBotImportExperience findOneByChatUserId(int $stdbie_stcusr_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stcusr_id column
 * @method     ChildDeepBotImportExperience findOneByExp(int $stdbie_exp) Return the first ChildDeepBotImportExperience filtered by the stdbie_exp column
 * @method     ChildDeepBotImportExperience findOneByTime(double $stdbie_time) Return the first ChildDeepBotImportExperience filtered by the stdbie_time column
 * @method     ChildDeepBotImportExperience findOneByJoinAt(string $stdbie_join_at) Return the first ChildDeepBotImportExperience filtered by the stdbie_join_at column
 * @method     ChildDeepBotImportExperience findOneByLastSeenAt(string $stdbie_last_seen_at) Return the first ChildDeepBotImportExperience filtered by the stdbie_last_seen_at column *

 * @method     ChildDeepBotImportExperience requirePk($key, ConnectionInterface $con = null) Return the ChildDeepBotImportExperience by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOne(ConnectionInterface $con = null) Return the first ChildDeepBotImportExperience matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeepBotImportExperience requireOneById(int $stdbie_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByStreamId(int $stdbie_ststrm_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByChannelId(int $stdbie_stchan_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneBySiteId(int $stdbie_stsite_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByChatUserId(int $stdbie_stcusr_id) Return the first ChildDeepBotImportExperience filtered by the stdbie_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByExp(int $stdbie_exp) Return the first ChildDeepBotImportExperience filtered by the stdbie_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByTime(double $stdbie_time) Return the first ChildDeepBotImportExperience filtered by the stdbie_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByJoinAt(string $stdbie_join_at) Return the first ChildDeepBotImportExperience filtered by the stdbie_join_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeepBotImportExperience requireOneByLastSeenAt(string $stdbie_last_seen_at) Return the first ChildDeepBotImportExperience filtered by the stdbie_last_seen_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeepBotImportExperience[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDeepBotImportExperience objects based on current ModelCriteria
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findById(int $stdbie_id) Return ChildDeepBotImportExperience objects filtered by the stdbie_id column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByStreamId(int $stdbie_ststrm_id) Return ChildDeepBotImportExperience objects filtered by the stdbie_ststrm_id column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByChannelId(int $stdbie_stchan_id) Return ChildDeepBotImportExperience objects filtered by the stdbie_stchan_id column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findBySiteId(int $stdbie_stsite_id) Return ChildDeepBotImportExperience objects filtered by the stdbie_stsite_id column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByChatUserId(int $stdbie_stcusr_id) Return ChildDeepBotImportExperience objects filtered by the stdbie_stcusr_id column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByExp(int $stdbie_exp) Return ChildDeepBotImportExperience objects filtered by the stdbie_exp column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByTime(double $stdbie_time) Return ChildDeepBotImportExperience objects filtered by the stdbie_time column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByJoinAt(string $stdbie_join_at) Return ChildDeepBotImportExperience objects filtered by the stdbie_join_at column
 * @method     ChildDeepBotImportExperience[]|ObjectCollection findByLastSeenAt(string $stdbie_last_seen_at) Return ChildDeepBotImportExperience objects filtered by the stdbie_last_seen_at column
 * @method     ChildDeepBotImportExperience[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DeepBotImportExperienceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\DeepBotImportExperienceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDeepBotImportExperienceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDeepBotImportExperienceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDeepBotImportExperienceQuery) {
            return $criteria;
        }
        $query = new ChildDeepBotImportExperienceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDeepBotImportExperience|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DeepBotImportExperienceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DeepBotImportExperienceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeepBotImportExperience A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stdbie_id, stdbie_ststrm_id, stdbie_stchan_id, stdbie_stsite_id, stdbie_stcusr_id, stdbie_exp, stdbie_time, stdbie_join_at, stdbie_last_seen_at FROM stream_deepbot_import_experience_stdbie WHERE stdbie_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDeepBotImportExperience $obj */
            $obj = new ChildDeepBotImportExperience();
            $obj->hydrate($row);
            DeepBotImportExperienceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDeepBotImportExperience|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stdbie_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stdbie_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stdbie_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stdbie_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stdbie_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stdbie_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stdbie_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stdbie_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stdbie_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stdbie_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stdbie_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stdbie_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stdbie_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stdbie_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stdbie_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stdbie_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stdbie_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stdbie_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stdbie_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stdbie_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stdbie_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByExp(1234); // WHERE stdbie_exp = 1234
     * $query->filterByExp(array(12, 34)); // WHERE stdbie_exp IN (12, 34)
     * $query->filterByExp(array('min' => 12)); // WHERE stdbie_exp > 12
     * </code>
     *
     * @param     mixed $exp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByExp($exp = null, $comparison = null)
    {
        if (is_array($exp)) {
            $useMinMax = false;
            if (isset($exp['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_EXP, $exp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exp['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_EXP, $exp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_EXP, $exp, $comparison);
    }

    /**
     * Filter the query on the stdbie_time column
     *
     * Example usage:
     * <code>
     * $query->filterByTime(1234); // WHERE stdbie_time = 1234
     * $query->filterByTime(array(12, 34)); // WHERE stdbie_time IN (12, 34)
     * $query->filterByTime(array('min' => 12)); // WHERE stdbie_time > 12
     * </code>
     *
     * @param     mixed $time The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByTime($time = null, $comparison = null)
    {
        if (is_array($time)) {
            $useMinMax = false;
            if (isset($time['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_TIME, $time['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($time['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_TIME, $time['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_TIME, $time, $comparison);
    }

    /**
     * Filter the query on the stdbie_join_at column
     *
     * Example usage:
     * <code>
     * $query->filterByJoinAt('2011-03-14'); // WHERE stdbie_join_at = '2011-03-14'
     * $query->filterByJoinAt('now'); // WHERE stdbie_join_at = '2011-03-14'
     * $query->filterByJoinAt(array('max' => 'yesterday')); // WHERE stdbie_join_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $joinAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByJoinAt($joinAt = null, $comparison = null)
    {
        if (is_array($joinAt)) {
            $useMinMax = false;
            if (isset($joinAt['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT, $joinAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($joinAt['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT, $joinAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT, $joinAt, $comparison);
    }

    /**
     * Filter the query on the stdbie_last_seen_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSeenAt('2011-03-14'); // WHERE stdbie_last_seen_at = '2011-03-14'
     * $query->filterByLastSeenAt('now'); // WHERE stdbie_last_seen_at = '2011-03-14'
     * $query->filterByLastSeenAt(array('max' => 'yesterday')); // WHERE stdbie_last_seen_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSeenAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByLastSeenAt($lastSeenAt = null, $comparison = null)
    {
        if (is_array($lastSeenAt)) {
            $useMinMax = false;
            if (isset($lastSeenAt['min'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT, $lastSeenAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSeenAt['max'])) {
                $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT, $lastSeenAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT, $lastSeenAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDeepBotImportExperience $deepBotImportExperience Object to remove from the list of results
     *
     * @return $this|ChildDeepBotImportExperienceQuery The current query, for fluid interface
     */
    public function prune($deepBotImportExperience = null)
    {
        if ($deepBotImportExperience) {
            $this->addUsingAlias(DeepBotImportExperienceTableMap::COL_STDBIE_ID, $deepBotImportExperience->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_deepbot_import_experience_stdbie table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeepBotImportExperienceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DeepBotImportExperienceTableMap::clearInstancePool();
            DeepBotImportExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeepBotImportExperienceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DeepBotImportExperienceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DeepBotImportExperienceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DeepBotImportExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DeepBotImportExperienceQuery

<?php

namespace IiMedias\StreamBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\StreamBundle\Model\Channel as ChildChannel;
use IiMedias\StreamBundle\Model\ChannelQuery as ChildChannelQuery;
use IiMedias\StreamBundle\Model\ChatterExperience as ChildChatterExperience;
use IiMedias\StreamBundle\Model\ChatterExperienceQuery as ChildChatterExperienceQuery;
use IiMedias\StreamBundle\Model\DeepBotImportExperience as ChildDeepBotImportExperience;
use IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery as ChildDeepBotImportExperienceQuery;
use IiMedias\StreamBundle\Model\Experience as ChildExperience;
use IiMedias\StreamBundle\Model\ExperienceQuery as ChildExperienceQuery;
use IiMedias\StreamBundle\Model\FollowDiffData as ChildFollowDiffData;
use IiMedias\StreamBundle\Model\FollowDiffDataQuery as ChildFollowDiffDataQuery;
use IiMedias\StreamBundle\Model\FollowExperience as ChildFollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery as ChildFollowExperienceQuery;
use IiMedias\StreamBundle\Model\GameData as ChildGameData;
use IiMedias\StreamBundle\Model\GameDataQuery as ChildGameDataQuery;
use IiMedias\StreamBundle\Model\HostExperience as ChildHostExperience;
use IiMedias\StreamBundle\Model\HostExperienceQuery as ChildHostExperienceQuery;
use IiMedias\StreamBundle\Model\MessageExperience as ChildMessageExperience;
use IiMedias\StreamBundle\Model\MessageExperienceQuery as ChildMessageExperienceQuery;
use IiMedias\StreamBundle\Model\Site as ChildSite;
use IiMedias\StreamBundle\Model\SiteQuery as ChildSiteQuery;
use IiMedias\StreamBundle\Model\Stat as ChildStat;
use IiMedias\StreamBundle\Model\StatQuery as ChildStatQuery;
use IiMedias\StreamBundle\Model\StatusData as ChildStatusData;
use IiMedias\StreamBundle\Model\StatusDataQuery as ChildStatusDataQuery;
use IiMedias\StreamBundle\Model\Stream as ChildStream;
use IiMedias\StreamBundle\Model\StreamQuery as ChildStreamQuery;
use IiMedias\StreamBundle\Model\TypeData as ChildTypeData;
use IiMedias\StreamBundle\Model\TypeDataQuery as ChildTypeDataQuery;
use IiMedias\StreamBundle\Model\UserExperience as ChildUserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery as ChildUserExperienceQuery;
use IiMedias\StreamBundle\Model\ViewDiffData as ChildViewDiffData;
use IiMedias\StreamBundle\Model\ViewDiffDataQuery as ChildViewDiffDataQuery;
use IiMedias\StreamBundle\Model\ViewerData as ChildViewerData;
use IiMedias\StreamBundle\Model\ViewerDataQuery as ChildViewerDataQuery;
use IiMedias\StreamBundle\Model\Map\ChannelTableMap;
use IiMedias\StreamBundle\Model\Map\ChatterExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\DeepBotImportExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\ExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\FollowDiffDataTableMap;
use IiMedias\StreamBundle\Model\Map\FollowExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\GameDataTableMap;
use IiMedias\StreamBundle\Model\Map\HostExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\MessageExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\StatTableMap;
use IiMedias\StreamBundle\Model\Map\StatusDataTableMap;
use IiMedias\StreamBundle\Model\Map\TypeDataTableMap;
use IiMedias\StreamBundle\Model\Map\UserExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\ViewDiffDataTableMap;
use IiMedias\StreamBundle\Model\Map\ViewerDataTableMap;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'stream_channel_stchan' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class Channel implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\ChannelTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the stchan_id field.
     *
     * @var        int
     */
    protected $stchan_id;

    /**
     * The value for the stchan_ststrm_id field.
     *
     * @var        int
     */
    protected $stchan_ststrm_id;

    /**
     * The value for the stchan_stsite_id field.
     *
     * @var        int
     */
    protected $stchan_stsite_id;

    /**
     * The value for the stchan_channel field.
     *
     * @var        string
     */
    protected $stchan_channel;

    /**
     * The value for the stchan_ststrm_bot_id field.
     *
     * @var        int
     */
    protected $stchan_ststrm_bot_id;

    /**
     * The value for the stchan_can_scan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $stchan_can_scan;

    /**
     * The value for the stchan_password_oauth field.
     *
     * @var        string
     */
    protected $stchan_password_oauth;

    /**
     * The value for the stchan_channel_id field.
     *
     * @var        int
     */
    protected $stchan_channel_id;

    /**
     * The value for the stchan_client_id field.
     *
     * @var        string
     */
    protected $stchan_client_id;

    /**
     * The value for the stchan_secred_id field.
     *
     * @var        string
     */
    protected $stchan_secred_id;

    /**
     * The value for the stchan_channel_partner field.
     *
     * @var        boolean
     */
    protected $stchan_channel_partner;

    /**
     * The value for the stchan_live_mode field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_live_mode;

    /**
     * The value for the stchan_status field.
     *
     * @var        string
     */
    protected $stchan_status;

    /**
     * The value for the stchan_vgatga_id field.
     *
     * @var        int
     */
    protected $stchan_vgatga_id;

    /**
     * The value for the stchan_game field.
     *
     * @var        string
     */
    protected $stchan_game;

    /**
     * The value for the stchan_stream_type field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_stream_type;

    /**
     * The value for the stchan_followers_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_followers_count;

    /**
     * The value for the stchan_followers_diff field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_followers_diff;

    /**
     * The value for the stchan_views_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_views_count;

    /**
     * The value for the stchan_views_diff field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_views_diff;

    /**
     * The value for the stchan_viewers_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_viewers_count;

    /**
     * The value for the stchan_chatters_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_chatters_count;

    /**
     * The value for the stchan_hosts_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_hosts_count;

    /**
     * The value for the stchan_live_viewers_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_live_viewers_count;

    /**
     * The value for the stchan_messages_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stchan_messages_count;

    /**
     * The value for the stchan_lock_chatters_scan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $stchan_lock_chatters_scan;

    /**
     * The value for the stchan_lock_hosts_scan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $stchan_lock_hosts_scan;

    /**
     * The value for the stchan_created_by_user_id field.
     *
     * @var        int
     */
    protected $stchan_created_by_user_id;

    /**
     * The value for the stchan_updated_by_user_id field.
     *
     * @var        int
     */
    protected $stchan_updated_by_user_id;

    /**
     * The value for the stchan_created_at field.
     *
     * @var        DateTime
     */
    protected $stchan_created_at;

    /**
     * The value for the stchan_updated_at field.
     *
     * @var        DateTime
     */
    protected $stchan_updated_at;

    /**
     * @var        ChildStream
     */
    protected $aStream;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * @var        ChildStream
     */
    protected $aBot;

    /**
     * @var        ApiTwitchGame
     */
    protected $aApiTwitchGame;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildUserExperience[] Collection to store aggregation of ChildUserExperience objects.
     */
    protected $collUserExperiences;
    protected $collUserExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildDeepBotImportExperience[] Collection to store aggregation of ChildDeepBotImportExperience objects.
     */
    protected $collDeepBotImportExperiences;
    protected $collDeepBotImportExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildMessageExperience[] Collection to store aggregation of ChildMessageExperience objects.
     */
    protected $collMessageExperiences;
    protected $collMessageExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildChatterExperience[] Collection to store aggregation of ChildChatterExperience objects.
     */
    protected $collChatterExperiences;
    protected $collChatterExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildFollowExperience[] Collection to store aggregation of ChildFollowExperience objects.
     */
    protected $collFollowExperiences;
    protected $collFollowExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildHostExperience[] Collection to store aggregation of ChildHostExperience objects.
     */
    protected $collHostExperiences;
    protected $collHostExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildViewDiffData[] Collection to store aggregation of ChildViewDiffData objects.
     */
    protected $collViewDiffDatas;
    protected $collViewDiffDatasPartial;

    /**
     * @var        ObjectCollection|ChildViewerData[] Collection to store aggregation of ChildViewerData objects.
     */
    protected $collViewerDatas;
    protected $collViewerDatasPartial;

    /**
     * @var        ObjectCollection|ChildFollowDiffData[] Collection to store aggregation of ChildFollowDiffData objects.
     */
    protected $collFollowDiffDatas;
    protected $collFollowDiffDatasPartial;

    /**
     * @var        ObjectCollection|ChildStatusData[] Collection to store aggregation of ChildStatusData objects.
     */
    protected $collStatusDatas;
    protected $collStatusDatasPartial;

    /**
     * @var        ObjectCollection|ChildTypeData[] Collection to store aggregation of ChildTypeData objects.
     */
    protected $collTypeDatas;
    protected $collTypeDatasPartial;

    /**
     * @var        ObjectCollection|ChildGameData[] Collection to store aggregation of ChildGameData objects.
     */
    protected $collGameDatas;
    protected $collGameDatasPartial;

    /**
     * @var        ObjectCollection|ChildStat[] Collection to store aggregation of ChildStat objects.
     */
    protected $collStats;
    protected $collStatsPartial;

    /**
     * @var        ObjectCollection|ChildExperience[] Collection to store aggregation of ChildExperience objects.
     */
    protected $collExperiences;
    protected $collExperiencesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserExperience[]
     */
    protected $userExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDeepBotImportExperience[]
     */
    protected $deepBotImportExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMessageExperience[]
     */
    protected $messageExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChatterExperience[]
     */
    protected $chatterExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFollowExperience[]
     */
    protected $followExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildHostExperience[]
     */
    protected $hostExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildViewDiffData[]
     */
    protected $viewDiffDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildViewerData[]
     */
    protected $viewerDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFollowDiffData[]
     */
    protected $followDiffDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildStatusData[]
     */
    protected $statusDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTypeData[]
     */
    protected $typeDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildGameData[]
     */
    protected $gameDatasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildStat[]
     */
    protected $statsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildExperience[]
     */
    protected $experiencesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->stchan_can_scan = false;
        $this->stchan_live_mode = 0;
        $this->stchan_stream_type = 0;
        $this->stchan_followers_count = 0;
        $this->stchan_followers_diff = 0;
        $this->stchan_views_count = 0;
        $this->stchan_views_diff = 0;
        $this->stchan_viewers_count = 0;
        $this->stchan_chatters_count = 0;
        $this->stchan_hosts_count = 0;
        $this->stchan_live_viewers_count = 0;
        $this->stchan_messages_count = 0;
        $this->stchan_lock_chatters_scan = false;
        $this->stchan_lock_hosts_scan = false;
    }

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\Channel object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Channel</code> instance.  If
     * <code>obj</code> is an instance of <code>Channel</code>, delegates to
     * <code>equals(Channel)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Channel The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [stchan_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->stchan_id;
    }

    /**
     * Get the [stchan_ststrm_id] column value.
     *
     * @return int
     */
    public function getStreamId()
    {
        return $this->stchan_ststrm_id;
    }

    /**
     * Get the [stchan_stsite_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->stchan_stsite_id;
    }

    /**
     * Get the [stchan_channel] column value.
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->stchan_channel;
    }

    /**
     * Get the [stchan_ststrm_bot_id] column value.
     *
     * @return int
     */
    public function getBotId()
    {
        return $this->stchan_ststrm_bot_id;
    }

    /**
     * Get the [stchan_can_scan] column value.
     *
     * @return boolean
     */
    public function getCanScan()
    {
        return $this->stchan_can_scan;
    }

    /**
     * Get the [stchan_can_scan] column value.
     *
     * @return boolean
     */
    public function isCanScan()
    {
        return $this->getCanScan();
    }

    /**
     * Get the [stchan_password_oauth] column value.
     *
     * @return string
     */
    public function getPasswordOauth()
    {
        return $this->stchan_password_oauth;
    }

    /**
     * Get the [stchan_channel_id] column value.
     *
     * @return int
     */
    public function getChannelId()
    {
        return $this->stchan_channel_id;
    }

    /**
     * Get the [stchan_client_id] column value.
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->stchan_client_id;
    }

    /**
     * Get the [stchan_secred_id] column value.
     *
     * @return string
     */
    public function getSecretId()
    {
        return $this->stchan_secred_id;
    }

    /**
     * Get the [stchan_channel_partner] column value.
     *
     * @return boolean
     */
    public function getChannelPartner()
    {
        return $this->stchan_channel_partner;
    }

    /**
     * Get the [stchan_channel_partner] column value.
     *
     * @return boolean
     */
    public function isChannelPartner()
    {
        return $this->getChannelPartner();
    }

    /**
     * Get the [stchan_live_mode] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getLiveMode()
    {
        if (null === $this->stchan_live_mode) {
            return null;
        }
        $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_LIVE_MODE);
        if (!isset($valueSet[$this->stchan_live_mode])) {
            throw new PropelException('Unknown stored enum key: ' . $this->stchan_live_mode);
        }

        return $valueSet[$this->stchan_live_mode];
    }

    /**
     * Get the [stchan_status] column value.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->stchan_status;
    }

    /**
     * Get the [stchan_vgatga_id] column value.
     *
     * @return int
     */
    public function getTwitchGameId()
    {
        return $this->stchan_vgatga_id;
    }

    /**
     * Get the [stchan_game] column value.
     *
     * @return string
     */
    public function getGame()
    {
        return $this->stchan_game;
    }

    /**
     * Get the [stchan_stream_type] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getStreamType()
    {
        if (null === $this->stchan_stream_type) {
            return null;
        }
        $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_STREAM_TYPE);
        if (!isset($valueSet[$this->stchan_stream_type])) {
            throw new PropelException('Unknown stored enum key: ' . $this->stchan_stream_type);
        }

        return $valueSet[$this->stchan_stream_type];
    }

    /**
     * Get the [stchan_followers_count] column value.
     *
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->stchan_followers_count;
    }

    /**
     * Get the [stchan_followers_diff] column value.
     *
     * @return int
     */
    public function getFollowersDiff()
    {
        return $this->stchan_followers_diff;
    }

    /**
     * Get the [stchan_views_count] column value.
     *
     * @return int
     */
    public function getViewsCount()
    {
        return $this->stchan_views_count;
    }

    /**
     * Get the [stchan_views_diff] column value.
     *
     * @return int
     */
    public function getViewsDiff()
    {
        return $this->stchan_views_diff;
    }

    /**
     * Get the [stchan_viewers_count] column value.
     *
     * @return int
     */
    public function getViewersCount()
    {
        return $this->stchan_viewers_count;
    }

    /**
     * Get the [stchan_chatters_count] column value.
     *
     * @return int
     */
    public function getChattersCount()
    {
        return $this->stchan_chatters_count;
    }

    /**
     * Get the [stchan_hosts_count] column value.
     *
     * @return int
     */
    public function getHostsCount()
    {
        return $this->stchan_hosts_count;
    }

    /**
     * Get the [stchan_live_viewers_count] column value.
     *
     * @return int
     */
    public function getLiveViewersCount()
    {
        return $this->stchan_live_viewers_count;
    }

    /**
     * Get the [stchan_messages_count] column value.
     *
     * @return int
     */
    public function getMessagesCount()
    {
        return $this->stchan_messages_count;
    }

    /**
     * Get the [stchan_lock_chatters_scan] column value.
     *
     * @return boolean
     */
    public function getLockChattersScan()
    {
        return $this->stchan_lock_chatters_scan;
    }

    /**
     * Get the [stchan_lock_chatters_scan] column value.
     *
     * @return boolean
     */
    public function isLockChattersScan()
    {
        return $this->getLockChattersScan();
    }

    /**
     * Get the [stchan_lock_hosts_scan] column value.
     *
     * @return boolean
     */
    public function getLockHostsScan()
    {
        return $this->stchan_lock_hosts_scan;
    }

    /**
     * Get the [stchan_lock_hosts_scan] column value.
     *
     * @return boolean
     */
    public function isLockHostsScan()
    {
        return $this->getLockHostsScan();
    }

    /**
     * Get the [stchan_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->stchan_created_by_user_id;
    }

    /**
     * Get the [stchan_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->stchan_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [stchan_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->stchan_created_at;
        } else {
            return $this->stchan_created_at instanceof \DateTimeInterface ? $this->stchan_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [stchan_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->stchan_updated_at;
        } else {
            return $this->stchan_updated_at instanceof \DateTimeInterface ? $this->stchan_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [stchan_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_id !== $v) {
            $this->stchan_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [stchan_ststrm_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setStreamId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_ststrm_id !== $v) {
            $this->stchan_ststrm_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_STSTRM_ID] = true;
        }

        if ($this->aStream !== null && $this->aStream->getId() !== $v) {
            $this->aStream = null;
        }

        return $this;
    } // setStreamId()

    /**
     * Set the value of [stchan_stsite_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_stsite_id !== $v) {
            $this->stchan_stsite_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_STSITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [stchan_channel] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setChannel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_channel !== $v) {
            $this->stchan_channel = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CHANNEL] = true;
        }

        return $this;
    } // setChannel()

    /**
     * Set the value of [stchan_ststrm_bot_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setBotId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_ststrm_bot_id !== $v) {
            $this->stchan_ststrm_bot_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID] = true;
        }

        if ($this->aBot !== null && $this->aBot->getId() !== $v) {
            $this->aBot = null;
        }

        return $this;
    } // setBotId()

    /**
     * Sets the value of the [stchan_can_scan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setCanScan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stchan_can_scan !== $v) {
            $this->stchan_can_scan = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CAN_SCAN] = true;
        }

        return $this;
    } // setCanScan()

    /**
     * Set the value of [stchan_password_oauth] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setPasswordOauth($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_password_oauth !== $v) {
            $this->stchan_password_oauth = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH] = true;
        }

        return $this;
    } // setPasswordOauth()

    /**
     * Set the value of [stchan_channel_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setChannelId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_channel_id !== $v) {
            $this->stchan_channel_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CHANNEL_ID] = true;
        }

        return $this;
    } // setChannelId()

    /**
     * Set the value of [stchan_client_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setClientId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_client_id !== $v) {
            $this->stchan_client_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CLIENT_ID] = true;
        }

        return $this;
    } // setClientId()

    /**
     * Set the value of [stchan_secred_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setSecretId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_secred_id !== $v) {
            $this->stchan_secred_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_SECRED_ID] = true;
        }

        return $this;
    } // setSecretId()

    /**
     * Sets the value of the [stchan_channel_partner] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setChannelPartner($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stchan_channel_partner !== $v) {
            $this->stchan_channel_partner = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER] = true;
        }

        return $this;
    } // setChannelPartner()

    /**
     * Set the value of [stchan_live_mode] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setLiveMode($v)
    {
        if ($v !== null) {
            $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_LIVE_MODE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->stchan_live_mode !== $v) {
            $this->stchan_live_mode = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_LIVE_MODE] = true;
        }

        return $this;
    } // setLiveMode()

    /**
     * Set the value of [stchan_status] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_status !== $v) {
            $this->stchan_status = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [stchan_vgatga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setTwitchGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_vgatga_id !== $v) {
            $this->stchan_vgatga_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_VGATGA_ID] = true;
        }

        if ($this->aApiTwitchGame !== null && $this->aApiTwitchGame->getId() !== $v) {
            $this->aApiTwitchGame = null;
        }

        return $this;
    } // setTwitchGameId()

    /**
     * Set the value of [stchan_game] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setGame($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stchan_game !== $v) {
            $this->stchan_game = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_GAME] = true;
        }

        return $this;
    } // setGame()

    /**
     * Set the value of [stchan_stream_type] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setStreamType($v)
    {
        if ($v !== null) {
            $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_STREAM_TYPE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->stchan_stream_type !== $v) {
            $this->stchan_stream_type = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_STREAM_TYPE] = true;
        }

        return $this;
    } // setStreamType()

    /**
     * Set the value of [stchan_followers_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setFollowersCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_followers_count !== $v) {
            $this->stchan_followers_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT] = true;
        }

        return $this;
    } // setFollowersCount()

    /**
     * Set the value of [stchan_followers_diff] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setFollowersDiff($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_followers_diff !== $v) {
            $this->stchan_followers_diff = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF] = true;
        }

        return $this;
    } // setFollowersDiff()

    /**
     * Set the value of [stchan_views_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setViewsCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_views_count !== $v) {
            $this->stchan_views_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_VIEWS_COUNT] = true;
        }

        return $this;
    } // setViewsCount()

    /**
     * Set the value of [stchan_views_diff] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setViewsDiff($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_views_diff !== $v) {
            $this->stchan_views_diff = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_VIEWS_DIFF] = true;
        }

        return $this;
    } // setViewsDiff()

    /**
     * Set the value of [stchan_viewers_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setViewersCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_viewers_count !== $v) {
            $this->stchan_viewers_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_VIEWERS_COUNT] = true;
        }

        return $this;
    } // setViewersCount()

    /**
     * Set the value of [stchan_chatters_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setChattersCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_chatters_count !== $v) {
            $this->stchan_chatters_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CHATTERS_COUNT] = true;
        }

        return $this;
    } // setChattersCount()

    /**
     * Set the value of [stchan_hosts_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setHostsCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_hosts_count !== $v) {
            $this->stchan_hosts_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_HOSTS_COUNT] = true;
        }

        return $this;
    } // setHostsCount()

    /**
     * Set the value of [stchan_live_viewers_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setLiveViewersCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_live_viewers_count !== $v) {
            $this->stchan_live_viewers_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT] = true;
        }

        return $this;
    } // setLiveViewersCount()

    /**
     * Set the value of [stchan_messages_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setMessagesCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_messages_count !== $v) {
            $this->stchan_messages_count = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_MESSAGES_COUNT] = true;
        }

        return $this;
    } // setMessagesCount()

    /**
     * Sets the value of the [stchan_lock_chatters_scan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setLockChattersScan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stchan_lock_chatters_scan !== $v) {
            $this->stchan_lock_chatters_scan = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN] = true;
        }

        return $this;
    } // setLockChattersScan()

    /**
     * Sets the value of the [stchan_lock_hosts_scan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setLockHostsScan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stchan_lock_hosts_scan !== $v) {
            $this->stchan_lock_hosts_scan = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN] = true;
        }

        return $this;
    } // setLockHostsScan()

    /**
     * Set the value of [stchan_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_created_by_user_id !== $v) {
            $this->stchan_created_by_user_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [stchan_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stchan_updated_by_user_id !== $v) {
            $this->stchan_updated_by_user_id = $v;
            $this->modifiedColumns[ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [stchan_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stchan_created_at !== null || $dt !== null) {
            if ($this->stchan_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stchan_created_at->format("Y-m-d H:i:s.u")) {
                $this->stchan_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChannelTableMap::COL_STCHAN_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [stchan_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stchan_updated_at !== null || $dt !== null) {
            if ($this->stchan_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stchan_updated_at->format("Y-m-d H:i:s.u")) {
                $this->stchan_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChannelTableMap::COL_STCHAN_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->stchan_can_scan !== false) {
                return false;
            }

            if ($this->stchan_live_mode !== 0) {
                return false;
            }

            if ($this->stchan_stream_type !== 0) {
                return false;
            }

            if ($this->stchan_followers_count !== 0) {
                return false;
            }

            if ($this->stchan_followers_diff !== 0) {
                return false;
            }

            if ($this->stchan_views_count !== 0) {
                return false;
            }

            if ($this->stchan_views_diff !== 0) {
                return false;
            }

            if ($this->stchan_viewers_count !== 0) {
                return false;
            }

            if ($this->stchan_chatters_count !== 0) {
                return false;
            }

            if ($this->stchan_hosts_count !== 0) {
                return false;
            }

            if ($this->stchan_live_viewers_count !== 0) {
                return false;
            }

            if ($this->stchan_messages_count !== 0) {
                return false;
            }

            if ($this->stchan_lock_chatters_scan !== false) {
                return false;
            }

            if ($this->stchan_lock_hosts_scan !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ChannelTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ChannelTableMap::translateFieldName('StreamId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_ststrm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ChannelTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_stsite_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ChannelTableMap::translateFieldName('Channel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_channel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ChannelTableMap::translateFieldName('BotId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_ststrm_bot_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ChannelTableMap::translateFieldName('CanScan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_can_scan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ChannelTableMap::translateFieldName('PasswordOauth', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_password_oauth = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ChannelTableMap::translateFieldName('ChannelId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_channel_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ChannelTableMap::translateFieldName('ClientId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_client_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ChannelTableMap::translateFieldName('SecretId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_secred_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ChannelTableMap::translateFieldName('ChannelPartner', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_channel_partner = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ChannelTableMap::translateFieldName('LiveMode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_live_mode = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ChannelTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ChannelTableMap::translateFieldName('TwitchGameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_vgatga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ChannelTableMap::translateFieldName('Game', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_game = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ChannelTableMap::translateFieldName('StreamType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_stream_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ChannelTableMap::translateFieldName('FollowersCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_followers_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ChannelTableMap::translateFieldName('FollowersDiff', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_followers_diff = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ChannelTableMap::translateFieldName('ViewsCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_views_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ChannelTableMap::translateFieldName('ViewsDiff', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_views_diff = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ChannelTableMap::translateFieldName('ViewersCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_viewers_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ChannelTableMap::translateFieldName('ChattersCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_chatters_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ChannelTableMap::translateFieldName('HostsCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_hosts_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ChannelTableMap::translateFieldName('LiveViewersCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_live_viewers_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : ChannelTableMap::translateFieldName('MessagesCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_messages_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : ChannelTableMap::translateFieldName('LockChattersScan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_lock_chatters_scan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : ChannelTableMap::translateFieldName('LockHostsScan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_lock_hosts_scan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : ChannelTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : ChannelTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stchan_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : ChannelTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stchan_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : ChannelTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stchan_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 31; // 31 = ChannelTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\Channel'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStream !== null && $this->stchan_ststrm_id !== $this->aStream->getId()) {
            $this->aStream = null;
        }
        if ($this->aSite !== null && $this->stchan_stsite_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
        if ($this->aBot !== null && $this->stchan_ststrm_bot_id !== $this->aBot->getId()) {
            $this->aBot = null;
        }
        if ($this->aApiTwitchGame !== null && $this->stchan_vgatga_id !== $this->aApiTwitchGame->getId()) {
            $this->aApiTwitchGame = null;
        }
        if ($this->aCreatedByUser !== null && $this->stchan_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->stchan_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChannelTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildChannelQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStream = null;
            $this->aSite = null;
            $this->aBot = null;
            $this->aApiTwitchGame = null;
            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collUserExperiences = null;

            $this->collDeepBotImportExperiences = null;

            $this->collMessageExperiences = null;

            $this->collChatterExperiences = null;

            $this->collFollowExperiences = null;

            $this->collHostExperiences = null;

            $this->collViewDiffDatas = null;

            $this->collViewerDatas = null;

            $this->collFollowDiffDatas = null;

            $this->collStatusDatas = null;

            $this->collTypeDatas = null;

            $this->collGameDatas = null;

            $this->collStats = null;

            $this->collExperiences = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Channel::setDeleted()
     * @see Channel::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildChannelQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ChannelTableMap::COL_STCHAN_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChannelTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStream !== null) {
                if ($this->aStream->isModified() || $this->aStream->isNew()) {
                    $affectedRows += $this->aStream->save($con);
                }
                $this->setStream($this->aStream);
            }

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aBot !== null) {
                if ($this->aBot->isModified() || $this->aBot->isNew()) {
                    $affectedRows += $this->aBot->save($con);
                }
                $this->setBot($this->aBot);
            }

            if ($this->aApiTwitchGame !== null) {
                if ($this->aApiTwitchGame->isModified() || $this->aApiTwitchGame->isNew()) {
                    $affectedRows += $this->aApiTwitchGame->save($con);
                }
                $this->setApiTwitchGame($this->aApiTwitchGame);
            }

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->userExperiencesScheduledForDeletion !== null) {
                if (!$this->userExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\UserExperienceQuery::create()
                        ->filterByPrimaryKeys($this->userExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collUserExperiences !== null) {
                foreach ($this->collUserExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->deepBotImportExperiencesScheduledForDeletion !== null) {
                if (!$this->deepBotImportExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery::create()
                        ->filterByPrimaryKeys($this->deepBotImportExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->deepBotImportExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collDeepBotImportExperiences !== null) {
                foreach ($this->collDeepBotImportExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->messageExperiencesScheduledForDeletion !== null) {
                if (!$this->messageExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\MessageExperienceQuery::create()
                        ->filterByPrimaryKeys($this->messageExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->messageExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collMessageExperiences !== null) {
                foreach ($this->collMessageExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->chatterExperiencesScheduledForDeletion !== null) {
                if (!$this->chatterExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatterExperienceQuery::create()
                        ->filterByPrimaryKeys($this->chatterExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->chatterExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collChatterExperiences !== null) {
                foreach ($this->collChatterExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->followExperiencesScheduledForDeletion !== null) {
                if (!$this->followExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\FollowExperienceQuery::create()
                        ->filterByPrimaryKeys($this->followExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->followExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collFollowExperiences !== null) {
                foreach ($this->collFollowExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->hostExperiencesScheduledForDeletion !== null) {
                if (!$this->hostExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\HostExperienceQuery::create()
                        ->filterByPrimaryKeys($this->hostExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->hostExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collHostExperiences !== null) {
                foreach ($this->collHostExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->viewDiffDatasScheduledForDeletion !== null) {
                if (!$this->viewDiffDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ViewDiffDataQuery::create()
                        ->filterByPrimaryKeys($this->viewDiffDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->viewDiffDatasScheduledForDeletion = null;
                }
            }

            if ($this->collViewDiffDatas !== null) {
                foreach ($this->collViewDiffDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->viewerDatasScheduledForDeletion !== null) {
                if (!$this->viewerDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ViewerDataQuery::create()
                        ->filterByPrimaryKeys($this->viewerDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->viewerDatasScheduledForDeletion = null;
                }
            }

            if ($this->collViewerDatas !== null) {
                foreach ($this->collViewerDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->followDiffDatasScheduledForDeletion !== null) {
                if (!$this->followDiffDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\FollowDiffDataQuery::create()
                        ->filterByPrimaryKeys($this->followDiffDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->followDiffDatasScheduledForDeletion = null;
                }
            }

            if ($this->collFollowDiffDatas !== null) {
                foreach ($this->collFollowDiffDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->statusDatasScheduledForDeletion !== null) {
                if (!$this->statusDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\StatusDataQuery::create()
                        ->filterByPrimaryKeys($this->statusDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->statusDatasScheduledForDeletion = null;
                }
            }

            if ($this->collStatusDatas !== null) {
                foreach ($this->collStatusDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->typeDatasScheduledForDeletion !== null) {
                if (!$this->typeDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\TypeDataQuery::create()
                        ->filterByPrimaryKeys($this->typeDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->typeDatasScheduledForDeletion = null;
                }
            }

            if ($this->collTypeDatas !== null) {
                foreach ($this->collTypeDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->gameDatasScheduledForDeletion !== null) {
                if (!$this->gameDatasScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\GameDataQuery::create()
                        ->filterByPrimaryKeys($this->gameDatasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->gameDatasScheduledForDeletion = null;
                }
            }

            if ($this->collGameDatas !== null) {
                foreach ($this->collGameDatas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->statsScheduledForDeletion !== null) {
                if (!$this->statsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\StatQuery::create()
                        ->filterByPrimaryKeys($this->statsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->statsScheduledForDeletion = null;
                }
            }

            if ($this->collStats !== null) {
                foreach ($this->collStats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->experiencesScheduledForDeletion !== null) {
                if (!$this->experiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ExperienceQuery::create()
                        ->filterByPrimaryKeys($this->experiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->experiencesScheduledForDeletion = null;
                }
            }

            if ($this->collExperiences !== null) {
                foreach ($this->collExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ChannelTableMap::COL_STCHAN_ID] = true;
        if (null !== $this->stchan_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ChannelTableMap::COL_STCHAN_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSTRM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_ststrm_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_stsite_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_channel';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_ststrm_bot_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CAN_SCAN)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_can_scan';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_password_oauth';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_channel_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CLIENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_client_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_SECRED_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_secred_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_channel_partner';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LIVE_MODE)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_live_mode';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_status';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VGATGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_vgatga_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_GAME)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_game';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STREAM_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_stream_type';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_followers_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_followers_diff';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_views_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWS_DIFF)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_views_diff';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_viewers_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_chatters_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_HOSTS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_hosts_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_live_viewers_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_messages_count';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_lock_chatters_scan';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_lock_hosts_scan';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_created_by_user_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_updated_by_user_id';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_created_at';
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stchan_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO stream_channel_stchan (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'stchan_id':
                        $stmt->bindValue($identifier, $this->stchan_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_ststrm_id':
                        $stmt->bindValue($identifier, $this->stchan_ststrm_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_stsite_id':
                        $stmt->bindValue($identifier, $this->stchan_stsite_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_channel':
                        $stmt->bindValue($identifier, $this->stchan_channel, PDO::PARAM_STR);
                        break;
                    case 'stchan_ststrm_bot_id':
                        $stmt->bindValue($identifier, $this->stchan_ststrm_bot_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_can_scan':
                        $stmt->bindValue($identifier, (int) $this->stchan_can_scan, PDO::PARAM_INT);
                        break;
                    case 'stchan_password_oauth':
                        $stmt->bindValue($identifier, $this->stchan_password_oauth, PDO::PARAM_STR);
                        break;
                    case 'stchan_channel_id':
                        $stmt->bindValue($identifier, $this->stchan_channel_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_client_id':
                        $stmt->bindValue($identifier, $this->stchan_client_id, PDO::PARAM_STR);
                        break;
                    case 'stchan_secred_id':
                        $stmt->bindValue($identifier, $this->stchan_secred_id, PDO::PARAM_STR);
                        break;
                    case 'stchan_channel_partner':
                        $stmt->bindValue($identifier, (int) $this->stchan_channel_partner, PDO::PARAM_INT);
                        break;
                    case 'stchan_live_mode':
                        $stmt->bindValue($identifier, $this->stchan_live_mode, PDO::PARAM_INT);
                        break;
                    case 'stchan_status':
                        $stmt->bindValue($identifier, $this->stchan_status, PDO::PARAM_STR);
                        break;
                    case 'stchan_vgatga_id':
                        $stmt->bindValue($identifier, $this->stchan_vgatga_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_game':
                        $stmt->bindValue($identifier, $this->stchan_game, PDO::PARAM_STR);
                        break;
                    case 'stchan_stream_type':
                        $stmt->bindValue($identifier, $this->stchan_stream_type, PDO::PARAM_INT);
                        break;
                    case 'stchan_followers_count':
                        $stmt->bindValue($identifier, $this->stchan_followers_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_followers_diff':
                        $stmt->bindValue($identifier, $this->stchan_followers_diff, PDO::PARAM_INT);
                        break;
                    case 'stchan_views_count':
                        $stmt->bindValue($identifier, $this->stchan_views_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_views_diff':
                        $stmt->bindValue($identifier, $this->stchan_views_diff, PDO::PARAM_INT);
                        break;
                    case 'stchan_viewers_count':
                        $stmt->bindValue($identifier, $this->stchan_viewers_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_chatters_count':
                        $stmt->bindValue($identifier, $this->stchan_chatters_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_hosts_count':
                        $stmt->bindValue($identifier, $this->stchan_hosts_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_live_viewers_count':
                        $stmt->bindValue($identifier, $this->stchan_live_viewers_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_messages_count':
                        $stmt->bindValue($identifier, $this->stchan_messages_count, PDO::PARAM_INT);
                        break;
                    case 'stchan_lock_chatters_scan':
                        $stmt->bindValue($identifier, (int) $this->stchan_lock_chatters_scan, PDO::PARAM_INT);
                        break;
                    case 'stchan_lock_hosts_scan':
                        $stmt->bindValue($identifier, (int) $this->stchan_lock_hosts_scan, PDO::PARAM_INT);
                        break;
                    case 'stchan_created_by_user_id':
                        $stmt->bindValue($identifier, $this->stchan_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->stchan_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'stchan_created_at':
                        $stmt->bindValue($identifier, $this->stchan_created_at ? $this->stchan_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'stchan_updated_at':
                        $stmt->bindValue($identifier, $this->stchan_updated_at ? $this->stchan_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ChannelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getStreamId();
                break;
            case 2:
                return $this->getSiteId();
                break;
            case 3:
                return $this->getChannel();
                break;
            case 4:
                return $this->getBotId();
                break;
            case 5:
                return $this->getCanScan();
                break;
            case 6:
                return $this->getPasswordOauth();
                break;
            case 7:
                return $this->getChannelId();
                break;
            case 8:
                return $this->getClientId();
                break;
            case 9:
                return $this->getSecretId();
                break;
            case 10:
                return $this->getChannelPartner();
                break;
            case 11:
                return $this->getLiveMode();
                break;
            case 12:
                return $this->getStatus();
                break;
            case 13:
                return $this->getTwitchGameId();
                break;
            case 14:
                return $this->getGame();
                break;
            case 15:
                return $this->getStreamType();
                break;
            case 16:
                return $this->getFollowersCount();
                break;
            case 17:
                return $this->getFollowersDiff();
                break;
            case 18:
                return $this->getViewsCount();
                break;
            case 19:
                return $this->getViewsDiff();
                break;
            case 20:
                return $this->getViewersCount();
                break;
            case 21:
                return $this->getChattersCount();
                break;
            case 22:
                return $this->getHostsCount();
                break;
            case 23:
                return $this->getLiveViewersCount();
                break;
            case 24:
                return $this->getMessagesCount();
                break;
            case 25:
                return $this->getLockChattersScan();
                break;
            case 26:
                return $this->getLockHostsScan();
                break;
            case 27:
                return $this->getCreatedByUserId();
                break;
            case 28:
                return $this->getUpdatedByUserId();
                break;
            case 29:
                return $this->getCreatedAt();
                break;
            case 30:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Channel'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Channel'][$this->hashCode()] = true;
        $keys = ChannelTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getStreamId(),
            $keys[2] => $this->getSiteId(),
            $keys[3] => $this->getChannel(),
            $keys[4] => $this->getBotId(),
            $keys[5] => $this->getCanScan(),
            $keys[6] => $this->getPasswordOauth(),
            $keys[7] => $this->getChannelId(),
            $keys[8] => $this->getClientId(),
            $keys[9] => $this->getSecretId(),
            $keys[10] => $this->getChannelPartner(),
            $keys[11] => $this->getLiveMode(),
            $keys[12] => $this->getStatus(),
            $keys[13] => $this->getTwitchGameId(),
            $keys[14] => $this->getGame(),
            $keys[15] => $this->getStreamType(),
            $keys[16] => $this->getFollowersCount(),
            $keys[17] => $this->getFollowersDiff(),
            $keys[18] => $this->getViewsCount(),
            $keys[19] => $this->getViewsDiff(),
            $keys[20] => $this->getViewersCount(),
            $keys[21] => $this->getChattersCount(),
            $keys[22] => $this->getHostsCount(),
            $keys[23] => $this->getLiveViewersCount(),
            $keys[24] => $this->getMessagesCount(),
            $keys[25] => $this->getLockChattersScan(),
            $keys[26] => $this->getLockHostsScan(),
            $keys[27] => $this->getCreatedByUserId(),
            $keys[28] => $this->getUpdatedByUserId(),
            $keys[29] => $this->getCreatedAt(),
            $keys[30] => $this->getUpdatedAt(),
        );
        if ($result[$keys[29]] instanceof \DateTimeInterface) {
            $result[$keys[29]] = $result[$keys[29]]->format('c');
        }

        if ($result[$keys[30]] instanceof \DateTimeInterface) {
            $result[$keys[30]] = $result[$keys[30]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStream) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stream';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrm';
                        break;
                    default:
                        $key = 'Stream';
                }

                $result[$key] = $this->aStream->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsite';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBot) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stream';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrm';
                        break;
                    default:
                        $key = 'Bot';
                }

                $result[$key] = $this->aBot->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiTwitchGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiTwitchGame';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_twitch_vgatga';
                        break;
                    default:
                        $key = 'ApiTwitchGame';
                }

                $result[$key] = $this->aApiTwitchGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUserExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_user_experience_stuexps';
                        break;
                    default:
                        $key = 'UserExperiences';
                }

                $result[$key] = $this->collUserExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDeepBotImportExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'deepBotImportExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_deepbot_import_experience_stdbies';
                        break;
                    default:
                        $key = 'DeepBotImportExperiences';
                }

                $result[$key] = $this->collDeepBotImportExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMessageExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'messageExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_message_experience_stmexps';
                        break;
                    default:
                        $key = 'MessageExperiences';
                }

                $result[$key] = $this->collMessageExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChatterExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatterExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chatter_experience_stcexps';
                        break;
                    default:
                        $key = 'ChatterExperiences';
                }

                $result[$key] = $this->collChatterExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFollowExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'followExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_follow_experience_stfexps';
                        break;
                    default:
                        $key = 'FollowExperiences';
                }

                $result[$key] = $this->collFollowExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collHostExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'hostExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_host_experience_sthexps';
                        break;
                    default:
                        $key = 'HostExperiences';
                }

                $result[$key] = $this->collHostExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collViewDiffDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'viewDiffDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_view_diff_data_stddats';
                        break;
                    default:
                        $key = 'ViewDiffDatas';
                }

                $result[$key] = $this->collViewDiffDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collViewerDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'viewerDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_viewer_data_stvdats';
                        break;
                    default:
                        $key = 'ViewerDatas';
                }

                $result[$key] = $this->collViewerDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFollowDiffDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'followDiffDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_follow_diff_stfdats';
                        break;
                    default:
                        $key = 'FollowDiffDatas';
                }

                $result[$key] = $this->collFollowDiffDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collStatusDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'statusDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_status_data_stsdats';
                        break;
                    default:
                        $key = 'StatusDatas';
                }

                $result[$key] = $this->collStatusDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTypeDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'typeDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_type_data_sttdats';
                        break;
                    default:
                        $key = 'TypeDatas';
                }

                $result[$key] = $this->collTypeDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collGameDatas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'gameDatas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_game_data_stgdats';
                        break;
                    default:
                        $key = 'GameDatas';
                }

                $result[$key] = $this->collGameDatas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collStats) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stats';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stat_ststats';
                        break;
                    default:
                        $key = 'Stats';
                }

                $result[$key] = $this->collStats->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'experiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_experience_stexprs';
                        break;
                    default:
                        $key = 'Experiences';
                }

                $result[$key] = $this->collExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\Channel
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ChannelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\Channel
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setStreamId($value);
                break;
            case 2:
                $this->setSiteId($value);
                break;
            case 3:
                $this->setChannel($value);
                break;
            case 4:
                $this->setBotId($value);
                break;
            case 5:
                $this->setCanScan($value);
                break;
            case 6:
                $this->setPasswordOauth($value);
                break;
            case 7:
                $this->setChannelId($value);
                break;
            case 8:
                $this->setClientId($value);
                break;
            case 9:
                $this->setSecretId($value);
                break;
            case 10:
                $this->setChannelPartner($value);
                break;
            case 11:
                $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_LIVE_MODE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setLiveMode($value);
                break;
            case 12:
                $this->setStatus($value);
                break;
            case 13:
                $this->setTwitchGameId($value);
                break;
            case 14:
                $this->setGame($value);
                break;
            case 15:
                $valueSet = ChannelTableMap::getValueSet(ChannelTableMap::COL_STCHAN_STREAM_TYPE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setStreamType($value);
                break;
            case 16:
                $this->setFollowersCount($value);
                break;
            case 17:
                $this->setFollowersDiff($value);
                break;
            case 18:
                $this->setViewsCount($value);
                break;
            case 19:
                $this->setViewsDiff($value);
                break;
            case 20:
                $this->setViewersCount($value);
                break;
            case 21:
                $this->setChattersCount($value);
                break;
            case 22:
                $this->setHostsCount($value);
                break;
            case 23:
                $this->setLiveViewersCount($value);
                break;
            case 24:
                $this->setMessagesCount($value);
                break;
            case 25:
                $this->setLockChattersScan($value);
                break;
            case 26:
                $this->setLockHostsScan($value);
                break;
            case 27:
                $this->setCreatedByUserId($value);
                break;
            case 28:
                $this->setUpdatedByUserId($value);
                break;
            case 29:
                $this->setCreatedAt($value);
                break;
            case 30:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ChannelTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setStreamId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSiteId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setChannel($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setBotId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCanScan($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPasswordOauth($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setChannelId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setClientId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSecretId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setChannelPartner($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLiveMode($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setStatus($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTwitchGameId($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setGame($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setStreamType($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setFollowersCount($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setFollowersDiff($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setViewsCount($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setViewsDiff($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setViewersCount($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setChattersCount($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setHostsCount($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setLiveViewersCount($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setMessagesCount($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setLockChattersScan($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setLockHostsScan($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setCreatedByUserId($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setCreatedAt($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setUpdatedAt($arr[$keys[30]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChannelTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_ID, $this->stchan_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSTRM_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_STSTRM_ID, $this->stchan_ststrm_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSITE_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_STSITE_ID, $this->stchan_stsite_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CHANNEL, $this->stchan_channel);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, $this->stchan_ststrm_bot_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CAN_SCAN)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CAN_SCAN, $this->stchan_can_scan);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH, $this->stchan_password_oauth);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CHANNEL_ID, $this->stchan_channel_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CLIENT_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CLIENT_ID, $this->stchan_client_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_SECRED_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_SECRED_ID, $this->stchan_secred_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER, $this->stchan_channel_partner);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LIVE_MODE)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_LIVE_MODE, $this->stchan_live_mode);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STATUS)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_STATUS, $this->stchan_status);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VGATGA_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_VGATGA_ID, $this->stchan_vgatga_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_GAME)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_GAME, $this->stchan_game);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_STREAM_TYPE)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_STREAM_TYPE, $this->stchan_stream_type);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT, $this->stchan_followers_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF, $this->stchan_followers_diff);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_VIEWS_COUNT, $this->stchan_views_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWS_DIFF)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_VIEWS_DIFF, $this->stchan_views_diff);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT, $this->stchan_viewers_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT, $this->stchan_chatters_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_HOSTS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_HOSTS_COUNT, $this->stchan_hosts_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT, $this->stchan_live_viewers_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT, $this->stchan_messages_count);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN, $this->stchan_lock_chatters_scan);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN, $this->stchan_lock_hosts_scan);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, $this->stchan_created_by_user_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, $this->stchan_updated_by_user_id);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_CREATED_AT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_CREATED_AT, $this->stchan_created_at);
        }
        if ($this->isColumnModified(ChannelTableMap::COL_STCHAN_UPDATED_AT)) {
            $criteria->add(ChannelTableMap::COL_STCHAN_UPDATED_AT, $this->stchan_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildChannelQuery::create();
        $criteria->add(ChannelTableMap::COL_STCHAN_ID, $this->stchan_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (stchan_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\Channel (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setStreamId($this->getStreamId());
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setChannel($this->getChannel());
        $copyObj->setBotId($this->getBotId());
        $copyObj->setCanScan($this->getCanScan());
        $copyObj->setPasswordOauth($this->getPasswordOauth());
        $copyObj->setChannelId($this->getChannelId());
        $copyObj->setClientId($this->getClientId());
        $copyObj->setSecretId($this->getSecretId());
        $copyObj->setChannelPartner($this->getChannelPartner());
        $copyObj->setLiveMode($this->getLiveMode());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setTwitchGameId($this->getTwitchGameId());
        $copyObj->setGame($this->getGame());
        $copyObj->setStreamType($this->getStreamType());
        $copyObj->setFollowersCount($this->getFollowersCount());
        $copyObj->setFollowersDiff($this->getFollowersDiff());
        $copyObj->setViewsCount($this->getViewsCount());
        $copyObj->setViewsDiff($this->getViewsDiff());
        $copyObj->setViewersCount($this->getViewersCount());
        $copyObj->setChattersCount($this->getChattersCount());
        $copyObj->setHostsCount($this->getHostsCount());
        $copyObj->setLiveViewersCount($this->getLiveViewersCount());
        $copyObj->setMessagesCount($this->getMessagesCount());
        $copyObj->setLockChattersScan($this->getLockChattersScan());
        $copyObj->setLockHostsScan($this->getLockHostsScan());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUserExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDeepBotImportExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDeepBotImportExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMessageExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMessageExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChatterExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChatterExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFollowExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFollowExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getHostExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addHostExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getViewDiffDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addViewDiffData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getViewerDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addViewerData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFollowDiffDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFollowDiffData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getStatusDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addStatusData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTypeDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTypeData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getGameDatas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addGameData($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getStats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addStat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addExperience($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\Channel Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStream object.
     *
     * @param  ChildStream $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStream(ChildStream $v = null)
    {
        if ($v === null) {
            $this->setStreamId(NULL);
        } else {
            $this->setStreamId($v->getId());
        }

        $this->aStream = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStream object, it will not be re-added.
        if ($v !== null) {
            $v->addChannel($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStream object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStream The associated ChildStream object.
     * @throws PropelException
     */
    public function getStream(ConnectionInterface $con = null)
    {
        if ($this->aStream === null && ($this->stchan_ststrm_id != 0)) {
            $this->aStream = ChildStreamQuery::create()->findPk($this->stchan_ststrm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStream->addChannels($this);
             */
        }

        return $this->aStream;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addChannel($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->stchan_stsite_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->stchan_stsite_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addChannels($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a ChildStream object.
     *
     * @param  ChildStream $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBot(ChildStream $v = null)
    {
        if ($v === null) {
            $this->setBotId(NULL);
        } else {
            $this->setBotId($v->getId());
        }

        $this->aBot = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStream object, it will not be re-added.
        if ($v !== null) {
            $v->addChannelBot($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStream object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStream The associated ChildStream object.
     * @throws PropelException
     */
    public function getBot(ConnectionInterface $con = null)
    {
        if ($this->aBot === null && ($this->stchan_ststrm_bot_id != 0)) {
            $this->aBot = ChildStreamQuery::create()->findPk($this->stchan_ststrm_bot_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBot->addChannelBots($this);
             */
        }

        return $this->aBot;
    }

    /**
     * Declares an association between this object and a ApiTwitchGame object.
     *
     * @param  ApiTwitchGame $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiTwitchGame(ApiTwitchGame $v = null)
    {
        if ($v === null) {
            $this->setTwitchGameId(NULL);
        } else {
            $this->setTwitchGameId($v->getId());
        }

        $this->aApiTwitchGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ApiTwitchGame object, it will not be re-added.
        if ($v !== null) {
            $v->addChannel($this);
        }


        return $this;
    }


    /**
     * Get the associated ApiTwitchGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ApiTwitchGame The associated ApiTwitchGame object.
     * @throws PropelException
     */
    public function getApiTwitchGame(ConnectionInterface $con = null)
    {
        if ($this->aApiTwitchGame === null && ($this->stchan_vgatga_id != 0)) {
            $this->aApiTwitchGame = ApiTwitchGameQuery::create()->findPk($this->stchan_vgatga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiTwitchGame->addChannels($this);
             */
        }

        return $this->aApiTwitchGame;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserStchan($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->stchan_created_by_user_id != 0)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->stchan_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserStchans($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserStchan($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->stchan_updated_by_user_id != 0)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->stchan_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserStchans($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserExperience' == $relationName) {
            $this->initUserExperiences();
            return;
        }
        if ('DeepBotImportExperience' == $relationName) {
            $this->initDeepBotImportExperiences();
            return;
        }
        if ('MessageExperience' == $relationName) {
            $this->initMessageExperiences();
            return;
        }
        if ('ChatterExperience' == $relationName) {
            $this->initChatterExperiences();
            return;
        }
        if ('FollowExperience' == $relationName) {
            $this->initFollowExperiences();
            return;
        }
        if ('HostExperience' == $relationName) {
            $this->initHostExperiences();
            return;
        }
        if ('ViewDiffData' == $relationName) {
            $this->initViewDiffDatas();
            return;
        }
        if ('ViewerData' == $relationName) {
            $this->initViewerDatas();
            return;
        }
        if ('FollowDiffData' == $relationName) {
            $this->initFollowDiffDatas();
            return;
        }
        if ('StatusData' == $relationName) {
            $this->initStatusDatas();
            return;
        }
        if ('TypeData' == $relationName) {
            $this->initTypeDatas();
            return;
        }
        if ('GameData' == $relationName) {
            $this->initGameDatas();
            return;
        }
        if ('Stat' == $relationName) {
            $this->initStats();
            return;
        }
        if ('Experience' == $relationName) {
            $this->initExperiences();
            return;
        }
    }

    /**
     * Clears out the collUserExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserExperiences()
     */
    public function clearUserExperiences()
    {
        $this->collUserExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserExperiences collection loaded partially.
     */
    public function resetPartialUserExperiences($v = true)
    {
        $this->collUserExperiencesPartial = $v;
    }

    /**
     * Initializes the collUserExperiences collection.
     *
     * By default this just sets the collUserExperiences collection to an empty array (like clearcollUserExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserExperiences($overrideExisting = true)
    {
        if (null !== $this->collUserExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collUserExperiences = new $collectionClassName;
        $this->collUserExperiences->setModel('\IiMedias\StreamBundle\Model\UserExperience');
    }

    /**
     * Gets an array of ChildUserExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     * @throws PropelException
     */
    public function getUserExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                // return empty collection
                $this->initUserExperiences();
            } else {
                $collUserExperiences = ChildUserExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserExperiencesPartial && count($collUserExperiences)) {
                        $this->initUserExperiences(false);

                        foreach ($collUserExperiences as $obj) {
                            if (false == $this->collUserExperiences->contains($obj)) {
                                $this->collUserExperiences->append($obj);
                            }
                        }

                        $this->collUserExperiencesPartial = true;
                    }

                    return $collUserExperiences;
                }

                if ($partial && $this->collUserExperiences) {
                    foreach ($this->collUserExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collUserExperiences[] = $obj;
                        }
                    }
                }

                $this->collUserExperiences = $collUserExperiences;
                $this->collUserExperiencesPartial = false;
            }
        }

        return $this->collUserExperiences;
    }

    /**
     * Sets a collection of ChildUserExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setUserExperiences(Collection $userExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildUserExperience[] $userExperiencesToDelete */
        $userExperiencesToDelete = $this->getUserExperiences(new Criteria(), $con)->diff($userExperiences);


        $this->userExperiencesScheduledForDeletion = $userExperiencesToDelete;

        foreach ($userExperiencesToDelete as $userExperienceRemoved) {
            $userExperienceRemoved->setChannel(null);
        }

        $this->collUserExperiences = null;
        foreach ($userExperiences as $userExperience) {
            $this->addUserExperience($userExperience);
        }

        $this->collUserExperiences = $userExperiences;
        $this->collUserExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserExperience objects.
     * @throws PropelException
     */
    public function countUserExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserExperiences());
            }

            $query = ChildUserExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collUserExperiences);
    }

    /**
     * Method called to associate a ChildUserExperience object to this object
     * through the ChildUserExperience foreign key attribute.
     *
     * @param  ChildUserExperience $l ChildUserExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addUserExperience(ChildUserExperience $l)
    {
        if ($this->collUserExperiences === null) {
            $this->initUserExperiences();
            $this->collUserExperiencesPartial = true;
        }

        if (!$this->collUserExperiences->contains($l)) {
            $this->doAddUserExperience($l);

            if ($this->userExperiencesScheduledForDeletion and $this->userExperiencesScheduledForDeletion->contains($l)) {
                $this->userExperiencesScheduledForDeletion->remove($this->userExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserExperience $userExperience The ChildUserExperience object to add.
     */
    protected function doAddUserExperience(ChildUserExperience $userExperience)
    {
        $this->collUserExperiences[]= $userExperience;
        $userExperience->setChannel($this);
    }

    /**
     * @param  ChildUserExperience $userExperience The ChildUserExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeUserExperience(ChildUserExperience $userExperience)
    {
        if ($this->getUserExperiences()->contains($userExperience)) {
            $pos = $this->collUserExperiences->search($userExperience);
            $this->collUserExperiences->remove($pos);
            if (null === $this->userExperiencesScheduledForDeletion) {
                $this->userExperiencesScheduledForDeletion = clone $this->collUserExperiences;
                $this->userExperiencesScheduledForDeletion->clear();
            }
            $this->userExperiencesScheduledForDeletion[]= clone $userExperience;
            $userExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Rank', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinAvatar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Avatar', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }

    /**
     * Clears out the collDeepBotImportExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDeepBotImportExperiences()
     */
    public function clearDeepBotImportExperiences()
    {
        $this->collDeepBotImportExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDeepBotImportExperiences collection loaded partially.
     */
    public function resetPartialDeepBotImportExperiences($v = true)
    {
        $this->collDeepBotImportExperiencesPartial = $v;
    }

    /**
     * Initializes the collDeepBotImportExperiences collection.
     *
     * By default this just sets the collDeepBotImportExperiences collection to an empty array (like clearcollDeepBotImportExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDeepBotImportExperiences($overrideExisting = true)
    {
        if (null !== $this->collDeepBotImportExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = DeepBotImportExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collDeepBotImportExperiences = new $collectionClassName;
        $this->collDeepBotImportExperiences->setModel('\IiMedias\StreamBundle\Model\DeepBotImportExperience');
    }

    /**
     * Gets an array of ChildDeepBotImportExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     * @throws PropelException
     */
    public function getDeepBotImportExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDeepBotImportExperiencesPartial && !$this->isNew();
        if (null === $this->collDeepBotImportExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDeepBotImportExperiences) {
                // return empty collection
                $this->initDeepBotImportExperiences();
            } else {
                $collDeepBotImportExperiences = ChildDeepBotImportExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDeepBotImportExperiencesPartial && count($collDeepBotImportExperiences)) {
                        $this->initDeepBotImportExperiences(false);

                        foreach ($collDeepBotImportExperiences as $obj) {
                            if (false == $this->collDeepBotImportExperiences->contains($obj)) {
                                $this->collDeepBotImportExperiences->append($obj);
                            }
                        }

                        $this->collDeepBotImportExperiencesPartial = true;
                    }

                    return $collDeepBotImportExperiences;
                }

                if ($partial && $this->collDeepBotImportExperiences) {
                    foreach ($this->collDeepBotImportExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collDeepBotImportExperiences[] = $obj;
                        }
                    }
                }

                $this->collDeepBotImportExperiences = $collDeepBotImportExperiences;
                $this->collDeepBotImportExperiencesPartial = false;
            }
        }

        return $this->collDeepBotImportExperiences;
    }

    /**
     * Sets a collection of ChildDeepBotImportExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $deepBotImportExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setDeepBotImportExperiences(Collection $deepBotImportExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildDeepBotImportExperience[] $deepBotImportExperiencesToDelete */
        $deepBotImportExperiencesToDelete = $this->getDeepBotImportExperiences(new Criteria(), $con)->diff($deepBotImportExperiences);


        $this->deepBotImportExperiencesScheduledForDeletion = $deepBotImportExperiencesToDelete;

        foreach ($deepBotImportExperiencesToDelete as $deepBotImportExperienceRemoved) {
            $deepBotImportExperienceRemoved->setChannel(null);
        }

        $this->collDeepBotImportExperiences = null;
        foreach ($deepBotImportExperiences as $deepBotImportExperience) {
            $this->addDeepBotImportExperience($deepBotImportExperience);
        }

        $this->collDeepBotImportExperiences = $deepBotImportExperiences;
        $this->collDeepBotImportExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DeepBotImportExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DeepBotImportExperience objects.
     * @throws PropelException
     */
    public function countDeepBotImportExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDeepBotImportExperiencesPartial && !$this->isNew();
        if (null === $this->collDeepBotImportExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDeepBotImportExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDeepBotImportExperiences());
            }

            $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collDeepBotImportExperiences);
    }

    /**
     * Method called to associate a ChildDeepBotImportExperience object to this object
     * through the ChildDeepBotImportExperience foreign key attribute.
     *
     * @param  ChildDeepBotImportExperience $l ChildDeepBotImportExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addDeepBotImportExperience(ChildDeepBotImportExperience $l)
    {
        if ($this->collDeepBotImportExperiences === null) {
            $this->initDeepBotImportExperiences();
            $this->collDeepBotImportExperiencesPartial = true;
        }

        if (!$this->collDeepBotImportExperiences->contains($l)) {
            $this->doAddDeepBotImportExperience($l);

            if ($this->deepBotImportExperiencesScheduledForDeletion and $this->deepBotImportExperiencesScheduledForDeletion->contains($l)) {
                $this->deepBotImportExperiencesScheduledForDeletion->remove($this->deepBotImportExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDeepBotImportExperience $deepBotImportExperience The ChildDeepBotImportExperience object to add.
     */
    protected function doAddDeepBotImportExperience(ChildDeepBotImportExperience $deepBotImportExperience)
    {
        $this->collDeepBotImportExperiences[]= $deepBotImportExperience;
        $deepBotImportExperience->setChannel($this);
    }

    /**
     * @param  ChildDeepBotImportExperience $deepBotImportExperience The ChildDeepBotImportExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeDeepBotImportExperience(ChildDeepBotImportExperience $deepBotImportExperience)
    {
        if ($this->getDeepBotImportExperiences()->contains($deepBotImportExperience)) {
            $pos = $this->collDeepBotImportExperiences->search($deepBotImportExperience);
            $this->collDeepBotImportExperiences->remove($pos);
            if (null === $this->deepBotImportExperiencesScheduledForDeletion) {
                $this->deepBotImportExperiencesScheduledForDeletion = clone $this->collDeepBotImportExperiences;
                $this->deepBotImportExperiencesScheduledForDeletion->clear();
            }
            $this->deepBotImportExperiencesScheduledForDeletion[]= clone $deepBotImportExperience;
            $deepBotImportExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }

    /**
     * Clears out the collMessageExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMessageExperiences()
     */
    public function clearMessageExperiences()
    {
        $this->collMessageExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMessageExperiences collection loaded partially.
     */
    public function resetPartialMessageExperiences($v = true)
    {
        $this->collMessageExperiencesPartial = $v;
    }

    /**
     * Initializes the collMessageExperiences collection.
     *
     * By default this just sets the collMessageExperiences collection to an empty array (like clearcollMessageExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMessageExperiences($overrideExisting = true)
    {
        if (null !== $this->collMessageExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = MessageExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collMessageExperiences = new $collectionClassName;
        $this->collMessageExperiences->setModel('\IiMedias\StreamBundle\Model\MessageExperience');
    }

    /**
     * Gets an array of ChildMessageExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     * @throws PropelException
     */
    public function getMessageExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                // return empty collection
                $this->initMessageExperiences();
            } else {
                $collMessageExperiences = ChildMessageExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMessageExperiencesPartial && count($collMessageExperiences)) {
                        $this->initMessageExperiences(false);

                        foreach ($collMessageExperiences as $obj) {
                            if (false == $this->collMessageExperiences->contains($obj)) {
                                $this->collMessageExperiences->append($obj);
                            }
                        }

                        $this->collMessageExperiencesPartial = true;
                    }

                    return $collMessageExperiences;
                }

                if ($partial && $this->collMessageExperiences) {
                    foreach ($this->collMessageExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collMessageExperiences[] = $obj;
                        }
                    }
                }

                $this->collMessageExperiences = $collMessageExperiences;
                $this->collMessageExperiencesPartial = false;
            }
        }

        return $this->collMessageExperiences;
    }

    /**
     * Sets a collection of ChildMessageExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $messageExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setMessageExperiences(Collection $messageExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildMessageExperience[] $messageExperiencesToDelete */
        $messageExperiencesToDelete = $this->getMessageExperiences(new Criteria(), $con)->diff($messageExperiences);


        $this->messageExperiencesScheduledForDeletion = $messageExperiencesToDelete;

        foreach ($messageExperiencesToDelete as $messageExperienceRemoved) {
            $messageExperienceRemoved->setChannel(null);
        }

        $this->collMessageExperiences = null;
        foreach ($messageExperiences as $messageExperience) {
            $this->addMessageExperience($messageExperience);
        }

        $this->collMessageExperiences = $messageExperiences;
        $this->collMessageExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MessageExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MessageExperience objects.
     * @throws PropelException
     */
    public function countMessageExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMessageExperiences());
            }

            $query = ChildMessageExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collMessageExperiences);
    }

    /**
     * Method called to associate a ChildMessageExperience object to this object
     * through the ChildMessageExperience foreign key attribute.
     *
     * @param  ChildMessageExperience $l ChildMessageExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addMessageExperience(ChildMessageExperience $l)
    {
        if ($this->collMessageExperiences === null) {
            $this->initMessageExperiences();
            $this->collMessageExperiencesPartial = true;
        }

        if (!$this->collMessageExperiences->contains($l)) {
            $this->doAddMessageExperience($l);

            if ($this->messageExperiencesScheduledForDeletion and $this->messageExperiencesScheduledForDeletion->contains($l)) {
                $this->messageExperiencesScheduledForDeletion->remove($this->messageExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMessageExperience $messageExperience The ChildMessageExperience object to add.
     */
    protected function doAddMessageExperience(ChildMessageExperience $messageExperience)
    {
        $this->collMessageExperiences[]= $messageExperience;
        $messageExperience->setChannel($this);
    }

    /**
     * @param  ChildMessageExperience $messageExperience The ChildMessageExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeMessageExperience(ChildMessageExperience $messageExperience)
    {
        if ($this->getMessageExperiences()->contains($messageExperience)) {
            $pos = $this->collMessageExperiences->search($messageExperience);
            $this->collMessageExperiences->remove($pos);
            if (null === $this->messageExperiencesScheduledForDeletion) {
                $this->messageExperiencesScheduledForDeletion = clone $this->collMessageExperiences;
                $this->messageExperiencesScheduledForDeletion->clear();
            }
            $this->messageExperiencesScheduledForDeletion[]= clone $messageExperience;
            $messageExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }

    /**
     * Clears out the collChatterExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChatterExperiences()
     */
    public function clearChatterExperiences()
    {
        $this->collChatterExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChatterExperiences collection loaded partially.
     */
    public function resetPartialChatterExperiences($v = true)
    {
        $this->collChatterExperiencesPartial = $v;
    }

    /**
     * Initializes the collChatterExperiences collection.
     *
     * By default this just sets the collChatterExperiences collection to an empty array (like clearcollChatterExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChatterExperiences($overrideExisting = true)
    {
        if (null !== $this->collChatterExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatterExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collChatterExperiences = new $collectionClassName;
        $this->collChatterExperiences->setModel('\IiMedias\StreamBundle\Model\ChatterExperience');
    }

    /**
     * Gets an array of ChildChatterExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     * @throws PropelException
     */
    public function getChatterExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                // return empty collection
                $this->initChatterExperiences();
            } else {
                $collChatterExperiences = ChildChatterExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChatterExperiencesPartial && count($collChatterExperiences)) {
                        $this->initChatterExperiences(false);

                        foreach ($collChatterExperiences as $obj) {
                            if (false == $this->collChatterExperiences->contains($obj)) {
                                $this->collChatterExperiences->append($obj);
                            }
                        }

                        $this->collChatterExperiencesPartial = true;
                    }

                    return $collChatterExperiences;
                }

                if ($partial && $this->collChatterExperiences) {
                    foreach ($this->collChatterExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collChatterExperiences[] = $obj;
                        }
                    }
                }

                $this->collChatterExperiences = $collChatterExperiences;
                $this->collChatterExperiencesPartial = false;
            }
        }

        return $this->collChatterExperiences;
    }

    /**
     * Sets a collection of ChildChatterExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chatterExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setChatterExperiences(Collection $chatterExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildChatterExperience[] $chatterExperiencesToDelete */
        $chatterExperiencesToDelete = $this->getChatterExperiences(new Criteria(), $con)->diff($chatterExperiences);


        $this->chatterExperiencesScheduledForDeletion = $chatterExperiencesToDelete;

        foreach ($chatterExperiencesToDelete as $chatterExperienceRemoved) {
            $chatterExperienceRemoved->setChannel(null);
        }

        $this->collChatterExperiences = null;
        foreach ($chatterExperiences as $chatterExperience) {
            $this->addChatterExperience($chatterExperience);
        }

        $this->collChatterExperiences = $chatterExperiences;
        $this->collChatterExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChatterExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ChatterExperience objects.
     * @throws PropelException
     */
    public function countChatterExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChatterExperiences());
            }

            $query = ChildChatterExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collChatterExperiences);
    }

    /**
     * Method called to associate a ChildChatterExperience object to this object
     * through the ChildChatterExperience foreign key attribute.
     *
     * @param  ChildChatterExperience $l ChildChatterExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addChatterExperience(ChildChatterExperience $l)
    {
        if ($this->collChatterExperiences === null) {
            $this->initChatterExperiences();
            $this->collChatterExperiencesPartial = true;
        }

        if (!$this->collChatterExperiences->contains($l)) {
            $this->doAddChatterExperience($l);

            if ($this->chatterExperiencesScheduledForDeletion and $this->chatterExperiencesScheduledForDeletion->contains($l)) {
                $this->chatterExperiencesScheduledForDeletion->remove($this->chatterExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChatterExperience $chatterExperience The ChildChatterExperience object to add.
     */
    protected function doAddChatterExperience(ChildChatterExperience $chatterExperience)
    {
        $this->collChatterExperiences[]= $chatterExperience;
        $chatterExperience->setChannel($this);
    }

    /**
     * @param  ChildChatterExperience $chatterExperience The ChildChatterExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeChatterExperience(ChildChatterExperience $chatterExperience)
    {
        if ($this->getChatterExperiences()->contains($chatterExperience)) {
            $pos = $this->collChatterExperiences->search($chatterExperience);
            $this->collChatterExperiences->remove($pos);
            if (null === $this->chatterExperiencesScheduledForDeletion) {
                $this->chatterExperiencesScheduledForDeletion = clone $this->collChatterExperiences;
                $this->chatterExperiencesScheduledForDeletion->clear();
            }
            $this->chatterExperiencesScheduledForDeletion[]= clone $chatterExperience;
            $chatterExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }

    /**
     * Clears out the collFollowExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFollowExperiences()
     */
    public function clearFollowExperiences()
    {
        $this->collFollowExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFollowExperiences collection loaded partially.
     */
    public function resetPartialFollowExperiences($v = true)
    {
        $this->collFollowExperiencesPartial = $v;
    }

    /**
     * Initializes the collFollowExperiences collection.
     *
     * By default this just sets the collFollowExperiences collection to an empty array (like clearcollFollowExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFollowExperiences($overrideExisting = true)
    {
        if (null !== $this->collFollowExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = FollowExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collFollowExperiences = new $collectionClassName;
        $this->collFollowExperiences->setModel('\IiMedias\StreamBundle\Model\FollowExperience');
    }

    /**
     * Gets an array of ChildFollowExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     * @throws PropelException
     */
    public function getFollowExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                // return empty collection
                $this->initFollowExperiences();
            } else {
                $collFollowExperiences = ChildFollowExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFollowExperiencesPartial && count($collFollowExperiences)) {
                        $this->initFollowExperiences(false);

                        foreach ($collFollowExperiences as $obj) {
                            if (false == $this->collFollowExperiences->contains($obj)) {
                                $this->collFollowExperiences->append($obj);
                            }
                        }

                        $this->collFollowExperiencesPartial = true;
                    }

                    return $collFollowExperiences;
                }

                if ($partial && $this->collFollowExperiences) {
                    foreach ($this->collFollowExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collFollowExperiences[] = $obj;
                        }
                    }
                }

                $this->collFollowExperiences = $collFollowExperiences;
                $this->collFollowExperiencesPartial = false;
            }
        }

        return $this->collFollowExperiences;
    }

    /**
     * Sets a collection of ChildFollowExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $followExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setFollowExperiences(Collection $followExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildFollowExperience[] $followExperiencesToDelete */
        $followExperiencesToDelete = $this->getFollowExperiences(new Criteria(), $con)->diff($followExperiences);


        $this->followExperiencesScheduledForDeletion = $followExperiencesToDelete;

        foreach ($followExperiencesToDelete as $followExperienceRemoved) {
            $followExperienceRemoved->setChannel(null);
        }

        $this->collFollowExperiences = null;
        foreach ($followExperiences as $followExperience) {
            $this->addFollowExperience($followExperience);
        }

        $this->collFollowExperiences = $followExperiences;
        $this->collFollowExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FollowExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FollowExperience objects.
     * @throws PropelException
     */
    public function countFollowExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFollowExperiences());
            }

            $query = ChildFollowExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collFollowExperiences);
    }

    /**
     * Method called to associate a ChildFollowExperience object to this object
     * through the ChildFollowExperience foreign key attribute.
     *
     * @param  ChildFollowExperience $l ChildFollowExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addFollowExperience(ChildFollowExperience $l)
    {
        if ($this->collFollowExperiences === null) {
            $this->initFollowExperiences();
            $this->collFollowExperiencesPartial = true;
        }

        if (!$this->collFollowExperiences->contains($l)) {
            $this->doAddFollowExperience($l);

            if ($this->followExperiencesScheduledForDeletion and $this->followExperiencesScheduledForDeletion->contains($l)) {
                $this->followExperiencesScheduledForDeletion->remove($this->followExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFollowExperience $followExperience The ChildFollowExperience object to add.
     */
    protected function doAddFollowExperience(ChildFollowExperience $followExperience)
    {
        $this->collFollowExperiences[]= $followExperience;
        $followExperience->setChannel($this);
    }

    /**
     * @param  ChildFollowExperience $followExperience The ChildFollowExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeFollowExperience(ChildFollowExperience $followExperience)
    {
        if ($this->getFollowExperiences()->contains($followExperience)) {
            $pos = $this->collFollowExperiences->search($followExperience);
            $this->collFollowExperiences->remove($pos);
            if (null === $this->followExperiencesScheduledForDeletion) {
                $this->followExperiencesScheduledForDeletion = clone $this->collFollowExperiences;
                $this->followExperiencesScheduledForDeletion->clear();
            }
            $this->followExperiencesScheduledForDeletion[]= clone $followExperience;
            $followExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }

    /**
     * Clears out the collHostExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addHostExperiences()
     */
    public function clearHostExperiences()
    {
        $this->collHostExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collHostExperiences collection loaded partially.
     */
    public function resetPartialHostExperiences($v = true)
    {
        $this->collHostExperiencesPartial = $v;
    }

    /**
     * Initializes the collHostExperiences collection.
     *
     * By default this just sets the collHostExperiences collection to an empty array (like clearcollHostExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initHostExperiences($overrideExisting = true)
    {
        if (null !== $this->collHostExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = HostExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collHostExperiences = new $collectionClassName;
        $this->collHostExperiences->setModel('\IiMedias\StreamBundle\Model\HostExperience');
    }

    /**
     * Gets an array of ChildHostExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     * @throws PropelException
     */
    public function getHostExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                // return empty collection
                $this->initHostExperiences();
            } else {
                $collHostExperiences = ChildHostExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collHostExperiencesPartial && count($collHostExperiences)) {
                        $this->initHostExperiences(false);

                        foreach ($collHostExperiences as $obj) {
                            if (false == $this->collHostExperiences->contains($obj)) {
                                $this->collHostExperiences->append($obj);
                            }
                        }

                        $this->collHostExperiencesPartial = true;
                    }

                    return $collHostExperiences;
                }

                if ($partial && $this->collHostExperiences) {
                    foreach ($this->collHostExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collHostExperiences[] = $obj;
                        }
                    }
                }

                $this->collHostExperiences = $collHostExperiences;
                $this->collHostExperiencesPartial = false;
            }
        }

        return $this->collHostExperiences;
    }

    /**
     * Sets a collection of ChildHostExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $hostExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setHostExperiences(Collection $hostExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildHostExperience[] $hostExperiencesToDelete */
        $hostExperiencesToDelete = $this->getHostExperiences(new Criteria(), $con)->diff($hostExperiences);


        $this->hostExperiencesScheduledForDeletion = $hostExperiencesToDelete;

        foreach ($hostExperiencesToDelete as $hostExperienceRemoved) {
            $hostExperienceRemoved->setChannel(null);
        }

        $this->collHostExperiences = null;
        foreach ($hostExperiences as $hostExperience) {
            $this->addHostExperience($hostExperience);
        }

        $this->collHostExperiences = $hostExperiences;
        $this->collHostExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related HostExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related HostExperience objects.
     * @throws PropelException
     */
    public function countHostExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getHostExperiences());
            }

            $query = ChildHostExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collHostExperiences);
    }

    /**
     * Method called to associate a ChildHostExperience object to this object
     * through the ChildHostExperience foreign key attribute.
     *
     * @param  ChildHostExperience $l ChildHostExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addHostExperience(ChildHostExperience $l)
    {
        if ($this->collHostExperiences === null) {
            $this->initHostExperiences();
            $this->collHostExperiencesPartial = true;
        }

        if (!$this->collHostExperiences->contains($l)) {
            $this->doAddHostExperience($l);

            if ($this->hostExperiencesScheduledForDeletion and $this->hostExperiencesScheduledForDeletion->contains($l)) {
                $this->hostExperiencesScheduledForDeletion->remove($this->hostExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildHostExperience $hostExperience The ChildHostExperience object to add.
     */
    protected function doAddHostExperience(ChildHostExperience $hostExperience)
    {
        $this->collHostExperiences[]= $hostExperience;
        $hostExperience->setChannel($this);
    }

    /**
     * @param  ChildHostExperience $hostExperience The ChildHostExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeHostExperience(ChildHostExperience $hostExperience)
    {
        if ($this->getHostExperiences()->contains($hostExperience)) {
            $pos = $this->collHostExperiences->search($hostExperience);
            $this->collHostExperiences->remove($pos);
            if (null === $this->hostExperiencesScheduledForDeletion) {
                $this->hostExperiencesScheduledForDeletion = clone $this->collHostExperiences;
                $this->hostExperiencesScheduledForDeletion->clear();
            }
            $this->hostExperiencesScheduledForDeletion[]= clone $hostExperience;
            $hostExperience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }

    /**
     * Clears out the collViewDiffDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addViewDiffDatas()
     */
    public function clearViewDiffDatas()
    {
        $this->collViewDiffDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collViewDiffDatas collection loaded partially.
     */
    public function resetPartialViewDiffDatas($v = true)
    {
        $this->collViewDiffDatasPartial = $v;
    }

    /**
     * Initializes the collViewDiffDatas collection.
     *
     * By default this just sets the collViewDiffDatas collection to an empty array (like clearcollViewDiffDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initViewDiffDatas($overrideExisting = true)
    {
        if (null !== $this->collViewDiffDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ViewDiffDataTableMap::getTableMap()->getCollectionClassName();

        $this->collViewDiffDatas = new $collectionClassName;
        $this->collViewDiffDatas->setModel('\IiMedias\StreamBundle\Model\ViewDiffData');
    }

    /**
     * Gets an array of ChildViewDiffData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildViewDiffData[] List of ChildViewDiffData objects
     * @throws PropelException
     */
    public function getViewDiffDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collViewDiffDatasPartial && !$this->isNew();
        if (null === $this->collViewDiffDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collViewDiffDatas) {
                // return empty collection
                $this->initViewDiffDatas();
            } else {
                $collViewDiffDatas = ChildViewDiffDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collViewDiffDatasPartial && count($collViewDiffDatas)) {
                        $this->initViewDiffDatas(false);

                        foreach ($collViewDiffDatas as $obj) {
                            if (false == $this->collViewDiffDatas->contains($obj)) {
                                $this->collViewDiffDatas->append($obj);
                            }
                        }

                        $this->collViewDiffDatasPartial = true;
                    }

                    return $collViewDiffDatas;
                }

                if ($partial && $this->collViewDiffDatas) {
                    foreach ($this->collViewDiffDatas as $obj) {
                        if ($obj->isNew()) {
                            $collViewDiffDatas[] = $obj;
                        }
                    }
                }

                $this->collViewDiffDatas = $collViewDiffDatas;
                $this->collViewDiffDatasPartial = false;
            }
        }

        return $this->collViewDiffDatas;
    }

    /**
     * Sets a collection of ChildViewDiffData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $viewDiffDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setViewDiffDatas(Collection $viewDiffDatas, ConnectionInterface $con = null)
    {
        /** @var ChildViewDiffData[] $viewDiffDatasToDelete */
        $viewDiffDatasToDelete = $this->getViewDiffDatas(new Criteria(), $con)->diff($viewDiffDatas);


        $this->viewDiffDatasScheduledForDeletion = $viewDiffDatasToDelete;

        foreach ($viewDiffDatasToDelete as $viewDiffDataRemoved) {
            $viewDiffDataRemoved->setChannel(null);
        }

        $this->collViewDiffDatas = null;
        foreach ($viewDiffDatas as $viewDiffData) {
            $this->addViewDiffData($viewDiffData);
        }

        $this->collViewDiffDatas = $viewDiffDatas;
        $this->collViewDiffDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ViewDiffData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ViewDiffData objects.
     * @throws PropelException
     */
    public function countViewDiffDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collViewDiffDatasPartial && !$this->isNew();
        if (null === $this->collViewDiffDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collViewDiffDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getViewDiffDatas());
            }

            $query = ChildViewDiffDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collViewDiffDatas);
    }

    /**
     * Method called to associate a ChildViewDiffData object to this object
     * through the ChildViewDiffData foreign key attribute.
     *
     * @param  ChildViewDiffData $l ChildViewDiffData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addViewDiffData(ChildViewDiffData $l)
    {
        if ($this->collViewDiffDatas === null) {
            $this->initViewDiffDatas();
            $this->collViewDiffDatasPartial = true;
        }

        if (!$this->collViewDiffDatas->contains($l)) {
            $this->doAddViewDiffData($l);

            if ($this->viewDiffDatasScheduledForDeletion and $this->viewDiffDatasScheduledForDeletion->contains($l)) {
                $this->viewDiffDatasScheduledForDeletion->remove($this->viewDiffDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildViewDiffData $viewDiffData The ChildViewDiffData object to add.
     */
    protected function doAddViewDiffData(ChildViewDiffData $viewDiffData)
    {
        $this->collViewDiffDatas[]= $viewDiffData;
        $viewDiffData->setChannel($this);
    }

    /**
     * @param  ChildViewDiffData $viewDiffData The ChildViewDiffData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeViewDiffData(ChildViewDiffData $viewDiffData)
    {
        if ($this->getViewDiffDatas()->contains($viewDiffData)) {
            $pos = $this->collViewDiffDatas->search($viewDiffData);
            $this->collViewDiffDatas->remove($pos);
            if (null === $this->viewDiffDatasScheduledForDeletion) {
                $this->viewDiffDatasScheduledForDeletion = clone $this->collViewDiffDatas;
                $this->viewDiffDatasScheduledForDeletion->clear();
            }
            $this->viewDiffDatasScheduledForDeletion[]= clone $viewDiffData;
            $viewDiffData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ViewDiffDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildViewDiffData[] List of ChildViewDiffData objects
     */
    public function getViewDiffDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildViewDiffDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getViewDiffDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ViewDiffDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildViewDiffData[] List of ChildViewDiffData objects
     */
    public function getViewDiffDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildViewDiffDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getViewDiffDatas($query, $con);
    }

    /**
     * Clears out the collViewerDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addViewerDatas()
     */
    public function clearViewerDatas()
    {
        $this->collViewerDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collViewerDatas collection loaded partially.
     */
    public function resetPartialViewerDatas($v = true)
    {
        $this->collViewerDatasPartial = $v;
    }

    /**
     * Initializes the collViewerDatas collection.
     *
     * By default this just sets the collViewerDatas collection to an empty array (like clearcollViewerDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initViewerDatas($overrideExisting = true)
    {
        if (null !== $this->collViewerDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ViewerDataTableMap::getTableMap()->getCollectionClassName();

        $this->collViewerDatas = new $collectionClassName;
        $this->collViewerDatas->setModel('\IiMedias\StreamBundle\Model\ViewerData');
    }

    /**
     * Gets an array of ChildViewerData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildViewerData[] List of ChildViewerData objects
     * @throws PropelException
     */
    public function getViewerDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collViewerDatasPartial && !$this->isNew();
        if (null === $this->collViewerDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collViewerDatas) {
                // return empty collection
                $this->initViewerDatas();
            } else {
                $collViewerDatas = ChildViewerDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collViewerDatasPartial && count($collViewerDatas)) {
                        $this->initViewerDatas(false);

                        foreach ($collViewerDatas as $obj) {
                            if (false == $this->collViewerDatas->contains($obj)) {
                                $this->collViewerDatas->append($obj);
                            }
                        }

                        $this->collViewerDatasPartial = true;
                    }

                    return $collViewerDatas;
                }

                if ($partial && $this->collViewerDatas) {
                    foreach ($this->collViewerDatas as $obj) {
                        if ($obj->isNew()) {
                            $collViewerDatas[] = $obj;
                        }
                    }
                }

                $this->collViewerDatas = $collViewerDatas;
                $this->collViewerDatasPartial = false;
            }
        }

        return $this->collViewerDatas;
    }

    /**
     * Sets a collection of ChildViewerData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $viewerDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setViewerDatas(Collection $viewerDatas, ConnectionInterface $con = null)
    {
        /** @var ChildViewerData[] $viewerDatasToDelete */
        $viewerDatasToDelete = $this->getViewerDatas(new Criteria(), $con)->diff($viewerDatas);


        $this->viewerDatasScheduledForDeletion = $viewerDatasToDelete;

        foreach ($viewerDatasToDelete as $viewerDataRemoved) {
            $viewerDataRemoved->setChannel(null);
        }

        $this->collViewerDatas = null;
        foreach ($viewerDatas as $viewerData) {
            $this->addViewerData($viewerData);
        }

        $this->collViewerDatas = $viewerDatas;
        $this->collViewerDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ViewerData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ViewerData objects.
     * @throws PropelException
     */
    public function countViewerDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collViewerDatasPartial && !$this->isNew();
        if (null === $this->collViewerDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collViewerDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getViewerDatas());
            }

            $query = ChildViewerDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collViewerDatas);
    }

    /**
     * Method called to associate a ChildViewerData object to this object
     * through the ChildViewerData foreign key attribute.
     *
     * @param  ChildViewerData $l ChildViewerData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addViewerData(ChildViewerData $l)
    {
        if ($this->collViewerDatas === null) {
            $this->initViewerDatas();
            $this->collViewerDatasPartial = true;
        }

        if (!$this->collViewerDatas->contains($l)) {
            $this->doAddViewerData($l);

            if ($this->viewerDatasScheduledForDeletion and $this->viewerDatasScheduledForDeletion->contains($l)) {
                $this->viewerDatasScheduledForDeletion->remove($this->viewerDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildViewerData $viewerData The ChildViewerData object to add.
     */
    protected function doAddViewerData(ChildViewerData $viewerData)
    {
        $this->collViewerDatas[]= $viewerData;
        $viewerData->setChannel($this);
    }

    /**
     * @param  ChildViewerData $viewerData The ChildViewerData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeViewerData(ChildViewerData $viewerData)
    {
        if ($this->getViewerDatas()->contains($viewerData)) {
            $pos = $this->collViewerDatas->search($viewerData);
            $this->collViewerDatas->remove($pos);
            if (null === $this->viewerDatasScheduledForDeletion) {
                $this->viewerDatasScheduledForDeletion = clone $this->collViewerDatas;
                $this->viewerDatasScheduledForDeletion->clear();
            }
            $this->viewerDatasScheduledForDeletion[]= clone $viewerData;
            $viewerData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ViewerDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildViewerData[] List of ChildViewerData objects
     */
    public function getViewerDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildViewerDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getViewerDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related ViewerDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildViewerData[] List of ChildViewerData objects
     */
    public function getViewerDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildViewerDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getViewerDatas($query, $con);
    }

    /**
     * Clears out the collFollowDiffDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFollowDiffDatas()
     */
    public function clearFollowDiffDatas()
    {
        $this->collFollowDiffDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFollowDiffDatas collection loaded partially.
     */
    public function resetPartialFollowDiffDatas($v = true)
    {
        $this->collFollowDiffDatasPartial = $v;
    }

    /**
     * Initializes the collFollowDiffDatas collection.
     *
     * By default this just sets the collFollowDiffDatas collection to an empty array (like clearcollFollowDiffDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFollowDiffDatas($overrideExisting = true)
    {
        if (null !== $this->collFollowDiffDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = FollowDiffDataTableMap::getTableMap()->getCollectionClassName();

        $this->collFollowDiffDatas = new $collectionClassName;
        $this->collFollowDiffDatas->setModel('\IiMedias\StreamBundle\Model\FollowDiffData');
    }

    /**
     * Gets an array of ChildFollowDiffData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFollowDiffData[] List of ChildFollowDiffData objects
     * @throws PropelException
     */
    public function getFollowDiffDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowDiffDatasPartial && !$this->isNew();
        if (null === $this->collFollowDiffDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFollowDiffDatas) {
                // return empty collection
                $this->initFollowDiffDatas();
            } else {
                $collFollowDiffDatas = ChildFollowDiffDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFollowDiffDatasPartial && count($collFollowDiffDatas)) {
                        $this->initFollowDiffDatas(false);

                        foreach ($collFollowDiffDatas as $obj) {
                            if (false == $this->collFollowDiffDatas->contains($obj)) {
                                $this->collFollowDiffDatas->append($obj);
                            }
                        }

                        $this->collFollowDiffDatasPartial = true;
                    }

                    return $collFollowDiffDatas;
                }

                if ($partial && $this->collFollowDiffDatas) {
                    foreach ($this->collFollowDiffDatas as $obj) {
                        if ($obj->isNew()) {
                            $collFollowDiffDatas[] = $obj;
                        }
                    }
                }

                $this->collFollowDiffDatas = $collFollowDiffDatas;
                $this->collFollowDiffDatasPartial = false;
            }
        }

        return $this->collFollowDiffDatas;
    }

    /**
     * Sets a collection of ChildFollowDiffData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $followDiffDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setFollowDiffDatas(Collection $followDiffDatas, ConnectionInterface $con = null)
    {
        /** @var ChildFollowDiffData[] $followDiffDatasToDelete */
        $followDiffDatasToDelete = $this->getFollowDiffDatas(new Criteria(), $con)->diff($followDiffDatas);


        $this->followDiffDatasScheduledForDeletion = $followDiffDatasToDelete;

        foreach ($followDiffDatasToDelete as $followDiffDataRemoved) {
            $followDiffDataRemoved->setChannel(null);
        }

        $this->collFollowDiffDatas = null;
        foreach ($followDiffDatas as $followDiffData) {
            $this->addFollowDiffData($followDiffData);
        }

        $this->collFollowDiffDatas = $followDiffDatas;
        $this->collFollowDiffDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FollowDiffData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FollowDiffData objects.
     * @throws PropelException
     */
    public function countFollowDiffDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowDiffDatasPartial && !$this->isNew();
        if (null === $this->collFollowDiffDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFollowDiffDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFollowDiffDatas());
            }

            $query = ChildFollowDiffDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collFollowDiffDatas);
    }

    /**
     * Method called to associate a ChildFollowDiffData object to this object
     * through the ChildFollowDiffData foreign key attribute.
     *
     * @param  ChildFollowDiffData $l ChildFollowDiffData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addFollowDiffData(ChildFollowDiffData $l)
    {
        if ($this->collFollowDiffDatas === null) {
            $this->initFollowDiffDatas();
            $this->collFollowDiffDatasPartial = true;
        }

        if (!$this->collFollowDiffDatas->contains($l)) {
            $this->doAddFollowDiffData($l);

            if ($this->followDiffDatasScheduledForDeletion and $this->followDiffDatasScheduledForDeletion->contains($l)) {
                $this->followDiffDatasScheduledForDeletion->remove($this->followDiffDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFollowDiffData $followDiffData The ChildFollowDiffData object to add.
     */
    protected function doAddFollowDiffData(ChildFollowDiffData $followDiffData)
    {
        $this->collFollowDiffDatas[]= $followDiffData;
        $followDiffData->setChannel($this);
    }

    /**
     * @param  ChildFollowDiffData $followDiffData The ChildFollowDiffData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeFollowDiffData(ChildFollowDiffData $followDiffData)
    {
        if ($this->getFollowDiffDatas()->contains($followDiffData)) {
            $pos = $this->collFollowDiffDatas->search($followDiffData);
            $this->collFollowDiffDatas->remove($pos);
            if (null === $this->followDiffDatasScheduledForDeletion) {
                $this->followDiffDatasScheduledForDeletion = clone $this->collFollowDiffDatas;
                $this->followDiffDatasScheduledForDeletion->clear();
            }
            $this->followDiffDatasScheduledForDeletion[]= clone $followDiffData;
            $followDiffData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowDiffDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowDiffData[] List of ChildFollowDiffData objects
     */
    public function getFollowDiffDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowDiffDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getFollowDiffDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related FollowDiffDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowDiffData[] List of ChildFollowDiffData objects
     */
    public function getFollowDiffDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowDiffDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getFollowDiffDatas($query, $con);
    }

    /**
     * Clears out the collStatusDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addStatusDatas()
     */
    public function clearStatusDatas()
    {
        $this->collStatusDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collStatusDatas collection loaded partially.
     */
    public function resetPartialStatusDatas($v = true)
    {
        $this->collStatusDatasPartial = $v;
    }

    /**
     * Initializes the collStatusDatas collection.
     *
     * By default this just sets the collStatusDatas collection to an empty array (like clearcollStatusDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStatusDatas($overrideExisting = true)
    {
        if (null !== $this->collStatusDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = StatusDataTableMap::getTableMap()->getCollectionClassName();

        $this->collStatusDatas = new $collectionClassName;
        $this->collStatusDatas->setModel('\IiMedias\StreamBundle\Model\StatusData');
    }

    /**
     * Gets an array of ChildStatusData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildStatusData[] List of ChildStatusData objects
     * @throws PropelException
     */
    public function getStatusDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collStatusDatasPartial && !$this->isNew();
        if (null === $this->collStatusDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStatusDatas) {
                // return empty collection
                $this->initStatusDatas();
            } else {
                $collStatusDatas = ChildStatusDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collStatusDatasPartial && count($collStatusDatas)) {
                        $this->initStatusDatas(false);

                        foreach ($collStatusDatas as $obj) {
                            if (false == $this->collStatusDatas->contains($obj)) {
                                $this->collStatusDatas->append($obj);
                            }
                        }

                        $this->collStatusDatasPartial = true;
                    }

                    return $collStatusDatas;
                }

                if ($partial && $this->collStatusDatas) {
                    foreach ($this->collStatusDatas as $obj) {
                        if ($obj->isNew()) {
                            $collStatusDatas[] = $obj;
                        }
                    }
                }

                $this->collStatusDatas = $collStatusDatas;
                $this->collStatusDatasPartial = false;
            }
        }

        return $this->collStatusDatas;
    }

    /**
     * Sets a collection of ChildStatusData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $statusDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setStatusDatas(Collection $statusDatas, ConnectionInterface $con = null)
    {
        /** @var ChildStatusData[] $statusDatasToDelete */
        $statusDatasToDelete = $this->getStatusDatas(new Criteria(), $con)->diff($statusDatas);


        $this->statusDatasScheduledForDeletion = $statusDatasToDelete;

        foreach ($statusDatasToDelete as $statusDataRemoved) {
            $statusDataRemoved->setChannel(null);
        }

        $this->collStatusDatas = null;
        foreach ($statusDatas as $statusData) {
            $this->addStatusData($statusData);
        }

        $this->collStatusDatas = $statusDatas;
        $this->collStatusDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related StatusData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related StatusData objects.
     * @throws PropelException
     */
    public function countStatusDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collStatusDatasPartial && !$this->isNew();
        if (null === $this->collStatusDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStatusDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStatusDatas());
            }

            $query = ChildStatusDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collStatusDatas);
    }

    /**
     * Method called to associate a ChildStatusData object to this object
     * through the ChildStatusData foreign key attribute.
     *
     * @param  ChildStatusData $l ChildStatusData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addStatusData(ChildStatusData $l)
    {
        if ($this->collStatusDatas === null) {
            $this->initStatusDatas();
            $this->collStatusDatasPartial = true;
        }

        if (!$this->collStatusDatas->contains($l)) {
            $this->doAddStatusData($l);

            if ($this->statusDatasScheduledForDeletion and $this->statusDatasScheduledForDeletion->contains($l)) {
                $this->statusDatasScheduledForDeletion->remove($this->statusDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildStatusData $statusData The ChildStatusData object to add.
     */
    protected function doAddStatusData(ChildStatusData $statusData)
    {
        $this->collStatusDatas[]= $statusData;
        $statusData->setChannel($this);
    }

    /**
     * @param  ChildStatusData $statusData The ChildStatusData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeStatusData(ChildStatusData $statusData)
    {
        if ($this->getStatusDatas()->contains($statusData)) {
            $pos = $this->collStatusDatas->search($statusData);
            $this->collStatusDatas->remove($pos);
            if (null === $this->statusDatasScheduledForDeletion) {
                $this->statusDatasScheduledForDeletion = clone $this->collStatusDatas;
                $this->statusDatasScheduledForDeletion->clear();
            }
            $this->statusDatasScheduledForDeletion[]= clone $statusData;
            $statusData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related StatusDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildStatusData[] List of ChildStatusData objects
     */
    public function getStatusDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildStatusDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getStatusDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related StatusDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildStatusData[] List of ChildStatusData objects
     */
    public function getStatusDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildStatusDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getStatusDatas($query, $con);
    }

    /**
     * Clears out the collTypeDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTypeDatas()
     */
    public function clearTypeDatas()
    {
        $this->collTypeDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTypeDatas collection loaded partially.
     */
    public function resetPartialTypeDatas($v = true)
    {
        $this->collTypeDatasPartial = $v;
    }

    /**
     * Initializes the collTypeDatas collection.
     *
     * By default this just sets the collTypeDatas collection to an empty array (like clearcollTypeDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTypeDatas($overrideExisting = true)
    {
        if (null !== $this->collTypeDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = TypeDataTableMap::getTableMap()->getCollectionClassName();

        $this->collTypeDatas = new $collectionClassName;
        $this->collTypeDatas->setModel('\IiMedias\StreamBundle\Model\TypeData');
    }

    /**
     * Gets an array of ChildTypeData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTypeData[] List of ChildTypeData objects
     * @throws PropelException
     */
    public function getTypeDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTypeDatasPartial && !$this->isNew();
        if (null === $this->collTypeDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTypeDatas) {
                // return empty collection
                $this->initTypeDatas();
            } else {
                $collTypeDatas = ChildTypeDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTypeDatasPartial && count($collTypeDatas)) {
                        $this->initTypeDatas(false);

                        foreach ($collTypeDatas as $obj) {
                            if (false == $this->collTypeDatas->contains($obj)) {
                                $this->collTypeDatas->append($obj);
                            }
                        }

                        $this->collTypeDatasPartial = true;
                    }

                    return $collTypeDatas;
                }

                if ($partial && $this->collTypeDatas) {
                    foreach ($this->collTypeDatas as $obj) {
                        if ($obj->isNew()) {
                            $collTypeDatas[] = $obj;
                        }
                    }
                }

                $this->collTypeDatas = $collTypeDatas;
                $this->collTypeDatasPartial = false;
            }
        }

        return $this->collTypeDatas;
    }

    /**
     * Sets a collection of ChildTypeData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $typeDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setTypeDatas(Collection $typeDatas, ConnectionInterface $con = null)
    {
        /** @var ChildTypeData[] $typeDatasToDelete */
        $typeDatasToDelete = $this->getTypeDatas(new Criteria(), $con)->diff($typeDatas);


        $this->typeDatasScheduledForDeletion = $typeDatasToDelete;

        foreach ($typeDatasToDelete as $typeDataRemoved) {
            $typeDataRemoved->setChannel(null);
        }

        $this->collTypeDatas = null;
        foreach ($typeDatas as $typeData) {
            $this->addTypeData($typeData);
        }

        $this->collTypeDatas = $typeDatas;
        $this->collTypeDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TypeData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related TypeData objects.
     * @throws PropelException
     */
    public function countTypeDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTypeDatasPartial && !$this->isNew();
        if (null === $this->collTypeDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTypeDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTypeDatas());
            }

            $query = ChildTypeDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collTypeDatas);
    }

    /**
     * Method called to associate a ChildTypeData object to this object
     * through the ChildTypeData foreign key attribute.
     *
     * @param  ChildTypeData $l ChildTypeData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addTypeData(ChildTypeData $l)
    {
        if ($this->collTypeDatas === null) {
            $this->initTypeDatas();
            $this->collTypeDatasPartial = true;
        }

        if (!$this->collTypeDatas->contains($l)) {
            $this->doAddTypeData($l);

            if ($this->typeDatasScheduledForDeletion and $this->typeDatasScheduledForDeletion->contains($l)) {
                $this->typeDatasScheduledForDeletion->remove($this->typeDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTypeData $typeData The ChildTypeData object to add.
     */
    protected function doAddTypeData(ChildTypeData $typeData)
    {
        $this->collTypeDatas[]= $typeData;
        $typeData->setChannel($this);
    }

    /**
     * @param  ChildTypeData $typeData The ChildTypeData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeTypeData(ChildTypeData $typeData)
    {
        if ($this->getTypeDatas()->contains($typeData)) {
            $pos = $this->collTypeDatas->search($typeData);
            $this->collTypeDatas->remove($pos);
            if (null === $this->typeDatasScheduledForDeletion) {
                $this->typeDatasScheduledForDeletion = clone $this->collTypeDatas;
                $this->typeDatasScheduledForDeletion->clear();
            }
            $this->typeDatasScheduledForDeletion[]= clone $typeData;
            $typeData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related TypeDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTypeData[] List of ChildTypeData objects
     */
    public function getTypeDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTypeDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getTypeDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related TypeDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTypeData[] List of ChildTypeData objects
     */
    public function getTypeDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTypeDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getTypeDatas($query, $con);
    }

    /**
     * Clears out the collGameDatas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addGameDatas()
     */
    public function clearGameDatas()
    {
        $this->collGameDatas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collGameDatas collection loaded partially.
     */
    public function resetPartialGameDatas($v = true)
    {
        $this->collGameDatasPartial = $v;
    }

    /**
     * Initializes the collGameDatas collection.
     *
     * By default this just sets the collGameDatas collection to an empty array (like clearcollGameDatas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initGameDatas($overrideExisting = true)
    {
        if (null !== $this->collGameDatas && !$overrideExisting) {
            return;
        }

        $collectionClassName = GameDataTableMap::getTableMap()->getCollectionClassName();

        $this->collGameDatas = new $collectionClassName;
        $this->collGameDatas->setModel('\IiMedias\StreamBundle\Model\GameData');
    }

    /**
     * Gets an array of ChildGameData objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildGameData[] List of ChildGameData objects
     * @throws PropelException
     */
    public function getGameDatas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collGameDatasPartial && !$this->isNew();
        if (null === $this->collGameDatas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collGameDatas) {
                // return empty collection
                $this->initGameDatas();
            } else {
                $collGameDatas = ChildGameDataQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collGameDatasPartial && count($collGameDatas)) {
                        $this->initGameDatas(false);

                        foreach ($collGameDatas as $obj) {
                            if (false == $this->collGameDatas->contains($obj)) {
                                $this->collGameDatas->append($obj);
                            }
                        }

                        $this->collGameDatasPartial = true;
                    }

                    return $collGameDatas;
                }

                if ($partial && $this->collGameDatas) {
                    foreach ($this->collGameDatas as $obj) {
                        if ($obj->isNew()) {
                            $collGameDatas[] = $obj;
                        }
                    }
                }

                $this->collGameDatas = $collGameDatas;
                $this->collGameDatasPartial = false;
            }
        }

        return $this->collGameDatas;
    }

    /**
     * Sets a collection of ChildGameData objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $gameDatas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setGameDatas(Collection $gameDatas, ConnectionInterface $con = null)
    {
        /** @var ChildGameData[] $gameDatasToDelete */
        $gameDatasToDelete = $this->getGameDatas(new Criteria(), $con)->diff($gameDatas);


        $this->gameDatasScheduledForDeletion = $gameDatasToDelete;

        foreach ($gameDatasToDelete as $gameDataRemoved) {
            $gameDataRemoved->setChannel(null);
        }

        $this->collGameDatas = null;
        foreach ($gameDatas as $gameData) {
            $this->addGameData($gameData);
        }

        $this->collGameDatas = $gameDatas;
        $this->collGameDatasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related GameData objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related GameData objects.
     * @throws PropelException
     */
    public function countGameDatas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collGameDatasPartial && !$this->isNew();
        if (null === $this->collGameDatas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collGameDatas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getGameDatas());
            }

            $query = ChildGameDataQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collGameDatas);
    }

    /**
     * Method called to associate a ChildGameData object to this object
     * through the ChildGameData foreign key attribute.
     *
     * @param  ChildGameData $l ChildGameData
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addGameData(ChildGameData $l)
    {
        if ($this->collGameDatas === null) {
            $this->initGameDatas();
            $this->collGameDatasPartial = true;
        }

        if (!$this->collGameDatas->contains($l)) {
            $this->doAddGameData($l);

            if ($this->gameDatasScheduledForDeletion and $this->gameDatasScheduledForDeletion->contains($l)) {
                $this->gameDatasScheduledForDeletion->remove($this->gameDatasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildGameData $gameData The ChildGameData object to add.
     */
    protected function doAddGameData(ChildGameData $gameData)
    {
        $this->collGameDatas[]= $gameData;
        $gameData->setChannel($this);
    }

    /**
     * @param  ChildGameData $gameData The ChildGameData object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeGameData(ChildGameData $gameData)
    {
        if ($this->getGameDatas()->contains($gameData)) {
            $pos = $this->collGameDatas->search($gameData);
            $this->collGameDatas->remove($pos);
            if (null === $this->gameDatasScheduledForDeletion) {
                $this->gameDatasScheduledForDeletion = clone $this->collGameDatas;
                $this->gameDatasScheduledForDeletion->clear();
            }
            $this->gameDatasScheduledForDeletion[]= clone $gameData;
            $gameData->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related GameDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGameData[] List of ChildGameData objects
     */
    public function getGameDatasJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGameDataQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getGameDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related GameDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGameData[] List of ChildGameData objects
     */
    public function getGameDatasJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGameDataQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getGameDatas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related GameDatas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGameData[] List of ChildGameData objects
     */
    public function getGameDatasJoinApiTwitchGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGameDataQuery::create(null, $criteria);
        $query->joinWith('ApiTwitchGame', $joinBehavior);

        return $this->getGameDatas($query, $con);
    }

    /**
     * Clears out the collStats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addStats()
     */
    public function clearStats()
    {
        $this->collStats = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collStats collection loaded partially.
     */
    public function resetPartialStats($v = true)
    {
        $this->collStatsPartial = $v;
    }

    /**
     * Initializes the collStats collection.
     *
     * By default this just sets the collStats collection to an empty array (like clearcollStats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStats($overrideExisting = true)
    {
        if (null !== $this->collStats && !$overrideExisting) {
            return;
        }

        $collectionClassName = StatTableMap::getTableMap()->getCollectionClassName();

        $this->collStats = new $collectionClassName;
        $this->collStats->setModel('\IiMedias\StreamBundle\Model\Stat');
    }

    /**
     * Gets an array of ChildStat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildStat[] List of ChildStat objects
     * @throws PropelException
     */
    public function getStats(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collStatsPartial && !$this->isNew();
        if (null === $this->collStats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStats) {
                // return empty collection
                $this->initStats();
            } else {
                $collStats = ChildStatQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collStatsPartial && count($collStats)) {
                        $this->initStats(false);

                        foreach ($collStats as $obj) {
                            if (false == $this->collStats->contains($obj)) {
                                $this->collStats->append($obj);
                            }
                        }

                        $this->collStatsPartial = true;
                    }

                    return $collStats;
                }

                if ($partial && $this->collStats) {
                    foreach ($this->collStats as $obj) {
                        if ($obj->isNew()) {
                            $collStats[] = $obj;
                        }
                    }
                }

                $this->collStats = $collStats;
                $this->collStatsPartial = false;
            }
        }

        return $this->collStats;
    }

    /**
     * Sets a collection of ChildStat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $stats A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setStats(Collection $stats, ConnectionInterface $con = null)
    {
        /** @var ChildStat[] $statsToDelete */
        $statsToDelete = $this->getStats(new Criteria(), $con)->diff($stats);


        $this->statsScheduledForDeletion = $statsToDelete;

        foreach ($statsToDelete as $statRemoved) {
            $statRemoved->setChannel(null);
        }

        $this->collStats = null;
        foreach ($stats as $stat) {
            $this->addStat($stat);
        }

        $this->collStats = $stats;
        $this->collStatsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Stat objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Stat objects.
     * @throws PropelException
     */
    public function countStats(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collStatsPartial && !$this->isNew();
        if (null === $this->collStats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStats());
            }

            $query = ChildStatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collStats);
    }

    /**
     * Method called to associate a ChildStat object to this object
     * through the ChildStat foreign key attribute.
     *
     * @param  ChildStat $l ChildStat
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addStat(ChildStat $l)
    {
        if ($this->collStats === null) {
            $this->initStats();
            $this->collStatsPartial = true;
        }

        if (!$this->collStats->contains($l)) {
            $this->doAddStat($l);

            if ($this->statsScheduledForDeletion and $this->statsScheduledForDeletion->contains($l)) {
                $this->statsScheduledForDeletion->remove($this->statsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildStat $stat The ChildStat object to add.
     */
    protected function doAddStat(ChildStat $stat)
    {
        $this->collStats[]= $stat;
        $stat->setChannel($this);
    }

    /**
     * @param  ChildStat $stat The ChildStat object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeStat(ChildStat $stat)
    {
        if ($this->getStats()->contains($stat)) {
            $pos = $this->collStats->search($stat);
            $this->collStats->remove($pos);
            if (null === $this->statsScheduledForDeletion) {
                $this->statsScheduledForDeletion = clone $this->collStats;
                $this->statsScheduledForDeletion->clear();
            }
            $this->statsScheduledForDeletion[]= clone $stat;
            $stat->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related Stats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildStat[] List of ChildStat objects
     */
    public function getStatsJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildStatQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getStats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related Stats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildStat[] List of ChildStat objects
     */
    public function getStatsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildStatQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getStats($query, $con);
    }

    /**
     * Clears out the collExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addExperiences()
     */
    public function clearExperiences()
    {
        $this->collExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collExperiences collection loaded partially.
     */
    public function resetPartialExperiences($v = true)
    {
        $this->collExperiencesPartial = $v;
    }

    /**
     * Initializes the collExperiences collection.
     *
     * By default this just sets the collExperiences collection to an empty array (like clearcollExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initExperiences($overrideExisting = true)
    {
        if (null !== $this->collExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = ExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collExperiences = new $collectionClassName;
        $this->collExperiences->setModel('\IiMedias\StreamBundle\Model\Experience');
    }

    /**
     * Gets an array of ChildExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChannel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     * @throws PropelException
     */
    public function getExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collExperiencesPartial && !$this->isNew();
        if (null === $this->collExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collExperiences) {
                // return empty collection
                $this->initExperiences();
            } else {
                $collExperiences = ChildExperienceQuery::create(null, $criteria)
                    ->filterByChannel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collExperiencesPartial && count($collExperiences)) {
                        $this->initExperiences(false);

                        foreach ($collExperiences as $obj) {
                            if (false == $this->collExperiences->contains($obj)) {
                                $this->collExperiences->append($obj);
                            }
                        }

                        $this->collExperiencesPartial = true;
                    }

                    return $collExperiences;
                }

                if ($partial && $this->collExperiences) {
                    foreach ($this->collExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collExperiences[] = $obj;
                        }
                    }
                }

                $this->collExperiences = $collExperiences;
                $this->collExperiencesPartial = false;
            }
        }

        return $this->collExperiences;
    }

    /**
     * Sets a collection of ChildExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $experiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function setExperiences(Collection $experiences, ConnectionInterface $con = null)
    {
        /** @var ChildExperience[] $experiencesToDelete */
        $experiencesToDelete = $this->getExperiences(new Criteria(), $con)->diff($experiences);


        $this->experiencesScheduledForDeletion = $experiencesToDelete;

        foreach ($experiencesToDelete as $experienceRemoved) {
            $experienceRemoved->setChannel(null);
        }

        $this->collExperiences = null;
        foreach ($experiences as $experience) {
            $this->addExperience($experience);
        }

        $this->collExperiences = $experiences;
        $this->collExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Experience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Experience objects.
     * @throws PropelException
     */
    public function countExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collExperiencesPartial && !$this->isNew();
        if (null === $this->collExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getExperiences());
            }

            $query = ChildExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChannel($this)
                ->count($con);
        }

        return count($this->collExperiences);
    }

    /**
     * Method called to associate a ChildExperience object to this object
     * through the ChildExperience foreign key attribute.
     *
     * @param  ChildExperience $l ChildExperience
     * @return $this|\IiMedias\StreamBundle\Model\Channel The current object (for fluent API support)
     */
    public function addExperience(ChildExperience $l)
    {
        if ($this->collExperiences === null) {
            $this->initExperiences();
            $this->collExperiencesPartial = true;
        }

        if (!$this->collExperiences->contains($l)) {
            $this->doAddExperience($l);

            if ($this->experiencesScheduledForDeletion and $this->experiencesScheduledForDeletion->contains($l)) {
                $this->experiencesScheduledForDeletion->remove($this->experiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildExperience $experience The ChildExperience object to add.
     */
    protected function doAddExperience(ChildExperience $experience)
    {
        $this->collExperiences[]= $experience;
        $experience->setChannel($this);
    }

    /**
     * @param  ChildExperience $experience The ChildExperience object to remove.
     * @return $this|ChildChannel The current object (for fluent API support)
     */
    public function removeExperience(ChildExperience $experience)
    {
        if ($this->getExperiences()->contains($experience)) {
            $pos = $this->collExperiences->search($experience);
            $this->collExperiences->remove($pos);
            if (null === $this->experiencesScheduledForDeletion) {
                $this->experiencesScheduledForDeletion = clone $this->collExperiences;
                $this->experiencesScheduledForDeletion->clear();
            }
            $this->experiencesScheduledForDeletion[]= clone $experience;
            $experience->setChannel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Channel is new, it will return
     * an empty collection; or if this Channel has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Channel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getExperiences($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStream) {
            $this->aStream->removeChannel($this);
        }
        if (null !== $this->aSite) {
            $this->aSite->removeChannel($this);
        }
        if (null !== $this->aBot) {
            $this->aBot->removeChannelBot($this);
        }
        if (null !== $this->aApiTwitchGame) {
            $this->aApiTwitchGame->removeChannel($this);
        }
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserStchan($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserStchan($this);
        }
        $this->stchan_id = null;
        $this->stchan_ststrm_id = null;
        $this->stchan_stsite_id = null;
        $this->stchan_channel = null;
        $this->stchan_ststrm_bot_id = null;
        $this->stchan_can_scan = null;
        $this->stchan_password_oauth = null;
        $this->stchan_channel_id = null;
        $this->stchan_client_id = null;
        $this->stchan_secred_id = null;
        $this->stchan_channel_partner = null;
        $this->stchan_live_mode = null;
        $this->stchan_status = null;
        $this->stchan_vgatga_id = null;
        $this->stchan_game = null;
        $this->stchan_stream_type = null;
        $this->stchan_followers_count = null;
        $this->stchan_followers_diff = null;
        $this->stchan_views_count = null;
        $this->stchan_views_diff = null;
        $this->stchan_viewers_count = null;
        $this->stchan_chatters_count = null;
        $this->stchan_hosts_count = null;
        $this->stchan_live_viewers_count = null;
        $this->stchan_messages_count = null;
        $this->stchan_lock_chatters_scan = null;
        $this->stchan_lock_hosts_scan = null;
        $this->stchan_created_by_user_id = null;
        $this->stchan_updated_by_user_id = null;
        $this->stchan_created_at = null;
        $this->stchan_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUserExperiences) {
                foreach ($this->collUserExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDeepBotImportExperiences) {
                foreach ($this->collDeepBotImportExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMessageExperiences) {
                foreach ($this->collMessageExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChatterExperiences) {
                foreach ($this->collChatterExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFollowExperiences) {
                foreach ($this->collFollowExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collHostExperiences) {
                foreach ($this->collHostExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collViewDiffDatas) {
                foreach ($this->collViewDiffDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collViewerDatas) {
                foreach ($this->collViewerDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFollowDiffDatas) {
                foreach ($this->collFollowDiffDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collStatusDatas) {
                foreach ($this->collStatusDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTypeDatas) {
                foreach ($this->collTypeDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collGameDatas) {
                foreach ($this->collGameDatas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collStats) {
                foreach ($this->collStats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collExperiences) {
                foreach ($this->collExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUserExperiences = null;
        $this->collDeepBotImportExperiences = null;
        $this->collMessageExperiences = null;
        $this->collChatterExperiences = null;
        $this->collFollowExperiences = null;
        $this->collHostExperiences = null;
        $this->collViewDiffDatas = null;
        $this->collViewerDatas = null;
        $this->collFollowDiffDatas = null;
        $this->collStatusDatas = null;
        $this->collTypeDatas = null;
        $this->collGameDatas = null;
        $this->collStats = null;
        $this->collExperiences = null;
        $this->aStream = null;
        $this->aSite = null;
        $this->aBot = null;
        $this->aApiTwitchGame = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChannelTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildChannel The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ChannelTableMap::COL_STCHAN_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

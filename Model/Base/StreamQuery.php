<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\Stream as ChildStream;
use IiMedias\StreamBundle\Model\StreamQuery as ChildStreamQuery;
use IiMedias\StreamBundle\Model\Map\StreamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_stream_ststrm' table.
 *
 *
 *
 * @method     ChildStreamQuery orderById($order = Criteria::ASC) Order by the ststrm_id column
 * @method     ChildStreamQuery orderByType($order = Criteria::ASC) Order by the ststrm_type column
 * @method     ChildStreamQuery orderByName($order = Criteria::ASC) Order by the ststrm_name column
 * @method     ChildStreamQuery orderByCanScan($order = Criteria::ASC) Order by the ststrm_can_scan column
 * @method     ChildStreamQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the ststrm_created_by_user_id column
 * @method     ChildStreamQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the ststrm_updated_by_user_id column
 * @method     ChildStreamQuery orderByCreatedAt($order = Criteria::ASC) Order by the ststrm_created_at column
 * @method     ChildStreamQuery orderByUpdatedAt($order = Criteria::ASC) Order by the ststrm_updated_at column
 *
 * @method     ChildStreamQuery groupById() Group by the ststrm_id column
 * @method     ChildStreamQuery groupByType() Group by the ststrm_type column
 * @method     ChildStreamQuery groupByName() Group by the ststrm_name column
 * @method     ChildStreamQuery groupByCanScan() Group by the ststrm_can_scan column
 * @method     ChildStreamQuery groupByCreatedByUserId() Group by the ststrm_created_by_user_id column
 * @method     ChildStreamQuery groupByUpdatedByUserId() Group by the ststrm_updated_by_user_id column
 * @method     ChildStreamQuery groupByCreatedAt() Group by the ststrm_created_at column
 * @method     ChildStreamQuery groupByUpdatedAt() Group by the ststrm_updated_at column
 *
 * @method     ChildStreamQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStreamQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStreamQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStreamQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStreamQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStreamQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStreamQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildStreamQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildStreamQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildStreamQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildStreamQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildStreamQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildStreamQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildStreamQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildStreamQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildStreamQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildStreamQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildStreamQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildStreamQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildStreamQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildStreamQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildStreamQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildStreamQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildStreamQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildStreamQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildStreamQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildStreamQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildStreamQuery leftJoinChannelBot($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChannelBot relation
 * @method     ChildStreamQuery rightJoinChannelBot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChannelBot relation
 * @method     ChildStreamQuery innerJoinChannelBot($relationAlias = null) Adds a INNER JOIN clause to the query using the ChannelBot relation
 *
 * @method     ChildStreamQuery joinWithChannelBot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChannelBot relation
 *
 * @method     ChildStreamQuery leftJoinWithChannelBot() Adds a LEFT JOIN clause and with to the query using the ChannelBot relation
 * @method     ChildStreamQuery rightJoinWithChannelBot() Adds a RIGHT JOIN clause and with to the query using the ChannelBot relation
 * @method     ChildStreamQuery innerJoinWithChannelBot() Adds a INNER JOIN clause and with to the query using the ChannelBot relation
 *
 * @method     ChildStreamQuery leftJoinRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rank relation
 * @method     ChildStreamQuery rightJoinRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rank relation
 * @method     ChildStreamQuery innerJoinRank($relationAlias = null) Adds a INNER JOIN clause to the query using the Rank relation
 *
 * @method     ChildStreamQuery joinWithRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rank relation
 *
 * @method     ChildStreamQuery leftJoinWithRank() Adds a LEFT JOIN clause and with to the query using the Rank relation
 * @method     ChildStreamQuery rightJoinWithRank() Adds a RIGHT JOIN clause and with to the query using the Rank relation
 * @method     ChildStreamQuery innerJoinWithRank() Adds a INNER JOIN clause and with to the query using the Rank relation
 *
 * @method     ChildStreamQuery leftJoinAvatar($relationAlias = null) Adds a LEFT JOIN clause to the query using the Avatar relation
 * @method     ChildStreamQuery rightJoinAvatar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Avatar relation
 * @method     ChildStreamQuery innerJoinAvatar($relationAlias = null) Adds a INNER JOIN clause to the query using the Avatar relation
 *
 * @method     ChildStreamQuery joinWithAvatar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Avatar relation
 *
 * @method     ChildStreamQuery leftJoinWithAvatar() Adds a LEFT JOIN clause and with to the query using the Avatar relation
 * @method     ChildStreamQuery rightJoinWithAvatar() Adds a RIGHT JOIN clause and with to the query using the Avatar relation
 * @method     ChildStreamQuery innerJoinWithAvatar() Adds a INNER JOIN clause and with to the query using the Avatar relation
 *
 * @method     ChildStreamQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildStreamQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildStreamQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildStreamQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildStreamQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildStreamQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     ChildStreamQuery leftJoinDeepBotImportExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildStreamQuery rightJoinDeepBotImportExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildStreamQuery innerJoinDeepBotImportExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the DeepBotImportExperience relation
 *
 * @method     ChildStreamQuery joinWithDeepBotImportExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithDeepBotImportExperience() Adds a LEFT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildStreamQuery rightJoinWithDeepBotImportExperience() Adds a RIGHT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildStreamQuery innerJoinWithDeepBotImportExperience() Adds a INNER JOIN clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildStreamQuery leftJoinMessageExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageExperience relation
 * @method     ChildStreamQuery rightJoinMessageExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageExperience relation
 * @method     ChildStreamQuery innerJoinMessageExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageExperience relation
 *
 * @method     ChildStreamQuery joinWithMessageExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithMessageExperience() Adds a LEFT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildStreamQuery rightJoinWithMessageExperience() Adds a RIGHT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildStreamQuery innerJoinWithMessageExperience() Adds a INNER JOIN clause and with to the query using the MessageExperience relation
 *
 * @method     ChildStreamQuery leftJoinChatterExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildStreamQuery rightJoinChatterExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildStreamQuery innerJoinChatterExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatterExperience relation
 *
 * @method     ChildStreamQuery joinWithChatterExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithChatterExperience() Adds a LEFT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildStreamQuery rightJoinWithChatterExperience() Adds a RIGHT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildStreamQuery innerJoinWithChatterExperience() Adds a INNER JOIN clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildStreamQuery leftJoinFollowExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowExperience relation
 * @method     ChildStreamQuery rightJoinFollowExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowExperience relation
 * @method     ChildStreamQuery innerJoinFollowExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowExperience relation
 *
 * @method     ChildStreamQuery joinWithFollowExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithFollowExperience() Adds a LEFT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildStreamQuery rightJoinWithFollowExperience() Adds a RIGHT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildStreamQuery innerJoinWithFollowExperience() Adds a INNER JOIN clause and with to the query using the FollowExperience relation
 *
 * @method     ChildStreamQuery leftJoinHostExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the HostExperience relation
 * @method     ChildStreamQuery rightJoinHostExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HostExperience relation
 * @method     ChildStreamQuery innerJoinHostExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the HostExperience relation
 *
 * @method     ChildStreamQuery joinWithHostExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HostExperience relation
 *
 * @method     ChildStreamQuery leftJoinWithHostExperience() Adds a LEFT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildStreamQuery rightJoinWithHostExperience() Adds a RIGHT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildStreamQuery innerJoinWithHostExperience() Adds a INNER JOIN clause and with to the query using the HostExperience relation
 *
 * @method     ChildStreamQuery leftJoinViewDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildStreamQuery rightJoinViewDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildStreamQuery innerJoinViewDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewDiffData relation
 *
 * @method     ChildStreamQuery joinWithViewDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildStreamQuery leftJoinWithViewDiffData() Adds a LEFT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildStreamQuery rightJoinWithViewDiffData() Adds a RIGHT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildStreamQuery innerJoinWithViewDiffData() Adds a INNER JOIN clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildStreamQuery leftJoinViewerData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewerData relation
 * @method     ChildStreamQuery rightJoinViewerData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewerData relation
 * @method     ChildStreamQuery innerJoinViewerData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewerData relation
 *
 * @method     ChildStreamQuery joinWithViewerData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewerData relation
 *
 * @method     ChildStreamQuery leftJoinWithViewerData() Adds a LEFT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildStreamQuery rightJoinWithViewerData() Adds a RIGHT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildStreamQuery innerJoinWithViewerData() Adds a INNER JOIN clause and with to the query using the ViewerData relation
 *
 * @method     ChildStreamQuery leftJoinFollowDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildStreamQuery rightJoinFollowDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildStreamQuery innerJoinFollowDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowDiffData relation
 *
 * @method     ChildStreamQuery joinWithFollowDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildStreamQuery leftJoinWithFollowDiffData() Adds a LEFT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildStreamQuery rightJoinWithFollowDiffData() Adds a RIGHT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildStreamQuery innerJoinWithFollowDiffData() Adds a INNER JOIN clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildStreamQuery leftJoinStatusData($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusData relation
 * @method     ChildStreamQuery rightJoinStatusData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusData relation
 * @method     ChildStreamQuery innerJoinStatusData($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusData relation
 *
 * @method     ChildStreamQuery joinWithStatusData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StatusData relation
 *
 * @method     ChildStreamQuery leftJoinWithStatusData() Adds a LEFT JOIN clause and with to the query using the StatusData relation
 * @method     ChildStreamQuery rightJoinWithStatusData() Adds a RIGHT JOIN clause and with to the query using the StatusData relation
 * @method     ChildStreamQuery innerJoinWithStatusData() Adds a INNER JOIN clause and with to the query using the StatusData relation
 *
 * @method     ChildStreamQuery leftJoinTypeData($relationAlias = null) Adds a LEFT JOIN clause to the query using the TypeData relation
 * @method     ChildStreamQuery rightJoinTypeData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TypeData relation
 * @method     ChildStreamQuery innerJoinTypeData($relationAlias = null) Adds a INNER JOIN clause to the query using the TypeData relation
 *
 * @method     ChildStreamQuery joinWithTypeData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TypeData relation
 *
 * @method     ChildStreamQuery leftJoinWithTypeData() Adds a LEFT JOIN clause and with to the query using the TypeData relation
 * @method     ChildStreamQuery rightJoinWithTypeData() Adds a RIGHT JOIN clause and with to the query using the TypeData relation
 * @method     ChildStreamQuery innerJoinWithTypeData() Adds a INNER JOIN clause and with to the query using the TypeData relation
 *
 * @method     ChildStreamQuery leftJoinGameData($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameData relation
 * @method     ChildStreamQuery rightJoinGameData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameData relation
 * @method     ChildStreamQuery innerJoinGameData($relationAlias = null) Adds a INNER JOIN clause to the query using the GameData relation
 *
 * @method     ChildStreamQuery joinWithGameData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameData relation
 *
 * @method     ChildStreamQuery leftJoinWithGameData() Adds a LEFT JOIN clause and with to the query using the GameData relation
 * @method     ChildStreamQuery rightJoinWithGameData() Adds a RIGHT JOIN clause and with to the query using the GameData relation
 * @method     ChildStreamQuery innerJoinWithGameData() Adds a INNER JOIN clause and with to the query using the GameData relation
 *
 * @method     ChildStreamQuery leftJoinStat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stat relation
 * @method     ChildStreamQuery rightJoinStat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stat relation
 * @method     ChildStreamQuery innerJoinStat($relationAlias = null) Adds a INNER JOIN clause to the query using the Stat relation
 *
 * @method     ChildStreamQuery joinWithStat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stat relation
 *
 * @method     ChildStreamQuery leftJoinWithStat() Adds a LEFT JOIN clause and with to the query using the Stat relation
 * @method     ChildStreamQuery rightJoinWithStat() Adds a RIGHT JOIN clause and with to the query using the Stat relation
 * @method     ChildStreamQuery innerJoinWithStat() Adds a INNER JOIN clause and with to the query using the Stat relation
 *
 * @method     ChildStreamQuery leftJoinExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Experience relation
 * @method     ChildStreamQuery rightJoinExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Experience relation
 * @method     ChildStreamQuery innerJoinExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the Experience relation
 *
 * @method     ChildStreamQuery joinWithExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Experience relation
 *
 * @method     ChildStreamQuery leftJoinWithExperience() Adds a LEFT JOIN clause and with to the query using the Experience relation
 * @method     ChildStreamQuery rightJoinWithExperience() Adds a RIGHT JOIN clause and with to the query using the Experience relation
 * @method     ChildStreamQuery innerJoinWithExperience() Adds a INNER JOIN clause and with to the query using the Experience relation
 *
 * @method     \IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\RankQuery|\IiMedias\StreamBundle\Model\AvatarQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery|\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery|\IiMedias\StreamBundle\Model\MessageExperienceQuery|\IiMedias\StreamBundle\Model\ChatterExperienceQuery|\IiMedias\StreamBundle\Model\FollowExperienceQuery|\IiMedias\StreamBundle\Model\HostExperienceQuery|\IiMedias\StreamBundle\Model\ViewDiffDataQuery|\IiMedias\StreamBundle\Model\ViewerDataQuery|\IiMedias\StreamBundle\Model\FollowDiffDataQuery|\IiMedias\StreamBundle\Model\StatusDataQuery|\IiMedias\StreamBundle\Model\TypeDataQuery|\IiMedias\StreamBundle\Model\GameDataQuery|\IiMedias\StreamBundle\Model\StatQuery|\IiMedias\StreamBundle\Model\ExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStream findOne(ConnectionInterface $con = null) Return the first ChildStream matching the query
 * @method     ChildStream findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStream matching the query, or a new ChildStream object populated from the query conditions when no match is found
 *
 * @method     ChildStream findOneById(int $ststrm_id) Return the first ChildStream filtered by the ststrm_id column
 * @method     ChildStream findOneByType(int $ststrm_type) Return the first ChildStream filtered by the ststrm_type column
 * @method     ChildStream findOneByName(string $ststrm_name) Return the first ChildStream filtered by the ststrm_name column
 * @method     ChildStream findOneByCanScan(boolean $ststrm_can_scan) Return the first ChildStream filtered by the ststrm_can_scan column
 * @method     ChildStream findOneByCreatedByUserId(int $ststrm_created_by_user_id) Return the first ChildStream filtered by the ststrm_created_by_user_id column
 * @method     ChildStream findOneByUpdatedByUserId(int $ststrm_updated_by_user_id) Return the first ChildStream filtered by the ststrm_updated_by_user_id column
 * @method     ChildStream findOneByCreatedAt(string $ststrm_created_at) Return the first ChildStream filtered by the ststrm_created_at column
 * @method     ChildStream findOneByUpdatedAt(string $ststrm_updated_at) Return the first ChildStream filtered by the ststrm_updated_at column *

 * @method     ChildStream requirePk($key, ConnectionInterface $con = null) Return the ChildStream by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOne(ConnectionInterface $con = null) Return the first ChildStream matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStream requireOneById(int $ststrm_id) Return the first ChildStream filtered by the ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByType(int $ststrm_type) Return the first ChildStream filtered by the ststrm_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByName(string $ststrm_name) Return the first ChildStream filtered by the ststrm_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByCanScan(boolean $ststrm_can_scan) Return the first ChildStream filtered by the ststrm_can_scan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByCreatedByUserId(int $ststrm_created_by_user_id) Return the first ChildStream filtered by the ststrm_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByUpdatedByUserId(int $ststrm_updated_by_user_id) Return the first ChildStream filtered by the ststrm_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByCreatedAt(string $ststrm_created_at) Return the first ChildStream filtered by the ststrm_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStream requireOneByUpdatedAt(string $ststrm_updated_at) Return the first ChildStream filtered by the ststrm_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStream[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStream objects based on current ModelCriteria
 * @method     ChildStream[]|ObjectCollection findById(int $ststrm_id) Return ChildStream objects filtered by the ststrm_id column
 * @method     ChildStream[]|ObjectCollection findByType(int $ststrm_type) Return ChildStream objects filtered by the ststrm_type column
 * @method     ChildStream[]|ObjectCollection findByName(string $ststrm_name) Return ChildStream objects filtered by the ststrm_name column
 * @method     ChildStream[]|ObjectCollection findByCanScan(boolean $ststrm_can_scan) Return ChildStream objects filtered by the ststrm_can_scan column
 * @method     ChildStream[]|ObjectCollection findByCreatedByUserId(int $ststrm_created_by_user_id) Return ChildStream objects filtered by the ststrm_created_by_user_id column
 * @method     ChildStream[]|ObjectCollection findByUpdatedByUserId(int $ststrm_updated_by_user_id) Return ChildStream objects filtered by the ststrm_updated_by_user_id column
 * @method     ChildStream[]|ObjectCollection findByCreatedAt(string $ststrm_created_at) Return ChildStream objects filtered by the ststrm_created_at column
 * @method     ChildStream[]|ObjectCollection findByUpdatedAt(string $ststrm_updated_at) Return ChildStream objects filtered by the ststrm_updated_at column
 * @method     ChildStream[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StreamQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\StreamQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Stream', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStreamQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStreamQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStreamQuery) {
            return $criteria;
        }
        $query = new ChildStreamQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStream|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StreamTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StreamTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStream A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT ststrm_id, ststrm_type, ststrm_name, ststrm_can_scan, ststrm_created_by_user_id, ststrm_updated_by_user_id, ststrm_created_at, ststrm_updated_at FROM stream_stream_ststrm WHERE ststrm_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStream $obj */
            $obj = new ChildStream();
            $obj->hydrate($row);
            StreamTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStream|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ststrm_id = 1234
     * $query->filterById(array(12, 34)); // WHERE ststrm_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ststrm_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ststrm_type column
     *
     * @param     mixed $type The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        $valueSet = StreamTableMap::getValueSet(StreamTableMap::COL_STSTRM_TYPE);
        if (is_scalar($type)) {
            if (!in_array($type, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $type));
            }
            $type = array_search($type, $valueSet);
        } elseif (is_array($type)) {
            $convertedValues = array();
            foreach ($type as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $type = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the ststrm_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE ststrm_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE ststrm_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the ststrm_can_scan column
     *
     * Example usage:
     * <code>
     * $query->filterByCanScan(true); // WHERE ststrm_can_scan = true
     * $query->filterByCanScan('yes'); // WHERE ststrm_can_scan = true
     * </code>
     *
     * @param     boolean|string $canScan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByCanScan($canScan = null, $comparison = null)
    {
        if (is_string($canScan)) {
            $canScan = in_array(strtolower($canScan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_CAN_SCAN, $canScan, $comparison);
    }

    /**
     * Filter the query on the ststrm_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE ststrm_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE ststrm_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE ststrm_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the ststrm_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE ststrm_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE ststrm_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE ststrm_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the ststrm_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE ststrm_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE ststrm_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE ststrm_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the ststrm_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE ststrm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE ststrm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE ststrm_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $channel->getStreamId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useChannelQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByChannelBot($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $channel->getBotId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useChannelBotQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChannelBot() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChannelBot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinChannelBot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChannelBot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChannelBot');
        }

        return $this;
    }

    /**
     * Use the ChannelBot relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelBotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChannelBot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChannelBot', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByRank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $rank->getStreamId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            return $this
                ->useRankQuery()
                ->filterByPrimaryKeys($rank->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinRank($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rank');
        }

        return $this;
    }

    /**
     * Use the Rank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useRankQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByAvatar($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $avatar->getStreamId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            return $this
                ->useAvatarQuery()
                ->filterByPrimaryKeys($avatar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvatar() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Avatar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinAvatar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Avatar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Avatar');
        }

        return $this;
    }

    /**
     * Use the Avatar relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useAvatarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAvatar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Avatar', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $userExperience->getStreamId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\DeepBotImportExperience object
     *
     * @param \IiMedias\StreamBundle\Model\DeepBotImportExperience|ObjectCollection $deepBotImportExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByDeepBotImportExperience($deepBotImportExperience, $comparison = null)
    {
        if ($deepBotImportExperience instanceof \IiMedias\StreamBundle\Model\DeepBotImportExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $deepBotImportExperience->getStreamId(), $comparison);
        } elseif ($deepBotImportExperience instanceof ObjectCollection) {
            return $this
                ->useDeepBotImportExperienceQuery()
                ->filterByPrimaryKeys($deepBotImportExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDeepBotImportExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\DeepBotImportExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeepBotImportExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinDeepBotImportExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeepBotImportExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeepBotImportExperience');
        }

        return $this;
    }

    /**
     * Use the DeepBotImportExperience relation DeepBotImportExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery A secondary query class using the current class as primary query
     */
    public function useDeepBotImportExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDeepBotImportExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeepBotImportExperience', '\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\MessageExperience object
     *
     * @param \IiMedias\StreamBundle\Model\MessageExperience|ObjectCollection $messageExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByMessageExperience($messageExperience, $comparison = null)
    {
        if ($messageExperience instanceof \IiMedias\StreamBundle\Model\MessageExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $messageExperience->getStreamId(), $comparison);
        } elseif ($messageExperience instanceof ObjectCollection) {
            return $this
                ->useMessageExperienceQuery()
                ->filterByPrimaryKeys($messageExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\MessageExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinMessageExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageExperience');
        }

        return $this;
    }

    /**
     * Use the MessageExperience relation MessageExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\MessageExperienceQuery A secondary query class using the current class as primary query
     */
    public function useMessageExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageExperience', '\IiMedias\StreamBundle\Model\MessageExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatterExperience object
     *
     * @param \IiMedias\StreamBundle\Model\ChatterExperience|ObjectCollection $chatterExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByChatterExperience($chatterExperience, $comparison = null)
    {
        if ($chatterExperience instanceof \IiMedias\StreamBundle\Model\ChatterExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $chatterExperience->getStreamId(), $comparison);
        } elseif ($chatterExperience instanceof ObjectCollection) {
            return $this
                ->useChatterExperienceQuery()
                ->filterByPrimaryKeys($chatterExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatterExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatterExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatterExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinChatterExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatterExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatterExperience');
        }

        return $this;
    }

    /**
     * Use the ChatterExperience relation ChatterExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatterExperienceQuery A secondary query class using the current class as primary query
     */
    public function useChatterExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatterExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatterExperience', '\IiMedias\StreamBundle\Model\ChatterExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowExperience object
     *
     * @param \IiMedias\StreamBundle\Model\FollowExperience|ObjectCollection $followExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByFollowExperience($followExperience, $comparison = null)
    {
        if ($followExperience instanceof \IiMedias\StreamBundle\Model\FollowExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $followExperience->getStreamId(), $comparison);
        } elseif ($followExperience instanceof ObjectCollection) {
            return $this
                ->useFollowExperienceQuery()
                ->filterByPrimaryKeys($followExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinFollowExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowExperience');
        }

        return $this;
    }

    /**
     * Use the FollowExperience relation FollowExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowExperienceQuery A secondary query class using the current class as primary query
     */
    public function useFollowExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowExperience', '\IiMedias\StreamBundle\Model\FollowExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\HostExperience object
     *
     * @param \IiMedias\StreamBundle\Model\HostExperience|ObjectCollection $hostExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByHostExperience($hostExperience, $comparison = null)
    {
        if ($hostExperience instanceof \IiMedias\StreamBundle\Model\HostExperience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $hostExperience->getStreamId(), $comparison);
        } elseif ($hostExperience instanceof ObjectCollection) {
            return $this
                ->useHostExperienceQuery()
                ->filterByPrimaryKeys($hostExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHostExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\HostExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HostExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinHostExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HostExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HostExperience');
        }

        return $this;
    }

    /**
     * Use the HostExperience relation HostExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\HostExperienceQuery A secondary query class using the current class as primary query
     */
    public function useHostExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinHostExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HostExperience', '\IiMedias\StreamBundle\Model\HostExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewDiffData|ObjectCollection $viewDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByViewDiffData($viewDiffData, $comparison = null)
    {
        if ($viewDiffData instanceof \IiMedias\StreamBundle\Model\ViewDiffData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $viewDiffData->getStreamId(), $comparison);
        } elseif ($viewDiffData instanceof ObjectCollection) {
            return $this
                ->useViewDiffDataQuery()
                ->filterByPrimaryKeys($viewDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinViewDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewDiffData');
        }

        return $this;
    }

    /**
     * Use the ViewDiffData relation ViewDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useViewDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewDiffData', '\IiMedias\StreamBundle\Model\ViewDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewerData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewerData|ObjectCollection $viewerData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByViewerData($viewerData, $comparison = null)
    {
        if ($viewerData instanceof \IiMedias\StreamBundle\Model\ViewerData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $viewerData->getStreamId(), $comparison);
        } elseif ($viewerData instanceof ObjectCollection) {
            return $this
                ->useViewerDataQuery()
                ->filterByPrimaryKeys($viewerData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewerData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewerData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewerData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinViewerData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewerData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewerData');
        }

        return $this;
    }

    /**
     * Use the ViewerData relation ViewerData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewerDataQuery A secondary query class using the current class as primary query
     */
    public function useViewerDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewerData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewerData', '\IiMedias\StreamBundle\Model\ViewerDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\FollowDiffData|ObjectCollection $followDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByFollowDiffData($followDiffData, $comparison = null)
    {
        if ($followDiffData instanceof \IiMedias\StreamBundle\Model\FollowDiffData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $followDiffData->getStreamId(), $comparison);
        } elseif ($followDiffData instanceof ObjectCollection) {
            return $this
                ->useFollowDiffDataQuery()
                ->filterByPrimaryKeys($followDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinFollowDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowDiffData');
        }

        return $this;
    }

    /**
     * Use the FollowDiffData relation FollowDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useFollowDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowDiffData', '\IiMedias\StreamBundle\Model\FollowDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\StatusData object
     *
     * @param \IiMedias\StreamBundle\Model\StatusData|ObjectCollection $statusData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByStatusData($statusData, $comparison = null)
    {
        if ($statusData instanceof \IiMedias\StreamBundle\Model\StatusData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $statusData->getStreamId(), $comparison);
        } elseif ($statusData instanceof ObjectCollection) {
            return $this
                ->useStatusDataQuery()
                ->filterByPrimaryKeys($statusData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStatusData() only accepts arguments of type \IiMedias\StreamBundle\Model\StatusData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinStatusData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusData');
        }

        return $this;
    }

    /**
     * Use the StatusData relation StatusData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatusDataQuery A secondary query class using the current class as primary query
     */
    public function useStatusDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusData', '\IiMedias\StreamBundle\Model\StatusDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\TypeData object
     *
     * @param \IiMedias\StreamBundle\Model\TypeData|ObjectCollection $typeData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByTypeData($typeData, $comparison = null)
    {
        if ($typeData instanceof \IiMedias\StreamBundle\Model\TypeData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $typeData->getStreamId(), $comparison);
        } elseif ($typeData instanceof ObjectCollection) {
            return $this
                ->useTypeDataQuery()
                ->filterByPrimaryKeys($typeData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTypeData() only accepts arguments of type \IiMedias\StreamBundle\Model\TypeData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TypeData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinTypeData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TypeData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TypeData');
        }

        return $this;
    }

    /**
     * Use the TypeData relation TypeData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\TypeDataQuery A secondary query class using the current class as primary query
     */
    public function useTypeDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTypeData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TypeData', '\IiMedias\StreamBundle\Model\TypeDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\GameData object
     *
     * @param \IiMedias\StreamBundle\Model\GameData|ObjectCollection $gameData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByGameData($gameData, $comparison = null)
    {
        if ($gameData instanceof \IiMedias\StreamBundle\Model\GameData) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $gameData->getStreamId(), $comparison);
        } elseif ($gameData instanceof ObjectCollection) {
            return $this
                ->useGameDataQuery()
                ->filterByPrimaryKeys($gameData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameData() only accepts arguments of type \IiMedias\StreamBundle\Model\GameData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinGameData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameData');
        }

        return $this;
    }

    /**
     * Use the GameData relation GameData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\GameDataQuery A secondary query class using the current class as primary query
     */
    public function useGameDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameData', '\IiMedias\StreamBundle\Model\GameDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stat object
     *
     * @param \IiMedias\StreamBundle\Model\Stat|ObjectCollection $stat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByStat($stat, $comparison = null)
    {
        if ($stat instanceof \IiMedias\StreamBundle\Model\Stat) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $stat->getStreamId(), $comparison);
        } elseif ($stat instanceof ObjectCollection) {
            return $this
                ->useStatQuery()
                ->filterByPrimaryKeys($stat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStat() only accepts arguments of type \IiMedias\StreamBundle\Model\Stat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinStat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stat');
        }

        return $this;
    }

    /**
     * Use the Stat relation Stat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatQuery A secondary query class using the current class as primary query
     */
    public function useStatQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stat', '\IiMedias\StreamBundle\Model\StatQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Experience object
     *
     * @param \IiMedias\StreamBundle\Model\Experience|ObjectCollection $experience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreamQuery The current query, for fluid interface
     */
    public function filterByExperience($experience, $comparison = null)
    {
        if ($experience instanceof \IiMedias\StreamBundle\Model\Experience) {
            return $this
                ->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $experience->getStreamId(), $comparison);
        } elseif ($experience instanceof ObjectCollection) {
            return $this
                ->useExperienceQuery()
                ->filterByPrimaryKeys($experience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\Experience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Experience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function joinExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Experience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Experience');
        }

        return $this;
    }

    /**
     * Use the Experience relation Experience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ExperienceQuery A secondary query class using the current class as primary query
     */
    public function useExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Experience', '\IiMedias\StreamBundle\Model\ExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStream $stream Object to remove from the list of results
     *
     * @return $this|ChildStreamQuery The current query, for fluid interface
     */
    public function prune($stream = null)
    {
        if ($stream) {
            $this->addUsingAlias(StreamTableMap::COL_STSTRM_ID, $stream->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_stream_ststrm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreamTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StreamTableMap::clearInstancePool();
            StreamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreamTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StreamTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StreamTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StreamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(StreamTableMap::COL_STSTRM_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(StreamTableMap::COL_STSTRM_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(StreamTableMap::COL_STSTRM_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(StreamTableMap::COL_STSTRM_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildStreamQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(StreamTableMap::COL_STSTRM_CREATED_AT);
    }

} // StreamQuery

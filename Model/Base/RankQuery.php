<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\Rank as ChildRank;
use IiMedias\StreamBundle\Model\RankQuery as ChildRankQuery;
use IiMedias\StreamBundle\Model\Map\RankTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_rank_strank' table.
 *
 *
 *
 * @method     ChildRankQuery orderById($order = Criteria::ASC) Order by the strank_id column
 * @method     ChildRankQuery orderByStreamId($order = Criteria::ASC) Order by the strank_ststrm_id column
 * @method     ChildRankQuery orderByName($order = Criteria::ASC) Order by the strank_name column
 * @method     ChildRankQuery orderByIsDefault($order = Criteria::ASC) Order by the strank_is_default column
 * @method     ChildRankQuery orderBySetIfFollowEvent($order = Criteria::ASC) Order by the strank_set_if_follow_event column
 * @method     ChildRankQuery orderBySetIfModerator($order = Criteria::ASC) Order by the strank_set_if_moderator column
 * @method     ChildRankQuery orderBySetIfStreamer($order = Criteria::ASC) Order by the strank_set_if_streamer column
 * @method     ChildRankQuery orderByDependencyRankIds($order = Criteria::ASC) Order by the strank_dependency_rank_ids column
 * @method     ChildRankQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the strank_created_by_user_id column
 * @method     ChildRankQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the strank_updated_by_user_id column
 * @method     ChildRankQuery orderByCreatedAt($order = Criteria::ASC) Order by the strank_created_at column
 * @method     ChildRankQuery orderByUpdatedAt($order = Criteria::ASC) Order by the strank_updated_at column
 *
 * @method     ChildRankQuery groupById() Group by the strank_id column
 * @method     ChildRankQuery groupByStreamId() Group by the strank_ststrm_id column
 * @method     ChildRankQuery groupByName() Group by the strank_name column
 * @method     ChildRankQuery groupByIsDefault() Group by the strank_is_default column
 * @method     ChildRankQuery groupBySetIfFollowEvent() Group by the strank_set_if_follow_event column
 * @method     ChildRankQuery groupBySetIfModerator() Group by the strank_set_if_moderator column
 * @method     ChildRankQuery groupBySetIfStreamer() Group by the strank_set_if_streamer column
 * @method     ChildRankQuery groupByDependencyRankIds() Group by the strank_dependency_rank_ids column
 * @method     ChildRankQuery groupByCreatedByUserId() Group by the strank_created_by_user_id column
 * @method     ChildRankQuery groupByUpdatedByUserId() Group by the strank_updated_by_user_id column
 * @method     ChildRankQuery groupByCreatedAt() Group by the strank_created_at column
 * @method     ChildRankQuery groupByUpdatedAt() Group by the strank_updated_at column
 *
 * @method     ChildRankQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRankQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRankQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRankQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRankQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRankQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRankQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildRankQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildRankQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildRankQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildRankQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildRankQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildRankQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildRankQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildRankQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildRankQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildRankQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildRankQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildRankQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildRankQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildRankQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildRankQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildRankQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildRankQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildRankQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildRankQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildRankQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildRankQuery leftJoinAvatarByMinRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvatarByMinRank relation
 * @method     ChildRankQuery rightJoinAvatarByMinRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvatarByMinRank relation
 * @method     ChildRankQuery innerJoinAvatarByMinRank($relationAlias = null) Adds a INNER JOIN clause to the query using the AvatarByMinRank relation
 *
 * @method     ChildRankQuery joinWithAvatarByMinRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AvatarByMinRank relation
 *
 * @method     ChildRankQuery leftJoinWithAvatarByMinRank() Adds a LEFT JOIN clause and with to the query using the AvatarByMinRank relation
 * @method     ChildRankQuery rightJoinWithAvatarByMinRank() Adds a RIGHT JOIN clause and with to the query using the AvatarByMinRank relation
 * @method     ChildRankQuery innerJoinWithAvatarByMinRank() Adds a INNER JOIN clause and with to the query using the AvatarByMinRank relation
 *
 * @method     ChildRankQuery leftJoinAvatarByAssociatedRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvatarByAssociatedRank relation
 * @method     ChildRankQuery rightJoinAvatarByAssociatedRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvatarByAssociatedRank relation
 * @method     ChildRankQuery innerJoinAvatarByAssociatedRank($relationAlias = null) Adds a INNER JOIN clause to the query using the AvatarByAssociatedRank relation
 *
 * @method     ChildRankQuery joinWithAvatarByAssociatedRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AvatarByAssociatedRank relation
 *
 * @method     ChildRankQuery leftJoinWithAvatarByAssociatedRank() Adds a LEFT JOIN clause and with to the query using the AvatarByAssociatedRank relation
 * @method     ChildRankQuery rightJoinWithAvatarByAssociatedRank() Adds a RIGHT JOIN clause and with to the query using the AvatarByAssociatedRank relation
 * @method     ChildRankQuery innerJoinWithAvatarByAssociatedRank() Adds a INNER JOIN clause and with to the query using the AvatarByAssociatedRank relation
 *
 * @method     ChildRankQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildRankQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildRankQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildRankQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildRankQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildRankQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildRankQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\AvatarQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRank findOne(ConnectionInterface $con = null) Return the first ChildRank matching the query
 * @method     ChildRank findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRank matching the query, or a new ChildRank object populated from the query conditions when no match is found
 *
 * @method     ChildRank findOneById(int $strank_id) Return the first ChildRank filtered by the strank_id column
 * @method     ChildRank findOneByStreamId(int $strank_ststrm_id) Return the first ChildRank filtered by the strank_ststrm_id column
 * @method     ChildRank findOneByName(string $strank_name) Return the first ChildRank filtered by the strank_name column
 * @method     ChildRank findOneByIsDefault(boolean $strank_is_default) Return the first ChildRank filtered by the strank_is_default column
 * @method     ChildRank findOneBySetIfFollowEvent(boolean $strank_set_if_follow_event) Return the first ChildRank filtered by the strank_set_if_follow_event column
 * @method     ChildRank findOneBySetIfModerator(boolean $strank_set_if_moderator) Return the first ChildRank filtered by the strank_set_if_moderator column
 * @method     ChildRank findOneBySetIfStreamer(boolean $strank_set_if_streamer) Return the first ChildRank filtered by the strank_set_if_streamer column
 * @method     ChildRank findOneByDependencyRankIds(array $strank_dependency_rank_ids) Return the first ChildRank filtered by the strank_dependency_rank_ids column
 * @method     ChildRank findOneByCreatedByUserId(int $strank_created_by_user_id) Return the first ChildRank filtered by the strank_created_by_user_id column
 * @method     ChildRank findOneByUpdatedByUserId(int $strank_updated_by_user_id) Return the first ChildRank filtered by the strank_updated_by_user_id column
 * @method     ChildRank findOneByCreatedAt(string $strank_created_at) Return the first ChildRank filtered by the strank_created_at column
 * @method     ChildRank findOneByUpdatedAt(string $strank_updated_at) Return the first ChildRank filtered by the strank_updated_at column *

 * @method     ChildRank requirePk($key, ConnectionInterface $con = null) Return the ChildRank by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOne(ConnectionInterface $con = null) Return the first ChildRank matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRank requireOneById(int $strank_id) Return the first ChildRank filtered by the strank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByStreamId(int $strank_ststrm_id) Return the first ChildRank filtered by the strank_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByName(string $strank_name) Return the first ChildRank filtered by the strank_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByIsDefault(boolean $strank_is_default) Return the first ChildRank filtered by the strank_is_default column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneBySetIfFollowEvent(boolean $strank_set_if_follow_event) Return the first ChildRank filtered by the strank_set_if_follow_event column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneBySetIfModerator(boolean $strank_set_if_moderator) Return the first ChildRank filtered by the strank_set_if_moderator column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneBySetIfStreamer(boolean $strank_set_if_streamer) Return the first ChildRank filtered by the strank_set_if_streamer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByDependencyRankIds(array $strank_dependency_rank_ids) Return the first ChildRank filtered by the strank_dependency_rank_ids column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByCreatedByUserId(int $strank_created_by_user_id) Return the first ChildRank filtered by the strank_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByUpdatedByUserId(int $strank_updated_by_user_id) Return the first ChildRank filtered by the strank_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByCreatedAt(string $strank_created_at) Return the first ChildRank filtered by the strank_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRank requireOneByUpdatedAt(string $strank_updated_at) Return the first ChildRank filtered by the strank_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRank[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRank objects based on current ModelCriteria
 * @method     ChildRank[]|ObjectCollection findById(int $strank_id) Return ChildRank objects filtered by the strank_id column
 * @method     ChildRank[]|ObjectCollection findByStreamId(int $strank_ststrm_id) Return ChildRank objects filtered by the strank_ststrm_id column
 * @method     ChildRank[]|ObjectCollection findByName(string $strank_name) Return ChildRank objects filtered by the strank_name column
 * @method     ChildRank[]|ObjectCollection findByIsDefault(boolean $strank_is_default) Return ChildRank objects filtered by the strank_is_default column
 * @method     ChildRank[]|ObjectCollection findBySetIfFollowEvent(boolean $strank_set_if_follow_event) Return ChildRank objects filtered by the strank_set_if_follow_event column
 * @method     ChildRank[]|ObjectCollection findBySetIfModerator(boolean $strank_set_if_moderator) Return ChildRank objects filtered by the strank_set_if_moderator column
 * @method     ChildRank[]|ObjectCollection findBySetIfStreamer(boolean $strank_set_if_streamer) Return ChildRank objects filtered by the strank_set_if_streamer column
 * @method     ChildRank[]|ObjectCollection findByDependencyRankIds(array $strank_dependency_rank_ids) Return ChildRank objects filtered by the strank_dependency_rank_ids column
 * @method     ChildRank[]|ObjectCollection findByCreatedByUserId(int $strank_created_by_user_id) Return ChildRank objects filtered by the strank_created_by_user_id column
 * @method     ChildRank[]|ObjectCollection findByUpdatedByUserId(int $strank_updated_by_user_id) Return ChildRank objects filtered by the strank_updated_by_user_id column
 * @method     ChildRank[]|ObjectCollection findByCreatedAt(string $strank_created_at) Return ChildRank objects filtered by the strank_created_at column
 * @method     ChildRank[]|ObjectCollection findByUpdatedAt(string $strank_updated_at) Return ChildRank objects filtered by the strank_updated_at column
 * @method     ChildRank[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RankQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\RankQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Rank', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRankQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRankQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRankQuery) {
            return $criteria;
        }
        $query = new ChildRankQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRank|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RankTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RankTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRank A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT strank_id, strank_ststrm_id, strank_name, strank_is_default, strank_set_if_follow_event, strank_set_if_moderator, strank_set_if_streamer, strank_dependency_rank_ids, strank_created_by_user_id, strank_updated_by_user_id, strank_created_at, strank_updated_at FROM stream_rank_strank WHERE strank_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRank $obj */
            $obj = new ChildRank();
            $obj->hydrate($row);
            RankTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRank|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the strank_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE strank_id = 1234
     * $query->filterById(array(12, 34)); // WHERE strank_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE strank_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $id, $comparison);
    }

    /**
     * Filter the query on the strank_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE strank_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE strank_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE strank_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the strank_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE strank_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE strank_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the strank_is_default column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefault(true); // WHERE strank_is_default = true
     * $query->filterByIsDefault('yes'); // WHERE strank_is_default = true
     * </code>
     *
     * @param     boolean|string $isDefault The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByIsDefault($isDefault = null, $comparison = null)
    {
        if (is_string($isDefault)) {
            $isDefault = in_array(strtolower($isDefault), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_IS_DEFAULT, $isDefault, $comparison);
    }

    /**
     * Filter the query on the strank_set_if_follow_event column
     *
     * Example usage:
     * <code>
     * $query->filterBySetIfFollowEvent(true); // WHERE strank_set_if_follow_event = true
     * $query->filterBySetIfFollowEvent('yes'); // WHERE strank_set_if_follow_event = true
     * </code>
     *
     * @param     boolean|string $setIfFollowEvent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterBySetIfFollowEvent($setIfFollowEvent = null, $comparison = null)
    {
        if (is_string($setIfFollowEvent)) {
            $setIfFollowEvent = in_array(strtolower($setIfFollowEvent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT, $setIfFollowEvent, $comparison);
    }

    /**
     * Filter the query on the strank_set_if_moderator column
     *
     * Example usage:
     * <code>
     * $query->filterBySetIfModerator(true); // WHERE strank_set_if_moderator = true
     * $query->filterBySetIfModerator('yes'); // WHERE strank_set_if_moderator = true
     * </code>
     *
     * @param     boolean|string $setIfModerator The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterBySetIfModerator($setIfModerator = null, $comparison = null)
    {
        if (is_string($setIfModerator)) {
            $setIfModerator = in_array(strtolower($setIfModerator), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_SET_IF_MODERATOR, $setIfModerator, $comparison);
    }

    /**
     * Filter the query on the strank_set_if_streamer column
     *
     * Example usage:
     * <code>
     * $query->filterBySetIfStreamer(true); // WHERE strank_set_if_streamer = true
     * $query->filterBySetIfStreamer('yes'); // WHERE strank_set_if_streamer = true
     * </code>
     *
     * @param     boolean|string $setIfStreamer The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterBySetIfStreamer($setIfStreamer = null, $comparison = null)
    {
        if (is_string($setIfStreamer)) {
            $setIfStreamer = in_array(strtolower($setIfStreamer), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_SET_IF_STREAMER, $setIfStreamer, $comparison);
    }

    /**
     * Filter the query on the strank_dependency_rank_ids column
     *
     * @param     array $dependencyRankIds The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByDependencyRankIds($dependencyRankIds = null, $comparison = null)
    {
        $key = $this->getAliasedColName(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($dependencyRankIds as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($dependencyRankIds as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($dependencyRankIds as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS, $dependencyRankIds, $comparison);
    }

    /**
     * Filter the query on the strank_dependency_rank_ids column
     * @param     mixed $dependencyRankIds The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByDependencyRankId($dependencyRankIds = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($dependencyRankIds)) {
                $dependencyRankIds = '%| ' . $dependencyRankIds . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $dependencyRankIds = '%| ' . $dependencyRankIds . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $dependencyRankIds, $comparison);
            } else {
                $this->addAnd($key, $dependencyRankIds, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS, $dependencyRankIds, $comparison);
    }

    /**
     * Filter the query on the strank_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE strank_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE strank_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE strank_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the strank_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE strank_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE strank_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE strank_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the strank_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE strank_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE strank_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE strank_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the strank_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE strank_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE strank_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE strank_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByAvatarByMinRank($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_ID, $avatar->getMinRankId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            return $this
                ->useAvatarByMinRankQuery()
                ->filterByPrimaryKeys($avatar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvatarByMinRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvatarByMinRank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinAvatarByMinRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvatarByMinRank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvatarByMinRank');
        }

        return $this;
    }

    /**
     * Use the AvatarByMinRank relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useAvatarByMinRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvatarByMinRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvatarByMinRank', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByAvatarByAssociatedRank($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_ID, $avatar->getAssociatedRankId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            return $this
                ->useAvatarByAssociatedRankQuery()
                ->filterByPrimaryKeys($avatar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvatarByAssociatedRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvatarByAssociatedRank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinAvatarByAssociatedRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvatarByAssociatedRank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvatarByAssociatedRank');
        }

        return $this;
    }

    /**
     * Use the AvatarByAssociatedRank relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useAvatarByAssociatedRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvatarByAssociatedRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvatarByAssociatedRank', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRankQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(RankTableMap::COL_STRANK_ID, $userExperience->getRankId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRank $rank Object to remove from the list of results
     *
     * @return $this|ChildRankQuery The current query, for fluid interface
     */
    public function prune($rank = null)
    {
        if ($rank) {
            $this->addUsingAlias(RankTableMap::COL_STRANK_ID, $rank->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_rank_strank table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RankTableMap::clearInstancePool();
            RankTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RankTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RankTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RankTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(RankTableMap::COL_STRANK_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(RankTableMap::COL_STRANK_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(RankTableMap::COL_STRANK_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(RankTableMap::COL_STRANK_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(RankTableMap::COL_STRANK_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildRankQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(RankTableMap::COL_STRANK_CREATED_AT);
    }

} // RankQuery

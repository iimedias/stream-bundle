<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\Stat as ChildStat;
use IiMedias\StreamBundle\Model\StatQuery as ChildStatQuery;
use IiMedias\StreamBundle\Model\Map\StatTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_stat_ststat' table.
 *
 *
 *
 * @method     ChildStatQuery orderById($order = Criteria::ASC) Order by the ststat_id column
 * @method     ChildStatQuery orderByStreamId($order = Criteria::ASC) Order by the ststat_ststrm_id column
 * @method     ChildStatQuery orderByChannelId($order = Criteria::ASC) Order by the ststat_stchan_id column
 * @method     ChildStatQuery orderBySiteId($order = Criteria::ASC) Order by the ststat_stsite_id column
 * @method     ChildStatQuery orderByDuration($order = Criteria::ASC) Order by the ststat_duration column
 * @method     ChildStatQuery orderByStartAt($order = Criteria::ASC) Order by the ststat_start_at column
 * @method     ChildStatQuery orderByEndAt($order = Criteria::ASC) Order by the ststat_end_at column
 * @method     ChildStatQuery orderByCanRescan($order = Criteria::ASC) Order by the ststat_can_rescan column
 * @method     ChildStatQuery orderByStreamType($order = Criteria::ASC) Order by the ststat_stream_type column
 * @method     ChildStatQuery orderByViews($order = Criteria::ASC) Order by the ststat_views column
 * @method     ChildStatQuery orderByMaximumViewers($order = Criteria::ASC) Order by the ststat_maximum_viewers column
 * @method     ChildStatQuery orderByAverageViewers($order = Criteria::ASC) Order by the ststat_average_viewers column
 * @method     ChildStatQuery orderByChatters($order = Criteria::ASC) Order by the ststat_chatters column
 * @method     ChildStatQuery orderByMessages($order = Criteria::ASC) Order by the ststat_messages column
 * @method     ChildStatQuery orderByHosts($order = Criteria::ASC) Order by the ststat_hosts column
 * @method     ChildStatQuery orderByFollows($order = Criteria::ASC) Order by the ststat_follows column
 * @method     ChildStatQuery orderByUnfollows($order = Criteria::ASC) Order by the ststat_unfollows column
 *
 * @method     ChildStatQuery groupById() Group by the ststat_id column
 * @method     ChildStatQuery groupByStreamId() Group by the ststat_ststrm_id column
 * @method     ChildStatQuery groupByChannelId() Group by the ststat_stchan_id column
 * @method     ChildStatQuery groupBySiteId() Group by the ststat_stsite_id column
 * @method     ChildStatQuery groupByDuration() Group by the ststat_duration column
 * @method     ChildStatQuery groupByStartAt() Group by the ststat_start_at column
 * @method     ChildStatQuery groupByEndAt() Group by the ststat_end_at column
 * @method     ChildStatQuery groupByCanRescan() Group by the ststat_can_rescan column
 * @method     ChildStatQuery groupByStreamType() Group by the ststat_stream_type column
 * @method     ChildStatQuery groupByViews() Group by the ststat_views column
 * @method     ChildStatQuery groupByMaximumViewers() Group by the ststat_maximum_viewers column
 * @method     ChildStatQuery groupByAverageViewers() Group by the ststat_average_viewers column
 * @method     ChildStatQuery groupByChatters() Group by the ststat_chatters column
 * @method     ChildStatQuery groupByMessages() Group by the ststat_messages column
 * @method     ChildStatQuery groupByHosts() Group by the ststat_hosts column
 * @method     ChildStatQuery groupByFollows() Group by the ststat_follows column
 * @method     ChildStatQuery groupByUnfollows() Group by the ststat_unfollows column
 *
 * @method     ChildStatQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStatQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStatQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStatQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStatQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStatQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStatQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildStatQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildStatQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildStatQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildStatQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildStatQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildStatQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildStatQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildStatQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildStatQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildStatQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildStatQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildStatQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildStatQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildStatQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildStatQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildStatQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildStatQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildStatQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildStatQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildStatQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStat findOne(ConnectionInterface $con = null) Return the first ChildStat matching the query
 * @method     ChildStat findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStat matching the query, or a new ChildStat object populated from the query conditions when no match is found
 *
 * @method     ChildStat findOneById(int $ststat_id) Return the first ChildStat filtered by the ststat_id column
 * @method     ChildStat findOneByStreamId(int $ststat_ststrm_id) Return the first ChildStat filtered by the ststat_ststrm_id column
 * @method     ChildStat findOneByChannelId(int $ststat_stchan_id) Return the first ChildStat filtered by the ststat_stchan_id column
 * @method     ChildStat findOneBySiteId(int $ststat_stsite_id) Return the first ChildStat filtered by the ststat_stsite_id column
 * @method     ChildStat findOneByDuration(int $ststat_duration) Return the first ChildStat filtered by the ststat_duration column
 * @method     ChildStat findOneByStartAt(string $ststat_start_at) Return the first ChildStat filtered by the ststat_start_at column
 * @method     ChildStat findOneByEndAt(string $ststat_end_at) Return the first ChildStat filtered by the ststat_end_at column
 * @method     ChildStat findOneByCanRescan(boolean $ststat_can_rescan) Return the first ChildStat filtered by the ststat_can_rescan column
 * @method     ChildStat findOneByStreamType(int $ststat_stream_type) Return the first ChildStat filtered by the ststat_stream_type column
 * @method     ChildStat findOneByViews(int $ststat_views) Return the first ChildStat filtered by the ststat_views column
 * @method     ChildStat findOneByMaximumViewers(int $ststat_maximum_viewers) Return the first ChildStat filtered by the ststat_maximum_viewers column
 * @method     ChildStat findOneByAverageViewers(int $ststat_average_viewers) Return the first ChildStat filtered by the ststat_average_viewers column
 * @method     ChildStat findOneByChatters(int $ststat_chatters) Return the first ChildStat filtered by the ststat_chatters column
 * @method     ChildStat findOneByMessages(int $ststat_messages) Return the first ChildStat filtered by the ststat_messages column
 * @method     ChildStat findOneByHosts(int $ststat_hosts) Return the first ChildStat filtered by the ststat_hosts column
 * @method     ChildStat findOneByFollows(int $ststat_follows) Return the first ChildStat filtered by the ststat_follows column
 * @method     ChildStat findOneByUnfollows(int $ststat_unfollows) Return the first ChildStat filtered by the ststat_unfollows column *

 * @method     ChildStat requirePk($key, ConnectionInterface $con = null) Return the ChildStat by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOne(ConnectionInterface $con = null) Return the first ChildStat matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStat requireOneById(int $ststat_id) Return the first ChildStat filtered by the ststat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByStreamId(int $ststat_ststrm_id) Return the first ChildStat filtered by the ststat_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByChannelId(int $ststat_stchan_id) Return the first ChildStat filtered by the ststat_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneBySiteId(int $ststat_stsite_id) Return the first ChildStat filtered by the ststat_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByDuration(int $ststat_duration) Return the first ChildStat filtered by the ststat_duration column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByStartAt(string $ststat_start_at) Return the first ChildStat filtered by the ststat_start_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByEndAt(string $ststat_end_at) Return the first ChildStat filtered by the ststat_end_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByCanRescan(boolean $ststat_can_rescan) Return the first ChildStat filtered by the ststat_can_rescan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByStreamType(int $ststat_stream_type) Return the first ChildStat filtered by the ststat_stream_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByViews(int $ststat_views) Return the first ChildStat filtered by the ststat_views column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByMaximumViewers(int $ststat_maximum_viewers) Return the first ChildStat filtered by the ststat_maximum_viewers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByAverageViewers(int $ststat_average_viewers) Return the first ChildStat filtered by the ststat_average_viewers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByChatters(int $ststat_chatters) Return the first ChildStat filtered by the ststat_chatters column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByMessages(int $ststat_messages) Return the first ChildStat filtered by the ststat_messages column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByHosts(int $ststat_hosts) Return the first ChildStat filtered by the ststat_hosts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByFollows(int $ststat_follows) Return the first ChildStat filtered by the ststat_follows column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStat requireOneByUnfollows(int $ststat_unfollows) Return the first ChildStat filtered by the ststat_unfollows column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStat[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStat objects based on current ModelCriteria
 * @method     ChildStat[]|ObjectCollection findById(int $ststat_id) Return ChildStat objects filtered by the ststat_id column
 * @method     ChildStat[]|ObjectCollection findByStreamId(int $ststat_ststrm_id) Return ChildStat objects filtered by the ststat_ststrm_id column
 * @method     ChildStat[]|ObjectCollection findByChannelId(int $ststat_stchan_id) Return ChildStat objects filtered by the ststat_stchan_id column
 * @method     ChildStat[]|ObjectCollection findBySiteId(int $ststat_stsite_id) Return ChildStat objects filtered by the ststat_stsite_id column
 * @method     ChildStat[]|ObjectCollection findByDuration(int $ststat_duration) Return ChildStat objects filtered by the ststat_duration column
 * @method     ChildStat[]|ObjectCollection findByStartAt(string $ststat_start_at) Return ChildStat objects filtered by the ststat_start_at column
 * @method     ChildStat[]|ObjectCollection findByEndAt(string $ststat_end_at) Return ChildStat objects filtered by the ststat_end_at column
 * @method     ChildStat[]|ObjectCollection findByCanRescan(boolean $ststat_can_rescan) Return ChildStat objects filtered by the ststat_can_rescan column
 * @method     ChildStat[]|ObjectCollection findByStreamType(int $ststat_stream_type) Return ChildStat objects filtered by the ststat_stream_type column
 * @method     ChildStat[]|ObjectCollection findByViews(int $ststat_views) Return ChildStat objects filtered by the ststat_views column
 * @method     ChildStat[]|ObjectCollection findByMaximumViewers(int $ststat_maximum_viewers) Return ChildStat objects filtered by the ststat_maximum_viewers column
 * @method     ChildStat[]|ObjectCollection findByAverageViewers(int $ststat_average_viewers) Return ChildStat objects filtered by the ststat_average_viewers column
 * @method     ChildStat[]|ObjectCollection findByChatters(int $ststat_chatters) Return ChildStat objects filtered by the ststat_chatters column
 * @method     ChildStat[]|ObjectCollection findByMessages(int $ststat_messages) Return ChildStat objects filtered by the ststat_messages column
 * @method     ChildStat[]|ObjectCollection findByHosts(int $ststat_hosts) Return ChildStat objects filtered by the ststat_hosts column
 * @method     ChildStat[]|ObjectCollection findByFollows(int $ststat_follows) Return ChildStat objects filtered by the ststat_follows column
 * @method     ChildStat[]|ObjectCollection findByUnfollows(int $ststat_unfollows) Return ChildStat objects filtered by the ststat_unfollows column
 * @method     ChildStat[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StatQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\StatQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Stat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStatQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStatQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStatQuery) {
            return $criteria;
        }
        $query = new ChildStatQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStat|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StatTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StatTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStat A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT ststat_id, ststat_ststrm_id, ststat_stchan_id, ststat_stsite_id, ststat_duration, ststat_start_at, ststat_end_at, ststat_can_rescan, ststat_stream_type, ststat_views, ststat_maximum_viewers, ststat_average_viewers, ststat_chatters, ststat_messages, ststat_hosts, ststat_follows, ststat_unfollows FROM stream_stat_ststat WHERE ststat_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStat $obj */
            $obj = new ChildStat();
            $obj->hydrate($row);
            StatTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStat|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ststat_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ststat_id = 1234
     * $query->filterById(array(12, 34)); // WHERE ststat_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ststat_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ststat_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE ststat_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE ststat_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE ststat_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the ststat_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE ststat_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE ststat_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE ststat_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the ststat_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE ststat_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE ststat_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE ststat_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the ststat_duration column
     *
     * @param     mixed $duration The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByDuration($duration = null, $comparison = null)
    {
        $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_DURATION);
        if (is_scalar($duration)) {
            if (!in_array($duration, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $duration));
            }
            $duration = array_search($duration, $valueSet);
        } elseif (is_array($duration)) {
            $convertedValues = array();
            foreach ($duration as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $duration = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_DURATION, $duration, $comparison);
    }

    /**
     * Filter the query on the ststat_start_at column
     *
     * Example usage:
     * <code>
     * $query->filterByStartAt('2011-03-14'); // WHERE ststat_start_at = '2011-03-14'
     * $query->filterByStartAt('now'); // WHERE ststat_start_at = '2011-03-14'
     * $query->filterByStartAt(array('max' => 'yesterday')); // WHERE ststat_start_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $startAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByStartAt($startAt = null, $comparison = null)
    {
        if (is_array($startAt)) {
            $useMinMax = false;
            if (isset($startAt['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_START_AT, $startAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startAt['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_START_AT, $startAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_START_AT, $startAt, $comparison);
    }

    /**
     * Filter the query on the ststat_end_at column
     *
     * Example usage:
     * <code>
     * $query->filterByEndAt('2011-03-14'); // WHERE ststat_end_at = '2011-03-14'
     * $query->filterByEndAt('now'); // WHERE ststat_end_at = '2011-03-14'
     * $query->filterByEndAt(array('max' => 'yesterday')); // WHERE ststat_end_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $endAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByEndAt($endAt = null, $comparison = null)
    {
        if (is_array($endAt)) {
            $useMinMax = false;
            if (isset($endAt['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_END_AT, $endAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endAt['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_END_AT, $endAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_END_AT, $endAt, $comparison);
    }

    /**
     * Filter the query on the ststat_can_rescan column
     *
     * Example usage:
     * <code>
     * $query->filterByCanRescan(true); // WHERE ststat_can_rescan = true
     * $query->filterByCanRescan('yes'); // WHERE ststat_can_rescan = true
     * </code>
     *
     * @param     boolean|string $canRescan The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByCanRescan($canRescan = null, $comparison = null)
    {
        if (is_string($canRescan)) {
            $canRescan = in_array(strtolower($canRescan), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_CAN_RESCAN, $canRescan, $comparison);
    }

    /**
     * Filter the query on the ststat_stream_type column
     *
     * @param     mixed $streamType The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByStreamType($streamType = null, $comparison = null)
    {
        $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_STREAM_TYPE);
        if (is_scalar($streamType)) {
            if (!in_array($streamType, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $streamType));
            }
            $streamType = array_search($streamType, $valueSet);
        } elseif (is_array($streamType)) {
            $convertedValues = array();
            foreach ($streamType as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $streamType = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_STREAM_TYPE, $streamType, $comparison);
    }

    /**
     * Filter the query on the ststat_views column
     *
     * Example usage:
     * <code>
     * $query->filterByViews(1234); // WHERE ststat_views = 1234
     * $query->filterByViews(array(12, 34)); // WHERE ststat_views IN (12, 34)
     * $query->filterByViews(array('min' => 12)); // WHERE ststat_views > 12
     * </code>
     *
     * @param     mixed $views The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByViews($views = null, $comparison = null)
    {
        if (is_array($views)) {
            $useMinMax = false;
            if (isset($views['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_VIEWS, $views['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($views['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_VIEWS, $views['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_VIEWS, $views, $comparison);
    }

    /**
     * Filter the query on the ststat_maximum_viewers column
     *
     * Example usage:
     * <code>
     * $query->filterByMaximumViewers(1234); // WHERE ststat_maximum_viewers = 1234
     * $query->filterByMaximumViewers(array(12, 34)); // WHERE ststat_maximum_viewers IN (12, 34)
     * $query->filterByMaximumViewers(array('min' => 12)); // WHERE ststat_maximum_viewers > 12
     * </code>
     *
     * @param     mixed $maximumViewers The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByMaximumViewers($maximumViewers = null, $comparison = null)
    {
        if (is_array($maximumViewers)) {
            $useMinMax = false;
            if (isset($maximumViewers['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS, $maximumViewers['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maximumViewers['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS, $maximumViewers['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS, $maximumViewers, $comparison);
    }

    /**
     * Filter the query on the ststat_average_viewers column
     *
     * Example usage:
     * <code>
     * $query->filterByAverageViewers(1234); // WHERE ststat_average_viewers = 1234
     * $query->filterByAverageViewers(array(12, 34)); // WHERE ststat_average_viewers IN (12, 34)
     * $query->filterByAverageViewers(array('min' => 12)); // WHERE ststat_average_viewers > 12
     * </code>
     *
     * @param     mixed $averageViewers The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByAverageViewers($averageViewers = null, $comparison = null)
    {
        if (is_array($averageViewers)) {
            $useMinMax = false;
            if (isset($averageViewers['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS, $averageViewers['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($averageViewers['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS, $averageViewers['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS, $averageViewers, $comparison);
    }

    /**
     * Filter the query on the ststat_chatters column
     *
     * Example usage:
     * <code>
     * $query->filterByChatters(1234); // WHERE ststat_chatters = 1234
     * $query->filterByChatters(array(12, 34)); // WHERE ststat_chatters IN (12, 34)
     * $query->filterByChatters(array('min' => 12)); // WHERE ststat_chatters > 12
     * </code>
     *
     * @param     mixed $chatters The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByChatters($chatters = null, $comparison = null)
    {
        if (is_array($chatters)) {
            $useMinMax = false;
            if (isset($chatters['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_CHATTERS, $chatters['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatters['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_CHATTERS, $chatters['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_CHATTERS, $chatters, $comparison);
    }

    /**
     * Filter the query on the ststat_messages column
     *
     * Example usage:
     * <code>
     * $query->filterByMessages(1234); // WHERE ststat_messages = 1234
     * $query->filterByMessages(array(12, 34)); // WHERE ststat_messages IN (12, 34)
     * $query->filterByMessages(array('min' => 12)); // WHERE ststat_messages > 12
     * </code>
     *
     * @param     mixed $messages The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByMessages($messages = null, $comparison = null)
    {
        if (is_array($messages)) {
            $useMinMax = false;
            if (isset($messages['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_MESSAGES, $messages['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($messages['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_MESSAGES, $messages['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_MESSAGES, $messages, $comparison);
    }

    /**
     * Filter the query on the ststat_hosts column
     *
     * Example usage:
     * <code>
     * $query->filterByHosts(1234); // WHERE ststat_hosts = 1234
     * $query->filterByHosts(array(12, 34)); // WHERE ststat_hosts IN (12, 34)
     * $query->filterByHosts(array('min' => 12)); // WHERE ststat_hosts > 12
     * </code>
     *
     * @param     mixed $hosts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByHosts($hosts = null, $comparison = null)
    {
        if (is_array($hosts)) {
            $useMinMax = false;
            if (isset($hosts['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_HOSTS, $hosts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hosts['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_HOSTS, $hosts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_HOSTS, $hosts, $comparison);
    }

    /**
     * Filter the query on the ststat_follows column
     *
     * Example usage:
     * <code>
     * $query->filterByFollows(1234); // WHERE ststat_follows = 1234
     * $query->filterByFollows(array(12, 34)); // WHERE ststat_follows IN (12, 34)
     * $query->filterByFollows(array('min' => 12)); // WHERE ststat_follows > 12
     * </code>
     *
     * @param     mixed $follows The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByFollows($follows = null, $comparison = null)
    {
        if (is_array($follows)) {
            $useMinMax = false;
            if (isset($follows['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_FOLLOWS, $follows['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($follows['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_FOLLOWS, $follows['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_FOLLOWS, $follows, $comparison);
    }

    /**
     * Filter the query on the ststat_unfollows column
     *
     * Example usage:
     * <code>
     * $query->filterByUnfollows(1234); // WHERE ststat_unfollows = 1234
     * $query->filterByUnfollows(array(12, 34)); // WHERE ststat_unfollows IN (12, 34)
     * $query->filterByUnfollows(array('min' => 12)); // WHERE ststat_unfollows > 12
     * </code>
     *
     * @param     mixed $unfollows The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function filterByUnfollows($unfollows = null, $comparison = null)
    {
        if (is_array($unfollows)) {
            $useMinMax = false;
            if (isset($unfollows['min'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_UNFOLLOWS, $unfollows['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unfollows['max'])) {
                $this->addUsingAlias(StatTableMap::COL_STSTAT_UNFOLLOWS, $unfollows['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StatTableMap::COL_STSTAT_UNFOLLOWS, $unfollows, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStatQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStatQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStatQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StatTableMap::COL_STSTAT_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStat $stat Object to remove from the list of results
     *
     * @return $this|ChildStatQuery The current query, for fluid interface
     */
    public function prune($stat = null)
    {
        if ($stat) {
            $this->addUsingAlias(StatTableMap::COL_STSTAT_ID, $stat->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_stat_ststat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StatTableMap::clearInstancePool();
            StatTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StatTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StatTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StatTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StatQuery

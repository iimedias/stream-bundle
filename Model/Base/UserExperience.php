<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\Avatar as ChildAvatar;
use IiMedias\StreamBundle\Model\AvatarQuery as ChildAvatarQuery;
use IiMedias\StreamBundle\Model\Channel as ChildChannel;
use IiMedias\StreamBundle\Model\ChannelQuery as ChildChannelQuery;
use IiMedias\StreamBundle\Model\ChatUser as ChildChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery as ChildChatUserQuery;
use IiMedias\StreamBundle\Model\ChatterExperience as ChildChatterExperience;
use IiMedias\StreamBundle\Model\ChatterExperienceQuery as ChildChatterExperienceQuery;
use IiMedias\StreamBundle\Model\FollowExperience as ChildFollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery as ChildFollowExperienceQuery;
use IiMedias\StreamBundle\Model\HostExperience as ChildHostExperience;
use IiMedias\StreamBundle\Model\HostExperienceQuery as ChildHostExperienceQuery;
use IiMedias\StreamBundle\Model\MessageExperience as ChildMessageExperience;
use IiMedias\StreamBundle\Model\MessageExperienceQuery as ChildMessageExperienceQuery;
use IiMedias\StreamBundle\Model\Rank as ChildRank;
use IiMedias\StreamBundle\Model\RankQuery as ChildRankQuery;
use IiMedias\StreamBundle\Model\Site as ChildSite;
use IiMedias\StreamBundle\Model\SiteQuery as ChildSiteQuery;
use IiMedias\StreamBundle\Model\Stream as ChildStream;
use IiMedias\StreamBundle\Model\StreamQuery as ChildStreamQuery;
use IiMedias\StreamBundle\Model\UserExperience as ChildUserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery as ChildUserExperienceQuery;
use IiMedias\StreamBundle\Model\Map\ChatterExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\FollowExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\HostExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\MessageExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\UserExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'stream_user_experience_stuexp' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class UserExperience implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\UserExperienceTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the stuexp_id field.
     *
     * @var        int
     */
    protected $stuexp_id;

    /**
     * The value for the stuexp_parent_id field.
     *
     * @var        int
     */
    protected $stuexp_parent_id;

    /**
     * The value for the stuexp_ststrm_id field.
     *
     * @var        int
     */
    protected $stuexp_ststrm_id;

    /**
     * The value for the stuexp_stchan_id field.
     *
     * @var        int
     */
    protected $stuexp_stchan_id;

    /**
     * The value for the stuexp_stsite_id field.
     *
     * @var        int
     */
    protected $stuexp_stsite_id;

    /**
     * The value for the stuexp_stcusr_id field.
     *
     * @var        int
     */
    protected $stuexp_stcusr_id;

    /**
     * The value for the stuexp_chat_type field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_chat_type;

    /**
     * The value for the stuexp_exp field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_exp;

    /**
     * The value for the stuexp_total_exp field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_total_exp;

    /**
     * The value for the stuexp_is_following field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $stuexp_is_following;

    /**
     * The value for the stuexp_messages_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_messages_count;

    /**
     * The value for the stuexp_follows_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_follows_count;

    /**
     * The value for the stuexp_hosts_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_hosts_count;

    /**
     * The value for the stuexp_chatters_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stuexp_chatters_count;

    /**
     * The value for the stuexp_strank_id field.
     *
     * @var        int
     */
    protected $stuexp_strank_id;

    /**
     * The value for the stuexp_stavtr_id field.
     *
     * @var        int
     */
    protected $stuexp_stavtr_id;

    /**
     * @var        ChildStream
     */
    protected $aStream;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * @var        ChildChannel
     */
    protected $aChannel;

    /**
     * @var        ChildChatUser
     */
    protected $aChatUser;

    /**
     * @var        ChildRank
     */
    protected $aRank;

    /**
     * @var        ChildAvatar
     */
    protected $aAvatar;

    /**
     * @var        ObjectCollection|ChildMessageExperience[] Collection to store aggregation of ChildMessageExperience objects.
     */
    protected $collMessageExperiences;
    protected $collMessageExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildChatterExperience[] Collection to store aggregation of ChildChatterExperience objects.
     */
    protected $collChatterExperiences;
    protected $collChatterExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildFollowExperience[] Collection to store aggregation of ChildFollowExperience objects.
     */
    protected $collFollowExperiences;
    protected $collFollowExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildHostExperience[] Collection to store aggregation of ChildHostExperience objects.
     */
    protected $collHostExperiences;
    protected $collHostExperiencesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMessageExperience[]
     */
    protected $messageExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChatterExperience[]
     */
    protected $chatterExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFollowExperience[]
     */
    protected $followExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildHostExperience[]
     */
    protected $hostExperiencesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->stuexp_chat_type = 0;
        $this->stuexp_exp = 0;
        $this->stuexp_total_exp = 0;
        $this->stuexp_is_following = false;
        $this->stuexp_messages_count = 0;
        $this->stuexp_follows_count = 0;
        $this->stuexp_hosts_count = 0;
        $this->stuexp_chatters_count = 0;
    }

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\UserExperience object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UserExperience</code> instance.  If
     * <code>obj</code> is an instance of <code>UserExperience</code>, delegates to
     * <code>equals(UserExperience)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|UserExperience The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [stuexp_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->stuexp_id;
    }

    /**
     * Get the [stuexp_parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->stuexp_parent_id;
    }

    /**
     * Get the [stuexp_ststrm_id] column value.
     *
     * @return int
     */
    public function getStreamId()
    {
        return $this->stuexp_ststrm_id;
    }

    /**
     * Get the [stuexp_stchan_id] column value.
     *
     * @return int
     */
    public function getChannelId()
    {
        return $this->stuexp_stchan_id;
    }

    /**
     * Get the [stuexp_stsite_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->stuexp_stsite_id;
    }

    /**
     * Get the [stuexp_stcusr_id] column value.
     *
     * @return int
     */
    public function getChatUserId()
    {
        return $this->stuexp_stcusr_id;
    }

    /**
     * Get the [stuexp_chat_type] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getChatType()
    {
        if (null === $this->stuexp_chat_type) {
            return null;
        }
        $valueSet = UserExperienceTableMap::getValueSet(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE);
        if (!isset($valueSet[$this->stuexp_chat_type])) {
            throw new PropelException('Unknown stored enum key: ' . $this->stuexp_chat_type);
        }

        return $valueSet[$this->stuexp_chat_type];
    }

    /**
     * Get the [stuexp_exp] column value.
     *
     * @return int
     */
    public function getExp()
    {
        return $this->stuexp_exp;
    }

    /**
     * Get the [stuexp_total_exp] column value.
     *
     * @return int
     */
    public function getTotalExp()
    {
        return $this->stuexp_total_exp;
    }

    /**
     * Get the [stuexp_is_following] column value.
     *
     * @return boolean
     */
    public function getIsFollowing()
    {
        return $this->stuexp_is_following;
    }

    /**
     * Get the [stuexp_is_following] column value.
     *
     * @return boolean
     */
    public function isFollowing()
    {
        return $this->getIsFollowing();
    }

    /**
     * Get the [stuexp_messages_count] column value.
     *
     * @return int
     */
    public function getMessagesCount()
    {
        return $this->stuexp_messages_count;
    }

    /**
     * Get the [stuexp_follows_count] column value.
     *
     * @return int
     */
    public function getFollowsCount()
    {
        return $this->stuexp_follows_count;
    }

    /**
     * Get the [stuexp_hosts_count] column value.
     *
     * @return int
     */
    public function getHostsCount()
    {
        return $this->stuexp_hosts_count;
    }

    /**
     * Get the [stuexp_chatters_count] column value.
     *
     * @return int
     */
    public function getChattersCount()
    {
        return $this->stuexp_chatters_count;
    }

    /**
     * Get the [stuexp_strank_id] column value.
     *
     * @return int
     */
    public function getRankId()
    {
        return $this->stuexp_strank_id;
    }

    /**
     * Get the [stuexp_stavtr_id] column value.
     *
     * @return int
     */
    public function getAvatarId()
    {
        return $this->stuexp_stavtr_id;
    }

    /**
     * Set the value of [stuexp_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_id !== $v) {
            $this->stuexp_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [stuexp_parent_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_parent_id !== $v) {
            $this->stuexp_parent_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_PARENT_ID] = true;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [stuexp_ststrm_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setStreamId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_ststrm_id !== $v) {
            $this->stuexp_ststrm_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STSTRM_ID] = true;
        }

        if ($this->aStream !== null && $this->aStream->getId() !== $v) {
            $this->aStream = null;
        }

        return $this;
    } // setStreamId()

    /**
     * Set the value of [stuexp_stchan_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setChannelId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_stchan_id !== $v) {
            $this->stuexp_stchan_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STCHAN_ID] = true;
        }

        if ($this->aChannel !== null && $this->aChannel->getId() !== $v) {
            $this->aChannel = null;
        }

        return $this;
    } // setChannelId()

    /**
     * Set the value of [stuexp_stsite_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_stsite_id !== $v) {
            $this->stuexp_stsite_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STSITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [stuexp_stcusr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setChatUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_stcusr_id !== $v) {
            $this->stuexp_stcusr_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STCUSR_ID] = true;
        }

        if ($this->aChatUser !== null && $this->aChatUser->getId() !== $v) {
            $this->aChatUser = null;
        }

        return $this;
    } // setChatUserId()

    /**
     * Set the value of [stuexp_chat_type] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setChatType($v)
    {
        if ($v !== null) {
            $valueSet = UserExperienceTableMap::getValueSet(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->stuexp_chat_type !== $v) {
            $this->stuexp_chat_type = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_CHAT_TYPE] = true;
        }

        return $this;
    } // setChatType()

    /**
     * Set the value of [stuexp_exp] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setExp($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_exp !== $v) {
            $this->stuexp_exp = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_EXP] = true;
        }

        return $this;
    } // setExp()

    /**
     * Set the value of [stuexp_total_exp] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setTotalExp($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_total_exp !== $v) {
            $this->stuexp_total_exp = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_TOTAL_EXP] = true;
        }

        return $this;
    } // setTotalExp()

    /**
     * Sets the value of the [stuexp_is_following] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setIsFollowing($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stuexp_is_following !== $v) {
            $this->stuexp_is_following = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING] = true;
        }

        return $this;
    } // setIsFollowing()

    /**
     * Set the value of [stuexp_messages_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setMessagesCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_messages_count !== $v) {
            $this->stuexp_messages_count = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT] = true;
        }

        return $this;
    } // setMessagesCount()

    /**
     * Set the value of [stuexp_follows_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setFollowsCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_follows_count !== $v) {
            $this->stuexp_follows_count = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT] = true;
        }

        return $this;
    } // setFollowsCount()

    /**
     * Set the value of [stuexp_hosts_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setHostsCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_hosts_count !== $v) {
            $this->stuexp_hosts_count = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT] = true;
        }

        return $this;
    } // setHostsCount()

    /**
     * Set the value of [stuexp_chatters_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setChattersCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_chatters_count !== $v) {
            $this->stuexp_chatters_count = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT] = true;
        }

        return $this;
    } // setChattersCount()

    /**
     * Set the value of [stuexp_strank_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setRankId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_strank_id !== $v) {
            $this->stuexp_strank_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STRANK_ID] = true;
        }

        if ($this->aRank !== null && $this->aRank->getId() !== $v) {
            $this->aRank = null;
        }

        return $this;
    } // setRankId()

    /**
     * Set the value of [stuexp_stavtr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function setAvatarId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stuexp_stavtr_id !== $v) {
            $this->stuexp_stavtr_id = $v;
            $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_STAVTR_ID] = true;
        }

        if ($this->aAvatar !== null && $this->aAvatar->getId() !== $v) {
            $this->aAvatar = null;
        }

        return $this;
    } // setAvatarId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->stuexp_chat_type !== 0) {
                return false;
            }

            if ($this->stuexp_exp !== 0) {
                return false;
            }

            if ($this->stuexp_total_exp !== 0) {
                return false;
            }

            if ($this->stuexp_is_following !== false) {
                return false;
            }

            if ($this->stuexp_messages_count !== 0) {
                return false;
            }

            if ($this->stuexp_follows_count !== 0) {
                return false;
            }

            if ($this->stuexp_hosts_count !== 0) {
                return false;
            }

            if ($this->stuexp_chatters_count !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserExperienceTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserExperienceTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserExperienceTableMap::translateFieldName('StreamId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_ststrm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserExperienceTableMap::translateFieldName('ChannelId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_stchan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserExperienceTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_stsite_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserExperienceTableMap::translateFieldName('ChatUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_stcusr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserExperienceTableMap::translateFieldName('ChatType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_chat_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserExperienceTableMap::translateFieldName('Exp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_exp = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserExperienceTableMap::translateFieldName('TotalExp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_total_exp = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserExperienceTableMap::translateFieldName('IsFollowing', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_is_following = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UserExperienceTableMap::translateFieldName('MessagesCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_messages_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UserExperienceTableMap::translateFieldName('FollowsCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_follows_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UserExperienceTableMap::translateFieldName('HostsCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_hosts_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UserExperienceTableMap::translateFieldName('ChattersCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_chatters_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : UserExperienceTableMap::translateFieldName('RankId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_strank_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : UserExperienceTableMap::translateFieldName('AvatarId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stuexp_stavtr_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = UserExperienceTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\UserExperience'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStream !== null && $this->stuexp_ststrm_id !== $this->aStream->getId()) {
            $this->aStream = null;
        }
        if ($this->aChannel !== null && $this->stuexp_stchan_id !== $this->aChannel->getId()) {
            $this->aChannel = null;
        }
        if ($this->aSite !== null && $this->stuexp_stsite_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
        if ($this->aChatUser !== null && $this->stuexp_stcusr_id !== $this->aChatUser->getId()) {
            $this->aChatUser = null;
        }
        if ($this->aRank !== null && $this->stuexp_strank_id !== $this->aRank->getId()) {
            $this->aRank = null;
        }
        if ($this->aAvatar !== null && $this->stuexp_stavtr_id !== $this->aAvatar->getId()) {
            $this->aAvatar = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserExperienceQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStream = null;
            $this->aSite = null;
            $this->aChannel = null;
            $this->aChatUser = null;
            $this->aRank = null;
            $this->aAvatar = null;
            $this->collMessageExperiences = null;

            $this->collChatterExperiences = null;

            $this->collFollowExperiences = null;

            $this->collHostExperiences = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UserExperience::setDeleted()
     * @see UserExperience::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserExperienceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserExperienceTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStream !== null) {
                if ($this->aStream->isModified() || $this->aStream->isNew()) {
                    $affectedRows += $this->aStream->save($con);
                }
                $this->setStream($this->aStream);
            }

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aChannel !== null) {
                if ($this->aChannel->isModified() || $this->aChannel->isNew()) {
                    $affectedRows += $this->aChannel->save($con);
                }
                $this->setChannel($this->aChannel);
            }

            if ($this->aChatUser !== null) {
                if ($this->aChatUser->isModified() || $this->aChatUser->isNew()) {
                    $affectedRows += $this->aChatUser->save($con);
                }
                $this->setChatUser($this->aChatUser);
            }

            if ($this->aRank !== null) {
                if ($this->aRank->isModified() || $this->aRank->isNew()) {
                    $affectedRows += $this->aRank->save($con);
                }
                $this->setRank($this->aRank);
            }

            if ($this->aAvatar !== null) {
                if ($this->aAvatar->isModified() || $this->aAvatar->isNew()) {
                    $affectedRows += $this->aAvatar->save($con);
                }
                $this->setAvatar($this->aAvatar);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->messageExperiencesScheduledForDeletion !== null) {
                if (!$this->messageExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\MessageExperienceQuery::create()
                        ->filterByPrimaryKeys($this->messageExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->messageExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collMessageExperiences !== null) {
                foreach ($this->collMessageExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->chatterExperiencesScheduledForDeletion !== null) {
                if (!$this->chatterExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatterExperienceQuery::create()
                        ->filterByPrimaryKeys($this->chatterExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->chatterExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collChatterExperiences !== null) {
                foreach ($this->collChatterExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->followExperiencesScheduledForDeletion !== null) {
                if (!$this->followExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\FollowExperienceQuery::create()
                        ->filterByPrimaryKeys($this->followExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->followExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collFollowExperiences !== null) {
                foreach ($this->collFollowExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->hostExperiencesScheduledForDeletion !== null) {
                if (!$this->hostExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\HostExperienceQuery::create()
                        ->filterByPrimaryKeys($this->hostExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->hostExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collHostExperiences !== null) {
                foreach ($this->collHostExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserExperienceTableMap::COL_STUEXP_ID] = true;
        if (null !== $this->stuexp_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserExperienceTableMap::COL_STUEXP_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_parent_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STSTRM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_ststrm_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STCHAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_stchan_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STSITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_stsite_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STCUSR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_stcusr_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_chat_type';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_EXP)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_exp';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_total_exp';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_is_following';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_messages_count';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_follows_count';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_hosts_count';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_chatters_count';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STRANK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_strank_id';
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STAVTR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stuexp_stavtr_id';
        }

        $sql = sprintf(
            'INSERT INTO stream_user_experience_stuexp (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'stuexp_id':
                        $stmt->bindValue($identifier, $this->stuexp_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_parent_id':
                        $stmt->bindValue($identifier, $this->stuexp_parent_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_ststrm_id':
                        $stmt->bindValue($identifier, $this->stuexp_ststrm_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_stchan_id':
                        $stmt->bindValue($identifier, $this->stuexp_stchan_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_stsite_id':
                        $stmt->bindValue($identifier, $this->stuexp_stsite_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_stcusr_id':
                        $stmt->bindValue($identifier, $this->stuexp_stcusr_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_chat_type':
                        $stmt->bindValue($identifier, $this->stuexp_chat_type, PDO::PARAM_INT);
                        break;
                    case 'stuexp_exp':
                        $stmt->bindValue($identifier, $this->stuexp_exp, PDO::PARAM_INT);
                        break;
                    case 'stuexp_total_exp':
                        $stmt->bindValue($identifier, $this->stuexp_total_exp, PDO::PARAM_INT);
                        break;
                    case 'stuexp_is_following':
                        $stmt->bindValue($identifier, (int) $this->stuexp_is_following, PDO::PARAM_INT);
                        break;
                    case 'stuexp_messages_count':
                        $stmt->bindValue($identifier, $this->stuexp_messages_count, PDO::PARAM_INT);
                        break;
                    case 'stuexp_follows_count':
                        $stmt->bindValue($identifier, $this->stuexp_follows_count, PDO::PARAM_INT);
                        break;
                    case 'stuexp_hosts_count':
                        $stmt->bindValue($identifier, $this->stuexp_hosts_count, PDO::PARAM_INT);
                        break;
                    case 'stuexp_chatters_count':
                        $stmt->bindValue($identifier, $this->stuexp_chatters_count, PDO::PARAM_INT);
                        break;
                    case 'stuexp_strank_id':
                        $stmt->bindValue($identifier, $this->stuexp_strank_id, PDO::PARAM_INT);
                        break;
                    case 'stuexp_stavtr_id':
                        $stmt->bindValue($identifier, $this->stuexp_stavtr_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserExperienceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getParentId();
                break;
            case 2:
                return $this->getStreamId();
                break;
            case 3:
                return $this->getChannelId();
                break;
            case 4:
                return $this->getSiteId();
                break;
            case 5:
                return $this->getChatUserId();
                break;
            case 6:
                return $this->getChatType();
                break;
            case 7:
                return $this->getExp();
                break;
            case 8:
                return $this->getTotalExp();
                break;
            case 9:
                return $this->getIsFollowing();
                break;
            case 10:
                return $this->getMessagesCount();
                break;
            case 11:
                return $this->getFollowsCount();
                break;
            case 12:
                return $this->getHostsCount();
                break;
            case 13:
                return $this->getChattersCount();
                break;
            case 14:
                return $this->getRankId();
                break;
            case 15:
                return $this->getAvatarId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UserExperience'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UserExperience'][$this->hashCode()] = true;
        $keys = UserExperienceTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getParentId(),
            $keys[2] => $this->getStreamId(),
            $keys[3] => $this->getChannelId(),
            $keys[4] => $this->getSiteId(),
            $keys[5] => $this->getChatUserId(),
            $keys[6] => $this->getChatType(),
            $keys[7] => $this->getExp(),
            $keys[8] => $this->getTotalExp(),
            $keys[9] => $this->getIsFollowing(),
            $keys[10] => $this->getMessagesCount(),
            $keys[11] => $this->getFollowsCount(),
            $keys[12] => $this->getHostsCount(),
            $keys[13] => $this->getChattersCount(),
            $keys[14] => $this->getRankId(),
            $keys[15] => $this->getAvatarId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStream) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stream';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrm';
                        break;
                    default:
                        $key = 'Stream';
                }

                $result[$key] = $this->aStream->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsite';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aChannel) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'channel';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_channel_stchan';
                        break;
                    default:
                        $key = 'Channel';
                }

                $result[$key] = $this->aChannel->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aChatUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatUser';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chat_user_stcusr';
                        break;
                    default:
                        $key = 'ChatUser';
                }

                $result[$key] = $this->aChatUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRank) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rank';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_rank_strank';
                        break;
                    default:
                        $key = 'Rank';
                }

                $result[$key] = $this->aRank->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAvatar) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'avatar';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_avatar_stavtr';
                        break;
                    default:
                        $key = 'Avatar';
                }

                $result[$key] = $this->aAvatar->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMessageExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'messageExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_message_experience_stmexps';
                        break;
                    default:
                        $key = 'MessageExperiences';
                }

                $result[$key] = $this->collMessageExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChatterExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatterExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chatter_experience_stcexps';
                        break;
                    default:
                        $key = 'ChatterExperiences';
                }

                $result[$key] = $this->collChatterExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFollowExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'followExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_follow_experience_stfexps';
                        break;
                    default:
                        $key = 'FollowExperiences';
                }

                $result[$key] = $this->collFollowExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collHostExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'hostExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_host_experience_sthexps';
                        break;
                    default:
                        $key = 'HostExperiences';
                }

                $result[$key] = $this->collHostExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserExperienceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setParentId($value);
                break;
            case 2:
                $this->setStreamId($value);
                break;
            case 3:
                $this->setChannelId($value);
                break;
            case 4:
                $this->setSiteId($value);
                break;
            case 5:
                $this->setChatUserId($value);
                break;
            case 6:
                $valueSet = UserExperienceTableMap::getValueSet(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setChatType($value);
                break;
            case 7:
                $this->setExp($value);
                break;
            case 8:
                $this->setTotalExp($value);
                break;
            case 9:
                $this->setIsFollowing($value);
                break;
            case 10:
                $this->setMessagesCount($value);
                break;
            case 11:
                $this->setFollowsCount($value);
                break;
            case 12:
                $this->setHostsCount($value);
                break;
            case 13:
                $this->setChattersCount($value);
                break;
            case 14:
                $this->setRankId($value);
                break;
            case 15:
                $this->setAvatarId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserExperienceTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setParentId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setStreamId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setChannelId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSiteId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setChatUserId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setChatType($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setExp($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTotalExp($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsFollowing($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setMessagesCount($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFollowsCount($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setHostsCount($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setChattersCount($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setRankId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setAvatarId($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserExperienceTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_ID, $this->stuexp_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_PARENT_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_PARENT_ID, $this->stuexp_parent_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STSTRM_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $this->stuexp_ststrm_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STCHAN_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $this->stuexp_stchan_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STSITE_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $this->stuexp_stsite_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STCUSR_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $this->stuexp_stcusr_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE, $this->stuexp_chat_type);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_EXP)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_EXP, $this->stuexp_exp);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP, $this->stuexp_total_exp);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING, $this->stuexp_is_following);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT, $this->stuexp_messages_count);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT, $this->stuexp_follows_count);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT, $this->stuexp_hosts_count);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT, $this->stuexp_chatters_count);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STRANK_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $this->stuexp_strank_id);
        }
        if ($this->isColumnModified(UserExperienceTableMap::COL_STUEXP_STAVTR_ID)) {
            $criteria->add(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $this->stuexp_stavtr_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserExperienceQuery::create();
        $criteria->add(UserExperienceTableMap::COL_STUEXP_ID, $this->stuexp_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (stuexp_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\UserExperience (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setParentId($this->getParentId());
        $copyObj->setStreamId($this->getStreamId());
        $copyObj->setChannelId($this->getChannelId());
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setChatUserId($this->getChatUserId());
        $copyObj->setChatType($this->getChatType());
        $copyObj->setExp($this->getExp());
        $copyObj->setTotalExp($this->getTotalExp());
        $copyObj->setIsFollowing($this->getIsFollowing());
        $copyObj->setMessagesCount($this->getMessagesCount());
        $copyObj->setFollowsCount($this->getFollowsCount());
        $copyObj->setHostsCount($this->getHostsCount());
        $copyObj->setChattersCount($this->getChattersCount());
        $copyObj->setRankId($this->getRankId());
        $copyObj->setAvatarId($this->getAvatarId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMessageExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMessageExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChatterExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChatterExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFollowExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFollowExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getHostExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addHostExperience($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\UserExperience Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStream object.
     *
     * @param  ChildStream $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStream(ChildStream $v = null)
    {
        if ($v === null) {
            $this->setStreamId(NULL);
        } else {
            $this->setStreamId($v->getId());
        }

        $this->aStream = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStream object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStream object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStream The associated ChildStream object.
     * @throws PropelException
     */
    public function getStream(ConnectionInterface $con = null)
    {
        if ($this->aStream === null && ($this->stuexp_ststrm_id != 0)) {
            $this->aStream = ChildStreamQuery::create()->findPk($this->stuexp_ststrm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStream->addUserExperiences($this);
             */
        }

        return $this->aStream;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->stuexp_stsite_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->stuexp_stsite_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addUserExperiences($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a ChildChannel object.
     *
     * @param  ChildChannel $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChannel(ChildChannel $v = null)
    {
        if ($v === null) {
            $this->setChannelId(NULL);
        } else {
            $this->setChannelId($v->getId());
        }

        $this->aChannel = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildChannel object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildChannel object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildChannel The associated ChildChannel object.
     * @throws PropelException
     */
    public function getChannel(ConnectionInterface $con = null)
    {
        if ($this->aChannel === null && ($this->stuexp_stchan_id != 0)) {
            $this->aChannel = ChildChannelQuery::create()->findPk($this->stuexp_stchan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChannel->addUserExperiences($this);
             */
        }

        return $this->aChannel;
    }

    /**
     * Declares an association between this object and a ChildChatUser object.
     *
     * @param  ChildChatUser $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChatUser(ChildChatUser $v = null)
    {
        if ($v === null) {
            $this->setChatUserId(NULL);
        } else {
            $this->setChatUserId($v->getId());
        }

        $this->aChatUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildChatUser object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildChatUser object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildChatUser The associated ChildChatUser object.
     * @throws PropelException
     */
    public function getChatUser(ConnectionInterface $con = null)
    {
        if ($this->aChatUser === null && ($this->stuexp_stcusr_id != 0)) {
            $this->aChatUser = ChildChatUserQuery::create()->findPk($this->stuexp_stcusr_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChatUser->addUserExperiences($this);
             */
        }

        return $this->aChatUser;
    }

    /**
     * Declares an association between this object and a ChildRank object.
     *
     * @param  ChildRank $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRank(ChildRank $v = null)
    {
        if ($v === null) {
            $this->setRankId(NULL);
        } else {
            $this->setRankId($v->getId());
        }

        $this->aRank = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildRank object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildRank object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildRank The associated ChildRank object.
     * @throws PropelException
     */
    public function getRank(ConnectionInterface $con = null)
    {
        if ($this->aRank === null && ($this->stuexp_strank_id != 0)) {
            $this->aRank = ChildRankQuery::create()->findPk($this->stuexp_strank_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRank->addUserExperiences($this);
             */
        }

        return $this->aRank;
    }

    /**
     * Declares an association between this object and a ChildAvatar object.
     *
     * @param  ChildAvatar $v
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAvatar(ChildAvatar $v = null)
    {
        if ($v === null) {
            $this->setAvatarId(NULL);
        } else {
            $this->setAvatarId($v->getId());
        }

        $this->aAvatar = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildAvatar object, it will not be re-added.
        if ($v !== null) {
            $v->addUserExperience($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildAvatar object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildAvatar The associated ChildAvatar object.
     * @throws PropelException
     */
    public function getAvatar(ConnectionInterface $con = null)
    {
        if ($this->aAvatar === null && ($this->stuexp_stavtr_id != 0)) {
            $this->aAvatar = ChildAvatarQuery::create()->findPk($this->stuexp_stavtr_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAvatar->addUserExperiences($this);
             */
        }

        return $this->aAvatar;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MessageExperience' == $relationName) {
            $this->initMessageExperiences();
            return;
        }
        if ('ChatterExperience' == $relationName) {
            $this->initChatterExperiences();
            return;
        }
        if ('FollowExperience' == $relationName) {
            $this->initFollowExperiences();
            return;
        }
        if ('HostExperience' == $relationName) {
            $this->initHostExperiences();
            return;
        }
    }

    /**
     * Clears out the collMessageExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMessageExperiences()
     */
    public function clearMessageExperiences()
    {
        $this->collMessageExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMessageExperiences collection loaded partially.
     */
    public function resetPartialMessageExperiences($v = true)
    {
        $this->collMessageExperiencesPartial = $v;
    }

    /**
     * Initializes the collMessageExperiences collection.
     *
     * By default this just sets the collMessageExperiences collection to an empty array (like clearcollMessageExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMessageExperiences($overrideExisting = true)
    {
        if (null !== $this->collMessageExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = MessageExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collMessageExperiences = new $collectionClassName;
        $this->collMessageExperiences->setModel('\IiMedias\StreamBundle\Model\MessageExperience');
    }

    /**
     * Gets an array of ChildMessageExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserExperience is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     * @throws PropelException
     */
    public function getMessageExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                // return empty collection
                $this->initMessageExperiences();
            } else {
                $collMessageExperiences = ChildMessageExperienceQuery::create(null, $criteria)
                    ->filterByUserExperience($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMessageExperiencesPartial && count($collMessageExperiences)) {
                        $this->initMessageExperiences(false);

                        foreach ($collMessageExperiences as $obj) {
                            if (false == $this->collMessageExperiences->contains($obj)) {
                                $this->collMessageExperiences->append($obj);
                            }
                        }

                        $this->collMessageExperiencesPartial = true;
                    }

                    return $collMessageExperiences;
                }

                if ($partial && $this->collMessageExperiences) {
                    foreach ($this->collMessageExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collMessageExperiences[] = $obj;
                        }
                    }
                }

                $this->collMessageExperiences = $collMessageExperiences;
                $this->collMessageExperiencesPartial = false;
            }
        }

        return $this->collMessageExperiences;
    }

    /**
     * Sets a collection of ChildMessageExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $messageExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function setMessageExperiences(Collection $messageExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildMessageExperience[] $messageExperiencesToDelete */
        $messageExperiencesToDelete = $this->getMessageExperiences(new Criteria(), $con)->diff($messageExperiences);


        $this->messageExperiencesScheduledForDeletion = $messageExperiencesToDelete;

        foreach ($messageExperiencesToDelete as $messageExperienceRemoved) {
            $messageExperienceRemoved->setUserExperience(null);
        }

        $this->collMessageExperiences = null;
        foreach ($messageExperiences as $messageExperience) {
            $this->addMessageExperience($messageExperience);
        }

        $this->collMessageExperiences = $messageExperiences;
        $this->collMessageExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MessageExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MessageExperience objects.
     * @throws PropelException
     */
    public function countMessageExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMessageExperiences());
            }

            $query = ChildMessageExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserExperience($this)
                ->count($con);
        }

        return count($this->collMessageExperiences);
    }

    /**
     * Method called to associate a ChildMessageExperience object to this object
     * through the ChildMessageExperience foreign key attribute.
     *
     * @param  ChildMessageExperience $l ChildMessageExperience
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function addMessageExperience(ChildMessageExperience $l)
    {
        if ($this->collMessageExperiences === null) {
            $this->initMessageExperiences();
            $this->collMessageExperiencesPartial = true;
        }

        if (!$this->collMessageExperiences->contains($l)) {
            $this->doAddMessageExperience($l);

            if ($this->messageExperiencesScheduledForDeletion and $this->messageExperiencesScheduledForDeletion->contains($l)) {
                $this->messageExperiencesScheduledForDeletion->remove($this->messageExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMessageExperience $messageExperience The ChildMessageExperience object to add.
     */
    protected function doAddMessageExperience(ChildMessageExperience $messageExperience)
    {
        $this->collMessageExperiences[]= $messageExperience;
        $messageExperience->setUserExperience($this);
    }

    /**
     * @param  ChildMessageExperience $messageExperience The ChildMessageExperience object to remove.
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function removeMessageExperience(ChildMessageExperience $messageExperience)
    {
        if ($this->getMessageExperiences()->contains($messageExperience)) {
            $pos = $this->collMessageExperiences->search($messageExperience);
            $this->collMessageExperiences->remove($pos);
            if (null === $this->messageExperiencesScheduledForDeletion) {
                $this->messageExperiencesScheduledForDeletion = clone $this->collMessageExperiences;
                $this->messageExperiencesScheduledForDeletion->clear();
            }
            $this->messageExperiencesScheduledForDeletion[]= $messageExperience;
            $messageExperience->setUserExperience(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }

    /**
     * Clears out the collChatterExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChatterExperiences()
     */
    public function clearChatterExperiences()
    {
        $this->collChatterExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChatterExperiences collection loaded partially.
     */
    public function resetPartialChatterExperiences($v = true)
    {
        $this->collChatterExperiencesPartial = $v;
    }

    /**
     * Initializes the collChatterExperiences collection.
     *
     * By default this just sets the collChatterExperiences collection to an empty array (like clearcollChatterExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChatterExperiences($overrideExisting = true)
    {
        if (null !== $this->collChatterExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatterExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collChatterExperiences = new $collectionClassName;
        $this->collChatterExperiences->setModel('\IiMedias\StreamBundle\Model\ChatterExperience');
    }

    /**
     * Gets an array of ChildChatterExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserExperience is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     * @throws PropelException
     */
    public function getChatterExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                // return empty collection
                $this->initChatterExperiences();
            } else {
                $collChatterExperiences = ChildChatterExperienceQuery::create(null, $criteria)
                    ->filterByUserExperience($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChatterExperiencesPartial && count($collChatterExperiences)) {
                        $this->initChatterExperiences(false);

                        foreach ($collChatterExperiences as $obj) {
                            if (false == $this->collChatterExperiences->contains($obj)) {
                                $this->collChatterExperiences->append($obj);
                            }
                        }

                        $this->collChatterExperiencesPartial = true;
                    }

                    return $collChatterExperiences;
                }

                if ($partial && $this->collChatterExperiences) {
                    foreach ($this->collChatterExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collChatterExperiences[] = $obj;
                        }
                    }
                }

                $this->collChatterExperiences = $collChatterExperiences;
                $this->collChatterExperiencesPartial = false;
            }
        }

        return $this->collChatterExperiences;
    }

    /**
     * Sets a collection of ChildChatterExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chatterExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function setChatterExperiences(Collection $chatterExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildChatterExperience[] $chatterExperiencesToDelete */
        $chatterExperiencesToDelete = $this->getChatterExperiences(new Criteria(), $con)->diff($chatterExperiences);


        $this->chatterExperiencesScheduledForDeletion = $chatterExperiencesToDelete;

        foreach ($chatterExperiencesToDelete as $chatterExperienceRemoved) {
            $chatterExperienceRemoved->setUserExperience(null);
        }

        $this->collChatterExperiences = null;
        foreach ($chatterExperiences as $chatterExperience) {
            $this->addChatterExperience($chatterExperience);
        }

        $this->collChatterExperiences = $chatterExperiences;
        $this->collChatterExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChatterExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ChatterExperience objects.
     * @throws PropelException
     */
    public function countChatterExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChatterExperiences());
            }

            $query = ChildChatterExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserExperience($this)
                ->count($con);
        }

        return count($this->collChatterExperiences);
    }

    /**
     * Method called to associate a ChildChatterExperience object to this object
     * through the ChildChatterExperience foreign key attribute.
     *
     * @param  ChildChatterExperience $l ChildChatterExperience
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function addChatterExperience(ChildChatterExperience $l)
    {
        if ($this->collChatterExperiences === null) {
            $this->initChatterExperiences();
            $this->collChatterExperiencesPartial = true;
        }

        if (!$this->collChatterExperiences->contains($l)) {
            $this->doAddChatterExperience($l);

            if ($this->chatterExperiencesScheduledForDeletion and $this->chatterExperiencesScheduledForDeletion->contains($l)) {
                $this->chatterExperiencesScheduledForDeletion->remove($this->chatterExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChatterExperience $chatterExperience The ChildChatterExperience object to add.
     */
    protected function doAddChatterExperience(ChildChatterExperience $chatterExperience)
    {
        $this->collChatterExperiences[]= $chatterExperience;
        $chatterExperience->setUserExperience($this);
    }

    /**
     * @param  ChildChatterExperience $chatterExperience The ChildChatterExperience object to remove.
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function removeChatterExperience(ChildChatterExperience $chatterExperience)
    {
        if ($this->getChatterExperiences()->contains($chatterExperience)) {
            $pos = $this->collChatterExperiences->search($chatterExperience);
            $this->collChatterExperiences->remove($pos);
            if (null === $this->chatterExperiencesScheduledForDeletion) {
                $this->chatterExperiencesScheduledForDeletion = clone $this->collChatterExperiences;
                $this->chatterExperiencesScheduledForDeletion->clear();
            }
            $this->chatterExperiencesScheduledForDeletion[]= $chatterExperience;
            $chatterExperience->setUserExperience(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }

    /**
     * Clears out the collFollowExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFollowExperiences()
     */
    public function clearFollowExperiences()
    {
        $this->collFollowExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFollowExperiences collection loaded partially.
     */
    public function resetPartialFollowExperiences($v = true)
    {
        $this->collFollowExperiencesPartial = $v;
    }

    /**
     * Initializes the collFollowExperiences collection.
     *
     * By default this just sets the collFollowExperiences collection to an empty array (like clearcollFollowExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFollowExperiences($overrideExisting = true)
    {
        if (null !== $this->collFollowExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = FollowExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collFollowExperiences = new $collectionClassName;
        $this->collFollowExperiences->setModel('\IiMedias\StreamBundle\Model\FollowExperience');
    }

    /**
     * Gets an array of ChildFollowExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserExperience is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     * @throws PropelException
     */
    public function getFollowExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                // return empty collection
                $this->initFollowExperiences();
            } else {
                $collFollowExperiences = ChildFollowExperienceQuery::create(null, $criteria)
                    ->filterByUserExperience($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFollowExperiencesPartial && count($collFollowExperiences)) {
                        $this->initFollowExperiences(false);

                        foreach ($collFollowExperiences as $obj) {
                            if (false == $this->collFollowExperiences->contains($obj)) {
                                $this->collFollowExperiences->append($obj);
                            }
                        }

                        $this->collFollowExperiencesPartial = true;
                    }

                    return $collFollowExperiences;
                }

                if ($partial && $this->collFollowExperiences) {
                    foreach ($this->collFollowExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collFollowExperiences[] = $obj;
                        }
                    }
                }

                $this->collFollowExperiences = $collFollowExperiences;
                $this->collFollowExperiencesPartial = false;
            }
        }

        return $this->collFollowExperiences;
    }

    /**
     * Sets a collection of ChildFollowExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $followExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function setFollowExperiences(Collection $followExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildFollowExperience[] $followExperiencesToDelete */
        $followExperiencesToDelete = $this->getFollowExperiences(new Criteria(), $con)->diff($followExperiences);


        $this->followExperiencesScheduledForDeletion = $followExperiencesToDelete;

        foreach ($followExperiencesToDelete as $followExperienceRemoved) {
            $followExperienceRemoved->setUserExperience(null);
        }

        $this->collFollowExperiences = null;
        foreach ($followExperiences as $followExperience) {
            $this->addFollowExperience($followExperience);
        }

        $this->collFollowExperiences = $followExperiences;
        $this->collFollowExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FollowExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FollowExperience objects.
     * @throws PropelException
     */
    public function countFollowExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFollowExperiences());
            }

            $query = ChildFollowExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserExperience($this)
                ->count($con);
        }

        return count($this->collFollowExperiences);
    }

    /**
     * Method called to associate a ChildFollowExperience object to this object
     * through the ChildFollowExperience foreign key attribute.
     *
     * @param  ChildFollowExperience $l ChildFollowExperience
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function addFollowExperience(ChildFollowExperience $l)
    {
        if ($this->collFollowExperiences === null) {
            $this->initFollowExperiences();
            $this->collFollowExperiencesPartial = true;
        }

        if (!$this->collFollowExperiences->contains($l)) {
            $this->doAddFollowExperience($l);

            if ($this->followExperiencesScheduledForDeletion and $this->followExperiencesScheduledForDeletion->contains($l)) {
                $this->followExperiencesScheduledForDeletion->remove($this->followExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFollowExperience $followExperience The ChildFollowExperience object to add.
     */
    protected function doAddFollowExperience(ChildFollowExperience $followExperience)
    {
        $this->collFollowExperiences[]= $followExperience;
        $followExperience->setUserExperience($this);
    }

    /**
     * @param  ChildFollowExperience $followExperience The ChildFollowExperience object to remove.
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function removeFollowExperience(ChildFollowExperience $followExperience)
    {
        if ($this->getFollowExperiences()->contains($followExperience)) {
            $pos = $this->collFollowExperiences->search($followExperience);
            $this->collFollowExperiences->remove($pos);
            if (null === $this->followExperiencesScheduledForDeletion) {
                $this->followExperiencesScheduledForDeletion = clone $this->collFollowExperiences;
                $this->followExperiencesScheduledForDeletion->clear();
            }
            $this->followExperiencesScheduledForDeletion[]= $followExperience;
            $followExperience->setUserExperience(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }

    /**
     * Clears out the collHostExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addHostExperiences()
     */
    public function clearHostExperiences()
    {
        $this->collHostExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collHostExperiences collection loaded partially.
     */
    public function resetPartialHostExperiences($v = true)
    {
        $this->collHostExperiencesPartial = $v;
    }

    /**
     * Initializes the collHostExperiences collection.
     *
     * By default this just sets the collHostExperiences collection to an empty array (like clearcollHostExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initHostExperiences($overrideExisting = true)
    {
        if (null !== $this->collHostExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = HostExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collHostExperiences = new $collectionClassName;
        $this->collHostExperiences->setModel('\IiMedias\StreamBundle\Model\HostExperience');
    }

    /**
     * Gets an array of ChildHostExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserExperience is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     * @throws PropelException
     */
    public function getHostExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                // return empty collection
                $this->initHostExperiences();
            } else {
                $collHostExperiences = ChildHostExperienceQuery::create(null, $criteria)
                    ->filterByUserExperience($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collHostExperiencesPartial && count($collHostExperiences)) {
                        $this->initHostExperiences(false);

                        foreach ($collHostExperiences as $obj) {
                            if (false == $this->collHostExperiences->contains($obj)) {
                                $this->collHostExperiences->append($obj);
                            }
                        }

                        $this->collHostExperiencesPartial = true;
                    }

                    return $collHostExperiences;
                }

                if ($partial && $this->collHostExperiences) {
                    foreach ($this->collHostExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collHostExperiences[] = $obj;
                        }
                    }
                }

                $this->collHostExperiences = $collHostExperiences;
                $this->collHostExperiencesPartial = false;
            }
        }

        return $this->collHostExperiences;
    }

    /**
     * Sets a collection of ChildHostExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $hostExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function setHostExperiences(Collection $hostExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildHostExperience[] $hostExperiencesToDelete */
        $hostExperiencesToDelete = $this->getHostExperiences(new Criteria(), $con)->diff($hostExperiences);


        $this->hostExperiencesScheduledForDeletion = $hostExperiencesToDelete;

        foreach ($hostExperiencesToDelete as $hostExperienceRemoved) {
            $hostExperienceRemoved->setUserExperience(null);
        }

        $this->collHostExperiences = null;
        foreach ($hostExperiences as $hostExperience) {
            $this->addHostExperience($hostExperience);
        }

        $this->collHostExperiences = $hostExperiences;
        $this->collHostExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related HostExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related HostExperience objects.
     * @throws PropelException
     */
    public function countHostExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getHostExperiences());
            }

            $query = ChildHostExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserExperience($this)
                ->count($con);
        }

        return count($this->collHostExperiences);
    }

    /**
     * Method called to associate a ChildHostExperience object to this object
     * through the ChildHostExperience foreign key attribute.
     *
     * @param  ChildHostExperience $l ChildHostExperience
     * @return $this|\IiMedias\StreamBundle\Model\UserExperience The current object (for fluent API support)
     */
    public function addHostExperience(ChildHostExperience $l)
    {
        if ($this->collHostExperiences === null) {
            $this->initHostExperiences();
            $this->collHostExperiencesPartial = true;
        }

        if (!$this->collHostExperiences->contains($l)) {
            $this->doAddHostExperience($l);

            if ($this->hostExperiencesScheduledForDeletion and $this->hostExperiencesScheduledForDeletion->contains($l)) {
                $this->hostExperiencesScheduledForDeletion->remove($this->hostExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildHostExperience $hostExperience The ChildHostExperience object to add.
     */
    protected function doAddHostExperience(ChildHostExperience $hostExperience)
    {
        $this->collHostExperiences[]= $hostExperience;
        $hostExperience->setUserExperience($this);
    }

    /**
     * @param  ChildHostExperience $hostExperience The ChildHostExperience object to remove.
     * @return $this|ChildUserExperience The current object (for fluent API support)
     */
    public function removeHostExperience(ChildHostExperience $hostExperience)
    {
        if ($this->getHostExperiences()->contains($hostExperience)) {
            $pos = $this->collHostExperiences->search($hostExperience);
            $this->collHostExperiences->remove($pos);
            if (null === $this->hostExperiencesScheduledForDeletion) {
                $this->hostExperiencesScheduledForDeletion = clone $this->collHostExperiences;
                $this->hostExperiencesScheduledForDeletion->clear();
            }
            $this->hostExperiencesScheduledForDeletion[]= $hostExperience;
            $hostExperience->setUserExperience(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserExperience is new, it will return
     * an empty collection; or if this UserExperience has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserExperience.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStream) {
            $this->aStream->removeUserExperience($this);
        }
        if (null !== $this->aSite) {
            $this->aSite->removeUserExperience($this);
        }
        if (null !== $this->aChannel) {
            $this->aChannel->removeUserExperience($this);
        }
        if (null !== $this->aChatUser) {
            $this->aChatUser->removeUserExperience($this);
        }
        if (null !== $this->aRank) {
            $this->aRank->removeUserExperience($this);
        }
        if (null !== $this->aAvatar) {
            $this->aAvatar->removeUserExperience($this);
        }
        $this->stuexp_id = null;
        $this->stuexp_parent_id = null;
        $this->stuexp_ststrm_id = null;
        $this->stuexp_stchan_id = null;
        $this->stuexp_stsite_id = null;
        $this->stuexp_stcusr_id = null;
        $this->stuexp_chat_type = null;
        $this->stuexp_exp = null;
        $this->stuexp_total_exp = null;
        $this->stuexp_is_following = null;
        $this->stuexp_messages_count = null;
        $this->stuexp_follows_count = null;
        $this->stuexp_hosts_count = null;
        $this->stuexp_chatters_count = null;
        $this->stuexp_strank_id = null;
        $this->stuexp_stavtr_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMessageExperiences) {
                foreach ($this->collMessageExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChatterExperiences) {
                foreach ($this->collChatterExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFollowExperiences) {
                foreach ($this->collFollowExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collHostExperiences) {
                foreach ($this->collHostExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMessageExperiences = null;
        $this->collChatterExperiences = null;
        $this->collFollowExperiences = null;
        $this->collHostExperiences = null;
        $this->aStream = null;
        $this->aSite = null;
        $this->aChannel = null;
        $this->aChatUser = null;
        $this->aRank = null;
        $this->aAvatar = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserExperienceTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

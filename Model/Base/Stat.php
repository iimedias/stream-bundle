<?php

namespace IiMedias\StreamBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\Channel as ChildChannel;
use IiMedias\StreamBundle\Model\ChannelQuery as ChildChannelQuery;
use IiMedias\StreamBundle\Model\Site as ChildSite;
use IiMedias\StreamBundle\Model\SiteQuery as ChildSiteQuery;
use IiMedias\StreamBundle\Model\StatQuery as ChildStatQuery;
use IiMedias\StreamBundle\Model\Stream as ChildStream;
use IiMedias\StreamBundle\Model\StreamQuery as ChildStreamQuery;
use IiMedias\StreamBundle\Model\Map\StatTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'stream_stat_ststat' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class Stat implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\StatTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the ststat_id field.
     *
     * @var        int
     */
    protected $ststat_id;

    /**
     * The value for the ststat_ststrm_id field.
     *
     * @var        int
     */
    protected $ststat_ststrm_id;

    /**
     * The value for the ststat_stchan_id field.
     *
     * @var        int
     */
    protected $ststat_stchan_id;

    /**
     * The value for the ststat_stsite_id field.
     *
     * @var        int
     */
    protected $ststat_stsite_id;

    /**
     * The value for the ststat_duration field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_duration;

    /**
     * The value for the ststat_start_at field.
     *
     * @var        DateTime
     */
    protected $ststat_start_at;

    /**
     * The value for the ststat_end_at field.
     *
     * @var        DateTime
     */
    protected $ststat_end_at;

    /**
     * The value for the ststat_can_rescan field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $ststat_can_rescan;

    /**
     * The value for the ststat_stream_type field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_stream_type;

    /**
     * The value for the ststat_views field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_views;

    /**
     * The value for the ststat_maximum_viewers field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_maximum_viewers;

    /**
     * The value for the ststat_average_viewers field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_average_viewers;

    /**
     * The value for the ststat_chatters field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_chatters;

    /**
     * The value for the ststat_messages field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_messages;

    /**
     * The value for the ststat_hosts field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_hosts;

    /**
     * The value for the ststat_follows field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_follows;

    /**
     * The value for the ststat_unfollows field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ststat_unfollows;

    /**
     * @var        ChildStream
     */
    protected $aStream;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * @var        ChildChannel
     */
    protected $aChannel;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->ststat_duration = 0;
        $this->ststat_can_rescan = false;
        $this->ststat_stream_type = 0;
        $this->ststat_views = 0;
        $this->ststat_maximum_viewers = 0;
        $this->ststat_average_viewers = 0;
        $this->ststat_chatters = 0;
        $this->ststat_messages = 0;
        $this->ststat_hosts = 0;
        $this->ststat_follows = 0;
        $this->ststat_unfollows = 0;
    }

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\Stat object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Stat</code> instance.  If
     * <code>obj</code> is an instance of <code>Stat</code>, delegates to
     * <code>equals(Stat)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Stat The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [ststat_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->ststat_id;
    }

    /**
     * Get the [ststat_ststrm_id] column value.
     *
     * @return int
     */
    public function getStreamId()
    {
        return $this->ststat_ststrm_id;
    }

    /**
     * Get the [ststat_stchan_id] column value.
     *
     * @return int
     */
    public function getChannelId()
    {
        return $this->ststat_stchan_id;
    }

    /**
     * Get the [ststat_stsite_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->ststat_stsite_id;
    }

    /**
     * Get the [ststat_duration] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getDuration()
    {
        if (null === $this->ststat_duration) {
            return null;
        }
        $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_DURATION);
        if (!isset($valueSet[$this->ststat_duration])) {
            throw new PropelException('Unknown stored enum key: ' . $this->ststat_duration);
        }

        return $valueSet[$this->ststat_duration];
    }

    /**
     * Get the [optionally formatted] temporal [ststat_start_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartAt($format = NULL)
    {
        if ($format === null) {
            return $this->ststat_start_at;
        } else {
            return $this->ststat_start_at instanceof \DateTimeInterface ? $this->ststat_start_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [ststat_end_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndAt($format = NULL)
    {
        if ($format === null) {
            return $this->ststat_end_at;
        } else {
            return $this->ststat_end_at instanceof \DateTimeInterface ? $this->ststat_end_at->format($format) : null;
        }
    }

    /**
     * Get the [ststat_can_rescan] column value.
     *
     * @return boolean
     */
    public function getCanRescan()
    {
        return $this->ststat_can_rescan;
    }

    /**
     * Get the [ststat_can_rescan] column value.
     *
     * @return boolean
     */
    public function isCanRescan()
    {
        return $this->getCanRescan();
    }

    /**
     * Get the [ststat_stream_type] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getStreamType()
    {
        if (null === $this->ststat_stream_type) {
            return null;
        }
        $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_STREAM_TYPE);
        if (!isset($valueSet[$this->ststat_stream_type])) {
            throw new PropelException('Unknown stored enum key: ' . $this->ststat_stream_type);
        }

        return $valueSet[$this->ststat_stream_type];
    }

    /**
     * Get the [ststat_views] column value.
     *
     * @return int
     */
    public function getViews()
    {
        return $this->ststat_views;
    }

    /**
     * Get the [ststat_maximum_viewers] column value.
     *
     * @return int
     */
    public function getMaximumViewers()
    {
        return $this->ststat_maximum_viewers;
    }

    /**
     * Get the [ststat_average_viewers] column value.
     *
     * @return int
     */
    public function getAverageViewers()
    {
        return $this->ststat_average_viewers;
    }

    /**
     * Get the [ststat_chatters] column value.
     *
     * @return int
     */
    public function getChatters()
    {
        return $this->ststat_chatters;
    }

    /**
     * Get the [ststat_messages] column value.
     *
     * @return int
     */
    public function getMessages()
    {
        return $this->ststat_messages;
    }

    /**
     * Get the [ststat_hosts] column value.
     *
     * @return int
     */
    public function getHosts()
    {
        return $this->ststat_hosts;
    }

    /**
     * Get the [ststat_follows] column value.
     *
     * @return int
     */
    public function getFollows()
    {
        return $this->ststat_follows;
    }

    /**
     * Get the [ststat_unfollows] column value.
     *
     * @return int
     */
    public function getUnfollows()
    {
        return $this->ststat_unfollows;
    }

    /**
     * Set the value of [ststat_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_id !== $v) {
            $this->ststat_id = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [ststat_ststrm_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setStreamId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_ststrm_id !== $v) {
            $this->ststat_ststrm_id = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_STSTRM_ID] = true;
        }

        if ($this->aStream !== null && $this->aStream->getId() !== $v) {
            $this->aStream = null;
        }

        return $this;
    } // setStreamId()

    /**
     * Set the value of [ststat_stchan_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setChannelId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_stchan_id !== $v) {
            $this->ststat_stchan_id = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_STCHAN_ID] = true;
        }

        if ($this->aChannel !== null && $this->aChannel->getId() !== $v) {
            $this->aChannel = null;
        }

        return $this;
    } // setChannelId()

    /**
     * Set the value of [ststat_stsite_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_stsite_id !== $v) {
            $this->ststat_stsite_id = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_STSITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [ststat_duration] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setDuration($v)
    {
        if ($v !== null) {
            $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_DURATION);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->ststat_duration !== $v) {
            $this->ststat_duration = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_DURATION] = true;
        }

        return $this;
    } // setDuration()

    /**
     * Sets the value of [ststat_start_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setStartAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ststat_start_at !== null || $dt !== null) {
            if ($this->ststat_start_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->ststat_start_at->format("Y-m-d H:i:s.u")) {
                $this->ststat_start_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StatTableMap::COL_STSTAT_START_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setStartAt()

    /**
     * Sets the value of [ststat_end_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setEndAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ststat_end_at !== null || $dt !== null) {
            if ($this->ststat_end_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->ststat_end_at->format("Y-m-d H:i:s.u")) {
                $this->ststat_end_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StatTableMap::COL_STSTAT_END_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setEndAt()

    /**
     * Sets the value of the [ststat_can_rescan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setCanRescan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ststat_can_rescan !== $v) {
            $this->ststat_can_rescan = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_CAN_RESCAN] = true;
        }

        return $this;
    } // setCanRescan()

    /**
     * Set the value of [ststat_stream_type] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setStreamType($v)
    {
        if ($v !== null) {
            $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_STREAM_TYPE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->ststat_stream_type !== $v) {
            $this->ststat_stream_type = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_STREAM_TYPE] = true;
        }

        return $this;
    } // setStreamType()

    /**
     * Set the value of [ststat_views] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setViews($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_views !== $v) {
            $this->ststat_views = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_VIEWS] = true;
        }

        return $this;
    } // setViews()

    /**
     * Set the value of [ststat_maximum_viewers] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setMaximumViewers($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_maximum_viewers !== $v) {
            $this->ststat_maximum_viewers = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS] = true;
        }

        return $this;
    } // setMaximumViewers()

    /**
     * Set the value of [ststat_average_viewers] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setAverageViewers($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_average_viewers !== $v) {
            $this->ststat_average_viewers = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_AVERAGE_VIEWERS] = true;
        }

        return $this;
    } // setAverageViewers()

    /**
     * Set the value of [ststat_chatters] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setChatters($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_chatters !== $v) {
            $this->ststat_chatters = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_CHATTERS] = true;
        }

        return $this;
    } // setChatters()

    /**
     * Set the value of [ststat_messages] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setMessages($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_messages !== $v) {
            $this->ststat_messages = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_MESSAGES] = true;
        }

        return $this;
    } // setMessages()

    /**
     * Set the value of [ststat_hosts] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setHosts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_hosts !== $v) {
            $this->ststat_hosts = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_HOSTS] = true;
        }

        return $this;
    } // setHosts()

    /**
     * Set the value of [ststat_follows] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setFollows($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_follows !== $v) {
            $this->ststat_follows = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_FOLLOWS] = true;
        }

        return $this;
    } // setFollows()

    /**
     * Set the value of [ststat_unfollows] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     */
    public function setUnfollows($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ststat_unfollows !== $v) {
            $this->ststat_unfollows = $v;
            $this->modifiedColumns[StatTableMap::COL_STSTAT_UNFOLLOWS] = true;
        }

        return $this;
    } // setUnfollows()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ststat_duration !== 0) {
                return false;
            }

            if ($this->ststat_can_rescan !== false) {
                return false;
            }

            if ($this->ststat_stream_type !== 0) {
                return false;
            }

            if ($this->ststat_views !== 0) {
                return false;
            }

            if ($this->ststat_maximum_viewers !== 0) {
                return false;
            }

            if ($this->ststat_average_viewers !== 0) {
                return false;
            }

            if ($this->ststat_chatters !== 0) {
                return false;
            }

            if ($this->ststat_messages !== 0) {
                return false;
            }

            if ($this->ststat_hosts !== 0) {
                return false;
            }

            if ($this->ststat_follows !== 0) {
                return false;
            }

            if ($this->ststat_unfollows !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : StatTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : StatTableMap::translateFieldName('StreamId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_ststrm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : StatTableMap::translateFieldName('ChannelId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_stchan_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : StatTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_stsite_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : StatTableMap::translateFieldName('Duration', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_duration = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : StatTableMap::translateFieldName('StartAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->ststat_start_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : StatTableMap::translateFieldName('EndAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->ststat_end_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : StatTableMap::translateFieldName('CanRescan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_can_rescan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : StatTableMap::translateFieldName('StreamType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_stream_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : StatTableMap::translateFieldName('Views', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_views = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : StatTableMap::translateFieldName('MaximumViewers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_maximum_viewers = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : StatTableMap::translateFieldName('AverageViewers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_average_viewers = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : StatTableMap::translateFieldName('Chatters', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_chatters = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : StatTableMap::translateFieldName('Messages', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_messages = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : StatTableMap::translateFieldName('Hosts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_hosts = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : StatTableMap::translateFieldName('Follows', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_follows = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : StatTableMap::translateFieldName('Unfollows', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ststat_unfollows = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 17; // 17 = StatTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\Stat'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStream !== null && $this->ststat_ststrm_id !== $this->aStream->getId()) {
            $this->aStream = null;
        }
        if ($this->aChannel !== null && $this->ststat_stchan_id !== $this->aChannel->getId()) {
            $this->aChannel = null;
        }
        if ($this->aSite !== null && $this->ststat_stsite_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StatTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildStatQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStream = null;
            $this->aSite = null;
            $this->aChannel = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Stat::setDeleted()
     * @see Stat::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildStatQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StatTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStream !== null) {
                if ($this->aStream->isModified() || $this->aStream->isNew()) {
                    $affectedRows += $this->aStream->save($con);
                }
                $this->setStream($this->aStream);
            }

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aChannel !== null) {
                if ($this->aChannel->isModified() || $this->aChannel->isNew()) {
                    $affectedRows += $this->aChannel->save($con);
                }
                $this->setChannel($this->aChannel);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[StatTableMap::COL_STSTAT_ID] = true;
        if (null !== $this->ststat_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . StatTableMap::COL_STSTAT_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_id';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STSTRM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_ststrm_id';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STCHAN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_stchan_id';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STSITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_stsite_id';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_DURATION)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_duration';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_START_AT)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_start_at';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_END_AT)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_end_at';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_CAN_RESCAN)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_can_rescan';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STREAM_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_stream_type';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_VIEWS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_views';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_maximum_viewers';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_average_viewers';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_CHATTERS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_chatters';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_MESSAGES)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_messages';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_HOSTS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_hosts';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_FOLLOWS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_follows';
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_UNFOLLOWS)) {
            $modifiedColumns[':p' . $index++]  = 'ststat_unfollows';
        }

        $sql = sprintf(
            'INSERT INTO stream_stat_ststat (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ststat_id':
                        $stmt->bindValue($identifier, $this->ststat_id, PDO::PARAM_INT);
                        break;
                    case 'ststat_ststrm_id':
                        $stmt->bindValue($identifier, $this->ststat_ststrm_id, PDO::PARAM_INT);
                        break;
                    case 'ststat_stchan_id':
                        $stmt->bindValue($identifier, $this->ststat_stchan_id, PDO::PARAM_INT);
                        break;
                    case 'ststat_stsite_id':
                        $stmt->bindValue($identifier, $this->ststat_stsite_id, PDO::PARAM_INT);
                        break;
                    case 'ststat_duration':
                        $stmt->bindValue($identifier, $this->ststat_duration, PDO::PARAM_INT);
                        break;
                    case 'ststat_start_at':
                        $stmt->bindValue($identifier, $this->ststat_start_at ? $this->ststat_start_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ststat_end_at':
                        $stmt->bindValue($identifier, $this->ststat_end_at ? $this->ststat_end_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ststat_can_rescan':
                        $stmt->bindValue($identifier, (int) $this->ststat_can_rescan, PDO::PARAM_INT);
                        break;
                    case 'ststat_stream_type':
                        $stmt->bindValue($identifier, $this->ststat_stream_type, PDO::PARAM_INT);
                        break;
                    case 'ststat_views':
                        $stmt->bindValue($identifier, $this->ststat_views, PDO::PARAM_INT);
                        break;
                    case 'ststat_maximum_viewers':
                        $stmt->bindValue($identifier, $this->ststat_maximum_viewers, PDO::PARAM_INT);
                        break;
                    case 'ststat_average_viewers':
                        $stmt->bindValue($identifier, $this->ststat_average_viewers, PDO::PARAM_INT);
                        break;
                    case 'ststat_chatters':
                        $stmt->bindValue($identifier, $this->ststat_chatters, PDO::PARAM_INT);
                        break;
                    case 'ststat_messages':
                        $stmt->bindValue($identifier, $this->ststat_messages, PDO::PARAM_INT);
                        break;
                    case 'ststat_hosts':
                        $stmt->bindValue($identifier, $this->ststat_hosts, PDO::PARAM_INT);
                        break;
                    case 'ststat_follows':
                        $stmt->bindValue($identifier, $this->ststat_follows, PDO::PARAM_INT);
                        break;
                    case 'ststat_unfollows':
                        $stmt->bindValue($identifier, $this->ststat_unfollows, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StatTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getStreamId();
                break;
            case 2:
                return $this->getChannelId();
                break;
            case 3:
                return $this->getSiteId();
                break;
            case 4:
                return $this->getDuration();
                break;
            case 5:
                return $this->getStartAt();
                break;
            case 6:
                return $this->getEndAt();
                break;
            case 7:
                return $this->getCanRescan();
                break;
            case 8:
                return $this->getStreamType();
                break;
            case 9:
                return $this->getViews();
                break;
            case 10:
                return $this->getMaximumViewers();
                break;
            case 11:
                return $this->getAverageViewers();
                break;
            case 12:
                return $this->getChatters();
                break;
            case 13:
                return $this->getMessages();
                break;
            case 14:
                return $this->getHosts();
                break;
            case 15:
                return $this->getFollows();
                break;
            case 16:
                return $this->getUnfollows();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Stat'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Stat'][$this->hashCode()] = true;
        $keys = StatTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getStreamId(),
            $keys[2] => $this->getChannelId(),
            $keys[3] => $this->getSiteId(),
            $keys[4] => $this->getDuration(),
            $keys[5] => $this->getStartAt(),
            $keys[6] => $this->getEndAt(),
            $keys[7] => $this->getCanRescan(),
            $keys[8] => $this->getStreamType(),
            $keys[9] => $this->getViews(),
            $keys[10] => $this->getMaximumViewers(),
            $keys[11] => $this->getAverageViewers(),
            $keys[12] => $this->getChatters(),
            $keys[13] => $this->getMessages(),
            $keys[14] => $this->getHosts(),
            $keys[15] => $this->getFollows(),
            $keys[16] => $this->getUnfollows(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStream) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stream';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrm';
                        break;
                    default:
                        $key = 'Stream';
                }

                $result[$key] = $this->aStream->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsite';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aChannel) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'channel';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_channel_stchan';
                        break;
                    default:
                        $key = 'Channel';
                }

                $result[$key] = $this->aChannel->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\Stat
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StatTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\Stat
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setStreamId($value);
                break;
            case 2:
                $this->setChannelId($value);
                break;
            case 3:
                $this->setSiteId($value);
                break;
            case 4:
                $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_DURATION);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setDuration($value);
                break;
            case 5:
                $this->setStartAt($value);
                break;
            case 6:
                $this->setEndAt($value);
                break;
            case 7:
                $this->setCanRescan($value);
                break;
            case 8:
                $valueSet = StatTableMap::getValueSet(StatTableMap::COL_STSTAT_STREAM_TYPE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setStreamType($value);
                break;
            case 9:
                $this->setViews($value);
                break;
            case 10:
                $this->setMaximumViewers($value);
                break;
            case 11:
                $this->setAverageViewers($value);
                break;
            case 12:
                $this->setChatters($value);
                break;
            case 13:
                $this->setMessages($value);
                break;
            case 14:
                $this->setHosts($value);
                break;
            case 15:
                $this->setFollows($value);
                break;
            case 16:
                $this->setUnfollows($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = StatTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setStreamId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setChannelId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSiteId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDuration($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setStartAt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setEndAt($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCanRescan($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setStreamType($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setViews($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setMaximumViewers($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setAverageViewers($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setChatters($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setMessages($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setHosts($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setFollows($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setUnfollows($arr[$keys[16]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StatTableMap::DATABASE_NAME);

        if ($this->isColumnModified(StatTableMap::COL_STSTAT_ID)) {
            $criteria->add(StatTableMap::COL_STSTAT_ID, $this->ststat_id);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STSTRM_ID)) {
            $criteria->add(StatTableMap::COL_STSTAT_STSTRM_ID, $this->ststat_ststrm_id);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STCHAN_ID)) {
            $criteria->add(StatTableMap::COL_STSTAT_STCHAN_ID, $this->ststat_stchan_id);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STSITE_ID)) {
            $criteria->add(StatTableMap::COL_STSTAT_STSITE_ID, $this->ststat_stsite_id);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_DURATION)) {
            $criteria->add(StatTableMap::COL_STSTAT_DURATION, $this->ststat_duration);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_START_AT)) {
            $criteria->add(StatTableMap::COL_STSTAT_START_AT, $this->ststat_start_at);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_END_AT)) {
            $criteria->add(StatTableMap::COL_STSTAT_END_AT, $this->ststat_end_at);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_CAN_RESCAN)) {
            $criteria->add(StatTableMap::COL_STSTAT_CAN_RESCAN, $this->ststat_can_rescan);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_STREAM_TYPE)) {
            $criteria->add(StatTableMap::COL_STSTAT_STREAM_TYPE, $this->ststat_stream_type);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_VIEWS)) {
            $criteria->add(StatTableMap::COL_STSTAT_VIEWS, $this->ststat_views);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS)) {
            $criteria->add(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS, $this->ststat_maximum_viewers);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS)) {
            $criteria->add(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS, $this->ststat_average_viewers);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_CHATTERS)) {
            $criteria->add(StatTableMap::COL_STSTAT_CHATTERS, $this->ststat_chatters);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_MESSAGES)) {
            $criteria->add(StatTableMap::COL_STSTAT_MESSAGES, $this->ststat_messages);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_HOSTS)) {
            $criteria->add(StatTableMap::COL_STSTAT_HOSTS, $this->ststat_hosts);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_FOLLOWS)) {
            $criteria->add(StatTableMap::COL_STSTAT_FOLLOWS, $this->ststat_follows);
        }
        if ($this->isColumnModified(StatTableMap::COL_STSTAT_UNFOLLOWS)) {
            $criteria->add(StatTableMap::COL_STSTAT_UNFOLLOWS, $this->ststat_unfollows);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildStatQuery::create();
        $criteria->add(StatTableMap::COL_STSTAT_ID, $this->ststat_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (ststat_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\Stat (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setStreamId($this->getStreamId());
        $copyObj->setChannelId($this->getChannelId());
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setDuration($this->getDuration());
        $copyObj->setStartAt($this->getStartAt());
        $copyObj->setEndAt($this->getEndAt());
        $copyObj->setCanRescan($this->getCanRescan());
        $copyObj->setStreamType($this->getStreamType());
        $copyObj->setViews($this->getViews());
        $copyObj->setMaximumViewers($this->getMaximumViewers());
        $copyObj->setAverageViewers($this->getAverageViewers());
        $copyObj->setChatters($this->getChatters());
        $copyObj->setMessages($this->getMessages());
        $copyObj->setHosts($this->getHosts());
        $copyObj->setFollows($this->getFollows());
        $copyObj->setUnfollows($this->getUnfollows());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\Stat Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStream object.
     *
     * @param  ChildStream $v
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStream(ChildStream $v = null)
    {
        if ($v === null) {
            $this->setStreamId(NULL);
        } else {
            $this->setStreamId($v->getId());
        }

        $this->aStream = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStream object, it will not be re-added.
        if ($v !== null) {
            $v->addStat($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStream object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStream The associated ChildStream object.
     * @throws PropelException
     */
    public function getStream(ConnectionInterface $con = null)
    {
        if ($this->aStream === null && ($this->ststat_ststrm_id != 0)) {
            $this->aStream = ChildStreamQuery::create()->findPk($this->ststat_ststrm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStream->addStats($this);
             */
        }

        return $this->aStream;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addStat($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->ststat_stsite_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->ststat_stsite_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addStats($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a ChildChannel object.
     *
     * @param  ChildChannel $v
     * @return $this|\IiMedias\StreamBundle\Model\Stat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChannel(ChildChannel $v = null)
    {
        if ($v === null) {
            $this->setChannelId(NULL);
        } else {
            $this->setChannelId($v->getId());
        }

        $this->aChannel = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildChannel object, it will not be re-added.
        if ($v !== null) {
            $v->addStat($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildChannel object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildChannel The associated ChildChannel object.
     * @throws PropelException
     */
    public function getChannel(ConnectionInterface $con = null)
    {
        if ($this->aChannel === null && ($this->ststat_stchan_id != 0)) {
            $this->aChannel = ChildChannelQuery::create()->findPk($this->ststat_stchan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChannel->addStats($this);
             */
        }

        return $this->aChannel;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStream) {
            $this->aStream->removeStat($this);
        }
        if (null !== $this->aSite) {
            $this->aSite->removeStat($this);
        }
        if (null !== $this->aChannel) {
            $this->aChannel->removeStat($this);
        }
        $this->ststat_id = null;
        $this->ststat_ststrm_id = null;
        $this->ststat_stchan_id = null;
        $this->ststat_stsite_id = null;
        $this->ststat_duration = null;
        $this->ststat_start_at = null;
        $this->ststat_end_at = null;
        $this->ststat_can_rescan = null;
        $this->ststat_stream_type = null;
        $this->ststat_views = null;
        $this->ststat_maximum_viewers = null;
        $this->ststat_average_viewers = null;
        $this->ststat_chatters = null;
        $this->ststat_messages = null;
        $this->ststat_hosts = null;
        $this->ststat_follows = null;
        $this->ststat_unfollows = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aStream = null;
        $this->aSite = null;
        $this->aChannel = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StatTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

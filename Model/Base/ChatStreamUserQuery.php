<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\ChatStreamUser as ChildChatStreamUser;
use IiMedias\StreamBundle\Model\ChatStreamUserQuery as ChildChatStreamUserQuery;
use IiMedias\StreamBundle\Model\Map\ChatStreamUserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_chat_stream_user_stcsus' table.
 *
 *
 *
 * @method     ChildChatStreamUserQuery orderById($order = Criteria::ASC) Order by the stcsus_id column
 * @method     ChildChatStreamUserQuery orderByParentId($order = Criteria::ASC) Order by the stcsus_parent_id column
 * @method     ChildChatStreamUserQuery orderByStreamId($order = Criteria::ASC) Order by the stcsus_ststrm_id column
 * @method     ChildChatStreamUserQuery orderByChannelId($order = Criteria::ASC) Order by the stcsus_stchan_id column
 * @method     ChildChatStreamUserQuery orderBySiteId($order = Criteria::ASC) Order by the stcsus_stsite_id column
 * @method     ChildChatStreamUserQuery orderByChatUserId($order = Criteria::ASC) Order by the stcsus_stcusr_id column
 * @method     ChildChatStreamUserQuery orderByExp($order = Criteria::ASC) Order by the stcsus_exp column
 * @method     ChildChatStreamUserQuery orderByRankId($order = Criteria::ASC) Order by the stcsus_strank_id column
 * @method     ChildChatStreamUserQuery orderByAvatarId($order = Criteria::ASC) Order by the stcsus_stavtr_id column
 *
 * @method     ChildChatStreamUserQuery groupById() Group by the stcsus_id column
 * @method     ChildChatStreamUserQuery groupByParentId() Group by the stcsus_parent_id column
 * @method     ChildChatStreamUserQuery groupByStreamId() Group by the stcsus_ststrm_id column
 * @method     ChildChatStreamUserQuery groupByChannelId() Group by the stcsus_stchan_id column
 * @method     ChildChatStreamUserQuery groupBySiteId() Group by the stcsus_stsite_id column
 * @method     ChildChatStreamUserQuery groupByChatUserId() Group by the stcsus_stcusr_id column
 * @method     ChildChatStreamUserQuery groupByExp() Group by the stcsus_exp column
 * @method     ChildChatStreamUserQuery groupByRankId() Group by the stcsus_strank_id column
 * @method     ChildChatStreamUserQuery groupByAvatarId() Group by the stcsus_stavtr_id column
 *
 * @method     ChildChatStreamUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatStreamUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatStreamUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatStreamUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatStreamUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatStreamUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatStreamUserQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildChatStreamUserQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildChatStreamUserQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildChatStreamUserQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildChatStreamUserQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildChatStreamUserQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildChatStreamUserQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildChatStreamUserQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildChatStreamUserQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildChatStreamUserQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildChatStreamUserQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildChatStreamUserQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildChatStreamUserQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildChatStreamUserQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildChatStreamUserQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildChatStreamUserQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildChatStreamUserQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildChatStreamUserQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildChatStreamUserQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildChatStreamUserQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildChatStreamUserQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildChatStreamUserQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChatStreamUserQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildChatStreamUserQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     ChildChatStreamUserQuery leftJoinRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rank relation
 * @method     ChildChatStreamUserQuery rightJoinRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rank relation
 * @method     ChildChatStreamUserQuery innerJoinRank($relationAlias = null) Adds a INNER JOIN clause to the query using the Rank relation
 *
 * @method     ChildChatStreamUserQuery joinWithRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rank relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithRank() Adds a LEFT JOIN clause and with to the query using the Rank relation
 * @method     ChildChatStreamUserQuery rightJoinWithRank() Adds a RIGHT JOIN clause and with to the query using the Rank relation
 * @method     ChildChatStreamUserQuery innerJoinWithRank() Adds a INNER JOIN clause and with to the query using the Rank relation
 *
 * @method     ChildChatStreamUserQuery leftJoinAvatar($relationAlias = null) Adds a LEFT JOIN clause to the query using the Avatar relation
 * @method     ChildChatStreamUserQuery rightJoinAvatar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Avatar relation
 * @method     ChildChatStreamUserQuery innerJoinAvatar($relationAlias = null) Adds a INNER JOIN clause to the query using the Avatar relation
 *
 * @method     ChildChatStreamUserQuery joinWithAvatar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Avatar relation
 *
 * @method     ChildChatStreamUserQuery leftJoinWithAvatar() Adds a LEFT JOIN clause and with to the query using the Avatar relation
 * @method     ChildChatStreamUserQuery rightJoinWithAvatar() Adds a RIGHT JOIN clause and with to the query using the Avatar relation
 * @method     ChildChatStreamUserQuery innerJoinWithAvatar() Adds a INNER JOIN clause and with to the query using the Avatar relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\RankQuery|\IiMedias\StreamBundle\Model\AvatarQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChatStreamUser findOne(ConnectionInterface $con = null) Return the first ChildChatStreamUser matching the query
 * @method     ChildChatStreamUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatStreamUser matching the query, or a new ChildChatStreamUser object populated from the query conditions when no match is found
 *
 * @method     ChildChatStreamUser findOneById(int $stcsus_id) Return the first ChildChatStreamUser filtered by the stcsus_id column
 * @method     ChildChatStreamUser findOneByParentId(int $stcsus_parent_id) Return the first ChildChatStreamUser filtered by the stcsus_parent_id column
 * @method     ChildChatStreamUser findOneByStreamId(int $stcsus_ststrm_id) Return the first ChildChatStreamUser filtered by the stcsus_ststrm_id column
 * @method     ChildChatStreamUser findOneByChannelId(int $stcsus_stchan_id) Return the first ChildChatStreamUser filtered by the stcsus_stchan_id column
 * @method     ChildChatStreamUser findOneBySiteId(int $stcsus_stsite_id) Return the first ChildChatStreamUser filtered by the stcsus_stsite_id column
 * @method     ChildChatStreamUser findOneByChatUserId(int $stcsus_stcusr_id) Return the first ChildChatStreamUser filtered by the stcsus_stcusr_id column
 * @method     ChildChatStreamUser findOneByExp(int $stcsus_exp) Return the first ChildChatStreamUser filtered by the stcsus_exp column
 * @method     ChildChatStreamUser findOneByRankId(int $stcsus_strank_id) Return the first ChildChatStreamUser filtered by the stcsus_strank_id column
 * @method     ChildChatStreamUser findOneByAvatarId(int $stcsus_stavtr_id) Return the first ChildChatStreamUser filtered by the stcsus_stavtr_id column *

 * @method     ChildChatStreamUser requirePk($key, ConnectionInterface $con = null) Return the ChildChatStreamUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOne(ConnectionInterface $con = null) Return the first ChildChatStreamUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatStreamUser requireOneById(int $stcsus_id) Return the first ChildChatStreamUser filtered by the stcsus_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByParentId(int $stcsus_parent_id) Return the first ChildChatStreamUser filtered by the stcsus_parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByStreamId(int $stcsus_ststrm_id) Return the first ChildChatStreamUser filtered by the stcsus_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByChannelId(int $stcsus_stchan_id) Return the first ChildChatStreamUser filtered by the stcsus_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneBySiteId(int $stcsus_stsite_id) Return the first ChildChatStreamUser filtered by the stcsus_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByChatUserId(int $stcsus_stcusr_id) Return the first ChildChatStreamUser filtered by the stcsus_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByExp(int $stcsus_exp) Return the first ChildChatStreamUser filtered by the stcsus_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByRankId(int $stcsus_strank_id) Return the first ChildChatStreamUser filtered by the stcsus_strank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatStreamUser requireOneByAvatarId(int $stcsus_stavtr_id) Return the first ChildChatStreamUser filtered by the stcsus_stavtr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatStreamUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatStreamUser objects based on current ModelCriteria
 * @method     ChildChatStreamUser[]|ObjectCollection findById(int $stcsus_id) Return ChildChatStreamUser objects filtered by the stcsus_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByParentId(int $stcsus_parent_id) Return ChildChatStreamUser objects filtered by the stcsus_parent_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByStreamId(int $stcsus_ststrm_id) Return ChildChatStreamUser objects filtered by the stcsus_ststrm_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByChannelId(int $stcsus_stchan_id) Return ChildChatStreamUser objects filtered by the stcsus_stchan_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findBySiteId(int $stcsus_stsite_id) Return ChildChatStreamUser objects filtered by the stcsus_stsite_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByChatUserId(int $stcsus_stcusr_id) Return ChildChatStreamUser objects filtered by the stcsus_stcusr_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByExp(int $stcsus_exp) Return ChildChatStreamUser objects filtered by the stcsus_exp column
 * @method     ChildChatStreamUser[]|ObjectCollection findByRankId(int $stcsus_strank_id) Return ChildChatStreamUser objects filtered by the stcsus_strank_id column
 * @method     ChildChatStreamUser[]|ObjectCollection findByAvatarId(int $stcsus_stavtr_id) Return ChildChatStreamUser objects filtered by the stcsus_stavtr_id column
 * @method     ChildChatStreamUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatStreamUserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ChatStreamUserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\ChatStreamUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatStreamUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatStreamUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatStreamUserQuery) {
            return $criteria;
        }
        $query = new ChildChatStreamUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatStreamUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatStreamUserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChatStreamUserTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stcsus_id, stcsus_parent_id, stcsus_ststrm_id, stcsus_stchan_id, stcsus_stsite_id, stcsus_stcusr_id, stcsus_exp, stcsus_strank_id, stcsus_stavtr_id FROM stream_chat_stream_user_stcsus WHERE stcsus_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChatStreamUser $obj */
            $obj = new ChildChatStreamUser();
            $obj->hydrate($row);
            ChatStreamUserTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChatStreamUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stcsus_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stcsus_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stcsus_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stcsus_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stcsus_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE stcsus_parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE stcsus_parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE stcsus_parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the stcsus_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stcsus_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stcsus_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stcsus_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stcsus_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stcsus_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stcsus_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stcsus_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stcsus_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stcsus_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stcsus_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stcsus_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stcsus_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stcsus_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stcsus_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stcsus_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stcsus_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByExp(1234); // WHERE stcsus_exp = 1234
     * $query->filterByExp(array(12, 34)); // WHERE stcsus_exp IN (12, 34)
     * $query->filterByExp(array('min' => 12)); // WHERE stcsus_exp > 12
     * </code>
     *
     * @param     mixed $exp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByExp($exp = null, $comparison = null)
    {
        if (is_array($exp)) {
            $useMinMax = false;
            if (isset($exp['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_EXP, $exp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exp['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_EXP, $exp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_EXP, $exp, $comparison);
    }

    /**
     * Filter the query on the stcsus_strank_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRankId(1234); // WHERE stcsus_strank_id = 1234
     * $query->filterByRankId(array(12, 34)); // WHERE stcsus_strank_id IN (12, 34)
     * $query->filterByRankId(array('min' => 12)); // WHERE stcsus_strank_id > 12
     * </code>
     *
     * @see       filterByRank()
     *
     * @param     mixed $rankId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByRankId($rankId = null, $comparison = null)
    {
        if (is_array($rankId)) {
            $useMinMax = false;
            if (isset($rankId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, $rankId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rankId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, $rankId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, $rankId, $comparison);
    }

    /**
     * Filter the query on the stcsus_stavtr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAvatarId(1234); // WHERE stcsus_stavtr_id = 1234
     * $query->filterByAvatarId(array(12, 34)); // WHERE stcsus_stavtr_id IN (12, 34)
     * $query->filterByAvatarId(array('min' => 12)); // WHERE stcsus_stavtr_id > 12
     * </code>
     *
     * @see       filterByAvatar()
     *
     * @param     mixed $avatarId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByAvatarId($avatarId = null, $comparison = null)
    {
        if (is_array($avatarId)) {
            $useMinMax = false;
            if (isset($avatarId['min'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, $avatarId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($avatarId['max'])) {
                $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, $avatarId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, $avatarId, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByRank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, $rank->getId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, $rank->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rank');
        }

        return $this;
    }

    /**
     * Use the Rank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function filterByAvatar($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, $avatar->getId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, $avatar->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAvatar() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Avatar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function joinAvatar($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Avatar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Avatar');
        }

        return $this;
    }

    /**
     * Use the Avatar relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useAvatarQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvatar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Avatar', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatStreamUser $chatStreamUser Object to remove from the list of results
     *
     * @return $this|ChildChatStreamUserQuery The current query, for fluid interface
     */
    public function prune($chatStreamUser = null)
    {
        if ($chatStreamUser) {
            $this->addUsingAlias(ChatStreamUserTableMap::COL_STCSUS_ID, $chatStreamUser->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_chat_stream_user_stcsus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatStreamUserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatStreamUserTableMap::clearInstancePool();
            ChatStreamUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatStreamUserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatStreamUserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatStreamUserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatStreamUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChatStreamUserQuery

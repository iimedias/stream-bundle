<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\Site as ChildSite;
use IiMedias\StreamBundle\Model\SiteQuery as ChildSiteQuery;
use IiMedias\StreamBundle\Model\Map\SiteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_site_stsite' table.
 *
 *
 *
 * @method     ChildSiteQuery orderById($order = Criteria::ASC) Order by the stsite_id column
 * @method     ChildSiteQuery orderByName($order = Criteria::ASC) Order by the stsite_name column
 * @method     ChildSiteQuery orderByCode($order = Criteria::ASC) Order by the stsite_code column
 * @method     ChildSiteQuery orderByFontAwesomeIcon($order = Criteria::ASC) Order by the stsite_font_awesome_icon column
 * @method     ChildSiteQuery orderByColorHexa($order = Criteria::ASC) Order by the stsite_color_hexa column
 * @method     ChildSiteQuery orderByColorRgb($order = Criteria::ASC) Order by the stsite_color_rgb column
 * @method     ChildSiteQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the stsite_created_by_user_id column
 * @method     ChildSiteQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the stsite_updated_by_user_id column
 * @method     ChildSiteQuery orderByCreatedAt($order = Criteria::ASC) Order by the stsite_created_at column
 * @method     ChildSiteQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stsite_updated_at column
 *
 * @method     ChildSiteQuery groupById() Group by the stsite_id column
 * @method     ChildSiteQuery groupByName() Group by the stsite_name column
 * @method     ChildSiteQuery groupByCode() Group by the stsite_code column
 * @method     ChildSiteQuery groupByFontAwesomeIcon() Group by the stsite_font_awesome_icon column
 * @method     ChildSiteQuery groupByColorHexa() Group by the stsite_color_hexa column
 * @method     ChildSiteQuery groupByColorRgb() Group by the stsite_color_rgb column
 * @method     ChildSiteQuery groupByCreatedByUserId() Group by the stsite_created_by_user_id column
 * @method     ChildSiteQuery groupByUpdatedByUserId() Group by the stsite_updated_by_user_id column
 * @method     ChildSiteQuery groupByCreatedAt() Group by the stsite_created_at column
 * @method     ChildSiteQuery groupByUpdatedAt() Group by the stsite_updated_at column
 *
 * @method     ChildSiteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildSiteQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildSiteQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildSiteQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildSiteQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildSiteQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildSiteQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildSiteQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildSiteQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildSiteQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildSiteQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildSiteQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildSiteQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildSiteQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildSiteQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildSiteQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildSiteQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildSiteQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildSiteQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildSiteQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildSiteQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildSiteQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildSiteQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildSiteQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildSiteQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildSiteQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildSiteQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildSiteQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     ChildSiteQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildSiteQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildSiteQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildSiteQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildSiteQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildSiteQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     ChildSiteQuery leftJoinDeepBotImportExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildSiteQuery rightJoinDeepBotImportExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeepBotImportExperience relation
 * @method     ChildSiteQuery innerJoinDeepBotImportExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the DeepBotImportExperience relation
 *
 * @method     ChildSiteQuery joinWithDeepBotImportExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithDeepBotImportExperience() Adds a LEFT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildSiteQuery rightJoinWithDeepBotImportExperience() Adds a RIGHT JOIN clause and with to the query using the DeepBotImportExperience relation
 * @method     ChildSiteQuery innerJoinWithDeepBotImportExperience() Adds a INNER JOIN clause and with to the query using the DeepBotImportExperience relation
 *
 * @method     ChildSiteQuery leftJoinMessageExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageExperience relation
 * @method     ChildSiteQuery rightJoinMessageExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageExperience relation
 * @method     ChildSiteQuery innerJoinMessageExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageExperience relation
 *
 * @method     ChildSiteQuery joinWithMessageExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithMessageExperience() Adds a LEFT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildSiteQuery rightJoinWithMessageExperience() Adds a RIGHT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildSiteQuery innerJoinWithMessageExperience() Adds a INNER JOIN clause and with to the query using the MessageExperience relation
 *
 * @method     ChildSiteQuery leftJoinChatterExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildSiteQuery rightJoinChatterExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildSiteQuery innerJoinChatterExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatterExperience relation
 *
 * @method     ChildSiteQuery joinWithChatterExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithChatterExperience() Adds a LEFT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildSiteQuery rightJoinWithChatterExperience() Adds a RIGHT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildSiteQuery innerJoinWithChatterExperience() Adds a INNER JOIN clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildSiteQuery leftJoinFollowExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowExperience relation
 * @method     ChildSiteQuery rightJoinFollowExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowExperience relation
 * @method     ChildSiteQuery innerJoinFollowExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowExperience relation
 *
 * @method     ChildSiteQuery joinWithFollowExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithFollowExperience() Adds a LEFT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildSiteQuery rightJoinWithFollowExperience() Adds a RIGHT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildSiteQuery innerJoinWithFollowExperience() Adds a INNER JOIN clause and with to the query using the FollowExperience relation
 *
 * @method     ChildSiteQuery leftJoinHostExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the HostExperience relation
 * @method     ChildSiteQuery rightJoinHostExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HostExperience relation
 * @method     ChildSiteQuery innerJoinHostExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the HostExperience relation
 *
 * @method     ChildSiteQuery joinWithHostExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HostExperience relation
 *
 * @method     ChildSiteQuery leftJoinWithHostExperience() Adds a LEFT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildSiteQuery rightJoinWithHostExperience() Adds a RIGHT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildSiteQuery innerJoinWithHostExperience() Adds a INNER JOIN clause and with to the query using the HostExperience relation
 *
 * @method     ChildSiteQuery leftJoinViewDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildSiteQuery rightJoinViewDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewDiffData relation
 * @method     ChildSiteQuery innerJoinViewDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewDiffData relation
 *
 * @method     ChildSiteQuery joinWithViewDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildSiteQuery leftJoinWithViewDiffData() Adds a LEFT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildSiteQuery rightJoinWithViewDiffData() Adds a RIGHT JOIN clause and with to the query using the ViewDiffData relation
 * @method     ChildSiteQuery innerJoinWithViewDiffData() Adds a INNER JOIN clause and with to the query using the ViewDiffData relation
 *
 * @method     ChildSiteQuery leftJoinViewerData($relationAlias = null) Adds a LEFT JOIN clause to the query using the ViewerData relation
 * @method     ChildSiteQuery rightJoinViewerData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ViewerData relation
 * @method     ChildSiteQuery innerJoinViewerData($relationAlias = null) Adds a INNER JOIN clause to the query using the ViewerData relation
 *
 * @method     ChildSiteQuery joinWithViewerData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ViewerData relation
 *
 * @method     ChildSiteQuery leftJoinWithViewerData() Adds a LEFT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildSiteQuery rightJoinWithViewerData() Adds a RIGHT JOIN clause and with to the query using the ViewerData relation
 * @method     ChildSiteQuery innerJoinWithViewerData() Adds a INNER JOIN clause and with to the query using the ViewerData relation
 *
 * @method     ChildSiteQuery leftJoinFollowDiffData($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildSiteQuery rightJoinFollowDiffData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowDiffData relation
 * @method     ChildSiteQuery innerJoinFollowDiffData($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowDiffData relation
 *
 * @method     ChildSiteQuery joinWithFollowDiffData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildSiteQuery leftJoinWithFollowDiffData() Adds a LEFT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildSiteQuery rightJoinWithFollowDiffData() Adds a RIGHT JOIN clause and with to the query using the FollowDiffData relation
 * @method     ChildSiteQuery innerJoinWithFollowDiffData() Adds a INNER JOIN clause and with to the query using the FollowDiffData relation
 *
 * @method     ChildSiteQuery leftJoinStatusData($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusData relation
 * @method     ChildSiteQuery rightJoinStatusData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusData relation
 * @method     ChildSiteQuery innerJoinStatusData($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusData relation
 *
 * @method     ChildSiteQuery joinWithStatusData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StatusData relation
 *
 * @method     ChildSiteQuery leftJoinWithStatusData() Adds a LEFT JOIN clause and with to the query using the StatusData relation
 * @method     ChildSiteQuery rightJoinWithStatusData() Adds a RIGHT JOIN clause and with to the query using the StatusData relation
 * @method     ChildSiteQuery innerJoinWithStatusData() Adds a INNER JOIN clause and with to the query using the StatusData relation
 *
 * @method     ChildSiteQuery leftJoinTypeData($relationAlias = null) Adds a LEFT JOIN clause to the query using the TypeData relation
 * @method     ChildSiteQuery rightJoinTypeData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TypeData relation
 * @method     ChildSiteQuery innerJoinTypeData($relationAlias = null) Adds a INNER JOIN clause to the query using the TypeData relation
 *
 * @method     ChildSiteQuery joinWithTypeData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TypeData relation
 *
 * @method     ChildSiteQuery leftJoinWithTypeData() Adds a LEFT JOIN clause and with to the query using the TypeData relation
 * @method     ChildSiteQuery rightJoinWithTypeData() Adds a RIGHT JOIN clause and with to the query using the TypeData relation
 * @method     ChildSiteQuery innerJoinWithTypeData() Adds a INNER JOIN clause and with to the query using the TypeData relation
 *
 * @method     ChildSiteQuery leftJoinGameData($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameData relation
 * @method     ChildSiteQuery rightJoinGameData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameData relation
 * @method     ChildSiteQuery innerJoinGameData($relationAlias = null) Adds a INNER JOIN clause to the query using the GameData relation
 *
 * @method     ChildSiteQuery joinWithGameData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameData relation
 *
 * @method     ChildSiteQuery leftJoinWithGameData() Adds a LEFT JOIN clause and with to the query using the GameData relation
 * @method     ChildSiteQuery rightJoinWithGameData() Adds a RIGHT JOIN clause and with to the query using the GameData relation
 * @method     ChildSiteQuery innerJoinWithGameData() Adds a INNER JOIN clause and with to the query using the GameData relation
 *
 * @method     ChildSiteQuery leftJoinStat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stat relation
 * @method     ChildSiteQuery rightJoinStat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stat relation
 * @method     ChildSiteQuery innerJoinStat($relationAlias = null) Adds a INNER JOIN clause to the query using the Stat relation
 *
 * @method     ChildSiteQuery joinWithStat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stat relation
 *
 * @method     ChildSiteQuery leftJoinWithStat() Adds a LEFT JOIN clause and with to the query using the Stat relation
 * @method     ChildSiteQuery rightJoinWithStat() Adds a RIGHT JOIN clause and with to the query using the Stat relation
 * @method     ChildSiteQuery innerJoinWithStat() Adds a INNER JOIN clause and with to the query using the Stat relation
 *
 * @method     ChildSiteQuery leftJoinExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Experience relation
 * @method     ChildSiteQuery rightJoinExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Experience relation
 * @method     ChildSiteQuery innerJoinExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the Experience relation
 *
 * @method     ChildSiteQuery joinWithExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Experience relation
 *
 * @method     ChildSiteQuery leftJoinWithExperience() Adds a LEFT JOIN clause and with to the query using the Experience relation
 * @method     ChildSiteQuery rightJoinWithExperience() Adds a RIGHT JOIN clause and with to the query using the Experience relation
 * @method     ChildSiteQuery innerJoinWithExperience() Adds a INNER JOIN clause and with to the query using the Experience relation
 *
 * @method     \IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery|\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery|\IiMedias\StreamBundle\Model\MessageExperienceQuery|\IiMedias\StreamBundle\Model\ChatterExperienceQuery|\IiMedias\StreamBundle\Model\FollowExperienceQuery|\IiMedias\StreamBundle\Model\HostExperienceQuery|\IiMedias\StreamBundle\Model\ViewDiffDataQuery|\IiMedias\StreamBundle\Model\ViewerDataQuery|\IiMedias\StreamBundle\Model\FollowDiffDataQuery|\IiMedias\StreamBundle\Model\StatusDataQuery|\IiMedias\StreamBundle\Model\TypeDataQuery|\IiMedias\StreamBundle\Model\GameDataQuery|\IiMedias\StreamBundle\Model\StatQuery|\IiMedias\StreamBundle\Model\ExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSite findOne(ConnectionInterface $con = null) Return the first ChildSite matching the query
 * @method     ChildSite findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSite matching the query, or a new ChildSite object populated from the query conditions when no match is found
 *
 * @method     ChildSite findOneById(int $stsite_id) Return the first ChildSite filtered by the stsite_id column
 * @method     ChildSite findOneByName(string $stsite_name) Return the first ChildSite filtered by the stsite_name column
 * @method     ChildSite findOneByCode(string $stsite_code) Return the first ChildSite filtered by the stsite_code column
 * @method     ChildSite findOneByFontAwesomeIcon(string $stsite_font_awesome_icon) Return the first ChildSite filtered by the stsite_font_awesome_icon column
 * @method     ChildSite findOneByColorHexa(string $stsite_color_hexa) Return the first ChildSite filtered by the stsite_color_hexa column
 * @method     ChildSite findOneByColorRgb(string $stsite_color_rgb) Return the first ChildSite filtered by the stsite_color_rgb column
 * @method     ChildSite findOneByCreatedByUserId(int $stsite_created_by_user_id) Return the first ChildSite filtered by the stsite_created_by_user_id column
 * @method     ChildSite findOneByUpdatedByUserId(int $stsite_updated_by_user_id) Return the first ChildSite filtered by the stsite_updated_by_user_id column
 * @method     ChildSite findOneByCreatedAt(string $stsite_created_at) Return the first ChildSite filtered by the stsite_created_at column
 * @method     ChildSite findOneByUpdatedAt(string $stsite_updated_at) Return the first ChildSite filtered by the stsite_updated_at column *

 * @method     ChildSite requirePk($key, ConnectionInterface $con = null) Return the ChildSite by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOne(ConnectionInterface $con = null) Return the first ChildSite matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSite requireOneById(int $stsite_id) Return the first ChildSite filtered by the stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByName(string $stsite_name) Return the first ChildSite filtered by the stsite_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByCode(string $stsite_code) Return the first ChildSite filtered by the stsite_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByFontAwesomeIcon(string $stsite_font_awesome_icon) Return the first ChildSite filtered by the stsite_font_awesome_icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByColorHexa(string $stsite_color_hexa) Return the first ChildSite filtered by the stsite_color_hexa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByColorRgb(string $stsite_color_rgb) Return the first ChildSite filtered by the stsite_color_rgb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByCreatedByUserId(int $stsite_created_by_user_id) Return the first ChildSite filtered by the stsite_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByUpdatedByUserId(int $stsite_updated_by_user_id) Return the first ChildSite filtered by the stsite_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByCreatedAt(string $stsite_created_at) Return the first ChildSite filtered by the stsite_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByUpdatedAt(string $stsite_updated_at) Return the first ChildSite filtered by the stsite_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSite[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSite objects based on current ModelCriteria
 * @method     ChildSite[]|ObjectCollection findById(int $stsite_id) Return ChildSite objects filtered by the stsite_id column
 * @method     ChildSite[]|ObjectCollection findByName(string $stsite_name) Return ChildSite objects filtered by the stsite_name column
 * @method     ChildSite[]|ObjectCollection findByCode(string $stsite_code) Return ChildSite objects filtered by the stsite_code column
 * @method     ChildSite[]|ObjectCollection findByFontAwesomeIcon(string $stsite_font_awesome_icon) Return ChildSite objects filtered by the stsite_font_awesome_icon column
 * @method     ChildSite[]|ObjectCollection findByColorHexa(string $stsite_color_hexa) Return ChildSite objects filtered by the stsite_color_hexa column
 * @method     ChildSite[]|ObjectCollection findByColorRgb(string $stsite_color_rgb) Return ChildSite objects filtered by the stsite_color_rgb column
 * @method     ChildSite[]|ObjectCollection findByCreatedByUserId(int $stsite_created_by_user_id) Return ChildSite objects filtered by the stsite_created_by_user_id column
 * @method     ChildSite[]|ObjectCollection findByUpdatedByUserId(int $stsite_updated_by_user_id) Return ChildSite objects filtered by the stsite_updated_by_user_id column
 * @method     ChildSite[]|ObjectCollection findByCreatedAt(string $stsite_created_at) Return ChildSite objects filtered by the stsite_created_at column
 * @method     ChildSite[]|ObjectCollection findByUpdatedAt(string $stsite_updated_at) Return ChildSite objects filtered by the stsite_updated_at column
 * @method     ChildSite[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\SiteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Site', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteQuery) {
            return $criteria;
        }
        $query = new ChildSiteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSite|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSite A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stsite_id, stsite_name, stsite_code, stsite_font_awesome_icon, stsite_color_hexa, stsite_color_rgb, stsite_created_by_user_id, stsite_updated_by_user_id, stsite_created_at, stsite_updated_at FROM stream_site_stsite WHERE stsite_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSite $obj */
            $obj = new ChildSite();
            $obj->hydrate($row);
            SiteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSite|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stsite_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stsite_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stsite_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stsite_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE stsite_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE stsite_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the stsite_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE stsite_code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE stsite_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the stsite_font_awesome_icon column
     *
     * Example usage:
     * <code>
     * $query->filterByFontAwesomeIcon('fooValue');   // WHERE stsite_font_awesome_icon = 'fooValue'
     * $query->filterByFontAwesomeIcon('%fooValue%', Criteria::LIKE); // WHERE stsite_font_awesome_icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fontAwesomeIcon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByFontAwesomeIcon($fontAwesomeIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fontAwesomeIcon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_FONT_AWESOME_ICON, $fontAwesomeIcon, $comparison);
    }

    /**
     * Filter the query on the stsite_color_hexa column
     *
     * Example usage:
     * <code>
     * $query->filterByColorHexa('fooValue');   // WHERE stsite_color_hexa = 'fooValue'
     * $query->filterByColorHexa('%fooValue%', Criteria::LIKE); // WHERE stsite_color_hexa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorHexa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByColorHexa($colorHexa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorHexa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_COLOR_HEXA, $colorHexa, $comparison);
    }

    /**
     * Filter the query on the stsite_color_rgb column
     *
     * Example usage:
     * <code>
     * $query->filterByColorRgb('fooValue');   // WHERE stsite_color_rgb = 'fooValue'
     * $query->filterByColorRgb('%fooValue%', Criteria::LIKE); // WHERE stsite_color_rgb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorRgb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByColorRgb($colorRgb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorRgb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_COLOR_RGB, $colorRgb, $comparison);
    }

    /**
     * Filter the query on the stsite_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE stsite_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE stsite_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE stsite_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the stsite_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE stsite_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE stsite_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE stsite_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the stsite_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stsite_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stsite_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stsite_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stsite_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stsite_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stsite_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stsite_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $channel->getSiteId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useChannelQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $chatUser->getSiteId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            return $this
                ->useChatUserQuery()
                ->filterByPrimaryKeys($chatUser->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $userExperience->getSiteId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\DeepBotImportExperience object
     *
     * @param \IiMedias\StreamBundle\Model\DeepBotImportExperience|ObjectCollection $deepBotImportExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByDeepBotImportExperience($deepBotImportExperience, $comparison = null)
    {
        if ($deepBotImportExperience instanceof \IiMedias\StreamBundle\Model\DeepBotImportExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $deepBotImportExperience->getSiteId(), $comparison);
        } elseif ($deepBotImportExperience instanceof ObjectCollection) {
            return $this
                ->useDeepBotImportExperienceQuery()
                ->filterByPrimaryKeys($deepBotImportExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDeepBotImportExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\DeepBotImportExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeepBotImportExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinDeepBotImportExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeepBotImportExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeepBotImportExperience');
        }

        return $this;
    }

    /**
     * Use the DeepBotImportExperience relation DeepBotImportExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery A secondary query class using the current class as primary query
     */
    public function useDeepBotImportExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDeepBotImportExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeepBotImportExperience', '\IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\MessageExperience object
     *
     * @param \IiMedias\StreamBundle\Model\MessageExperience|ObjectCollection $messageExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByMessageExperience($messageExperience, $comparison = null)
    {
        if ($messageExperience instanceof \IiMedias\StreamBundle\Model\MessageExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $messageExperience->getSiteId(), $comparison);
        } elseif ($messageExperience instanceof ObjectCollection) {
            return $this
                ->useMessageExperienceQuery()
                ->filterByPrimaryKeys($messageExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\MessageExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinMessageExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageExperience');
        }

        return $this;
    }

    /**
     * Use the MessageExperience relation MessageExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\MessageExperienceQuery A secondary query class using the current class as primary query
     */
    public function useMessageExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageExperience', '\IiMedias\StreamBundle\Model\MessageExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatterExperience object
     *
     * @param \IiMedias\StreamBundle\Model\ChatterExperience|ObjectCollection $chatterExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByChatterExperience($chatterExperience, $comparison = null)
    {
        if ($chatterExperience instanceof \IiMedias\StreamBundle\Model\ChatterExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $chatterExperience->getSiteId(), $comparison);
        } elseif ($chatterExperience instanceof ObjectCollection) {
            return $this
                ->useChatterExperienceQuery()
                ->filterByPrimaryKeys($chatterExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatterExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatterExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatterExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinChatterExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatterExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatterExperience');
        }

        return $this;
    }

    /**
     * Use the ChatterExperience relation ChatterExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatterExperienceQuery A secondary query class using the current class as primary query
     */
    public function useChatterExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatterExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatterExperience', '\IiMedias\StreamBundle\Model\ChatterExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowExperience object
     *
     * @param \IiMedias\StreamBundle\Model\FollowExperience|ObjectCollection $followExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByFollowExperience($followExperience, $comparison = null)
    {
        if ($followExperience instanceof \IiMedias\StreamBundle\Model\FollowExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $followExperience->getSiteId(), $comparison);
        } elseif ($followExperience instanceof ObjectCollection) {
            return $this
                ->useFollowExperienceQuery()
                ->filterByPrimaryKeys($followExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinFollowExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowExperience');
        }

        return $this;
    }

    /**
     * Use the FollowExperience relation FollowExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowExperienceQuery A secondary query class using the current class as primary query
     */
    public function useFollowExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowExperience', '\IiMedias\StreamBundle\Model\FollowExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\HostExperience object
     *
     * @param \IiMedias\StreamBundle\Model\HostExperience|ObjectCollection $hostExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByHostExperience($hostExperience, $comparison = null)
    {
        if ($hostExperience instanceof \IiMedias\StreamBundle\Model\HostExperience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $hostExperience->getSiteId(), $comparison);
        } elseif ($hostExperience instanceof ObjectCollection) {
            return $this
                ->useHostExperienceQuery()
                ->filterByPrimaryKeys($hostExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHostExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\HostExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HostExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinHostExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HostExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HostExperience');
        }

        return $this;
    }

    /**
     * Use the HostExperience relation HostExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\HostExperienceQuery A secondary query class using the current class as primary query
     */
    public function useHostExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinHostExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HostExperience', '\IiMedias\StreamBundle\Model\HostExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewDiffData|ObjectCollection $viewDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByViewDiffData($viewDiffData, $comparison = null)
    {
        if ($viewDiffData instanceof \IiMedias\StreamBundle\Model\ViewDiffData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $viewDiffData->getSiteId(), $comparison);
        } elseif ($viewDiffData instanceof ObjectCollection) {
            return $this
                ->useViewDiffDataQuery()
                ->filterByPrimaryKeys($viewDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinViewDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewDiffData');
        }

        return $this;
    }

    /**
     * Use the ViewDiffData relation ViewDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useViewDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewDiffData', '\IiMedias\StreamBundle\Model\ViewDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ViewerData object
     *
     * @param \IiMedias\StreamBundle\Model\ViewerData|ObjectCollection $viewerData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByViewerData($viewerData, $comparison = null)
    {
        if ($viewerData instanceof \IiMedias\StreamBundle\Model\ViewerData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $viewerData->getSiteId(), $comparison);
        } elseif ($viewerData instanceof ObjectCollection) {
            return $this
                ->useViewerDataQuery()
                ->filterByPrimaryKeys($viewerData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByViewerData() only accepts arguments of type \IiMedias\StreamBundle\Model\ViewerData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ViewerData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinViewerData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ViewerData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ViewerData');
        }

        return $this;
    }

    /**
     * Use the ViewerData relation ViewerData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ViewerDataQuery A secondary query class using the current class as primary query
     */
    public function useViewerDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinViewerData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ViewerData', '\IiMedias\StreamBundle\Model\ViewerDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowDiffData object
     *
     * @param \IiMedias\StreamBundle\Model\FollowDiffData|ObjectCollection $followDiffData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByFollowDiffData($followDiffData, $comparison = null)
    {
        if ($followDiffData instanceof \IiMedias\StreamBundle\Model\FollowDiffData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $followDiffData->getSiteId(), $comparison);
        } elseif ($followDiffData instanceof ObjectCollection) {
            return $this
                ->useFollowDiffDataQuery()
                ->filterByPrimaryKeys($followDiffData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowDiffData() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowDiffData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowDiffData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinFollowDiffData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowDiffData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowDiffData');
        }

        return $this;
    }

    /**
     * Use the FollowDiffData relation FollowDiffData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowDiffDataQuery A secondary query class using the current class as primary query
     */
    public function useFollowDiffDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFollowDiffData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowDiffData', '\IiMedias\StreamBundle\Model\FollowDiffDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\StatusData object
     *
     * @param \IiMedias\StreamBundle\Model\StatusData|ObjectCollection $statusData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByStatusData($statusData, $comparison = null)
    {
        if ($statusData instanceof \IiMedias\StreamBundle\Model\StatusData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $statusData->getSiteId(), $comparison);
        } elseif ($statusData instanceof ObjectCollection) {
            return $this
                ->useStatusDataQuery()
                ->filterByPrimaryKeys($statusData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStatusData() only accepts arguments of type \IiMedias\StreamBundle\Model\StatusData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinStatusData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusData');
        }

        return $this;
    }

    /**
     * Use the StatusData relation StatusData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatusDataQuery A secondary query class using the current class as primary query
     */
    public function useStatusDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusData', '\IiMedias\StreamBundle\Model\StatusDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\TypeData object
     *
     * @param \IiMedias\StreamBundle\Model\TypeData|ObjectCollection $typeData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByTypeData($typeData, $comparison = null)
    {
        if ($typeData instanceof \IiMedias\StreamBundle\Model\TypeData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $typeData->getSiteId(), $comparison);
        } elseif ($typeData instanceof ObjectCollection) {
            return $this
                ->useTypeDataQuery()
                ->filterByPrimaryKeys($typeData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTypeData() only accepts arguments of type \IiMedias\StreamBundle\Model\TypeData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TypeData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinTypeData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TypeData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TypeData');
        }

        return $this;
    }

    /**
     * Use the TypeData relation TypeData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\TypeDataQuery A secondary query class using the current class as primary query
     */
    public function useTypeDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTypeData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TypeData', '\IiMedias\StreamBundle\Model\TypeDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\GameData object
     *
     * @param \IiMedias\StreamBundle\Model\GameData|ObjectCollection $gameData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByGameData($gameData, $comparison = null)
    {
        if ($gameData instanceof \IiMedias\StreamBundle\Model\GameData) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $gameData->getSiteId(), $comparison);
        } elseif ($gameData instanceof ObjectCollection) {
            return $this
                ->useGameDataQuery()
                ->filterByPrimaryKeys($gameData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameData() only accepts arguments of type \IiMedias\StreamBundle\Model\GameData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinGameData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameData');
        }

        return $this;
    }

    /**
     * Use the GameData relation GameData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\GameDataQuery A secondary query class using the current class as primary query
     */
    public function useGameDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameData', '\IiMedias\StreamBundle\Model\GameDataQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stat object
     *
     * @param \IiMedias\StreamBundle\Model\Stat|ObjectCollection $stat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByStat($stat, $comparison = null)
    {
        if ($stat instanceof \IiMedias\StreamBundle\Model\Stat) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $stat->getSiteId(), $comparison);
        } elseif ($stat instanceof ObjectCollection) {
            return $this
                ->useStatQuery()
                ->filterByPrimaryKeys($stat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStat() only accepts arguments of type \IiMedias\StreamBundle\Model\Stat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinStat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stat');
        }

        return $this;
    }

    /**
     * Use the Stat relation Stat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StatQuery A secondary query class using the current class as primary query
     */
    public function useStatQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stat', '\IiMedias\StreamBundle\Model\StatQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Experience object
     *
     * @param \IiMedias\StreamBundle\Model\Experience|ObjectCollection $experience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByExperience($experience, $comparison = null)
    {
        if ($experience instanceof \IiMedias\StreamBundle\Model\Experience) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_STSITE_ID, $experience->getSiteId(), $comparison);
        } elseif ($experience instanceof ObjectCollection) {
            return $this
                ->useExperienceQuery()
                ->filterByPrimaryKeys($experience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\Experience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Experience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinExperience($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Experience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Experience');
        }

        return $this;
    }

    /**
     * Use the Experience relation Experience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ExperienceQuery A secondary query class using the current class as primary query
     */
    public function useExperienceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Experience', '\IiMedias\StreamBundle\Model\ExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSite $site Object to remove from the list of results
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function prune($site = null)
    {
        if ($site) {
            $this->addUsingAlias(SiteTableMap::COL_STSITE_ID, $site->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_site_stsite table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteTableMap::clearInstancePool();
            SiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(SiteTableMap::COL_STSITE_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(SiteTableMap::COL_STSITE_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(SiteTableMap::COL_STSITE_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(SiteTableMap::COL_STSITE_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(SiteTableMap::COL_STSITE_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildSiteQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(SiteTableMap::COL_STSITE_CREATED_AT);
    }

} // SiteQuery

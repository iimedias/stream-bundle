<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\ViewDiffData as ChildViewDiffData;
use IiMedias\StreamBundle\Model\ViewDiffDataQuery as ChildViewDiffDataQuery;
use IiMedias\StreamBundle\Model\Map\ViewDiffDataTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_view_diff_data_stddat' table.
 *
 *
 *
 * @method     ChildViewDiffDataQuery orderById($order = Criteria::ASC) Order by the stddat_id column
 * @method     ChildViewDiffDataQuery orderByStreamId($order = Criteria::ASC) Order by the stddat_ststrm_id column
 * @method     ChildViewDiffDataQuery orderByChannelId($order = Criteria::ASC) Order by the stddat_stchan_id column
 * @method     ChildViewDiffDataQuery orderBySiteId($order = Criteria::ASC) Order by the stddat_stsite_id column
 * @method     ChildViewDiffDataQuery orderByDiff($order = Criteria::ASC) Order by the stddat_diff column
 * @method     ChildViewDiffDataQuery orderByAt($order = Criteria::ASC) Order by the stddat_at column
 *
 * @method     ChildViewDiffDataQuery groupById() Group by the stddat_id column
 * @method     ChildViewDiffDataQuery groupByStreamId() Group by the stddat_ststrm_id column
 * @method     ChildViewDiffDataQuery groupByChannelId() Group by the stddat_stchan_id column
 * @method     ChildViewDiffDataQuery groupBySiteId() Group by the stddat_stsite_id column
 * @method     ChildViewDiffDataQuery groupByDiff() Group by the stddat_diff column
 * @method     ChildViewDiffDataQuery groupByAt() Group by the stddat_at column
 *
 * @method     ChildViewDiffDataQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildViewDiffDataQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildViewDiffDataQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildViewDiffDataQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildViewDiffDataQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildViewDiffDataQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildViewDiffDataQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildViewDiffDataQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildViewDiffDataQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildViewDiffDataQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildViewDiffDataQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildViewDiffDataQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildViewDiffDataQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildViewDiffDataQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildViewDiffDataQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildViewDiffDataQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildViewDiffDataQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildViewDiffDataQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildViewDiffDataQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildViewDiffDataQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildViewDiffDataQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildViewDiffDataQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildViewDiffDataQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildViewDiffDataQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildViewDiffDataQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildViewDiffDataQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildViewDiffDataQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildViewDiffData findOne(ConnectionInterface $con = null) Return the first ChildViewDiffData matching the query
 * @method     ChildViewDiffData findOneOrCreate(ConnectionInterface $con = null) Return the first ChildViewDiffData matching the query, or a new ChildViewDiffData object populated from the query conditions when no match is found
 *
 * @method     ChildViewDiffData findOneById(int $stddat_id) Return the first ChildViewDiffData filtered by the stddat_id column
 * @method     ChildViewDiffData findOneByStreamId(int $stddat_ststrm_id) Return the first ChildViewDiffData filtered by the stddat_ststrm_id column
 * @method     ChildViewDiffData findOneByChannelId(int $stddat_stchan_id) Return the first ChildViewDiffData filtered by the stddat_stchan_id column
 * @method     ChildViewDiffData findOneBySiteId(int $stddat_stsite_id) Return the first ChildViewDiffData filtered by the stddat_stsite_id column
 * @method     ChildViewDiffData findOneByDiff(int $stddat_diff) Return the first ChildViewDiffData filtered by the stddat_diff column
 * @method     ChildViewDiffData findOneByAt(string $stddat_at) Return the first ChildViewDiffData filtered by the stddat_at column *

 * @method     ChildViewDiffData requirePk($key, ConnectionInterface $con = null) Return the ChildViewDiffData by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOne(ConnectionInterface $con = null) Return the first ChildViewDiffData matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildViewDiffData requireOneById(int $stddat_id) Return the first ChildViewDiffData filtered by the stddat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOneByStreamId(int $stddat_ststrm_id) Return the first ChildViewDiffData filtered by the stddat_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOneByChannelId(int $stddat_stchan_id) Return the first ChildViewDiffData filtered by the stddat_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOneBySiteId(int $stddat_stsite_id) Return the first ChildViewDiffData filtered by the stddat_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOneByDiff(int $stddat_diff) Return the first ChildViewDiffData filtered by the stddat_diff column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewDiffData requireOneByAt(string $stddat_at) Return the first ChildViewDiffData filtered by the stddat_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildViewDiffData[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildViewDiffData objects based on current ModelCriteria
 * @method     ChildViewDiffData[]|ObjectCollection findById(int $stddat_id) Return ChildViewDiffData objects filtered by the stddat_id column
 * @method     ChildViewDiffData[]|ObjectCollection findByStreamId(int $stddat_ststrm_id) Return ChildViewDiffData objects filtered by the stddat_ststrm_id column
 * @method     ChildViewDiffData[]|ObjectCollection findByChannelId(int $stddat_stchan_id) Return ChildViewDiffData objects filtered by the stddat_stchan_id column
 * @method     ChildViewDiffData[]|ObjectCollection findBySiteId(int $stddat_stsite_id) Return ChildViewDiffData objects filtered by the stddat_stsite_id column
 * @method     ChildViewDiffData[]|ObjectCollection findByDiff(int $stddat_diff) Return ChildViewDiffData objects filtered by the stddat_diff column
 * @method     ChildViewDiffData[]|ObjectCollection findByAt(string $stddat_at) Return ChildViewDiffData objects filtered by the stddat_at column
 * @method     ChildViewDiffData[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ViewDiffDataQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\ViewDiffDataQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\ViewDiffData', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildViewDiffDataQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildViewDiffDataQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildViewDiffDataQuery) {
            return $criteria;
        }
        $query = new ChildViewDiffDataQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildViewDiffData|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ViewDiffDataTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ViewDiffDataTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildViewDiffData A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stddat_id, stddat_ststrm_id, stddat_stchan_id, stddat_stsite_id, stddat_diff, stddat_at FROM stream_view_diff_data_stddat WHERE stddat_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildViewDiffData $obj */
            $obj = new ChildViewDiffData();
            $obj->hydrate($row);
            ViewDiffDataTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildViewDiffData|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stddat_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stddat_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stddat_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stddat_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stddat_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stddat_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stddat_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stddat_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stddat_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stddat_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stddat_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stddat_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stddat_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stddat_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stddat_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stddat_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stddat_diff column
     *
     * Example usage:
     * <code>
     * $query->filterByDiff(1234); // WHERE stddat_diff = 1234
     * $query->filterByDiff(array(12, 34)); // WHERE stddat_diff IN (12, 34)
     * $query->filterByDiff(array('min' => 12)); // WHERE stddat_diff > 12
     * </code>
     *
     * @param     mixed $diff The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByDiff($diff = null, $comparison = null)
    {
        if (is_array($diff)) {
            $useMinMax = false;
            if (isset($diff['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_DIFF, $diff['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($diff['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_DIFF, $diff['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_DIFF, $diff, $comparison);
    }

    /**
     * Filter the query on the stddat_at column
     *
     * Example usage:
     * <code>
     * $query->filterByAt('2011-03-14'); // WHERE stddat_at = '2011-03-14'
     * $query->filterByAt('now'); // WHERE stddat_at = '2011-03-14'
     * $query->filterByAt(array('max' => 'yesterday')); // WHERE stddat_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $at The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByAt($at = null, $comparison = null)
    {
        if (is_array($at)) {
            $useMinMax = false;
            if (isset($at['min'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_AT, $at['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($at['max'])) {
                $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_AT, $at['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_AT, $at, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildViewDiffData $viewDiffData Object to remove from the list of results
     *
     * @return $this|ChildViewDiffDataQuery The current query, for fluid interface
     */
    public function prune($viewDiffData = null)
    {
        if ($viewDiffData) {
            $this->addUsingAlias(ViewDiffDataTableMap::COL_STDDAT_ID, $viewDiffData->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_view_diff_data_stddat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewDiffDataTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ViewDiffDataTableMap::clearInstancePool();
            ViewDiffDataTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewDiffDataTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ViewDiffDataTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ViewDiffDataTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ViewDiffDataTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ViewDiffDataQuery

<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\StreamBundle\Model\Avatar as ChildAvatar;
use IiMedias\StreamBundle\Model\AvatarQuery as ChildAvatarQuery;
use IiMedias\StreamBundle\Model\Map\AvatarTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_avatar_stavtr' table.
 *
 *
 *
 * @method     ChildAvatarQuery orderById($order = Criteria::ASC) Order by the stavtr_id column
 * @method     ChildAvatarQuery orderByStreamId($order = Criteria::ASC) Order by the stavtr_ststrm_id column
 * @method     ChildAvatarQuery orderByName($order = Criteria::ASC) Order by the stavtr_name column
 * @method     ChildAvatarQuery orderByCode($order = Criteria::ASC) Order by the stavtr_code column
 * @method     ChildAvatarQuery orderByIsDefault($order = Criteria::ASC) Order by the stavtr_is_default column
 * @method     ChildAvatarQuery orderByCanBeOwned($order = Criteria::ASC) Order by the stavtr_can_be_owned column
 * @method     ChildAvatarQuery orderByMainColor($order = Criteria::ASC) Order by the stavtr_main_color column
 * @method     ChildAvatarQuery orderByMinRankId($order = Criteria::ASC) Order by the stavtr_min_strank_id column
 * @method     ChildAvatarQuery orderByAssociatedRankId($order = Criteria::ASC) Order by the stavtr_associated_strank_id column
 * @method     ChildAvatarQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the stavtr_created_by_user_id column
 * @method     ChildAvatarQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the stavtr_updated_by_user_id column
 * @method     ChildAvatarQuery orderByCreatedAt($order = Criteria::ASC) Order by the stavtr_created_at column
 * @method     ChildAvatarQuery orderByUpdatedAt($order = Criteria::ASC) Order by the stavtr_updated_at column
 *
 * @method     ChildAvatarQuery groupById() Group by the stavtr_id column
 * @method     ChildAvatarQuery groupByStreamId() Group by the stavtr_ststrm_id column
 * @method     ChildAvatarQuery groupByName() Group by the stavtr_name column
 * @method     ChildAvatarQuery groupByCode() Group by the stavtr_code column
 * @method     ChildAvatarQuery groupByIsDefault() Group by the stavtr_is_default column
 * @method     ChildAvatarQuery groupByCanBeOwned() Group by the stavtr_can_be_owned column
 * @method     ChildAvatarQuery groupByMainColor() Group by the stavtr_main_color column
 * @method     ChildAvatarQuery groupByMinRankId() Group by the stavtr_min_strank_id column
 * @method     ChildAvatarQuery groupByAssociatedRankId() Group by the stavtr_associated_strank_id column
 * @method     ChildAvatarQuery groupByCreatedByUserId() Group by the stavtr_created_by_user_id column
 * @method     ChildAvatarQuery groupByUpdatedByUserId() Group by the stavtr_updated_by_user_id column
 * @method     ChildAvatarQuery groupByCreatedAt() Group by the stavtr_created_at column
 * @method     ChildAvatarQuery groupByUpdatedAt() Group by the stavtr_updated_at column
 *
 * @method     ChildAvatarQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAvatarQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAvatarQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAvatarQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAvatarQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAvatarQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAvatarQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildAvatarQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildAvatarQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildAvatarQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildAvatarQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildAvatarQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildAvatarQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildAvatarQuery leftJoinMinRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the MinRank relation
 * @method     ChildAvatarQuery rightJoinMinRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MinRank relation
 * @method     ChildAvatarQuery innerJoinMinRank($relationAlias = null) Adds a INNER JOIN clause to the query using the MinRank relation
 *
 * @method     ChildAvatarQuery joinWithMinRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MinRank relation
 *
 * @method     ChildAvatarQuery leftJoinWithMinRank() Adds a LEFT JOIN clause and with to the query using the MinRank relation
 * @method     ChildAvatarQuery rightJoinWithMinRank() Adds a RIGHT JOIN clause and with to the query using the MinRank relation
 * @method     ChildAvatarQuery innerJoinWithMinRank() Adds a INNER JOIN clause and with to the query using the MinRank relation
 *
 * @method     ChildAvatarQuery leftJoinAssociatedRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the AssociatedRank relation
 * @method     ChildAvatarQuery rightJoinAssociatedRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AssociatedRank relation
 * @method     ChildAvatarQuery innerJoinAssociatedRank($relationAlias = null) Adds a INNER JOIN clause to the query using the AssociatedRank relation
 *
 * @method     ChildAvatarQuery joinWithAssociatedRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AssociatedRank relation
 *
 * @method     ChildAvatarQuery leftJoinWithAssociatedRank() Adds a LEFT JOIN clause and with to the query using the AssociatedRank relation
 * @method     ChildAvatarQuery rightJoinWithAssociatedRank() Adds a RIGHT JOIN clause and with to the query using the AssociatedRank relation
 * @method     ChildAvatarQuery innerJoinWithAssociatedRank() Adds a INNER JOIN clause and with to the query using the AssociatedRank relation
 *
 * @method     ChildAvatarQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildAvatarQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildAvatarQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildAvatarQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildAvatarQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildAvatarQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildAvatarQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildAvatarQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildAvatarQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildAvatarQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildAvatarQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildAvatarQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildAvatarQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildAvatarQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildAvatarQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildAvatarQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildAvatarQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildAvatarQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildAvatarQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildAvatarQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildAvatarQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\RankQuery|\IiMedias\AdminBundle\Model\UserQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildAvatar findOne(ConnectionInterface $con = null) Return the first ChildAvatar matching the query
 * @method     ChildAvatar findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAvatar matching the query, or a new ChildAvatar object populated from the query conditions when no match is found
 *
 * @method     ChildAvatar findOneById(int $stavtr_id) Return the first ChildAvatar filtered by the stavtr_id column
 * @method     ChildAvatar findOneByStreamId(int $stavtr_ststrm_id) Return the first ChildAvatar filtered by the stavtr_ststrm_id column
 * @method     ChildAvatar findOneByName(string $stavtr_name) Return the first ChildAvatar filtered by the stavtr_name column
 * @method     ChildAvatar findOneByCode(string $stavtr_code) Return the first ChildAvatar filtered by the stavtr_code column
 * @method     ChildAvatar findOneByIsDefault(boolean $stavtr_is_default) Return the first ChildAvatar filtered by the stavtr_is_default column
 * @method     ChildAvatar findOneByCanBeOwned(boolean $stavtr_can_be_owned) Return the first ChildAvatar filtered by the stavtr_can_be_owned column
 * @method     ChildAvatar findOneByMainColor(int $stavtr_main_color) Return the first ChildAvatar filtered by the stavtr_main_color column
 * @method     ChildAvatar findOneByMinRankId(int $stavtr_min_strank_id) Return the first ChildAvatar filtered by the stavtr_min_strank_id column
 * @method     ChildAvatar findOneByAssociatedRankId(int $stavtr_associated_strank_id) Return the first ChildAvatar filtered by the stavtr_associated_strank_id column
 * @method     ChildAvatar findOneByCreatedByUserId(int $stavtr_created_by_user_id) Return the first ChildAvatar filtered by the stavtr_created_by_user_id column
 * @method     ChildAvatar findOneByUpdatedByUserId(int $stavtr_updated_by_user_id) Return the first ChildAvatar filtered by the stavtr_updated_by_user_id column
 * @method     ChildAvatar findOneByCreatedAt(string $stavtr_created_at) Return the first ChildAvatar filtered by the stavtr_created_at column
 * @method     ChildAvatar findOneByUpdatedAt(string $stavtr_updated_at) Return the first ChildAvatar filtered by the stavtr_updated_at column *

 * @method     ChildAvatar requirePk($key, ConnectionInterface $con = null) Return the ChildAvatar by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOne(ConnectionInterface $con = null) Return the first ChildAvatar matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAvatar requireOneById(int $stavtr_id) Return the first ChildAvatar filtered by the stavtr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByStreamId(int $stavtr_ststrm_id) Return the first ChildAvatar filtered by the stavtr_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByName(string $stavtr_name) Return the first ChildAvatar filtered by the stavtr_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByCode(string $stavtr_code) Return the first ChildAvatar filtered by the stavtr_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByIsDefault(boolean $stavtr_is_default) Return the first ChildAvatar filtered by the stavtr_is_default column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByCanBeOwned(boolean $stavtr_can_be_owned) Return the first ChildAvatar filtered by the stavtr_can_be_owned column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByMainColor(int $stavtr_main_color) Return the first ChildAvatar filtered by the stavtr_main_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByMinRankId(int $stavtr_min_strank_id) Return the first ChildAvatar filtered by the stavtr_min_strank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByAssociatedRankId(int $stavtr_associated_strank_id) Return the first ChildAvatar filtered by the stavtr_associated_strank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByCreatedByUserId(int $stavtr_created_by_user_id) Return the first ChildAvatar filtered by the stavtr_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByUpdatedByUserId(int $stavtr_updated_by_user_id) Return the first ChildAvatar filtered by the stavtr_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByCreatedAt(string $stavtr_created_at) Return the first ChildAvatar filtered by the stavtr_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAvatar requireOneByUpdatedAt(string $stavtr_updated_at) Return the first ChildAvatar filtered by the stavtr_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAvatar[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAvatar objects based on current ModelCriteria
 * @method     ChildAvatar[]|ObjectCollection findById(int $stavtr_id) Return ChildAvatar objects filtered by the stavtr_id column
 * @method     ChildAvatar[]|ObjectCollection findByStreamId(int $stavtr_ststrm_id) Return ChildAvatar objects filtered by the stavtr_ststrm_id column
 * @method     ChildAvatar[]|ObjectCollection findByName(string $stavtr_name) Return ChildAvatar objects filtered by the stavtr_name column
 * @method     ChildAvatar[]|ObjectCollection findByCode(string $stavtr_code) Return ChildAvatar objects filtered by the stavtr_code column
 * @method     ChildAvatar[]|ObjectCollection findByIsDefault(boolean $stavtr_is_default) Return ChildAvatar objects filtered by the stavtr_is_default column
 * @method     ChildAvatar[]|ObjectCollection findByCanBeOwned(boolean $stavtr_can_be_owned) Return ChildAvatar objects filtered by the stavtr_can_be_owned column
 * @method     ChildAvatar[]|ObjectCollection findByMainColor(int $stavtr_main_color) Return ChildAvatar objects filtered by the stavtr_main_color column
 * @method     ChildAvatar[]|ObjectCollection findByMinRankId(int $stavtr_min_strank_id) Return ChildAvatar objects filtered by the stavtr_min_strank_id column
 * @method     ChildAvatar[]|ObjectCollection findByAssociatedRankId(int $stavtr_associated_strank_id) Return ChildAvatar objects filtered by the stavtr_associated_strank_id column
 * @method     ChildAvatar[]|ObjectCollection findByCreatedByUserId(int $stavtr_created_by_user_id) Return ChildAvatar objects filtered by the stavtr_created_by_user_id column
 * @method     ChildAvatar[]|ObjectCollection findByUpdatedByUserId(int $stavtr_updated_by_user_id) Return ChildAvatar objects filtered by the stavtr_updated_by_user_id column
 * @method     ChildAvatar[]|ObjectCollection findByCreatedAt(string $stavtr_created_at) Return ChildAvatar objects filtered by the stavtr_created_at column
 * @method     ChildAvatar[]|ObjectCollection findByUpdatedAt(string $stavtr_updated_at) Return ChildAvatar objects filtered by the stavtr_updated_at column
 * @method     ChildAvatar[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AvatarQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\AvatarQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\Avatar', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAvatarQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAvatarQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAvatarQuery) {
            return $criteria;
        }
        $query = new ChildAvatarQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAvatar|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AvatarTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AvatarTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatar A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stavtr_id, stavtr_ststrm_id, stavtr_name, stavtr_code, stavtr_is_default, stavtr_can_be_owned, stavtr_main_color, stavtr_min_strank_id, stavtr_associated_strank_id, stavtr_created_by_user_id, stavtr_updated_by_user_id, stavtr_created_at, stavtr_updated_at FROM stream_avatar_stavtr WHERE stavtr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAvatar $obj */
            $obj = new ChildAvatar();
            $obj->hydrate($row);
            AvatarTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAvatar|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stavtr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stavtr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stavtr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stavtr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stavtr_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stavtr_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stavtr_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stavtr_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stavtr_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE stavtr_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE stavtr_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the stavtr_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE stavtr_code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE stavtr_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the stavtr_is_default column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefault(true); // WHERE stavtr_is_default = true
     * $query->filterByIsDefault('yes'); // WHERE stavtr_is_default = true
     * </code>
     *
     * @param     boolean|string $isDefault The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByIsDefault($isDefault = null, $comparison = null)
    {
        if (is_string($isDefault)) {
            $isDefault = in_array(strtolower($isDefault), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_IS_DEFAULT, $isDefault, $comparison);
    }

    /**
     * Filter the query on the stavtr_can_be_owned column
     *
     * Example usage:
     * <code>
     * $query->filterByCanBeOwned(true); // WHERE stavtr_can_be_owned = true
     * $query->filterByCanBeOwned('yes'); // WHERE stavtr_can_be_owned = true
     * </code>
     *
     * @param     boolean|string $canBeOwned The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByCanBeOwned($canBeOwned = null, $comparison = null)
    {
        if (is_string($canBeOwned)) {
            $canBeOwned = in_array(strtolower($canBeOwned), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CAN_BE_OWNED, $canBeOwned, $comparison);
    }

    /**
     * Filter the query on the stavtr_main_color column
     *
     * @param     mixed $mainColor The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByMainColor($mainColor = null, $comparison = null)
    {
        $valueSet = AvatarTableMap::getValueSet(AvatarTableMap::COL_STAVTR_MAIN_COLOR);
        if (is_scalar($mainColor)) {
            if (!in_array($mainColor, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $mainColor));
            }
            $mainColor = array_search($mainColor, $valueSet);
        } elseif (is_array($mainColor)) {
            $convertedValues = array();
            foreach ($mainColor as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $mainColor = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_MAIN_COLOR, $mainColor, $comparison);
    }

    /**
     * Filter the query on the stavtr_min_strank_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMinRankId(1234); // WHERE stavtr_min_strank_id = 1234
     * $query->filterByMinRankId(array(12, 34)); // WHERE stavtr_min_strank_id IN (12, 34)
     * $query->filterByMinRankId(array('min' => 12)); // WHERE stavtr_min_strank_id > 12
     * </code>
     *
     * @see       filterByMinRank()
     *
     * @param     mixed $minRankId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByMinRankId($minRankId = null, $comparison = null)
    {
        if (is_array($minRankId)) {
            $useMinMax = false;
            if (isset($minRankId['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, $minRankId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minRankId['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, $minRankId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, $minRankId, $comparison);
    }

    /**
     * Filter the query on the stavtr_associated_strank_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAssociatedRankId(1234); // WHERE stavtr_associated_strank_id = 1234
     * $query->filterByAssociatedRankId(array(12, 34)); // WHERE stavtr_associated_strank_id IN (12, 34)
     * $query->filterByAssociatedRankId(array('min' => 12)); // WHERE stavtr_associated_strank_id > 12
     * </code>
     *
     * @see       filterByAssociatedRank()
     *
     * @param     mixed $associatedRankId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByAssociatedRankId($associatedRankId = null, $comparison = null)
    {
        if (is_array($associatedRankId)) {
            $useMinMax = false;
            if (isset($associatedRankId['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, $associatedRankId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($associatedRankId['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, $associatedRankId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, $associatedRankId, $comparison);
    }

    /**
     * Filter the query on the stavtr_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE stavtr_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE stavtr_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE stavtr_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the stavtr_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE stavtr_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE stavtr_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE stavtr_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the stavtr_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE stavtr_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE stavtr_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE stavtr_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the stavtr_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE stavtr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE stavtr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE stavtr_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByMinRank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, $rank->getId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, $rank->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMinRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MinRank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinMinRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MinRank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MinRank');
        }

        return $this;
    }

    /**
     * Use the MinRank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useMinRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMinRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MinRank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByAssociatedRank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, $rank->getId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, $rank->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAssociatedRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AssociatedRank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinAssociatedRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AssociatedRank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AssociatedRank');
        }

        return $this;
    }

    /**
     * Use the AssociatedRank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useAssociatedRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAssociatedRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AssociatedRank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildAvatarQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $userExperience->getAvatarId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            return $this
                ->useUserExperienceQuery()
                ->filterByPrimaryKeys($userExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAvatar $avatar Object to remove from the list of results
     *
     * @return $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function prune($avatar = null)
    {
        if ($avatar) {
            $this->addUsingAlias(AvatarTableMap::COL_STAVTR_ID, $avatar->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_avatar_stavtr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AvatarTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AvatarTableMap::clearInstancePool();
            AvatarTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AvatarTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AvatarTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AvatarTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AvatarTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(AvatarTableMap::COL_STAVTR_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(AvatarTableMap::COL_STAVTR_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(AvatarTableMap::COL_STAVTR_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(AvatarTableMap::COL_STAVTR_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildAvatarQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(AvatarTableMap::COL_STAVTR_CREATED_AT);
    }

} // AvatarQuery

<?php

namespace IiMedias\StreamBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\StreamBundle\Model\Channel as ChildChannel;
use IiMedias\StreamBundle\Model\ChannelQuery as ChildChannelQuery;
use IiMedias\StreamBundle\Model\TwitchBot as ChildTwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery as ChildTwitchBotQuery;
use IiMedias\StreamBundle\Model\Map\ChannelTableMap;
use IiMedias\StreamBundle\Model\Map\TwitchBotTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'stream_twitch_bot_sttwbt' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class TwitchBot implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\TwitchBotTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the sttwbt_id field.
     *
     * @var        int
     */
    protected $sttwbt_id;

    /**
     * The value for the sttwbt_username field.
     *
     * @var        string
     */
    protected $sttwbt_username;

    /**
     * The value for the sttwbt_password field.
     *
     * @var        string
     */
    protected $sttwbt_password;

    /**
     * The value for the sttwbt_created_by_user_id field.
     *
     * @var        int
     */
    protected $sttwbt_created_by_user_id;

    /**
     * The value for the sttwbt_updated_by_user_id field.
     *
     * @var        int
     */
    protected $sttwbt_updated_by_user_id;

    /**
     * The value for the sttwbt_created_at field.
     *
     * @var        DateTime
     */
    protected $sttwbt_created_at;

    /**
     * The value for the sttwbt_updated_at field.
     *
     * @var        DateTime
     */
    protected $sttwbt_updated_at;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildChannel[] Collection to store aggregation of ChildChannel objects.
     */
    protected $collChannels;
    protected $collChannelsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChannel[]
     */
    protected $channelsScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\TwitchBot object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>TwitchBot</code> instance.  If
     * <code>obj</code> is an instance of <code>TwitchBot</code>, delegates to
     * <code>equals(TwitchBot)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|TwitchBot The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [sttwbt_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->sttwbt_id;
    }

    /**
     * Get the [sttwbt_username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->sttwbt_username;
    }

    /**
     * Get the [sttwbt_password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->sttwbt_password;
    }

    /**
     * Get the [sttwbt_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->sttwbt_created_by_user_id;
    }

    /**
     * Get the [sttwbt_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->sttwbt_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [sttwbt_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->sttwbt_created_at;
        } else {
            return $this->sttwbt_created_at instanceof \DateTimeInterface ? $this->sttwbt_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [sttwbt_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->sttwbt_updated_at;
        } else {
            return $this->sttwbt_updated_at instanceof \DateTimeInterface ? $this->sttwbt_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [sttwbt_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sttwbt_id !== $v) {
            $this->sttwbt_id = $v;
            $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [sttwbt_username] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sttwbt_username !== $v) {
            $this->sttwbt_username = $v;
            $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [sttwbt_password] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sttwbt_password !== $v) {
            $this->sttwbt_password = $v;
            $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [sttwbt_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sttwbt_created_by_user_id !== $v) {
            $this->sttwbt_created_by_user_id = $v;
            $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [sttwbt_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sttwbt_updated_by_user_id !== $v) {
            $this->sttwbt_updated_by_user_id = $v;
            $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [sttwbt_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->sttwbt_created_at !== null || $dt !== null) {
            if ($this->sttwbt_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->sttwbt_created_at->format("Y-m-d H:i:s.u")) {
                $this->sttwbt_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [sttwbt_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->sttwbt_updated_at !== null || $dt !== null) {
            if ($this->sttwbt_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->sttwbt_updated_at->format("Y-m-d H:i:s.u")) {
                $this->sttwbt_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : TwitchBotTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sttwbt_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : TwitchBotTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sttwbt_username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : TwitchBotTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sttwbt_password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : TwitchBotTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sttwbt_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : TwitchBotTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sttwbt_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : TwitchBotTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->sttwbt_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : TwitchBotTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->sttwbt_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = TwitchBotTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\TwitchBot'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCreatedByUser !== null && $this->sttwbt_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->sttwbt_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TwitchBotTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildTwitchBotQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collChannels = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see TwitchBot::setDeleted()
     * @see TwitchBot::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TwitchBotTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildTwitchBotQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TwitchBotTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(TwitchBotTableMap::COL_STTWBT_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TwitchBotTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->channelsScheduledForDeletion !== null) {
                if (!$this->channelsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChannelQuery::create()
                        ->filterByPrimaryKeys($this->channelsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->channelsScheduledForDeletion = null;
                }
            }

            if ($this->collChannels !== null) {
                foreach ($this->collChannels as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_ID] = true;
        if (null !== $this->sttwbt_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TwitchBotTableMap::COL_STTWBT_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_id';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_username';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_password';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_created_by_user_id';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_updated_by_user_id';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_created_at';
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'sttwbt_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO stream_twitch_bot_sttwbt (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'sttwbt_id':
                        $stmt->bindValue($identifier, $this->sttwbt_id, PDO::PARAM_INT);
                        break;
                    case 'sttwbt_username':
                        $stmt->bindValue($identifier, $this->sttwbt_username, PDO::PARAM_STR);
                        break;
                    case 'sttwbt_password':
                        $stmt->bindValue($identifier, $this->sttwbt_password, PDO::PARAM_STR);
                        break;
                    case 'sttwbt_created_by_user_id':
                        $stmt->bindValue($identifier, $this->sttwbt_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'sttwbt_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->sttwbt_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'sttwbt_created_at':
                        $stmt->bindValue($identifier, $this->sttwbt_created_at ? $this->sttwbt_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'sttwbt_updated_at':
                        $stmt->bindValue($identifier, $this->sttwbt_updated_at ? $this->sttwbt_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TwitchBotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getCreatedByUserId();
                break;
            case 4:
                return $this->getUpdatedByUserId();
                break;
            case 5:
                return $this->getCreatedAt();
                break;
            case 6:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['TwitchBot'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['TwitchBot'][$this->hashCode()] = true;
        $keys = TwitchBotTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getCreatedByUserId(),
            $keys[4] => $this->getUpdatedByUserId(),
            $keys[5] => $this->getCreatedAt(),
            $keys[6] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collChannels) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'channels';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_channel_stchans';
                        break;
                    default:
                        $key = 'Channels';
                }

                $result[$key] = $this->collChannels->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TwitchBotTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setCreatedByUserId($value);
                break;
            case 4:
                $this->setUpdatedByUserId($value);
                break;
            case 5:
                $this->setCreatedAt($value);
                break;
            case 6:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = TwitchBotTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPassword($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCreatedByUserId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCreatedAt($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUpdatedAt($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TwitchBotTableMap::DATABASE_NAME);

        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_ID)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_ID, $this->sttwbt_id);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_USERNAME)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_USERNAME, $this->sttwbt_username);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_PASSWORD)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_PASSWORD, $this->sttwbt_password);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_CREATED_BY_USER_ID)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_CREATED_BY_USER_ID, $this->sttwbt_created_by_user_id);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_BY_USER_ID)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_UPDATED_BY_USER_ID, $this->sttwbt_updated_by_user_id);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_CREATED_AT)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_CREATED_AT, $this->sttwbt_created_at);
        }
        if ($this->isColumnModified(TwitchBotTableMap::COL_STTWBT_UPDATED_AT)) {
            $criteria->add(TwitchBotTableMap::COL_STTWBT_UPDATED_AT, $this->sttwbt_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildTwitchBotQuery::create();
        $criteria->add(TwitchBotTableMap::COL_STTWBT_ID, $this->sttwbt_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (sttwbt_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\TwitchBot (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getChannels() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChannel($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\TwitchBot Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserSttwbt($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->sttwbt_created_by_user_id !== null)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->sttwbt_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserSttwbts($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserSttwbt($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->sttwbt_updated_by_user_id !== null)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->sttwbt_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserSttwbts($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Channel' == $relationName) {
            return $this->initChannels();
        }
    }

    /**
     * Clears out the collChannels collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChannels()
     */
    public function clearChannels()
    {
        $this->collChannels = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChannels collection loaded partially.
     */
    public function resetPartialChannels($v = true)
    {
        $this->collChannelsPartial = $v;
    }

    /**
     * Initializes the collChannels collection.
     *
     * By default this just sets the collChannels collection to an empty array (like clearcollChannels());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChannels($overrideExisting = true)
    {
        if (null !== $this->collChannels && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChannelTableMap::getTableMap()->getCollectionClassName();

        $this->collChannels = new $collectionClassName;
        $this->collChannels->setModel('\IiMedias\StreamBundle\Model\Channel');
    }

    /**
     * Gets an array of ChildChannel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTwitchBot is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChannel[] List of ChildChannel objects
     * @throws PropelException
     */
    public function getChannels(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChannelsPartial && !$this->isNew();
        if (null === $this->collChannels || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChannels) {
                // return empty collection
                $this->initChannels();
            } else {
                $collChannels = ChildChannelQuery::create(null, $criteria)
                    ->filterByTwitchBot($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChannelsPartial && count($collChannels)) {
                        $this->initChannels(false);

                        foreach ($collChannels as $obj) {
                            if (false == $this->collChannels->contains($obj)) {
                                $this->collChannels->append($obj);
                            }
                        }

                        $this->collChannelsPartial = true;
                    }

                    return $collChannels;
                }

                if ($partial && $this->collChannels) {
                    foreach ($this->collChannels as $obj) {
                        if ($obj->isNew()) {
                            $collChannels[] = $obj;
                        }
                    }
                }

                $this->collChannels = $collChannels;
                $this->collChannelsPartial = false;
            }
        }

        return $this->collChannels;
    }

    /**
     * Sets a collection of ChildChannel objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $channels A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTwitchBot The current object (for fluent API support)
     */
    public function setChannels(Collection $channels, ConnectionInterface $con = null)
    {
        /** @var ChildChannel[] $channelsToDelete */
        $channelsToDelete = $this->getChannels(new Criteria(), $con)->diff($channels);


        $this->channelsScheduledForDeletion = $channelsToDelete;

        foreach ($channelsToDelete as $channelRemoved) {
            $channelRemoved->setTwitchBot(null);
        }

        $this->collChannels = null;
        foreach ($channels as $channel) {
            $this->addChannel($channel);
        }

        $this->collChannels = $channels;
        $this->collChannelsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Channel objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Channel objects.
     * @throws PropelException
     */
    public function countChannels(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChannelsPartial && !$this->isNew();
        if (null === $this->collChannels || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChannels) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChannels());
            }

            $query = ChildChannelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTwitchBot($this)
                ->count($con);
        }

        return count($this->collChannels);
    }

    /**
     * Method called to associate a ChildChannel object to this object
     * through the ChildChannel foreign key attribute.
     *
     * @param  ChildChannel $l ChildChannel
     * @return $this|\IiMedias\StreamBundle\Model\TwitchBot The current object (for fluent API support)
     */
    public function addChannel(ChildChannel $l)
    {
        if ($this->collChannels === null) {
            $this->initChannels();
            $this->collChannelsPartial = true;
        }

        if (!$this->collChannels->contains($l)) {
            $this->doAddChannel($l);

            if ($this->channelsScheduledForDeletion and $this->channelsScheduledForDeletion->contains($l)) {
                $this->channelsScheduledForDeletion->remove($this->channelsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChannel $channel The ChildChannel object to add.
     */
    protected function doAddChannel(ChildChannel $channel)
    {
        $this->collChannels[]= $channel;
        $channel->setTwitchBot($this);
    }

    /**
     * @param  ChildChannel $channel The ChildChannel object to remove.
     * @return $this|ChildTwitchBot The current object (for fluent API support)
     */
    public function removeChannel(ChildChannel $channel)
    {
        if ($this->getChannels()->contains($channel)) {
            $pos = $this->collChannels->search($channel);
            $this->collChannels->remove($pos);
            if (null === $this->channelsScheduledForDeletion) {
                $this->channelsScheduledForDeletion = clone $this->collChannels;
                $this->channelsScheduledForDeletion->clear();
            }
            $this->channelsScheduledForDeletion[]= $channel;
            $channel->setTwitchBot(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TwitchBot is new, it will return
     * an empty collection; or if this TwitchBot has previously
     * been saved, it will retrieve related Channels from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TwitchBot.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChannel[] List of ChildChannel objects
     */
    public function getChannelsJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChannelQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getChannels($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TwitchBot is new, it will return
     * an empty collection; or if this TwitchBot has previously
     * been saved, it will retrieve related Channels from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TwitchBot.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChannel[] List of ChildChannel objects
     */
    public function getChannelsJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChannelQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getChannels($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TwitchBot is new, it will return
     * an empty collection; or if this TwitchBot has previously
     * been saved, it will retrieve related Channels from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TwitchBot.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChannel[] List of ChildChannel objects
     */
    public function getChannelsJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChannelQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getChannels($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserSttwbt($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserSttwbt($this);
        }
        $this->sttwbt_id = null;
        $this->sttwbt_username = null;
        $this->sttwbt_password = null;
        $this->sttwbt_created_by_user_id = null;
        $this->sttwbt_updated_by_user_id = null;
        $this->sttwbt_created_at = null;
        $this->sttwbt_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChannels) {
                foreach ($this->collChannels as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collChannels = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TwitchBotTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildTwitchBot The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[TwitchBotTableMap::COL_STTWBT_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

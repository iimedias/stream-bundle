<?php

namespace IiMedias\StreamBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\StreamBundle\Model\ChatUser as ChildChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery as ChildChatUserQuery;
use IiMedias\StreamBundle\Model\ChatterExperience as ChildChatterExperience;
use IiMedias\StreamBundle\Model\ChatterExperienceQuery as ChildChatterExperienceQuery;
use IiMedias\StreamBundle\Model\DeepBotImportExperience as ChildDeepBotImportExperience;
use IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery as ChildDeepBotImportExperienceQuery;
use IiMedias\StreamBundle\Model\Experience as ChildExperience;
use IiMedias\StreamBundle\Model\ExperienceQuery as ChildExperienceQuery;
use IiMedias\StreamBundle\Model\FollowExperience as ChildFollowExperience;
use IiMedias\StreamBundle\Model\FollowExperienceQuery as ChildFollowExperienceQuery;
use IiMedias\StreamBundle\Model\HostExperience as ChildHostExperience;
use IiMedias\StreamBundle\Model\HostExperienceQuery as ChildHostExperienceQuery;
use IiMedias\StreamBundle\Model\MessageExperience as ChildMessageExperience;
use IiMedias\StreamBundle\Model\MessageExperienceQuery as ChildMessageExperienceQuery;
use IiMedias\StreamBundle\Model\Site as ChildSite;
use IiMedias\StreamBundle\Model\SiteQuery as ChildSiteQuery;
use IiMedias\StreamBundle\Model\UserExperience as ChildUserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery as ChildUserExperienceQuery;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;
use IiMedias\StreamBundle\Model\Map\ChatterExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\DeepBotImportExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\ExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\FollowExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\HostExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\MessageExperienceTableMap;
use IiMedias\StreamBundle\Model\Map\UserExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'stream_chat_user_stcusr' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class ChatUser implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\ChatUserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the stcusr_id field.
     *
     * @var        int
     */
    protected $stcusr_id;

    /**
     * The value for the stcusr_parent_id field.
     *
     * @var        int
     */
    protected $stcusr_parent_id;

    /**
     * The value for the stcusr_stsite_id field.
     *
     * @var        int
     */
    protected $stcusr_stsite_id;

    /**
     * The value for the stcusr_username field.
     *
     * @var        string
     */
    protected $stcusr_username;

    /**
     * The value for the stcusr_display_name field.
     *
     * @var        string
     */
    protected $stcusr_display_name;

    /**
     * The value for the stcusr_logo_url field.
     *
     * @var        string
     */
    protected $stcusr_logo_url;

    /**
     * The value for the stcusr_timeout_level field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $stcusr_timeout_level;

    /**
     * The value for the stcusr_last_timeout_at field.
     *
     * @var        DateTime
     */
    protected $stcusr_last_timeout_at;

    /**
     * The value for the stcusr_registered_at field.
     *
     * @var        DateTime
     */
    protected $stcusr_registered_at;

    /**
     * The value for the stcusr_is_deleted_account field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $stcusr_is_deleted_account;

    /**
     * The value for the stcusr_can_rescan field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $stcusr_can_rescan;

    /**
     * The value for the stcusr_created_by_user_id field.
     *
     * @var        int
     */
    protected $stcusr_created_by_user_id;

    /**
     * The value for the stcusr_updated_by_user_id field.
     *
     * @var        int
     */
    protected $stcusr_updated_by_user_id;

    /**
     * The value for the stcusr_created_at field.
     *
     * @var        DateTime
     */
    protected $stcusr_created_at;

    /**
     * The value for the stcusr_updated_at field.
     *
     * @var        DateTime
     */
    protected $stcusr_updated_at;

    /**
     * @var        ChildChatUser
     */
    protected $aParent;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildChatUser[] Collection to store aggregation of ChildChatUser objects.
     */
    protected $collChildren;
    protected $collChildrenPartial;

    /**
     * @var        ObjectCollection|ChildUserExperience[] Collection to store aggregation of ChildUserExperience objects.
     */
    protected $collUserExperiences;
    protected $collUserExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildDeepBotImportExperience[] Collection to store aggregation of ChildDeepBotImportExperience objects.
     */
    protected $collDeepBotImportExperiences;
    protected $collDeepBotImportExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildMessageExperience[] Collection to store aggregation of ChildMessageExperience objects.
     */
    protected $collMessageExperiences;
    protected $collMessageExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildChatterExperience[] Collection to store aggregation of ChildChatterExperience objects.
     */
    protected $collChatterExperiences;
    protected $collChatterExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildFollowExperience[] Collection to store aggregation of ChildFollowExperience objects.
     */
    protected $collFollowExperiences;
    protected $collFollowExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildHostExperience[] Collection to store aggregation of ChildHostExperience objects.
     */
    protected $collHostExperiences;
    protected $collHostExperiencesPartial;

    /**
     * @var        ObjectCollection|ChildExperience[] Collection to store aggregation of ChildExperience objects.
     */
    protected $collExperiences;
    protected $collExperiencesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChatUser[]
     */
    protected $childrenScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserExperience[]
     */
    protected $userExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDeepBotImportExperience[]
     */
    protected $deepBotImportExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMessageExperience[]
     */
    protected $messageExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChatterExperience[]
     */
    protected $chatterExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFollowExperience[]
     */
    protected $followExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildHostExperience[]
     */
    protected $hostExperiencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildExperience[]
     */
    protected $experiencesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->stcusr_timeout_level = 0;
        $this->stcusr_is_deleted_account = false;
        $this->stcusr_can_rescan = true;
    }

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\ChatUser object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ChatUser</code> instance.  If
     * <code>obj</code> is an instance of <code>ChatUser</code>, delegates to
     * <code>equals(ChatUser)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ChatUser The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [stcusr_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->stcusr_id;
    }

    /**
     * Get the [stcusr_parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->stcusr_parent_id;
    }

    /**
     * Get the [stcusr_stsite_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->stcusr_stsite_id;
    }

    /**
     * Get the [stcusr_username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->stcusr_username;
    }

    /**
     * Get the [stcusr_display_name] column value.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->stcusr_display_name;
    }

    /**
     * Get the [stcusr_logo_url] column value.
     *
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->stcusr_logo_url;
    }

    /**
     * Get the [stcusr_timeout_level] column value.
     *
     * @return int
     */
    public function getTimeoutLevel()
    {
        return $this->stcusr_timeout_level;
    }

    /**
     * Get the [optionally formatted] temporal [stcusr_last_timeout_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastTimeoutAt($format = NULL)
    {
        if ($format === null) {
            return $this->stcusr_last_timeout_at;
        } else {
            return $this->stcusr_last_timeout_at instanceof \DateTimeInterface ? $this->stcusr_last_timeout_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [stcusr_registered_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRegisteredAt($format = NULL)
    {
        if ($format === null) {
            return $this->stcusr_registered_at;
        } else {
            return $this->stcusr_registered_at instanceof \DateTimeInterface ? $this->stcusr_registered_at->format($format) : null;
        }
    }

    /**
     * Get the [stcusr_is_deleted_account] column value.
     *
     * @return boolean
     */
    public function getIsDeletedAccount()
    {
        return $this->stcusr_is_deleted_account;
    }

    /**
     * Get the [stcusr_is_deleted_account] column value.
     *
     * @return boolean
     */
    public function isDeletedAccount()
    {
        return $this->getIsDeletedAccount();
    }

    /**
     * Get the [stcusr_can_rescan] column value.
     *
     * @return boolean
     */
    public function getCanRescan()
    {
        return $this->stcusr_can_rescan;
    }

    /**
     * Get the [stcusr_can_rescan] column value.
     *
     * @return boolean
     */
    public function isCanRescan()
    {
        return $this->getCanRescan();
    }

    /**
     * Get the [stcusr_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->stcusr_created_by_user_id;
    }

    /**
     * Get the [stcusr_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->stcusr_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [stcusr_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->stcusr_created_at;
        } else {
            return $this->stcusr_created_at instanceof \DateTimeInterface ? $this->stcusr_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [stcusr_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->stcusr_updated_at;
        } else {
            return $this->stcusr_updated_at instanceof \DateTimeInterface ? $this->stcusr_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [stcusr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_id !== $v) {
            $this->stcusr_id = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [stcusr_parent_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_parent_id !== $v) {
            $this->stcusr_parent_id = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_PARENT_ID] = true;
        }

        if ($this->aParent !== null && $this->aParent->getId() !== $v) {
            $this->aParent = null;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [stcusr_stsite_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_stsite_id !== $v) {
            $this->stcusr_stsite_id = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_STSITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [stcusr_username] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stcusr_username !== $v) {
            $this->stcusr_username = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [stcusr_display_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setDisplayName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stcusr_display_name !== $v) {
            $this->stcusr_display_name = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_DISPLAY_NAME] = true;
        }

        return $this;
    } // setDisplayName()

    /**
     * Set the value of [stcusr_logo_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setLogoUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stcusr_logo_url !== $v) {
            $this->stcusr_logo_url = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_LOGO_URL] = true;
        }

        return $this;
    } // setLogoUrl()

    /**
     * Set the value of [stcusr_timeout_level] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setTimeoutLevel($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_timeout_level !== $v) {
            $this->stcusr_timeout_level = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL] = true;
        }

        return $this;
    } // setTimeoutLevel()

    /**
     * Sets the value of [stcusr_last_timeout_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setLastTimeoutAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stcusr_last_timeout_at !== null || $dt !== null) {
            if ($this->stcusr_last_timeout_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stcusr_last_timeout_at->format("Y-m-d H:i:s.u")) {
                $this->stcusr_last_timeout_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setLastTimeoutAt()

    /**
     * Sets the value of [stcusr_registered_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setRegisteredAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stcusr_registered_at !== null || $dt !== null) {
            if ($this->stcusr_registered_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stcusr_registered_at->format("Y-m-d H:i:s.u")) {
                $this->stcusr_registered_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_REGISTERED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setRegisteredAt()

    /**
     * Sets the value of the [stcusr_is_deleted_account] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setIsDeletedAccount($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stcusr_is_deleted_account !== $v) {
            $this->stcusr_is_deleted_account = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT] = true;
        }

        return $this;
    } // setIsDeletedAccount()

    /**
     * Sets the value of the [stcusr_can_rescan] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setCanRescan($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stcusr_can_rescan !== $v) {
            $this->stcusr_can_rescan = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_CAN_RESCAN] = true;
        }

        return $this;
    } // setCanRescan()

    /**
     * Set the value of [stcusr_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_created_by_user_id !== $v) {
            $this->stcusr_created_by_user_id = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [stcusr_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stcusr_updated_by_user_id !== $v) {
            $this->stcusr_updated_by_user_id = $v;
            $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [stcusr_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stcusr_created_at !== null || $dt !== null) {
            if ($this->stcusr_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stcusr_created_at->format("Y-m-d H:i:s.u")) {
                $this->stcusr_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [stcusr_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->stcusr_updated_at !== null || $dt !== null) {
            if ($this->stcusr_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->stcusr_updated_at->format("Y-m-d H:i:s.u")) {
                $this->stcusr_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->stcusr_timeout_level !== 0) {
                return false;
            }

            if ($this->stcusr_is_deleted_account !== false) {
                return false;
            }

            if ($this->stcusr_can_rescan !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ChatUserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ChatUserTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ChatUserTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_stsite_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ChatUserTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ChatUserTableMap::translateFieldName('DisplayName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_display_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ChatUserTableMap::translateFieldName('LogoUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_logo_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ChatUserTableMap::translateFieldName('TimeoutLevel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_timeout_level = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ChatUserTableMap::translateFieldName('LastTimeoutAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stcusr_last_timeout_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ChatUserTableMap::translateFieldName('RegisteredAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stcusr_registered_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ChatUserTableMap::translateFieldName('IsDeletedAccount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_is_deleted_account = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ChatUserTableMap::translateFieldName('CanRescan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_can_rescan = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ChatUserTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ChatUserTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stcusr_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ChatUserTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stcusr_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ChatUserTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->stcusr_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = ChatUserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\ChatUser'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aParent !== null && $this->stcusr_parent_id !== $this->aParent->getId()) {
            $this->aParent = null;
        }
        if ($this->aSite !== null && $this->stcusr_stsite_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
        if ($this->aCreatedByUser !== null && $this->stcusr_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->stcusr_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatUserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildChatUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aParent = null;
            $this->aSite = null;
            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collChildren = null;

            $this->collUserExperiences = null;

            $this->collDeepBotImportExperiences = null;

            $this->collMessageExperiences = null;

            $this->collChatterExperiences = null;

            $this->collFollowExperiences = null;

            $this->collHostExperiences = null;

            $this->collExperiences = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ChatUser::setDeleted()
     * @see ChatUser::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildChatUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ChatUserTableMap::COL_STCUSR_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChatUserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aParent !== null) {
                if ($this->aParent->isModified() || $this->aParent->isNew()) {
                    $affectedRows += $this->aParent->save($con);
                }
                $this->setParent($this->aParent);
            }

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->childrenScheduledForDeletion !== null) {
                if (!$this->childrenScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatUserQuery::create()
                        ->filterByPrimaryKeys($this->childrenScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->childrenScheduledForDeletion = null;
                }
            }

            if ($this->collChildren !== null) {
                foreach ($this->collChildren as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userExperiencesScheduledForDeletion !== null) {
                if (!$this->userExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\UserExperienceQuery::create()
                        ->filterByPrimaryKeys($this->userExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collUserExperiences !== null) {
                foreach ($this->collUserExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->deepBotImportExperiencesScheduledForDeletion !== null) {
                if (!$this->deepBotImportExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery::create()
                        ->filterByPrimaryKeys($this->deepBotImportExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->deepBotImportExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collDeepBotImportExperiences !== null) {
                foreach ($this->collDeepBotImportExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->messageExperiencesScheduledForDeletion !== null) {
                if (!$this->messageExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\MessageExperienceQuery::create()
                        ->filterByPrimaryKeys($this->messageExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->messageExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collMessageExperiences !== null) {
                foreach ($this->collMessageExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->chatterExperiencesScheduledForDeletion !== null) {
                if (!$this->chatterExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatterExperienceQuery::create()
                        ->filterByPrimaryKeys($this->chatterExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->chatterExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collChatterExperiences !== null) {
                foreach ($this->collChatterExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->followExperiencesScheduledForDeletion !== null) {
                if (!$this->followExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\FollowExperienceQuery::create()
                        ->filterByPrimaryKeys($this->followExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->followExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collFollowExperiences !== null) {
                foreach ($this->collFollowExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->hostExperiencesScheduledForDeletion !== null) {
                if (!$this->hostExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\HostExperienceQuery::create()
                        ->filterByPrimaryKeys($this->hostExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->hostExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collHostExperiences !== null) {
                foreach ($this->collHostExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->experiencesScheduledForDeletion !== null) {
                if (!$this->experiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ExperienceQuery::create()
                        ->filterByPrimaryKeys($this->experiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->experiencesScheduledForDeletion = null;
                }
            }

            if ($this->collExperiences !== null) {
                foreach ($this->collExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_ID] = true;
        if (null !== $this->stcusr_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ChatUserTableMap::COL_STCUSR_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_id';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_parent_id';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_STSITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_stsite_id';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_username';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_DISPLAY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_display_name';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_LOGO_URL)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_logo_url';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_timeout_level';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_last_timeout_at';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_REGISTERED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_registered_at';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_is_deleted_account';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CAN_RESCAN)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_can_rescan';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_created_by_user_id';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_updated_by_user_id';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_created_at';
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'stcusr_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO stream_chat_user_stcusr (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'stcusr_id':
                        $stmt->bindValue($identifier, $this->stcusr_id, PDO::PARAM_INT);
                        break;
                    case 'stcusr_parent_id':
                        $stmt->bindValue($identifier, $this->stcusr_parent_id, PDO::PARAM_INT);
                        break;
                    case 'stcusr_stsite_id':
                        $stmt->bindValue($identifier, $this->stcusr_stsite_id, PDO::PARAM_INT);
                        break;
                    case 'stcusr_username':
                        $stmt->bindValue($identifier, $this->stcusr_username, PDO::PARAM_STR);
                        break;
                    case 'stcusr_display_name':
                        $stmt->bindValue($identifier, $this->stcusr_display_name, PDO::PARAM_STR);
                        break;
                    case 'stcusr_logo_url':
                        $stmt->bindValue($identifier, $this->stcusr_logo_url, PDO::PARAM_STR);
                        break;
                    case 'stcusr_timeout_level':
                        $stmt->bindValue($identifier, $this->stcusr_timeout_level, PDO::PARAM_INT);
                        break;
                    case 'stcusr_last_timeout_at':
                        $stmt->bindValue($identifier, $this->stcusr_last_timeout_at ? $this->stcusr_last_timeout_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'stcusr_registered_at':
                        $stmt->bindValue($identifier, $this->stcusr_registered_at ? $this->stcusr_registered_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'stcusr_is_deleted_account':
                        $stmt->bindValue($identifier, (int) $this->stcusr_is_deleted_account, PDO::PARAM_INT);
                        break;
                    case 'stcusr_can_rescan':
                        $stmt->bindValue($identifier, (int) $this->stcusr_can_rescan, PDO::PARAM_INT);
                        break;
                    case 'stcusr_created_by_user_id':
                        $stmt->bindValue($identifier, $this->stcusr_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'stcusr_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->stcusr_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'stcusr_created_at':
                        $stmt->bindValue($identifier, $this->stcusr_created_at ? $this->stcusr_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'stcusr_updated_at':
                        $stmt->bindValue($identifier, $this->stcusr_updated_at ? $this->stcusr_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ChatUserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getParentId();
                break;
            case 2:
                return $this->getSiteId();
                break;
            case 3:
                return $this->getUsername();
                break;
            case 4:
                return $this->getDisplayName();
                break;
            case 5:
                return $this->getLogoUrl();
                break;
            case 6:
                return $this->getTimeoutLevel();
                break;
            case 7:
                return $this->getLastTimeoutAt();
                break;
            case 8:
                return $this->getRegisteredAt();
                break;
            case 9:
                return $this->getIsDeletedAccount();
                break;
            case 10:
                return $this->getCanRescan();
                break;
            case 11:
                return $this->getCreatedByUserId();
                break;
            case 12:
                return $this->getUpdatedByUserId();
                break;
            case 13:
                return $this->getCreatedAt();
                break;
            case 14:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ChatUser'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ChatUser'][$this->hashCode()] = true;
        $keys = ChatUserTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getParentId(),
            $keys[2] => $this->getSiteId(),
            $keys[3] => $this->getUsername(),
            $keys[4] => $this->getDisplayName(),
            $keys[5] => $this->getLogoUrl(),
            $keys[6] => $this->getTimeoutLevel(),
            $keys[7] => $this->getLastTimeoutAt(),
            $keys[8] => $this->getRegisteredAt(),
            $keys[9] => $this->getIsDeletedAccount(),
            $keys[10] => $this->getCanRescan(),
            $keys[11] => $this->getCreatedByUserId(),
            $keys[12] => $this->getUpdatedByUserId(),
            $keys[13] => $this->getCreatedAt(),
            $keys[14] => $this->getUpdatedAt(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aParent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatUser';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chat_user_stcusr';
                        break;
                    default:
                        $key = 'Parent';
                }

                $result[$key] = $this->aParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsite';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collChildren) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chat_user_stcusrs';
                        break;
                    default:
                        $key = 'Children';
                }

                $result[$key] = $this->collChildren->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_user_experience_stuexps';
                        break;
                    default:
                        $key = 'UserExperiences';
                }

                $result[$key] = $this->collUserExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDeepBotImportExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'deepBotImportExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_deepbot_import_experience_stdbies';
                        break;
                    default:
                        $key = 'DeepBotImportExperiences';
                }

                $result[$key] = $this->collDeepBotImportExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMessageExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'messageExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_message_experience_stmexps';
                        break;
                    default:
                        $key = 'MessageExperiences';
                }

                $result[$key] = $this->collMessageExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChatterExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatterExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chatter_experience_stcexps';
                        break;
                    default:
                        $key = 'ChatterExperiences';
                }

                $result[$key] = $this->collChatterExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFollowExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'followExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_follow_experience_stfexps';
                        break;
                    default:
                        $key = 'FollowExperiences';
                }

                $result[$key] = $this->collFollowExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collHostExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'hostExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_host_experience_sthexps';
                        break;
                    default:
                        $key = 'HostExperiences';
                }

                $result[$key] = $this->collHostExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'experiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_experience_stexprs';
                        break;
                    default:
                        $key = 'Experiences';
                }

                $result[$key] = $this->collExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ChatUserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setParentId($value);
                break;
            case 2:
                $this->setSiteId($value);
                break;
            case 3:
                $this->setUsername($value);
                break;
            case 4:
                $this->setDisplayName($value);
                break;
            case 5:
                $this->setLogoUrl($value);
                break;
            case 6:
                $this->setTimeoutLevel($value);
                break;
            case 7:
                $this->setLastTimeoutAt($value);
                break;
            case 8:
                $this->setRegisteredAt($value);
                break;
            case 9:
                $this->setIsDeletedAccount($value);
                break;
            case 10:
                $this->setCanRescan($value);
                break;
            case 11:
                $this->setCreatedByUserId($value);
                break;
            case 12:
                $this->setUpdatedByUserId($value);
                break;
            case 13:
                $this->setCreatedAt($value);
                break;
            case 14:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ChatUserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setParentId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSiteId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setUsername($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDisplayName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLogoUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTimeoutLevel($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setLastTimeoutAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRegisteredAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsDeletedAccount($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCanRescan($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCreatedByUserId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCreatedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUpdatedAt($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChatUserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_ID)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_ID, $this->stcusr_id);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_PARENT_ID)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_PARENT_ID, $this->stcusr_parent_id);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_STSITE_ID)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_STSITE_ID, $this->stcusr_stsite_id);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_USERNAME)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_USERNAME, $this->stcusr_username);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_DISPLAY_NAME)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_DISPLAY_NAME, $this->stcusr_display_name);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_LOGO_URL)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_LOGO_URL, $this->stcusr_logo_url);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL, $this->stcusr_timeout_level);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT, $this->stcusr_last_timeout_at);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_REGISTERED_AT)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_REGISTERED_AT, $this->stcusr_registered_at);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT, $this->stcusr_is_deleted_account);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CAN_RESCAN)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_CAN_RESCAN, $this->stcusr_can_rescan);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, $this->stcusr_created_by_user_id);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, $this->stcusr_updated_by_user_id);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_CREATED_AT)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_CREATED_AT, $this->stcusr_created_at);
        }
        if ($this->isColumnModified(ChatUserTableMap::COL_STCUSR_UPDATED_AT)) {
            $criteria->add(ChatUserTableMap::COL_STCUSR_UPDATED_AT, $this->stcusr_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildChatUserQuery::create();
        $criteria->add(ChatUserTableMap::COL_STCUSR_ID, $this->stcusr_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (stcusr_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\ChatUser (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setParentId($this->getParentId());
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setDisplayName($this->getDisplayName());
        $copyObj->setLogoUrl($this->getLogoUrl());
        $copyObj->setTimeoutLevel($this->getTimeoutLevel());
        $copyObj->setLastTimeoutAt($this->getLastTimeoutAt());
        $copyObj->setRegisteredAt($this->getRegisteredAt());
        $copyObj->setIsDeletedAccount($this->getIsDeletedAccount());
        $copyObj->setCanRescan($this->getCanRescan());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getChildren() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChild($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDeepBotImportExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDeepBotImportExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMessageExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMessageExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChatterExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChatterExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFollowExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFollowExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getHostExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addHostExperience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addExperience($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\ChatUser Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildChatUser object.
     *
     * @param  ChildChatUser $v
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     * @throws PropelException
     */
    public function setParent(ChildChatUser $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildChatUser object, it will not be re-added.
        if ($v !== null) {
            $v->addChild($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildChatUser object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildChatUser The associated ChildChatUser object.
     * @throws PropelException
     */
    public function getParent(ConnectionInterface $con = null)
    {
        if ($this->aParent === null && ($this->stcusr_parent_id != 0)) {
            $this->aParent = ChildChatUserQuery::create()->findPk($this->stcusr_parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aParent->addChildren($this);
             */
        }

        return $this->aParent;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addChatUser($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->stcusr_stsite_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->stcusr_stsite_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addChatUsers($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserStcusr($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->stcusr_created_by_user_id != 0)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->stcusr_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserStcusrs($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserStcusr($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->stcusr_updated_by_user_id != 0)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->stcusr_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserStcusrs($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Child' == $relationName) {
            $this->initChildren();
            return;
        }
        if ('UserExperience' == $relationName) {
            $this->initUserExperiences();
            return;
        }
        if ('DeepBotImportExperience' == $relationName) {
            $this->initDeepBotImportExperiences();
            return;
        }
        if ('MessageExperience' == $relationName) {
            $this->initMessageExperiences();
            return;
        }
        if ('ChatterExperience' == $relationName) {
            $this->initChatterExperiences();
            return;
        }
        if ('FollowExperience' == $relationName) {
            $this->initFollowExperiences();
            return;
        }
        if ('HostExperience' == $relationName) {
            $this->initHostExperiences();
            return;
        }
        if ('Experience' == $relationName) {
            $this->initExperiences();
            return;
        }
    }

    /**
     * Clears out the collChildren collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChildren()
     */
    public function clearChildren()
    {
        $this->collChildren = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChildren collection loaded partially.
     */
    public function resetPartialChildren($v = true)
    {
        $this->collChildrenPartial = $v;
    }

    /**
     * Initializes the collChildren collection.
     *
     * By default this just sets the collChildren collection to an empty array (like clearcollChildren());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChildren($overrideExisting = true)
    {
        if (null !== $this->collChildren && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatUserTableMap::getTableMap()->getCollectionClassName();

        $this->collChildren = new $collectionClassName;
        $this->collChildren->setModel('\IiMedias\StreamBundle\Model\ChatUser');
    }

    /**
     * Gets an array of ChildChatUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChatUser[] List of ChildChatUser objects
     * @throws PropelException
     */
    public function getChildren(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChildrenPartial && !$this->isNew();
        if (null === $this->collChildren || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChildren) {
                // return empty collection
                $this->initChildren();
            } else {
                $collChildren = ChildChatUserQuery::create(null, $criteria)
                    ->filterByParent($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChildrenPartial && count($collChildren)) {
                        $this->initChildren(false);

                        foreach ($collChildren as $obj) {
                            if (false == $this->collChildren->contains($obj)) {
                                $this->collChildren->append($obj);
                            }
                        }

                        $this->collChildrenPartial = true;
                    }

                    return $collChildren;
                }

                if ($partial && $this->collChildren) {
                    foreach ($this->collChildren as $obj) {
                        if ($obj->isNew()) {
                            $collChildren[] = $obj;
                        }
                    }
                }

                $this->collChildren = $collChildren;
                $this->collChildrenPartial = false;
            }
        }

        return $this->collChildren;
    }

    /**
     * Sets a collection of ChildChatUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $children A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setChildren(Collection $children, ConnectionInterface $con = null)
    {
        /** @var ChildChatUser[] $childrenToDelete */
        $childrenToDelete = $this->getChildren(new Criteria(), $con)->diff($children);


        $this->childrenScheduledForDeletion = $childrenToDelete;

        foreach ($childrenToDelete as $childRemoved) {
            $childRemoved->setParent(null);
        }

        $this->collChildren = null;
        foreach ($children as $child) {
            $this->addChild($child);
        }

        $this->collChildren = $children;
        $this->collChildrenPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChatUser objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ChatUser objects.
     * @throws PropelException
     */
    public function countChildren(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChildrenPartial && !$this->isNew();
        if (null === $this->collChildren || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChildren) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChildren());
            }

            $query = ChildChatUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByParent($this)
                ->count($con);
        }

        return count($this->collChildren);
    }

    /**
     * Method called to associate a ChildChatUser object to this object
     * through the ChildChatUser foreign key attribute.
     *
     * @param  ChildChatUser $l ChildChatUser
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addChild(ChildChatUser $l)
    {
        if ($this->collChildren === null) {
            $this->initChildren();
            $this->collChildrenPartial = true;
        }

        if (!$this->collChildren->contains($l)) {
            $this->doAddChild($l);

            if ($this->childrenScheduledForDeletion and $this->childrenScheduledForDeletion->contains($l)) {
                $this->childrenScheduledForDeletion->remove($this->childrenScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChatUser $child The ChildChatUser object to add.
     */
    protected function doAddChild(ChildChatUser $child)
    {
        $this->collChildren[]= $child;
        $child->setParent($this);
    }

    /**
     * @param  ChildChatUser $child The ChildChatUser object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeChild(ChildChatUser $child)
    {
        if ($this->getChildren()->contains($child)) {
            $pos = $this->collChildren->search($child);
            $this->collChildren->remove($pos);
            if (null === $this->childrenScheduledForDeletion) {
                $this->childrenScheduledForDeletion = clone $this->collChildren;
                $this->childrenScheduledForDeletion->clear();
            }
            $this->childrenScheduledForDeletion[]= $child;
            $child->setParent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Children from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatUser[] List of ChildChatUser objects
     */
    public function getChildrenJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatUserQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getChildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Children from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatUser[] List of ChildChatUser objects
     */
    public function getChildrenJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatUserQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getChildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Children from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatUser[] List of ChildChatUser objects
     */
    public function getChildrenJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatUserQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getChildren($query, $con);
    }

    /**
     * Clears out the collUserExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserExperiences()
     */
    public function clearUserExperiences()
    {
        $this->collUserExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserExperiences collection loaded partially.
     */
    public function resetPartialUserExperiences($v = true)
    {
        $this->collUserExperiencesPartial = $v;
    }

    /**
     * Initializes the collUserExperiences collection.
     *
     * By default this just sets the collUserExperiences collection to an empty array (like clearcollUserExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserExperiences($overrideExisting = true)
    {
        if (null !== $this->collUserExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collUserExperiences = new $collectionClassName;
        $this->collUserExperiences->setModel('\IiMedias\StreamBundle\Model\UserExperience');
    }

    /**
     * Gets an array of ChildUserExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     * @throws PropelException
     */
    public function getUserExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                // return empty collection
                $this->initUserExperiences();
            } else {
                $collUserExperiences = ChildUserExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserExperiencesPartial && count($collUserExperiences)) {
                        $this->initUserExperiences(false);

                        foreach ($collUserExperiences as $obj) {
                            if (false == $this->collUserExperiences->contains($obj)) {
                                $this->collUserExperiences->append($obj);
                            }
                        }

                        $this->collUserExperiencesPartial = true;
                    }

                    return $collUserExperiences;
                }

                if ($partial && $this->collUserExperiences) {
                    foreach ($this->collUserExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collUserExperiences[] = $obj;
                        }
                    }
                }

                $this->collUserExperiences = $collUserExperiences;
                $this->collUserExperiencesPartial = false;
            }
        }

        return $this->collUserExperiences;
    }

    /**
     * Sets a collection of ChildUserExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setUserExperiences(Collection $userExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildUserExperience[] $userExperiencesToDelete */
        $userExperiencesToDelete = $this->getUserExperiences(new Criteria(), $con)->diff($userExperiences);


        $this->userExperiencesScheduledForDeletion = $userExperiencesToDelete;

        foreach ($userExperiencesToDelete as $userExperienceRemoved) {
            $userExperienceRemoved->setChatUser(null);
        }

        $this->collUserExperiences = null;
        foreach ($userExperiences as $userExperience) {
            $this->addUserExperience($userExperience);
        }

        $this->collUserExperiences = $userExperiences;
        $this->collUserExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserExperience objects.
     * @throws PropelException
     */
    public function countUserExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserExperiences());
            }

            $query = ChildUserExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collUserExperiences);
    }

    /**
     * Method called to associate a ChildUserExperience object to this object
     * through the ChildUserExperience foreign key attribute.
     *
     * @param  ChildUserExperience $l ChildUserExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addUserExperience(ChildUserExperience $l)
    {
        if ($this->collUserExperiences === null) {
            $this->initUserExperiences();
            $this->collUserExperiencesPartial = true;
        }

        if (!$this->collUserExperiences->contains($l)) {
            $this->doAddUserExperience($l);

            if ($this->userExperiencesScheduledForDeletion and $this->userExperiencesScheduledForDeletion->contains($l)) {
                $this->userExperiencesScheduledForDeletion->remove($this->userExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserExperience $userExperience The ChildUserExperience object to add.
     */
    protected function doAddUserExperience(ChildUserExperience $userExperience)
    {
        $this->collUserExperiences[]= $userExperience;
        $userExperience->setChatUser($this);
    }

    /**
     * @param  ChildUserExperience $userExperience The ChildUserExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeUserExperience(ChildUserExperience $userExperience)
    {
        if ($this->getUserExperiences()->contains($userExperience)) {
            $pos = $this->collUserExperiences->search($userExperience);
            $this->collUserExperiences->remove($pos);
            if (null === $this->userExperiencesScheduledForDeletion) {
                $this->userExperiencesScheduledForDeletion = clone $this->collUserExperiences;
                $this->userExperiencesScheduledForDeletion->clear();
            }
            $this->userExperiencesScheduledForDeletion[]= clone $userExperience;
            $userExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Rank', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinAvatar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Avatar', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }

    /**
     * Clears out the collDeepBotImportExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDeepBotImportExperiences()
     */
    public function clearDeepBotImportExperiences()
    {
        $this->collDeepBotImportExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDeepBotImportExperiences collection loaded partially.
     */
    public function resetPartialDeepBotImportExperiences($v = true)
    {
        $this->collDeepBotImportExperiencesPartial = $v;
    }

    /**
     * Initializes the collDeepBotImportExperiences collection.
     *
     * By default this just sets the collDeepBotImportExperiences collection to an empty array (like clearcollDeepBotImportExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDeepBotImportExperiences($overrideExisting = true)
    {
        if (null !== $this->collDeepBotImportExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = DeepBotImportExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collDeepBotImportExperiences = new $collectionClassName;
        $this->collDeepBotImportExperiences->setModel('\IiMedias\StreamBundle\Model\DeepBotImportExperience');
    }

    /**
     * Gets an array of ChildDeepBotImportExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     * @throws PropelException
     */
    public function getDeepBotImportExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDeepBotImportExperiencesPartial && !$this->isNew();
        if (null === $this->collDeepBotImportExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDeepBotImportExperiences) {
                // return empty collection
                $this->initDeepBotImportExperiences();
            } else {
                $collDeepBotImportExperiences = ChildDeepBotImportExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDeepBotImportExperiencesPartial && count($collDeepBotImportExperiences)) {
                        $this->initDeepBotImportExperiences(false);

                        foreach ($collDeepBotImportExperiences as $obj) {
                            if (false == $this->collDeepBotImportExperiences->contains($obj)) {
                                $this->collDeepBotImportExperiences->append($obj);
                            }
                        }

                        $this->collDeepBotImportExperiencesPartial = true;
                    }

                    return $collDeepBotImportExperiences;
                }

                if ($partial && $this->collDeepBotImportExperiences) {
                    foreach ($this->collDeepBotImportExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collDeepBotImportExperiences[] = $obj;
                        }
                    }
                }

                $this->collDeepBotImportExperiences = $collDeepBotImportExperiences;
                $this->collDeepBotImportExperiencesPartial = false;
            }
        }

        return $this->collDeepBotImportExperiences;
    }

    /**
     * Sets a collection of ChildDeepBotImportExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $deepBotImportExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setDeepBotImportExperiences(Collection $deepBotImportExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildDeepBotImportExperience[] $deepBotImportExperiencesToDelete */
        $deepBotImportExperiencesToDelete = $this->getDeepBotImportExperiences(new Criteria(), $con)->diff($deepBotImportExperiences);


        $this->deepBotImportExperiencesScheduledForDeletion = $deepBotImportExperiencesToDelete;

        foreach ($deepBotImportExperiencesToDelete as $deepBotImportExperienceRemoved) {
            $deepBotImportExperienceRemoved->setChatUser(null);
        }

        $this->collDeepBotImportExperiences = null;
        foreach ($deepBotImportExperiences as $deepBotImportExperience) {
            $this->addDeepBotImportExperience($deepBotImportExperience);
        }

        $this->collDeepBotImportExperiences = $deepBotImportExperiences;
        $this->collDeepBotImportExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DeepBotImportExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DeepBotImportExperience objects.
     * @throws PropelException
     */
    public function countDeepBotImportExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDeepBotImportExperiencesPartial && !$this->isNew();
        if (null === $this->collDeepBotImportExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDeepBotImportExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDeepBotImportExperiences());
            }

            $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collDeepBotImportExperiences);
    }

    /**
     * Method called to associate a ChildDeepBotImportExperience object to this object
     * through the ChildDeepBotImportExperience foreign key attribute.
     *
     * @param  ChildDeepBotImportExperience $l ChildDeepBotImportExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addDeepBotImportExperience(ChildDeepBotImportExperience $l)
    {
        if ($this->collDeepBotImportExperiences === null) {
            $this->initDeepBotImportExperiences();
            $this->collDeepBotImportExperiencesPartial = true;
        }

        if (!$this->collDeepBotImportExperiences->contains($l)) {
            $this->doAddDeepBotImportExperience($l);

            if ($this->deepBotImportExperiencesScheduledForDeletion and $this->deepBotImportExperiencesScheduledForDeletion->contains($l)) {
                $this->deepBotImportExperiencesScheduledForDeletion->remove($this->deepBotImportExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDeepBotImportExperience $deepBotImportExperience The ChildDeepBotImportExperience object to add.
     */
    protected function doAddDeepBotImportExperience(ChildDeepBotImportExperience $deepBotImportExperience)
    {
        $this->collDeepBotImportExperiences[]= $deepBotImportExperience;
        $deepBotImportExperience->setChatUser($this);
    }

    /**
     * @param  ChildDeepBotImportExperience $deepBotImportExperience The ChildDeepBotImportExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeDeepBotImportExperience(ChildDeepBotImportExperience $deepBotImportExperience)
    {
        if ($this->getDeepBotImportExperiences()->contains($deepBotImportExperience)) {
            $pos = $this->collDeepBotImportExperiences->search($deepBotImportExperience);
            $this->collDeepBotImportExperiences->remove($pos);
            if (null === $this->deepBotImportExperiencesScheduledForDeletion) {
                $this->deepBotImportExperiencesScheduledForDeletion = clone $this->collDeepBotImportExperiences;
                $this->deepBotImportExperiencesScheduledForDeletion->clear();
            }
            $this->deepBotImportExperiencesScheduledForDeletion[]= clone $deepBotImportExperience;
            $deepBotImportExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related DeepBotImportExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDeepBotImportExperience[] List of ChildDeepBotImportExperience objects
     */
    public function getDeepBotImportExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDeepBotImportExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getDeepBotImportExperiences($query, $con);
    }

    /**
     * Clears out the collMessageExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMessageExperiences()
     */
    public function clearMessageExperiences()
    {
        $this->collMessageExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMessageExperiences collection loaded partially.
     */
    public function resetPartialMessageExperiences($v = true)
    {
        $this->collMessageExperiencesPartial = $v;
    }

    /**
     * Initializes the collMessageExperiences collection.
     *
     * By default this just sets the collMessageExperiences collection to an empty array (like clearcollMessageExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMessageExperiences($overrideExisting = true)
    {
        if (null !== $this->collMessageExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = MessageExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collMessageExperiences = new $collectionClassName;
        $this->collMessageExperiences->setModel('\IiMedias\StreamBundle\Model\MessageExperience');
    }

    /**
     * Gets an array of ChildMessageExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     * @throws PropelException
     */
    public function getMessageExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                // return empty collection
                $this->initMessageExperiences();
            } else {
                $collMessageExperiences = ChildMessageExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMessageExperiencesPartial && count($collMessageExperiences)) {
                        $this->initMessageExperiences(false);

                        foreach ($collMessageExperiences as $obj) {
                            if (false == $this->collMessageExperiences->contains($obj)) {
                                $this->collMessageExperiences->append($obj);
                            }
                        }

                        $this->collMessageExperiencesPartial = true;
                    }

                    return $collMessageExperiences;
                }

                if ($partial && $this->collMessageExperiences) {
                    foreach ($this->collMessageExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collMessageExperiences[] = $obj;
                        }
                    }
                }

                $this->collMessageExperiences = $collMessageExperiences;
                $this->collMessageExperiencesPartial = false;
            }
        }

        return $this->collMessageExperiences;
    }

    /**
     * Sets a collection of ChildMessageExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $messageExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setMessageExperiences(Collection $messageExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildMessageExperience[] $messageExperiencesToDelete */
        $messageExperiencesToDelete = $this->getMessageExperiences(new Criteria(), $con)->diff($messageExperiences);


        $this->messageExperiencesScheduledForDeletion = $messageExperiencesToDelete;

        foreach ($messageExperiencesToDelete as $messageExperienceRemoved) {
            $messageExperienceRemoved->setChatUser(null);
        }

        $this->collMessageExperiences = null;
        foreach ($messageExperiences as $messageExperience) {
            $this->addMessageExperience($messageExperience);
        }

        $this->collMessageExperiences = $messageExperiences;
        $this->collMessageExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MessageExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MessageExperience objects.
     * @throws PropelException
     */
    public function countMessageExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageExperiencesPartial && !$this->isNew();
        if (null === $this->collMessageExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMessageExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMessageExperiences());
            }

            $query = ChildMessageExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collMessageExperiences);
    }

    /**
     * Method called to associate a ChildMessageExperience object to this object
     * through the ChildMessageExperience foreign key attribute.
     *
     * @param  ChildMessageExperience $l ChildMessageExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addMessageExperience(ChildMessageExperience $l)
    {
        if ($this->collMessageExperiences === null) {
            $this->initMessageExperiences();
            $this->collMessageExperiencesPartial = true;
        }

        if (!$this->collMessageExperiences->contains($l)) {
            $this->doAddMessageExperience($l);

            if ($this->messageExperiencesScheduledForDeletion and $this->messageExperiencesScheduledForDeletion->contains($l)) {
                $this->messageExperiencesScheduledForDeletion->remove($this->messageExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMessageExperience $messageExperience The ChildMessageExperience object to add.
     */
    protected function doAddMessageExperience(ChildMessageExperience $messageExperience)
    {
        $this->collMessageExperiences[]= $messageExperience;
        $messageExperience->setChatUser($this);
    }

    /**
     * @param  ChildMessageExperience $messageExperience The ChildMessageExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeMessageExperience(ChildMessageExperience $messageExperience)
    {
        if ($this->getMessageExperiences()->contains($messageExperience)) {
            $pos = $this->collMessageExperiences->search($messageExperience);
            $this->collMessageExperiences->remove($pos);
            if (null === $this->messageExperiencesScheduledForDeletion) {
                $this->messageExperiencesScheduledForDeletion = clone $this->collMessageExperiences;
                $this->messageExperiencesScheduledForDeletion->clear();
            }
            $this->messageExperiencesScheduledForDeletion[]= clone $messageExperience;
            $messageExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related MessageExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMessageExperience[] List of ChildMessageExperience objects
     */
    public function getMessageExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMessageExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getMessageExperiences($query, $con);
    }

    /**
     * Clears out the collChatterExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChatterExperiences()
     */
    public function clearChatterExperiences()
    {
        $this->collChatterExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChatterExperiences collection loaded partially.
     */
    public function resetPartialChatterExperiences($v = true)
    {
        $this->collChatterExperiencesPartial = $v;
    }

    /**
     * Initializes the collChatterExperiences collection.
     *
     * By default this just sets the collChatterExperiences collection to an empty array (like clearcollChatterExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChatterExperiences($overrideExisting = true)
    {
        if (null !== $this->collChatterExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatterExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collChatterExperiences = new $collectionClassName;
        $this->collChatterExperiences->setModel('\IiMedias\StreamBundle\Model\ChatterExperience');
    }

    /**
     * Gets an array of ChildChatterExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     * @throws PropelException
     */
    public function getChatterExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                // return empty collection
                $this->initChatterExperiences();
            } else {
                $collChatterExperiences = ChildChatterExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChatterExperiencesPartial && count($collChatterExperiences)) {
                        $this->initChatterExperiences(false);

                        foreach ($collChatterExperiences as $obj) {
                            if (false == $this->collChatterExperiences->contains($obj)) {
                                $this->collChatterExperiences->append($obj);
                            }
                        }

                        $this->collChatterExperiencesPartial = true;
                    }

                    return $collChatterExperiences;
                }

                if ($partial && $this->collChatterExperiences) {
                    foreach ($this->collChatterExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collChatterExperiences[] = $obj;
                        }
                    }
                }

                $this->collChatterExperiences = $collChatterExperiences;
                $this->collChatterExperiencesPartial = false;
            }
        }

        return $this->collChatterExperiences;
    }

    /**
     * Sets a collection of ChildChatterExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chatterExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setChatterExperiences(Collection $chatterExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildChatterExperience[] $chatterExperiencesToDelete */
        $chatterExperiencesToDelete = $this->getChatterExperiences(new Criteria(), $con)->diff($chatterExperiences);


        $this->chatterExperiencesScheduledForDeletion = $chatterExperiencesToDelete;

        foreach ($chatterExperiencesToDelete as $chatterExperienceRemoved) {
            $chatterExperienceRemoved->setChatUser(null);
        }

        $this->collChatterExperiences = null;
        foreach ($chatterExperiences as $chatterExperience) {
            $this->addChatterExperience($chatterExperience);
        }

        $this->collChatterExperiences = $chatterExperiences;
        $this->collChatterExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChatterExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ChatterExperience objects.
     * @throws PropelException
     */
    public function countChatterExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChatterExperiencesPartial && !$this->isNew();
        if (null === $this->collChatterExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChatterExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChatterExperiences());
            }

            $query = ChildChatterExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collChatterExperiences);
    }

    /**
     * Method called to associate a ChildChatterExperience object to this object
     * through the ChildChatterExperience foreign key attribute.
     *
     * @param  ChildChatterExperience $l ChildChatterExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addChatterExperience(ChildChatterExperience $l)
    {
        if ($this->collChatterExperiences === null) {
            $this->initChatterExperiences();
            $this->collChatterExperiencesPartial = true;
        }

        if (!$this->collChatterExperiences->contains($l)) {
            $this->doAddChatterExperience($l);

            if ($this->chatterExperiencesScheduledForDeletion and $this->chatterExperiencesScheduledForDeletion->contains($l)) {
                $this->chatterExperiencesScheduledForDeletion->remove($this->chatterExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChatterExperience $chatterExperience The ChildChatterExperience object to add.
     */
    protected function doAddChatterExperience(ChildChatterExperience $chatterExperience)
    {
        $this->collChatterExperiences[]= $chatterExperience;
        $chatterExperience->setChatUser($this);
    }

    /**
     * @param  ChildChatterExperience $chatterExperience The ChildChatterExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeChatterExperience(ChildChatterExperience $chatterExperience)
    {
        if ($this->getChatterExperiences()->contains($chatterExperience)) {
            $pos = $this->collChatterExperiences->search($chatterExperience);
            $this->collChatterExperiences->remove($pos);
            if (null === $this->chatterExperiencesScheduledForDeletion) {
                $this->chatterExperiencesScheduledForDeletion = clone $this->collChatterExperiences;
                $this->chatterExperiencesScheduledForDeletion->clear();
            }
            $this->chatterExperiencesScheduledForDeletion[]= clone $chatterExperience;
            $chatterExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related ChatterExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChatterExperience[] List of ChildChatterExperience objects
     */
    public function getChatterExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChatterExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getChatterExperiences($query, $con);
    }

    /**
     * Clears out the collFollowExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFollowExperiences()
     */
    public function clearFollowExperiences()
    {
        $this->collFollowExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFollowExperiences collection loaded partially.
     */
    public function resetPartialFollowExperiences($v = true)
    {
        $this->collFollowExperiencesPartial = $v;
    }

    /**
     * Initializes the collFollowExperiences collection.
     *
     * By default this just sets the collFollowExperiences collection to an empty array (like clearcollFollowExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFollowExperiences($overrideExisting = true)
    {
        if (null !== $this->collFollowExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = FollowExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collFollowExperiences = new $collectionClassName;
        $this->collFollowExperiences->setModel('\IiMedias\StreamBundle\Model\FollowExperience');
    }

    /**
     * Gets an array of ChildFollowExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     * @throws PropelException
     */
    public function getFollowExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                // return empty collection
                $this->initFollowExperiences();
            } else {
                $collFollowExperiences = ChildFollowExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFollowExperiencesPartial && count($collFollowExperiences)) {
                        $this->initFollowExperiences(false);

                        foreach ($collFollowExperiences as $obj) {
                            if (false == $this->collFollowExperiences->contains($obj)) {
                                $this->collFollowExperiences->append($obj);
                            }
                        }

                        $this->collFollowExperiencesPartial = true;
                    }

                    return $collFollowExperiences;
                }

                if ($partial && $this->collFollowExperiences) {
                    foreach ($this->collFollowExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collFollowExperiences[] = $obj;
                        }
                    }
                }

                $this->collFollowExperiences = $collFollowExperiences;
                $this->collFollowExperiencesPartial = false;
            }
        }

        return $this->collFollowExperiences;
    }

    /**
     * Sets a collection of ChildFollowExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $followExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setFollowExperiences(Collection $followExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildFollowExperience[] $followExperiencesToDelete */
        $followExperiencesToDelete = $this->getFollowExperiences(new Criteria(), $con)->diff($followExperiences);


        $this->followExperiencesScheduledForDeletion = $followExperiencesToDelete;

        foreach ($followExperiencesToDelete as $followExperienceRemoved) {
            $followExperienceRemoved->setChatUser(null);
        }

        $this->collFollowExperiences = null;
        foreach ($followExperiences as $followExperience) {
            $this->addFollowExperience($followExperience);
        }

        $this->collFollowExperiences = $followExperiences;
        $this->collFollowExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FollowExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FollowExperience objects.
     * @throws PropelException
     */
    public function countFollowExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFollowExperiencesPartial && !$this->isNew();
        if (null === $this->collFollowExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFollowExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFollowExperiences());
            }

            $query = ChildFollowExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collFollowExperiences);
    }

    /**
     * Method called to associate a ChildFollowExperience object to this object
     * through the ChildFollowExperience foreign key attribute.
     *
     * @param  ChildFollowExperience $l ChildFollowExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addFollowExperience(ChildFollowExperience $l)
    {
        if ($this->collFollowExperiences === null) {
            $this->initFollowExperiences();
            $this->collFollowExperiencesPartial = true;
        }

        if (!$this->collFollowExperiences->contains($l)) {
            $this->doAddFollowExperience($l);

            if ($this->followExperiencesScheduledForDeletion and $this->followExperiencesScheduledForDeletion->contains($l)) {
                $this->followExperiencesScheduledForDeletion->remove($this->followExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFollowExperience $followExperience The ChildFollowExperience object to add.
     */
    protected function doAddFollowExperience(ChildFollowExperience $followExperience)
    {
        $this->collFollowExperiences[]= $followExperience;
        $followExperience->setChatUser($this);
    }

    /**
     * @param  ChildFollowExperience $followExperience The ChildFollowExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeFollowExperience(ChildFollowExperience $followExperience)
    {
        if ($this->getFollowExperiences()->contains($followExperience)) {
            $pos = $this->collFollowExperiences->search($followExperience);
            $this->collFollowExperiences->remove($pos);
            if (null === $this->followExperiencesScheduledForDeletion) {
                $this->followExperiencesScheduledForDeletion = clone $this->collFollowExperiences;
                $this->followExperiencesScheduledForDeletion->clear();
            }
            $this->followExperiencesScheduledForDeletion[]= clone $followExperience;
            $followExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related FollowExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFollowExperience[] List of ChildFollowExperience objects
     */
    public function getFollowExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFollowExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getFollowExperiences($query, $con);
    }

    /**
     * Clears out the collHostExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addHostExperiences()
     */
    public function clearHostExperiences()
    {
        $this->collHostExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collHostExperiences collection loaded partially.
     */
    public function resetPartialHostExperiences($v = true)
    {
        $this->collHostExperiencesPartial = $v;
    }

    /**
     * Initializes the collHostExperiences collection.
     *
     * By default this just sets the collHostExperiences collection to an empty array (like clearcollHostExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initHostExperiences($overrideExisting = true)
    {
        if (null !== $this->collHostExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = HostExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collHostExperiences = new $collectionClassName;
        $this->collHostExperiences->setModel('\IiMedias\StreamBundle\Model\HostExperience');
    }

    /**
     * Gets an array of ChildHostExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     * @throws PropelException
     */
    public function getHostExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                // return empty collection
                $this->initHostExperiences();
            } else {
                $collHostExperiences = ChildHostExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collHostExperiencesPartial && count($collHostExperiences)) {
                        $this->initHostExperiences(false);

                        foreach ($collHostExperiences as $obj) {
                            if (false == $this->collHostExperiences->contains($obj)) {
                                $this->collHostExperiences->append($obj);
                            }
                        }

                        $this->collHostExperiencesPartial = true;
                    }

                    return $collHostExperiences;
                }

                if ($partial && $this->collHostExperiences) {
                    foreach ($this->collHostExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collHostExperiences[] = $obj;
                        }
                    }
                }

                $this->collHostExperiences = $collHostExperiences;
                $this->collHostExperiencesPartial = false;
            }
        }

        return $this->collHostExperiences;
    }

    /**
     * Sets a collection of ChildHostExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $hostExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setHostExperiences(Collection $hostExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildHostExperience[] $hostExperiencesToDelete */
        $hostExperiencesToDelete = $this->getHostExperiences(new Criteria(), $con)->diff($hostExperiences);


        $this->hostExperiencesScheduledForDeletion = $hostExperiencesToDelete;

        foreach ($hostExperiencesToDelete as $hostExperienceRemoved) {
            $hostExperienceRemoved->setChatUser(null);
        }

        $this->collHostExperiences = null;
        foreach ($hostExperiences as $hostExperience) {
            $this->addHostExperience($hostExperience);
        }

        $this->collHostExperiences = $hostExperiences;
        $this->collHostExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related HostExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related HostExperience objects.
     * @throws PropelException
     */
    public function countHostExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collHostExperiencesPartial && !$this->isNew();
        if (null === $this->collHostExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collHostExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getHostExperiences());
            }

            $query = ChildHostExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collHostExperiences);
    }

    /**
     * Method called to associate a ChildHostExperience object to this object
     * through the ChildHostExperience foreign key attribute.
     *
     * @param  ChildHostExperience $l ChildHostExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addHostExperience(ChildHostExperience $l)
    {
        if ($this->collHostExperiences === null) {
            $this->initHostExperiences();
            $this->collHostExperiencesPartial = true;
        }

        if (!$this->collHostExperiences->contains($l)) {
            $this->doAddHostExperience($l);

            if ($this->hostExperiencesScheduledForDeletion and $this->hostExperiencesScheduledForDeletion->contains($l)) {
                $this->hostExperiencesScheduledForDeletion->remove($this->hostExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildHostExperience $hostExperience The ChildHostExperience object to add.
     */
    protected function doAddHostExperience(ChildHostExperience $hostExperience)
    {
        $this->collHostExperiences[]= $hostExperience;
        $hostExperience->setChatUser($this);
    }

    /**
     * @param  ChildHostExperience $hostExperience The ChildHostExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeHostExperience(ChildHostExperience $hostExperience)
    {
        if ($this->getHostExperiences()->contains($hostExperience)) {
            $pos = $this->collHostExperiences->search($hostExperience);
            $this->collHostExperiences->remove($pos);
            if (null === $this->hostExperiencesScheduledForDeletion) {
                $this->hostExperiencesScheduledForDeletion = clone $this->collHostExperiences;
                $this->hostExperiencesScheduledForDeletion->clear();
            }
            $this->hostExperiencesScheduledForDeletion[]= clone $hostExperience;
            $hostExperience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related HostExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildHostExperience[] List of ChildHostExperience objects
     */
    public function getHostExperiencesJoinUserExperience(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildHostExperienceQuery::create(null, $criteria);
        $query->joinWith('UserExperience', $joinBehavior);

        return $this->getHostExperiences($query, $con);
    }

    /**
     * Clears out the collExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addExperiences()
     */
    public function clearExperiences()
    {
        $this->collExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collExperiences collection loaded partially.
     */
    public function resetPartialExperiences($v = true)
    {
        $this->collExperiencesPartial = $v;
    }

    /**
     * Initializes the collExperiences collection.
     *
     * By default this just sets the collExperiences collection to an empty array (like clearcollExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initExperiences($overrideExisting = true)
    {
        if (null !== $this->collExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = ExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collExperiences = new $collectionClassName;
        $this->collExperiences->setModel('\IiMedias\StreamBundle\Model\Experience');
    }

    /**
     * Gets an array of ChildExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildChatUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     * @throws PropelException
     */
    public function getExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collExperiencesPartial && !$this->isNew();
        if (null === $this->collExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collExperiences) {
                // return empty collection
                $this->initExperiences();
            } else {
                $collExperiences = ChildExperienceQuery::create(null, $criteria)
                    ->filterByChatUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collExperiencesPartial && count($collExperiences)) {
                        $this->initExperiences(false);

                        foreach ($collExperiences as $obj) {
                            if (false == $this->collExperiences->contains($obj)) {
                                $this->collExperiences->append($obj);
                            }
                        }

                        $this->collExperiencesPartial = true;
                    }

                    return $collExperiences;
                }

                if ($partial && $this->collExperiences) {
                    foreach ($this->collExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collExperiences[] = $obj;
                        }
                    }
                }

                $this->collExperiences = $collExperiences;
                $this->collExperiencesPartial = false;
            }
        }

        return $this->collExperiences;
    }

    /**
     * Sets a collection of ChildExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $experiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function setExperiences(Collection $experiences, ConnectionInterface $con = null)
    {
        /** @var ChildExperience[] $experiencesToDelete */
        $experiencesToDelete = $this->getExperiences(new Criteria(), $con)->diff($experiences);


        $this->experiencesScheduledForDeletion = $experiencesToDelete;

        foreach ($experiencesToDelete as $experienceRemoved) {
            $experienceRemoved->setChatUser(null);
        }

        $this->collExperiences = null;
        foreach ($experiences as $experience) {
            $this->addExperience($experience);
        }

        $this->collExperiences = $experiences;
        $this->collExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Experience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Experience objects.
     * @throws PropelException
     */
    public function countExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collExperiencesPartial && !$this->isNew();
        if (null === $this->collExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getExperiences());
            }

            $query = ChildExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChatUser($this)
                ->count($con);
        }

        return count($this->collExperiences);
    }

    /**
     * Method called to associate a ChildExperience object to this object
     * through the ChildExperience foreign key attribute.
     *
     * @param  ChildExperience $l ChildExperience
     * @return $this|\IiMedias\StreamBundle\Model\ChatUser The current object (for fluent API support)
     */
    public function addExperience(ChildExperience $l)
    {
        if ($this->collExperiences === null) {
            $this->initExperiences();
            $this->collExperiencesPartial = true;
        }

        if (!$this->collExperiences->contains($l)) {
            $this->doAddExperience($l);

            if ($this->experiencesScheduledForDeletion and $this->experiencesScheduledForDeletion->contains($l)) {
                $this->experiencesScheduledForDeletion->remove($this->experiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildExperience $experience The ChildExperience object to add.
     */
    protected function doAddExperience(ChildExperience $experience)
    {
        $this->collExperiences[]= $experience;
        $experience->setChatUser($this);
    }

    /**
     * @param  ChildExperience $experience The ChildExperience object to remove.
     * @return $this|ChildChatUser The current object (for fluent API support)
     */
    public function removeExperience(ChildExperience $experience)
    {
        if ($this->getExperiences()->contains($experience)) {
            $pos = $this->collExperiences->search($experience);
            $this->collExperiences->remove($pos);
            if (null === $this->experiencesScheduledForDeletion) {
                $this->experiencesScheduledForDeletion = clone $this->collExperiences;
                $this->experiencesScheduledForDeletion->clear();
            }
            $this->experiencesScheduledForDeletion[]= clone $experience;
            $experience->setChatUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChatUser is new, it will return
     * an empty collection; or if this ChatUser has previously
     * been saved, it will retrieve related Experiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChatUser.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildExperience[] List of ChildExperience objects
     */
    public function getExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getExperiences($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aParent) {
            $this->aParent->removeChild($this);
        }
        if (null !== $this->aSite) {
            $this->aSite->removeChatUser($this);
        }
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserStcusr($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserStcusr($this);
        }
        $this->stcusr_id = null;
        $this->stcusr_parent_id = null;
        $this->stcusr_stsite_id = null;
        $this->stcusr_username = null;
        $this->stcusr_display_name = null;
        $this->stcusr_logo_url = null;
        $this->stcusr_timeout_level = null;
        $this->stcusr_last_timeout_at = null;
        $this->stcusr_registered_at = null;
        $this->stcusr_is_deleted_account = null;
        $this->stcusr_can_rescan = null;
        $this->stcusr_created_by_user_id = null;
        $this->stcusr_updated_by_user_id = null;
        $this->stcusr_created_at = null;
        $this->stcusr_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChildren) {
                foreach ($this->collChildren as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserExperiences) {
                foreach ($this->collUserExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDeepBotImportExperiences) {
                foreach ($this->collDeepBotImportExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMessageExperiences) {
                foreach ($this->collMessageExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChatterExperiences) {
                foreach ($this->collChatterExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFollowExperiences) {
                foreach ($this->collFollowExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collHostExperiences) {
                foreach ($this->collHostExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collExperiences) {
                foreach ($this->collExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collChildren = null;
        $this->collUserExperiences = null;
        $this->collDeepBotImportExperiences = null;
        $this->collMessageExperiences = null;
        $this->collChatterExperiences = null;
        $this->collFollowExperiences = null;
        $this->collHostExperiences = null;
        $this->collExperiences = null;
        $this->aParent = null;
        $this->aSite = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChatUserTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildChatUser The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ChatUserTableMap::COL_STCUSR_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

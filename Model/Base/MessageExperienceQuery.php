<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\MessageExperience as ChildMessageExperience;
use IiMedias\StreamBundle\Model\MessageExperienceQuery as ChildMessageExperienceQuery;
use IiMedias\StreamBundle\Model\Map\MessageExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_message_experience_stmexp' table.
 *
 *
 *
 * @method     ChildMessageExperienceQuery orderById($order = Criteria::ASC) Order by the stmexp_id column
 * @method     ChildMessageExperienceQuery orderByStreamId($order = Criteria::ASC) Order by the stmexp_ststrm_id column
 * @method     ChildMessageExperienceQuery orderByChannelId($order = Criteria::ASC) Order by the stmexp_stchan_id column
 * @method     ChildMessageExperienceQuery orderBySiteId($order = Criteria::ASC) Order by the stmexp_stsite_id column
 * @method     ChildMessageExperienceQuery orderByChatUserId($order = Criteria::ASC) Order by the stmexp_stcusr_id column
 * @method     ChildMessageExperienceQuery orderByUserExperienceId($order = Criteria::ASC) Order by the stmexp_stuexp_id column
 * @method     ChildMessageExperienceQuery orderByType($order = Criteria::ASC) Order by the stmexp_type column
 * @method     ChildMessageExperienceQuery orderByMessage($order = Criteria::ASC) Order by the stmexp_message column
 * @method     ChildMessageExperienceQuery orderByExp($order = Criteria::ASC) Order by the stmexp_exp column
 * @method     ChildMessageExperienceQuery orderByAt($order = Criteria::ASC) Order by the stexpr_at column
 *
 * @method     ChildMessageExperienceQuery groupById() Group by the stmexp_id column
 * @method     ChildMessageExperienceQuery groupByStreamId() Group by the stmexp_ststrm_id column
 * @method     ChildMessageExperienceQuery groupByChannelId() Group by the stmexp_stchan_id column
 * @method     ChildMessageExperienceQuery groupBySiteId() Group by the stmexp_stsite_id column
 * @method     ChildMessageExperienceQuery groupByChatUserId() Group by the stmexp_stcusr_id column
 * @method     ChildMessageExperienceQuery groupByUserExperienceId() Group by the stmexp_stuexp_id column
 * @method     ChildMessageExperienceQuery groupByType() Group by the stmexp_type column
 * @method     ChildMessageExperienceQuery groupByMessage() Group by the stmexp_message column
 * @method     ChildMessageExperienceQuery groupByExp() Group by the stmexp_exp column
 * @method     ChildMessageExperienceQuery groupByAt() Group by the stexpr_at column
 *
 * @method     ChildMessageExperienceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMessageExperienceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMessageExperienceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMessageExperienceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMessageExperienceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMessageExperienceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMessageExperienceQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildMessageExperienceQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildMessageExperienceQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildMessageExperienceQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildMessageExperienceQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildMessageExperienceQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildMessageExperienceQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildMessageExperienceQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildMessageExperienceQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildMessageExperienceQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildMessageExperienceQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildMessageExperienceQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildMessageExperienceQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildMessageExperienceQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildMessageExperienceQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildMessageExperienceQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildMessageExperienceQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildMessageExperienceQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildMessageExperienceQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildMessageExperienceQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildMessageExperienceQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildMessageExperienceQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildMessageExperienceQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildMessageExperienceQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildMessageExperienceQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildMessageExperienceQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildMessageExperienceQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildMessageExperienceQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     ChildMessageExperienceQuery leftJoinUserExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserExperience relation
 * @method     ChildMessageExperienceQuery rightJoinUserExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserExperience relation
 * @method     ChildMessageExperienceQuery innerJoinUserExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the UserExperience relation
 *
 * @method     ChildMessageExperienceQuery joinWithUserExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserExperience relation
 *
 * @method     ChildMessageExperienceQuery leftJoinWithUserExperience() Adds a LEFT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildMessageExperienceQuery rightJoinWithUserExperience() Adds a RIGHT JOIN clause and with to the query using the UserExperience relation
 * @method     ChildMessageExperienceQuery innerJoinWithUserExperience() Adds a INNER JOIN clause and with to the query using the UserExperience relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\UserExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMessageExperience findOne(ConnectionInterface $con = null) Return the first ChildMessageExperience matching the query
 * @method     ChildMessageExperience findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMessageExperience matching the query, or a new ChildMessageExperience object populated from the query conditions when no match is found
 *
 * @method     ChildMessageExperience findOneById(int $stmexp_id) Return the first ChildMessageExperience filtered by the stmexp_id column
 * @method     ChildMessageExperience findOneByStreamId(int $stmexp_ststrm_id) Return the first ChildMessageExperience filtered by the stmexp_ststrm_id column
 * @method     ChildMessageExperience findOneByChannelId(int $stmexp_stchan_id) Return the first ChildMessageExperience filtered by the stmexp_stchan_id column
 * @method     ChildMessageExperience findOneBySiteId(int $stmexp_stsite_id) Return the first ChildMessageExperience filtered by the stmexp_stsite_id column
 * @method     ChildMessageExperience findOneByChatUserId(int $stmexp_stcusr_id) Return the first ChildMessageExperience filtered by the stmexp_stcusr_id column
 * @method     ChildMessageExperience findOneByUserExperienceId(int $stmexp_stuexp_id) Return the first ChildMessageExperience filtered by the stmexp_stuexp_id column
 * @method     ChildMessageExperience findOneByType(int $stmexp_type) Return the first ChildMessageExperience filtered by the stmexp_type column
 * @method     ChildMessageExperience findOneByMessage(string $stmexp_message) Return the first ChildMessageExperience filtered by the stmexp_message column
 * @method     ChildMessageExperience findOneByExp(int $stmexp_exp) Return the first ChildMessageExperience filtered by the stmexp_exp column
 * @method     ChildMessageExperience findOneByAt(string $stexpr_at) Return the first ChildMessageExperience filtered by the stexpr_at column *

 * @method     ChildMessageExperience requirePk($key, ConnectionInterface $con = null) Return the ChildMessageExperience by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOne(ConnectionInterface $con = null) Return the first ChildMessageExperience matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessageExperience requireOneById(int $stmexp_id) Return the first ChildMessageExperience filtered by the stmexp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByStreamId(int $stmexp_ststrm_id) Return the first ChildMessageExperience filtered by the stmexp_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByChannelId(int $stmexp_stchan_id) Return the first ChildMessageExperience filtered by the stmexp_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneBySiteId(int $stmexp_stsite_id) Return the first ChildMessageExperience filtered by the stmexp_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByChatUserId(int $stmexp_stcusr_id) Return the first ChildMessageExperience filtered by the stmexp_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByUserExperienceId(int $stmexp_stuexp_id) Return the first ChildMessageExperience filtered by the stmexp_stuexp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByType(int $stmexp_type) Return the first ChildMessageExperience filtered by the stmexp_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByMessage(string $stmexp_message) Return the first ChildMessageExperience filtered by the stmexp_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByExp(int $stmexp_exp) Return the first ChildMessageExperience filtered by the stmexp_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageExperience requireOneByAt(string $stexpr_at) Return the first ChildMessageExperience filtered by the stexpr_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessageExperience[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMessageExperience objects based on current ModelCriteria
 * @method     ChildMessageExperience[]|ObjectCollection findById(int $stmexp_id) Return ChildMessageExperience objects filtered by the stmexp_id column
 * @method     ChildMessageExperience[]|ObjectCollection findByStreamId(int $stmexp_ststrm_id) Return ChildMessageExperience objects filtered by the stmexp_ststrm_id column
 * @method     ChildMessageExperience[]|ObjectCollection findByChannelId(int $stmexp_stchan_id) Return ChildMessageExperience objects filtered by the stmexp_stchan_id column
 * @method     ChildMessageExperience[]|ObjectCollection findBySiteId(int $stmexp_stsite_id) Return ChildMessageExperience objects filtered by the stmexp_stsite_id column
 * @method     ChildMessageExperience[]|ObjectCollection findByChatUserId(int $stmexp_stcusr_id) Return ChildMessageExperience objects filtered by the stmexp_stcusr_id column
 * @method     ChildMessageExperience[]|ObjectCollection findByUserExperienceId(int $stmexp_stuexp_id) Return ChildMessageExperience objects filtered by the stmexp_stuexp_id column
 * @method     ChildMessageExperience[]|ObjectCollection findByType(int $stmexp_type) Return ChildMessageExperience objects filtered by the stmexp_type column
 * @method     ChildMessageExperience[]|ObjectCollection findByMessage(string $stmexp_message) Return ChildMessageExperience objects filtered by the stmexp_message column
 * @method     ChildMessageExperience[]|ObjectCollection findByExp(int $stmexp_exp) Return ChildMessageExperience objects filtered by the stmexp_exp column
 * @method     ChildMessageExperience[]|ObjectCollection findByAt(string $stexpr_at) Return ChildMessageExperience objects filtered by the stexpr_at column
 * @method     ChildMessageExperience[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MessageExperienceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\MessageExperienceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\MessageExperience', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMessageExperienceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMessageExperienceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMessageExperienceQuery) {
            return $criteria;
        }
        $query = new ChildMessageExperienceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMessageExperience|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MessageExperienceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MessageExperienceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperience A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stmexp_id, stmexp_ststrm_id, stmexp_stchan_id, stmexp_stsite_id, stmexp_stcusr_id, stmexp_stuexp_id, stmexp_type, stmexp_message, stmexp_exp, stexpr_at FROM stream_message_experience_stmexp WHERE stmexp_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMessageExperience $obj */
            $obj = new ChildMessageExperience();
            $obj->hydrate($row);
            MessageExperienceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMessageExperience|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stmexp_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stmexp_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stmexp_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stmexp_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stmexp_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stmexp_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stmexp_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stmexp_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stmexp_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stmexp_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stmexp_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stmexp_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stmexp_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stmexp_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stmexp_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stmexp_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stmexp_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stmexp_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stmexp_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stmexp_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stmexp_stuexp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserExperienceId(1234); // WHERE stmexp_stuexp_id = 1234
     * $query->filterByUserExperienceId(array(12, 34)); // WHERE stmexp_stuexp_id IN (12, 34)
     * $query->filterByUserExperienceId(array('min' => 12)); // WHERE stmexp_stuexp_id > 12
     * </code>
     *
     * @see       filterByUserExperience()
     *
     * @param     mixed $userExperienceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByUserExperienceId($userExperienceId = null, $comparison = null)
    {
        if (is_array($userExperienceId)) {
            $useMinMax = false;
            if (isset($userExperienceId['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STUEXP_ID, $userExperienceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userExperienceId['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STUEXP_ID, $userExperienceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STUEXP_ID, $userExperienceId, $comparison);
    }

    /**
     * Filter the query on the stmexp_type column
     *
     * @param     mixed $type The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        $valueSet = MessageExperienceTableMap::getValueSet(MessageExperienceTableMap::COL_STMEXP_TYPE);
        if (is_scalar($type)) {
            if (!in_array($type, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $type));
            }
            $type = array_search($type, $valueSet);
        } elseif (is_array($type)) {
            $convertedValues = array();
            foreach ($type as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $type = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the stmexp_message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE stmexp_message = 'fooValue'
     * $query->filterByMessage('%fooValue%', Criteria::LIKE); // WHERE stmexp_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the stmexp_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByExp(1234); // WHERE stmexp_exp = 1234
     * $query->filterByExp(array(12, 34)); // WHERE stmexp_exp IN (12, 34)
     * $query->filterByExp(array('min' => 12)); // WHERE stmexp_exp > 12
     * </code>
     *
     * @param     mixed $exp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByExp($exp = null, $comparison = null)
    {
        if (is_array($exp)) {
            $useMinMax = false;
            if (isset($exp['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_EXP, $exp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exp['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_EXP, $exp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_EXP, $exp, $comparison);
    }

    /**
     * Filter the query on the stexpr_at column
     *
     * Example usage:
     * <code>
     * $query->filterByAt('2011-03-14'); // WHERE stexpr_at = '2011-03-14'
     * $query->filterByAt('now'); // WHERE stexpr_at = '2011-03-14'
     * $query->filterByAt(array('max' => 'yesterday')); // WHERE stexpr_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $at The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByAt($at = null, $comparison = null)
    {
        if (is_array($at)) {
            $useMinMax = false;
            if (isset($at['min'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STEXPR_AT, $at['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($at['max'])) {
                $this->addUsingAlias(MessageExperienceTableMap::COL_STEXPR_AT, $at['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageExperienceTableMap::COL_STEXPR_AT, $at, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\UserExperience object
     *
     * @param \IiMedias\StreamBundle\Model\UserExperience|ObjectCollection $userExperience The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function filterByUserExperience($userExperience, $comparison = null)
    {
        if ($userExperience instanceof \IiMedias\StreamBundle\Model\UserExperience) {
            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STUEXP_ID, $userExperience->getId(), $comparison);
        } elseif ($userExperience instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_STUEXP_ID, $userExperience->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\UserExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function joinUserExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserExperience');
        }

        return $this;
    }

    /**
     * Use the UserExperience relation UserExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\UserExperienceQuery A secondary query class using the current class as primary query
     */
    public function useUserExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserExperience', '\IiMedias\StreamBundle\Model\UserExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMessageExperience $messageExperience Object to remove from the list of results
     *
     * @return $this|ChildMessageExperienceQuery The current query, for fluid interface
     */
    public function prune($messageExperience = null)
    {
        if ($messageExperience) {
            $this->addUsingAlias(MessageExperienceTableMap::COL_STMEXP_ID, $messageExperience->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_message_experience_stmexp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageExperienceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MessageExperienceTableMap::clearInstancePool();
            MessageExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageExperienceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MessageExperienceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MessageExperienceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MessageExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MessageExperienceQuery

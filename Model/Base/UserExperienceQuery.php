<?php

namespace IiMedias\StreamBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\UserExperience as ChildUserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery as ChildUserExperienceQuery;
use IiMedias\StreamBundle\Model\Map\UserExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stream_user_experience_stuexp' table.
 *
 *
 *
 * @method     ChildUserExperienceQuery orderById($order = Criteria::ASC) Order by the stuexp_id column
 * @method     ChildUserExperienceQuery orderByParentId($order = Criteria::ASC) Order by the stuexp_parent_id column
 * @method     ChildUserExperienceQuery orderByStreamId($order = Criteria::ASC) Order by the stuexp_ststrm_id column
 * @method     ChildUserExperienceQuery orderByChannelId($order = Criteria::ASC) Order by the stuexp_stchan_id column
 * @method     ChildUserExperienceQuery orderBySiteId($order = Criteria::ASC) Order by the stuexp_stsite_id column
 * @method     ChildUserExperienceQuery orderByChatUserId($order = Criteria::ASC) Order by the stuexp_stcusr_id column
 * @method     ChildUserExperienceQuery orderByChatType($order = Criteria::ASC) Order by the stuexp_chat_type column
 * @method     ChildUserExperienceQuery orderByExp($order = Criteria::ASC) Order by the stuexp_exp column
 * @method     ChildUserExperienceQuery orderByTotalExp($order = Criteria::ASC) Order by the stuexp_total_exp column
 * @method     ChildUserExperienceQuery orderByIsFollowing($order = Criteria::ASC) Order by the stuexp_is_following column
 * @method     ChildUserExperienceQuery orderByMessagesCount($order = Criteria::ASC) Order by the stuexp_messages_count column
 * @method     ChildUserExperienceQuery orderByFollowsCount($order = Criteria::ASC) Order by the stuexp_follows_count column
 * @method     ChildUserExperienceQuery orderByHostsCount($order = Criteria::ASC) Order by the stuexp_hosts_count column
 * @method     ChildUserExperienceQuery orderByChattersCount($order = Criteria::ASC) Order by the stuexp_chatters_count column
 * @method     ChildUserExperienceQuery orderByRankId($order = Criteria::ASC) Order by the stuexp_strank_id column
 * @method     ChildUserExperienceQuery orderByAvatarId($order = Criteria::ASC) Order by the stuexp_stavtr_id column
 *
 * @method     ChildUserExperienceQuery groupById() Group by the stuexp_id column
 * @method     ChildUserExperienceQuery groupByParentId() Group by the stuexp_parent_id column
 * @method     ChildUserExperienceQuery groupByStreamId() Group by the stuexp_ststrm_id column
 * @method     ChildUserExperienceQuery groupByChannelId() Group by the stuexp_stchan_id column
 * @method     ChildUserExperienceQuery groupBySiteId() Group by the stuexp_stsite_id column
 * @method     ChildUserExperienceQuery groupByChatUserId() Group by the stuexp_stcusr_id column
 * @method     ChildUserExperienceQuery groupByChatType() Group by the stuexp_chat_type column
 * @method     ChildUserExperienceQuery groupByExp() Group by the stuexp_exp column
 * @method     ChildUserExperienceQuery groupByTotalExp() Group by the stuexp_total_exp column
 * @method     ChildUserExperienceQuery groupByIsFollowing() Group by the stuexp_is_following column
 * @method     ChildUserExperienceQuery groupByMessagesCount() Group by the stuexp_messages_count column
 * @method     ChildUserExperienceQuery groupByFollowsCount() Group by the stuexp_follows_count column
 * @method     ChildUserExperienceQuery groupByHostsCount() Group by the stuexp_hosts_count column
 * @method     ChildUserExperienceQuery groupByChattersCount() Group by the stuexp_chatters_count column
 * @method     ChildUserExperienceQuery groupByRankId() Group by the stuexp_strank_id column
 * @method     ChildUserExperienceQuery groupByAvatarId() Group by the stuexp_stavtr_id column
 *
 * @method     ChildUserExperienceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserExperienceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserExperienceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserExperienceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserExperienceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserExperienceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserExperienceQuery leftJoinStream($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stream relation
 * @method     ChildUserExperienceQuery rightJoinStream($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stream relation
 * @method     ChildUserExperienceQuery innerJoinStream($relationAlias = null) Adds a INNER JOIN clause to the query using the Stream relation
 *
 * @method     ChildUserExperienceQuery joinWithStream($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stream relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithStream() Adds a LEFT JOIN clause and with to the query using the Stream relation
 * @method     ChildUserExperienceQuery rightJoinWithStream() Adds a RIGHT JOIN clause and with to the query using the Stream relation
 * @method     ChildUserExperienceQuery innerJoinWithStream() Adds a INNER JOIN clause and with to the query using the Stream relation
 *
 * @method     ChildUserExperienceQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildUserExperienceQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildUserExperienceQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildUserExperienceQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildUserExperienceQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildUserExperienceQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildUserExperienceQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildUserExperienceQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildUserExperienceQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildUserExperienceQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildUserExperienceQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildUserExperienceQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildUserExperienceQuery leftJoinChatUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatUser relation
 * @method     ChildUserExperienceQuery rightJoinChatUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatUser relation
 * @method     ChildUserExperienceQuery innerJoinChatUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatUser relation
 *
 * @method     ChildUserExperienceQuery joinWithChatUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatUser relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithChatUser() Adds a LEFT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildUserExperienceQuery rightJoinWithChatUser() Adds a RIGHT JOIN clause and with to the query using the ChatUser relation
 * @method     ChildUserExperienceQuery innerJoinWithChatUser() Adds a INNER JOIN clause and with to the query using the ChatUser relation
 *
 * @method     ChildUserExperienceQuery leftJoinRank($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rank relation
 * @method     ChildUserExperienceQuery rightJoinRank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rank relation
 * @method     ChildUserExperienceQuery innerJoinRank($relationAlias = null) Adds a INNER JOIN clause to the query using the Rank relation
 *
 * @method     ChildUserExperienceQuery joinWithRank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rank relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithRank() Adds a LEFT JOIN clause and with to the query using the Rank relation
 * @method     ChildUserExperienceQuery rightJoinWithRank() Adds a RIGHT JOIN clause and with to the query using the Rank relation
 * @method     ChildUserExperienceQuery innerJoinWithRank() Adds a INNER JOIN clause and with to the query using the Rank relation
 *
 * @method     ChildUserExperienceQuery leftJoinAvatar($relationAlias = null) Adds a LEFT JOIN clause to the query using the Avatar relation
 * @method     ChildUserExperienceQuery rightJoinAvatar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Avatar relation
 * @method     ChildUserExperienceQuery innerJoinAvatar($relationAlias = null) Adds a INNER JOIN clause to the query using the Avatar relation
 *
 * @method     ChildUserExperienceQuery joinWithAvatar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Avatar relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithAvatar() Adds a LEFT JOIN clause and with to the query using the Avatar relation
 * @method     ChildUserExperienceQuery rightJoinWithAvatar() Adds a RIGHT JOIN clause and with to the query using the Avatar relation
 * @method     ChildUserExperienceQuery innerJoinWithAvatar() Adds a INNER JOIN clause and with to the query using the Avatar relation
 *
 * @method     ChildUserExperienceQuery leftJoinMessageExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageExperience relation
 * @method     ChildUserExperienceQuery rightJoinMessageExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageExperience relation
 * @method     ChildUserExperienceQuery innerJoinMessageExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageExperience relation
 *
 * @method     ChildUserExperienceQuery joinWithMessageExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithMessageExperience() Adds a LEFT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildUserExperienceQuery rightJoinWithMessageExperience() Adds a RIGHT JOIN clause and with to the query using the MessageExperience relation
 * @method     ChildUserExperienceQuery innerJoinWithMessageExperience() Adds a INNER JOIN clause and with to the query using the MessageExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinChatterExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildUserExperienceQuery rightJoinChatterExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatterExperience relation
 * @method     ChildUserExperienceQuery innerJoinChatterExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatterExperience relation
 *
 * @method     ChildUserExperienceQuery joinWithChatterExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithChatterExperience() Adds a LEFT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildUserExperienceQuery rightJoinWithChatterExperience() Adds a RIGHT JOIN clause and with to the query using the ChatterExperience relation
 * @method     ChildUserExperienceQuery innerJoinWithChatterExperience() Adds a INNER JOIN clause and with to the query using the ChatterExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinFollowExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the FollowExperience relation
 * @method     ChildUserExperienceQuery rightJoinFollowExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FollowExperience relation
 * @method     ChildUserExperienceQuery innerJoinFollowExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the FollowExperience relation
 *
 * @method     ChildUserExperienceQuery joinWithFollowExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FollowExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithFollowExperience() Adds a LEFT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildUserExperienceQuery rightJoinWithFollowExperience() Adds a RIGHT JOIN clause and with to the query using the FollowExperience relation
 * @method     ChildUserExperienceQuery innerJoinWithFollowExperience() Adds a INNER JOIN clause and with to the query using the FollowExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinHostExperience($relationAlias = null) Adds a LEFT JOIN clause to the query using the HostExperience relation
 * @method     ChildUserExperienceQuery rightJoinHostExperience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HostExperience relation
 * @method     ChildUserExperienceQuery innerJoinHostExperience($relationAlias = null) Adds a INNER JOIN clause to the query using the HostExperience relation
 *
 * @method     ChildUserExperienceQuery joinWithHostExperience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HostExperience relation
 *
 * @method     ChildUserExperienceQuery leftJoinWithHostExperience() Adds a LEFT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildUserExperienceQuery rightJoinWithHostExperience() Adds a RIGHT JOIN clause and with to the query using the HostExperience relation
 * @method     ChildUserExperienceQuery innerJoinWithHostExperience() Adds a INNER JOIN clause and with to the query using the HostExperience relation
 *
 * @method     \IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\RankQuery|\IiMedias\StreamBundle\Model\AvatarQuery|\IiMedias\StreamBundle\Model\MessageExperienceQuery|\IiMedias\StreamBundle\Model\ChatterExperienceQuery|\IiMedias\StreamBundle\Model\FollowExperienceQuery|\IiMedias\StreamBundle\Model\HostExperienceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserExperience findOne(ConnectionInterface $con = null) Return the first ChildUserExperience matching the query
 * @method     ChildUserExperience findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserExperience matching the query, or a new ChildUserExperience object populated from the query conditions when no match is found
 *
 * @method     ChildUserExperience findOneById(int $stuexp_id) Return the first ChildUserExperience filtered by the stuexp_id column
 * @method     ChildUserExperience findOneByParentId(int $stuexp_parent_id) Return the first ChildUserExperience filtered by the stuexp_parent_id column
 * @method     ChildUserExperience findOneByStreamId(int $stuexp_ststrm_id) Return the first ChildUserExperience filtered by the stuexp_ststrm_id column
 * @method     ChildUserExperience findOneByChannelId(int $stuexp_stchan_id) Return the first ChildUserExperience filtered by the stuexp_stchan_id column
 * @method     ChildUserExperience findOneBySiteId(int $stuexp_stsite_id) Return the first ChildUserExperience filtered by the stuexp_stsite_id column
 * @method     ChildUserExperience findOneByChatUserId(int $stuexp_stcusr_id) Return the first ChildUserExperience filtered by the stuexp_stcusr_id column
 * @method     ChildUserExperience findOneByChatType(int $stuexp_chat_type) Return the first ChildUserExperience filtered by the stuexp_chat_type column
 * @method     ChildUserExperience findOneByExp(int $stuexp_exp) Return the first ChildUserExperience filtered by the stuexp_exp column
 * @method     ChildUserExperience findOneByTotalExp(int $stuexp_total_exp) Return the first ChildUserExperience filtered by the stuexp_total_exp column
 * @method     ChildUserExperience findOneByIsFollowing(boolean $stuexp_is_following) Return the first ChildUserExperience filtered by the stuexp_is_following column
 * @method     ChildUserExperience findOneByMessagesCount(int $stuexp_messages_count) Return the first ChildUserExperience filtered by the stuexp_messages_count column
 * @method     ChildUserExperience findOneByFollowsCount(int $stuexp_follows_count) Return the first ChildUserExperience filtered by the stuexp_follows_count column
 * @method     ChildUserExperience findOneByHostsCount(int $stuexp_hosts_count) Return the first ChildUserExperience filtered by the stuexp_hosts_count column
 * @method     ChildUserExperience findOneByChattersCount(int $stuexp_chatters_count) Return the first ChildUserExperience filtered by the stuexp_chatters_count column
 * @method     ChildUserExperience findOneByRankId(int $stuexp_strank_id) Return the first ChildUserExperience filtered by the stuexp_strank_id column
 * @method     ChildUserExperience findOneByAvatarId(int $stuexp_stavtr_id) Return the first ChildUserExperience filtered by the stuexp_stavtr_id column *

 * @method     ChildUserExperience requirePk($key, ConnectionInterface $con = null) Return the ChildUserExperience by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOne(ConnectionInterface $con = null) Return the first ChildUserExperience matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserExperience requireOneById(int $stuexp_id) Return the first ChildUserExperience filtered by the stuexp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByParentId(int $stuexp_parent_id) Return the first ChildUserExperience filtered by the stuexp_parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByStreamId(int $stuexp_ststrm_id) Return the first ChildUserExperience filtered by the stuexp_ststrm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByChannelId(int $stuexp_stchan_id) Return the first ChildUserExperience filtered by the stuexp_stchan_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneBySiteId(int $stuexp_stsite_id) Return the first ChildUserExperience filtered by the stuexp_stsite_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByChatUserId(int $stuexp_stcusr_id) Return the first ChildUserExperience filtered by the stuexp_stcusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByChatType(int $stuexp_chat_type) Return the first ChildUserExperience filtered by the stuexp_chat_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByExp(int $stuexp_exp) Return the first ChildUserExperience filtered by the stuexp_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByTotalExp(int $stuexp_total_exp) Return the first ChildUserExperience filtered by the stuexp_total_exp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByIsFollowing(boolean $stuexp_is_following) Return the first ChildUserExperience filtered by the stuexp_is_following column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByMessagesCount(int $stuexp_messages_count) Return the first ChildUserExperience filtered by the stuexp_messages_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByFollowsCount(int $stuexp_follows_count) Return the first ChildUserExperience filtered by the stuexp_follows_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByHostsCount(int $stuexp_hosts_count) Return the first ChildUserExperience filtered by the stuexp_hosts_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByChattersCount(int $stuexp_chatters_count) Return the first ChildUserExperience filtered by the stuexp_chatters_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByRankId(int $stuexp_strank_id) Return the first ChildUserExperience filtered by the stuexp_strank_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserExperience requireOneByAvatarId(int $stuexp_stavtr_id) Return the first ChildUserExperience filtered by the stuexp_stavtr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserExperience[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserExperience objects based on current ModelCriteria
 * @method     ChildUserExperience[]|ObjectCollection findById(int $stuexp_id) Return ChildUserExperience objects filtered by the stuexp_id column
 * @method     ChildUserExperience[]|ObjectCollection findByParentId(int $stuexp_parent_id) Return ChildUserExperience objects filtered by the stuexp_parent_id column
 * @method     ChildUserExperience[]|ObjectCollection findByStreamId(int $stuexp_ststrm_id) Return ChildUserExperience objects filtered by the stuexp_ststrm_id column
 * @method     ChildUserExperience[]|ObjectCollection findByChannelId(int $stuexp_stchan_id) Return ChildUserExperience objects filtered by the stuexp_stchan_id column
 * @method     ChildUserExperience[]|ObjectCollection findBySiteId(int $stuexp_stsite_id) Return ChildUserExperience objects filtered by the stuexp_stsite_id column
 * @method     ChildUserExperience[]|ObjectCollection findByChatUserId(int $stuexp_stcusr_id) Return ChildUserExperience objects filtered by the stuexp_stcusr_id column
 * @method     ChildUserExperience[]|ObjectCollection findByChatType(int $stuexp_chat_type) Return ChildUserExperience objects filtered by the stuexp_chat_type column
 * @method     ChildUserExperience[]|ObjectCollection findByExp(int $stuexp_exp) Return ChildUserExperience objects filtered by the stuexp_exp column
 * @method     ChildUserExperience[]|ObjectCollection findByTotalExp(int $stuexp_total_exp) Return ChildUserExperience objects filtered by the stuexp_total_exp column
 * @method     ChildUserExperience[]|ObjectCollection findByIsFollowing(boolean $stuexp_is_following) Return ChildUserExperience objects filtered by the stuexp_is_following column
 * @method     ChildUserExperience[]|ObjectCollection findByMessagesCount(int $stuexp_messages_count) Return ChildUserExperience objects filtered by the stuexp_messages_count column
 * @method     ChildUserExperience[]|ObjectCollection findByFollowsCount(int $stuexp_follows_count) Return ChildUserExperience objects filtered by the stuexp_follows_count column
 * @method     ChildUserExperience[]|ObjectCollection findByHostsCount(int $stuexp_hosts_count) Return ChildUserExperience objects filtered by the stuexp_hosts_count column
 * @method     ChildUserExperience[]|ObjectCollection findByChattersCount(int $stuexp_chatters_count) Return ChildUserExperience objects filtered by the stuexp_chatters_count column
 * @method     ChildUserExperience[]|ObjectCollection findByRankId(int $stuexp_strank_id) Return ChildUserExperience objects filtered by the stuexp_strank_id column
 * @method     ChildUserExperience[]|ObjectCollection findByAvatarId(int $stuexp_stavtr_id) Return ChildUserExperience objects filtered by the stuexp_stavtr_id column
 * @method     ChildUserExperience[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserExperienceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\StreamBundle\Model\Base\UserExperienceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\StreamBundle\\Model\\UserExperience', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserExperienceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserExperienceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserExperienceQuery) {
            return $criteria;
        }
        $query = new ChildUserExperienceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserExperience|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserExperienceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperience A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT stuexp_id, stuexp_parent_id, stuexp_ststrm_id, stuexp_stchan_id, stuexp_stsite_id, stuexp_stcusr_id, stuexp_chat_type, stuexp_exp, stuexp_total_exp, stuexp_is_following, stuexp_messages_count, stuexp_follows_count, stuexp_hosts_count, stuexp_chatters_count, stuexp_strank_id, stuexp_stavtr_id FROM stream_user_experience_stuexp WHERE stuexp_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserExperience $obj */
            $obj = new ChildUserExperience();
            $obj->hydrate($row);
            UserExperienceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserExperience|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the stuexp_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE stuexp_id = 1234
     * $query->filterById(array(12, 34)); // WHERE stuexp_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE stuexp_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $id, $comparison);
    }

    /**
     * Filter the query on the stuexp_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE stuexp_parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE stuexp_parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE stuexp_parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the stuexp_ststrm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreamId(1234); // WHERE stuexp_ststrm_id = 1234
     * $query->filterByStreamId(array(12, 34)); // WHERE stuexp_ststrm_id IN (12, 34)
     * $query->filterByStreamId(array('min' => 12)); // WHERE stuexp_ststrm_id > 12
     * </code>
     *
     * @see       filterByStream()
     *
     * @param     mixed $streamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByStreamId($streamId = null, $comparison = null)
    {
        if (is_array($streamId)) {
            $useMinMax = false;
            if (isset($streamId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $streamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streamId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $streamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $streamId, $comparison);
    }

    /**
     * Filter the query on the stuexp_stchan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChannelId(1234); // WHERE stuexp_stchan_id = 1234
     * $query->filterByChannelId(array(12, 34)); // WHERE stuexp_stchan_id IN (12, 34)
     * $query->filterByChannelId(array('min' => 12)); // WHERE stuexp_stchan_id > 12
     * </code>
     *
     * @see       filterByChannel()
     *
     * @param     mixed $channelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChannelId($channelId = null, $comparison = null)
    {
        if (is_array($channelId)) {
            $useMinMax = false;
            if (isset($channelId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $channelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($channelId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $channelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $channelId, $comparison);
    }

    /**
     * Filter the query on the stuexp_stsite_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE stuexp_stsite_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE stuexp_stsite_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE stuexp_stsite_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the stuexp_stcusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatUserId(1234); // WHERE stuexp_stcusr_id = 1234
     * $query->filterByChatUserId(array(12, 34)); // WHERE stuexp_stcusr_id IN (12, 34)
     * $query->filterByChatUserId(array('min' => 12)); // WHERE stuexp_stcusr_id > 12
     * </code>
     *
     * @see       filterByChatUser()
     *
     * @param     mixed $chatUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUserId($chatUserId = null, $comparison = null)
    {
        if (is_array($chatUserId)) {
            $useMinMax = false;
            if (isset($chatUserId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $chatUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatUserId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $chatUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $chatUserId, $comparison);
    }

    /**
     * Filter the query on the stuexp_chat_type column
     *
     * @param     mixed $chatType The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChatType($chatType = null, $comparison = null)
    {
        $valueSet = UserExperienceTableMap::getValueSet(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE);
        if (is_scalar($chatType)) {
            if (!in_array($chatType, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $chatType));
            }
            $chatType = array_search($chatType, $valueSet);
        } elseif (is_array($chatType)) {
            $convertedValues = array();
            foreach ($chatType as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $chatType = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE, $chatType, $comparison);
    }

    /**
     * Filter the query on the stuexp_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByExp(1234); // WHERE stuexp_exp = 1234
     * $query->filterByExp(array(12, 34)); // WHERE stuexp_exp IN (12, 34)
     * $query->filterByExp(array('min' => 12)); // WHERE stuexp_exp > 12
     * </code>
     *
     * @param     mixed $exp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByExp($exp = null, $comparison = null)
    {
        if (is_array($exp)) {
            $useMinMax = false;
            if (isset($exp['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_EXP, $exp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exp['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_EXP, $exp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_EXP, $exp, $comparison);
    }

    /**
     * Filter the query on the stuexp_total_exp column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalExp(1234); // WHERE stuexp_total_exp = 1234
     * $query->filterByTotalExp(array(12, 34)); // WHERE stuexp_total_exp IN (12, 34)
     * $query->filterByTotalExp(array('min' => 12)); // WHERE stuexp_total_exp > 12
     * </code>
     *
     * @param     mixed $totalExp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByTotalExp($totalExp = null, $comparison = null)
    {
        if (is_array($totalExp)) {
            $useMinMax = false;
            if (isset($totalExp['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP, $totalExp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalExp['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP, $totalExp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP, $totalExp, $comparison);
    }

    /**
     * Filter the query on the stuexp_is_following column
     *
     * Example usage:
     * <code>
     * $query->filterByIsFollowing(true); // WHERE stuexp_is_following = true
     * $query->filterByIsFollowing('yes'); // WHERE stuexp_is_following = true
     * </code>
     *
     * @param     boolean|string $isFollowing The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByIsFollowing($isFollowing = null, $comparison = null)
    {
        if (is_string($isFollowing)) {
            $isFollowing = in_array(strtolower($isFollowing), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING, $isFollowing, $comparison);
    }

    /**
     * Filter the query on the stuexp_messages_count column
     *
     * Example usage:
     * <code>
     * $query->filterByMessagesCount(1234); // WHERE stuexp_messages_count = 1234
     * $query->filterByMessagesCount(array(12, 34)); // WHERE stuexp_messages_count IN (12, 34)
     * $query->filterByMessagesCount(array('min' => 12)); // WHERE stuexp_messages_count > 12
     * </code>
     *
     * @param     mixed $messagesCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByMessagesCount($messagesCount = null, $comparison = null)
    {
        if (is_array($messagesCount)) {
            $useMinMax = false;
            if (isset($messagesCount['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT, $messagesCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($messagesCount['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT, $messagesCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT, $messagesCount, $comparison);
    }

    /**
     * Filter the query on the stuexp_follows_count column
     *
     * Example usage:
     * <code>
     * $query->filterByFollowsCount(1234); // WHERE stuexp_follows_count = 1234
     * $query->filterByFollowsCount(array(12, 34)); // WHERE stuexp_follows_count IN (12, 34)
     * $query->filterByFollowsCount(array('min' => 12)); // WHERE stuexp_follows_count > 12
     * </code>
     *
     * @param     mixed $followsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByFollowsCount($followsCount = null, $comparison = null)
    {
        if (is_array($followsCount)) {
            $useMinMax = false;
            if (isset($followsCount['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT, $followsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($followsCount['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT, $followsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT, $followsCount, $comparison);
    }

    /**
     * Filter the query on the stuexp_hosts_count column
     *
     * Example usage:
     * <code>
     * $query->filterByHostsCount(1234); // WHERE stuexp_hosts_count = 1234
     * $query->filterByHostsCount(array(12, 34)); // WHERE stuexp_hosts_count IN (12, 34)
     * $query->filterByHostsCount(array('min' => 12)); // WHERE stuexp_hosts_count > 12
     * </code>
     *
     * @param     mixed $hostsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByHostsCount($hostsCount = null, $comparison = null)
    {
        if (is_array($hostsCount)) {
            $useMinMax = false;
            if (isset($hostsCount['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT, $hostsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hostsCount['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT, $hostsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT, $hostsCount, $comparison);
    }

    /**
     * Filter the query on the stuexp_chatters_count column
     *
     * Example usage:
     * <code>
     * $query->filterByChattersCount(1234); // WHERE stuexp_chatters_count = 1234
     * $query->filterByChattersCount(array(12, 34)); // WHERE stuexp_chatters_count IN (12, 34)
     * $query->filterByChattersCount(array('min' => 12)); // WHERE stuexp_chatters_count > 12
     * </code>
     *
     * @param     mixed $chattersCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChattersCount($chattersCount = null, $comparison = null)
    {
        if (is_array($chattersCount)) {
            $useMinMax = false;
            if (isset($chattersCount['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT, $chattersCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chattersCount['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT, $chattersCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT, $chattersCount, $comparison);
    }

    /**
     * Filter the query on the stuexp_strank_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRankId(1234); // WHERE stuexp_strank_id = 1234
     * $query->filterByRankId(array(12, 34)); // WHERE stuexp_strank_id IN (12, 34)
     * $query->filterByRankId(array('min' => 12)); // WHERE stuexp_strank_id > 12
     * </code>
     *
     * @see       filterByRank()
     *
     * @param     mixed $rankId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByRankId($rankId = null, $comparison = null)
    {
        if (is_array($rankId)) {
            $useMinMax = false;
            if (isset($rankId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $rankId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rankId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $rankId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $rankId, $comparison);
    }

    /**
     * Filter the query on the stuexp_stavtr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAvatarId(1234); // WHERE stuexp_stavtr_id = 1234
     * $query->filterByAvatarId(array(12, 34)); // WHERE stuexp_stavtr_id IN (12, 34)
     * $query->filterByAvatarId(array('min' => 12)); // WHERE stuexp_stavtr_id > 12
     * </code>
     *
     * @see       filterByAvatar()
     *
     * @param     mixed $avatarId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByAvatarId($avatarId = null, $comparison = null)
    {
        if (is_array($avatarId)) {
            $useMinMax = false;
            if (isset($avatarId['min'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $avatarId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($avatarId['max'])) {
                $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $avatarId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $avatarId, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByStream($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $stream->getId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSTRM_ID, $stream->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStream() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stream relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinStream($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stream');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stream');
        }

        return $this;
    }

    /**
     * Use the Stream relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useStreamQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStream($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stream', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STSITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $channel->getId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCHAN_ID, $channel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChatUser($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $chatUser->getId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STCUSR_ID, $chatUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatUser() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinChatUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatUser');
        }

        return $this;
    }

    /**
     * Use the ChatUser relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useChatUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatUser', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByRank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $rank->getId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STRANK_ID, $rank->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinRank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rank');
        }

        return $this;
    }

    /**
     * Use the Rank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useRankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByAvatar($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $avatar->getId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_STAVTR_ID, $avatar->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAvatar() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Avatar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinAvatar($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Avatar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Avatar');
        }

        return $this;
    }

    /**
     * Use the Avatar relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useAvatarQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvatar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Avatar', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\MessageExperience object
     *
     * @param \IiMedias\StreamBundle\Model\MessageExperience|ObjectCollection $messageExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByMessageExperience($messageExperience, $comparison = null)
    {
        if ($messageExperience instanceof \IiMedias\StreamBundle\Model\MessageExperience) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $messageExperience->getUserExperienceId(), $comparison);
        } elseif ($messageExperience instanceof ObjectCollection) {
            return $this
                ->useMessageExperienceQuery()
                ->filterByPrimaryKeys($messageExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\MessageExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinMessageExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageExperience');
        }

        return $this;
    }

    /**
     * Use the MessageExperience relation MessageExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\MessageExperienceQuery A secondary query class using the current class as primary query
     */
    public function useMessageExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMessageExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageExperience', '\IiMedias\StreamBundle\Model\MessageExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatterExperience object
     *
     * @param \IiMedias\StreamBundle\Model\ChatterExperience|ObjectCollection $chatterExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByChatterExperience($chatterExperience, $comparison = null)
    {
        if ($chatterExperience instanceof \IiMedias\StreamBundle\Model\ChatterExperience) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $chatterExperience->getUserExperienceId(), $comparison);
        } elseif ($chatterExperience instanceof ObjectCollection) {
            return $this
                ->useChatterExperienceQuery()
                ->filterByPrimaryKeys($chatterExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatterExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatterExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatterExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinChatterExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatterExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatterExperience');
        }

        return $this;
    }

    /**
     * Use the ChatterExperience relation ChatterExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatterExperienceQuery A secondary query class using the current class as primary query
     */
    public function useChatterExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChatterExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatterExperience', '\IiMedias\StreamBundle\Model\ChatterExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\FollowExperience object
     *
     * @param \IiMedias\StreamBundle\Model\FollowExperience|ObjectCollection $followExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByFollowExperience($followExperience, $comparison = null)
    {
        if ($followExperience instanceof \IiMedias\StreamBundle\Model\FollowExperience) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $followExperience->getUserExperienceId(), $comparison);
        } elseif ($followExperience instanceof ObjectCollection) {
            return $this
                ->useFollowExperienceQuery()
                ->filterByPrimaryKeys($followExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFollowExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\FollowExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FollowExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinFollowExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FollowExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FollowExperience');
        }

        return $this;
    }

    /**
     * Use the FollowExperience relation FollowExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\FollowExperienceQuery A secondary query class using the current class as primary query
     */
    public function useFollowExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFollowExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FollowExperience', '\IiMedias\StreamBundle\Model\FollowExperienceQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\HostExperience object
     *
     * @param \IiMedias\StreamBundle\Model\HostExperience|ObjectCollection $hostExperience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserExperienceQuery The current query, for fluid interface
     */
    public function filterByHostExperience($hostExperience, $comparison = null)
    {
        if ($hostExperience instanceof \IiMedias\StreamBundle\Model\HostExperience) {
            return $this
                ->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $hostExperience->getUserExperienceId(), $comparison);
        } elseif ($hostExperience instanceof ObjectCollection) {
            return $this
                ->useHostExperienceQuery()
                ->filterByPrimaryKeys($hostExperience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHostExperience() only accepts arguments of type \IiMedias\StreamBundle\Model\HostExperience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HostExperience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function joinHostExperience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HostExperience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HostExperience');
        }

        return $this;
    }

    /**
     * Use the HostExperience relation HostExperience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\HostExperienceQuery A secondary query class using the current class as primary query
     */
    public function useHostExperienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinHostExperience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HostExperience', '\IiMedias\StreamBundle\Model\HostExperienceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserExperience $userExperience Object to remove from the list of results
     *
     * @return $this|ChildUserExperienceQuery The current query, for fluid interface
     */
    public function prune($userExperience = null)
    {
        if ($userExperience) {
            $this->addUsingAlias(UserExperienceTableMap::COL_STUEXP_ID, $userExperience->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stream_user_experience_stuexp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserExperienceTableMap::clearInstancePool();
            UserExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserExperienceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserExperienceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserExperienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserExperienceQuery

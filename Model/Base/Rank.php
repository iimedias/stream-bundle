<?php

namespace IiMedias\StreamBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\StreamBundle\Model\Avatar as ChildAvatar;
use IiMedias\StreamBundle\Model\AvatarQuery as ChildAvatarQuery;
use IiMedias\StreamBundle\Model\Rank as ChildRank;
use IiMedias\StreamBundle\Model\RankQuery as ChildRankQuery;
use IiMedias\StreamBundle\Model\Stream as ChildStream;
use IiMedias\StreamBundle\Model\StreamQuery as ChildStreamQuery;
use IiMedias\StreamBundle\Model\UserExperience as ChildUserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery as ChildUserExperienceQuery;
use IiMedias\StreamBundle\Model\Map\AvatarTableMap;
use IiMedias\StreamBundle\Model\Map\RankTableMap;
use IiMedias\StreamBundle\Model\Map\UserExperienceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'stream_rank_strank' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.StreamBundle.Model.Base
 */
abstract class Rank implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\StreamBundle\\Model\\Map\\RankTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the strank_id field.
     *
     * @var        int
     */
    protected $strank_id;

    /**
     * The value for the strank_ststrm_id field.
     *
     * @var        int
     */
    protected $strank_ststrm_id;

    /**
     * The value for the strank_name field.
     *
     * @var        string
     */
    protected $strank_name;

    /**
     * The value for the strank_is_default field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $strank_is_default;

    /**
     * The value for the strank_set_if_follow_event field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $strank_set_if_follow_event;

    /**
     * The value for the strank_set_if_moderator field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $strank_set_if_moderator;

    /**
     * The value for the strank_set_if_streamer field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $strank_set_if_streamer;

    /**
     * The value for the strank_dependency_rank_ids field.
     *
     * @var        array
     */
    protected $strank_dependency_rank_ids;

    /**
     * The unserialized $strank_dependency_rank_ids value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $strank_dependency_rank_ids_unserialized;

    /**
     * The value for the strank_created_by_user_id field.
     *
     * @var        int
     */
    protected $strank_created_by_user_id;

    /**
     * The value for the strank_updated_by_user_id field.
     *
     * @var        int
     */
    protected $strank_updated_by_user_id;

    /**
     * The value for the strank_created_at field.
     *
     * @var        DateTime
     */
    protected $strank_created_at;

    /**
     * The value for the strank_updated_at field.
     *
     * @var        DateTime
     */
    protected $strank_updated_at;

    /**
     * @var        ChildStream
     */
    protected $aStream;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildAvatar[] Collection to store aggregation of ChildAvatar objects.
     */
    protected $collAvatarByMinRanks;
    protected $collAvatarByMinRanksPartial;

    /**
     * @var        ObjectCollection|ChildAvatar[] Collection to store aggregation of ChildAvatar objects.
     */
    protected $collAvatarByAssociatedRanks;
    protected $collAvatarByAssociatedRanksPartial;

    /**
     * @var        ObjectCollection|ChildUserExperience[] Collection to store aggregation of ChildUserExperience objects.
     */
    protected $collUserExperiences;
    protected $collUserExperiencesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAvatar[]
     */
    protected $avatarByMinRanksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAvatar[]
     */
    protected $avatarByAssociatedRanksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserExperience[]
     */
    protected $userExperiencesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->strank_is_default = false;
        $this->strank_set_if_follow_event = false;
        $this->strank_set_if_moderator = false;
        $this->strank_set_if_streamer = false;
    }

    /**
     * Initializes internal state of IiMedias\StreamBundle\Model\Base\Rank object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Rank</code> instance.  If
     * <code>obj</code> is an instance of <code>Rank</code>, delegates to
     * <code>equals(Rank)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Rank The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [strank_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->strank_id;
    }

    /**
     * Get the [strank_ststrm_id] column value.
     *
     * @return int
     */
    public function getStreamId()
    {
        return $this->strank_ststrm_id;
    }

    /**
     * Get the [strank_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->strank_name;
    }

    /**
     * Get the [strank_is_default] column value.
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->strank_is_default;
    }

    /**
     * Get the [strank_is_default] column value.
     *
     * @return boolean
     */
    public function isDefault()
    {
        return $this->getIsDefault();
    }

    /**
     * Get the [strank_set_if_follow_event] column value.
     *
     * @return boolean
     */
    public function getSetIfFollowEvent()
    {
        return $this->strank_set_if_follow_event;
    }

    /**
     * Get the [strank_set_if_follow_event] column value.
     *
     * @return boolean
     */
    public function isSetIfFollowEvent()
    {
        return $this->getSetIfFollowEvent();
    }

    /**
     * Get the [strank_set_if_moderator] column value.
     *
     * @return boolean
     */
    public function getSetIfModerator()
    {
        return $this->strank_set_if_moderator;
    }

    /**
     * Get the [strank_set_if_moderator] column value.
     *
     * @return boolean
     */
    public function isSetIfModerator()
    {
        return $this->getSetIfModerator();
    }

    /**
     * Get the [strank_set_if_streamer] column value.
     *
     * @return boolean
     */
    public function getSetIfStreamer()
    {
        return $this->strank_set_if_streamer;
    }

    /**
     * Get the [strank_set_if_streamer] column value.
     *
     * @return boolean
     */
    public function isSetIfStreamer()
    {
        return $this->getSetIfStreamer();
    }

    /**
     * Get the [strank_dependency_rank_ids] column value.
     *
     * @return array
     */
    public function getDependencyRankIds()
    {
        if (null === $this->strank_dependency_rank_ids_unserialized) {
            $this->strank_dependency_rank_ids_unserialized = array();
        }
        if (!$this->strank_dependency_rank_ids_unserialized && null !== $this->strank_dependency_rank_ids) {
            $strank_dependency_rank_ids_unserialized = substr($this->strank_dependency_rank_ids, 2, -2);
            $this->strank_dependency_rank_ids_unserialized = '' !== $strank_dependency_rank_ids_unserialized ? explode(' | ', $strank_dependency_rank_ids_unserialized) : array();
        }

        return $this->strank_dependency_rank_ids_unserialized;
    }

    /**
     * Test the presence of a value in the [strank_dependency_rank_ids] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasDependencyRankId($value)
    {
        return in_array($value, $this->getDependencyRankIds());
    } // hasDependencyRankId()

    /**
     * Get the [strank_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->strank_created_by_user_id;
    }

    /**
     * Get the [strank_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->strank_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [strank_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->strank_created_at;
        } else {
            return $this->strank_created_at instanceof \DateTimeInterface ? $this->strank_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [strank_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->strank_updated_at;
        } else {
            return $this->strank_updated_at instanceof \DateTimeInterface ? $this->strank_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [strank_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->strank_id !== $v) {
            $this->strank_id = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [strank_ststrm_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setStreamId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->strank_ststrm_id !== $v) {
            $this->strank_ststrm_id = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_STSTRM_ID] = true;
        }

        if ($this->aStream !== null && $this->aStream->getId() !== $v) {
            $this->aStream = null;
        }

        return $this;
    } // setStreamId()

    /**
     * Set the value of [strank_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->strank_name !== $v) {
            $this->strank_name = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Sets the value of the [strank_is_default] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setIsDefault($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->strank_is_default !== $v) {
            $this->strank_is_default = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_IS_DEFAULT] = true;
        }

        return $this;
    } // setIsDefault()

    /**
     * Sets the value of the [strank_set_if_follow_event] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setSetIfFollowEvent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->strank_set_if_follow_event !== $v) {
            $this->strank_set_if_follow_event = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT] = true;
        }

        return $this;
    } // setSetIfFollowEvent()

    /**
     * Sets the value of the [strank_set_if_moderator] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setSetIfModerator($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->strank_set_if_moderator !== $v) {
            $this->strank_set_if_moderator = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_SET_IF_MODERATOR] = true;
        }

        return $this;
    } // setSetIfModerator()

    /**
     * Sets the value of the [strank_set_if_streamer] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setSetIfStreamer($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->strank_set_if_streamer !== $v) {
            $this->strank_set_if_streamer = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_SET_IF_STREAMER] = true;
        }

        return $this;
    } // setSetIfStreamer()

    /**
     * Set the value of [strank_dependency_rank_ids] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setDependencyRankIds($v)
    {
        if ($this->strank_dependency_rank_ids_unserialized !== $v) {
            $this->strank_dependency_rank_ids_unserialized = $v;
            $this->strank_dependency_rank_ids = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS] = true;
        }

        return $this;
    } // setDependencyRankIds()

    /**
     * Adds a value to the [strank_dependency_rank_ids] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function addDependencyRankId($value)
    {
        $currentArray = $this->getDependencyRankIds();
        $currentArray []= $value;
        $this->setDependencyRankIds($currentArray);

        return $this;
    } // addDependencyRankId()

    /**
     * Removes a value from the [strank_dependency_rank_ids] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function removeDependencyRankId($value)
    {
        $targetArray = array();
        foreach ($this->getDependencyRankIds() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setDependencyRankIds($targetArray);

        return $this;
    } // removeDependencyRankId()

    /**
     * Set the value of [strank_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->strank_created_by_user_id !== $v) {
            $this->strank_created_by_user_id = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [strank_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->strank_updated_by_user_id !== $v) {
            $this->strank_updated_by_user_id = $v;
            $this->modifiedColumns[RankTableMap::COL_STRANK_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [strank_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->strank_created_at !== null || $dt !== null) {
            if ($this->strank_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->strank_created_at->format("Y-m-d H:i:s.u")) {
                $this->strank_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RankTableMap::COL_STRANK_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [strank_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->strank_updated_at !== null || $dt !== null) {
            if ($this->strank_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->strank_updated_at->format("Y-m-d H:i:s.u")) {
                $this->strank_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RankTableMap::COL_STRANK_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->strank_is_default !== false) {
                return false;
            }

            if ($this->strank_set_if_follow_event !== false) {
                return false;
            }

            if ($this->strank_set_if_moderator !== false) {
                return false;
            }

            if ($this->strank_set_if_streamer !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : RankTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : RankTableMap::translateFieldName('StreamId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_ststrm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : RankTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : RankTableMap::translateFieldName('IsDefault', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_is_default = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : RankTableMap::translateFieldName('SetIfFollowEvent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_set_if_follow_event = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : RankTableMap::translateFieldName('SetIfModerator', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_set_if_moderator = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : RankTableMap::translateFieldName('SetIfStreamer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_set_if_streamer = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : RankTableMap::translateFieldName('DependencyRankIds', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_dependency_rank_ids = $col;
            $this->strank_dependency_rank_ids_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : RankTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : RankTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strank_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : RankTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->strank_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : RankTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->strank_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = RankTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\StreamBundle\\Model\\Rank'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStream !== null && $this->strank_ststrm_id !== $this->aStream->getId()) {
            $this->aStream = null;
        }
        if ($this->aCreatedByUser !== null && $this->strank_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->strank_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RankTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildRankQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStream = null;
            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collAvatarByMinRanks = null;

            $this->collAvatarByAssociatedRanks = null;

            $this->collUserExperiences = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Rank::setDeleted()
     * @see Rank::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildRankQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(RankTableMap::COL_STRANK_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RankTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStream !== null) {
                if ($this->aStream->isModified() || $this->aStream->isNew()) {
                    $affectedRows += $this->aStream->save($con);
                }
                $this->setStream($this->aStream);
            }

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->avatarByMinRanksScheduledForDeletion !== null) {
                if (!$this->avatarByMinRanksScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\AvatarQuery::create()
                        ->filterByPrimaryKeys($this->avatarByMinRanksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->avatarByMinRanksScheduledForDeletion = null;
                }
            }

            if ($this->collAvatarByMinRanks !== null) {
                foreach ($this->collAvatarByMinRanks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->avatarByAssociatedRanksScheduledForDeletion !== null) {
                if (!$this->avatarByAssociatedRanksScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\AvatarQuery::create()
                        ->filterByPrimaryKeys($this->avatarByAssociatedRanksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->avatarByAssociatedRanksScheduledForDeletion = null;
                }
            }

            if ($this->collAvatarByAssociatedRanks !== null) {
                foreach ($this->collAvatarByAssociatedRanks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userExperiencesScheduledForDeletion !== null) {
                if (!$this->userExperiencesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\UserExperienceQuery::create()
                        ->filterByPrimaryKeys($this->userExperiencesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userExperiencesScheduledForDeletion = null;
                }
            }

            if ($this->collUserExperiences !== null) {
                foreach ($this->collUserExperiences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[RankTableMap::COL_STRANK_ID] = true;
        if (null !== $this->strank_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . RankTableMap::COL_STRANK_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(RankTableMap::COL_STRANK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'strank_id';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_STSTRM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'strank_ststrm_id';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'strank_name';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_IS_DEFAULT)) {
            $modifiedColumns[':p' . $index++]  = 'strank_is_default';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT)) {
            $modifiedColumns[':p' . $index++]  = 'strank_set_if_follow_event';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_MODERATOR)) {
            $modifiedColumns[':p' . $index++]  = 'strank_set_if_moderator';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_STREAMER)) {
            $modifiedColumns[':p' . $index++]  = 'strank_set_if_streamer';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS)) {
            $modifiedColumns[':p' . $index++]  = 'strank_dependency_rank_ids';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'strank_created_by_user_id';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'strank_updated_by_user_id';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'strank_created_at';
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'strank_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO stream_rank_strank (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'strank_id':
                        $stmt->bindValue($identifier, $this->strank_id, PDO::PARAM_INT);
                        break;
                    case 'strank_ststrm_id':
                        $stmt->bindValue($identifier, $this->strank_ststrm_id, PDO::PARAM_INT);
                        break;
                    case 'strank_name':
                        $stmt->bindValue($identifier, $this->strank_name, PDO::PARAM_STR);
                        break;
                    case 'strank_is_default':
                        $stmt->bindValue($identifier, (int) $this->strank_is_default, PDO::PARAM_INT);
                        break;
                    case 'strank_set_if_follow_event':
                        $stmt->bindValue($identifier, (int) $this->strank_set_if_follow_event, PDO::PARAM_INT);
                        break;
                    case 'strank_set_if_moderator':
                        $stmt->bindValue($identifier, (int) $this->strank_set_if_moderator, PDO::PARAM_INT);
                        break;
                    case 'strank_set_if_streamer':
                        $stmt->bindValue($identifier, (int) $this->strank_set_if_streamer, PDO::PARAM_INT);
                        break;
                    case 'strank_dependency_rank_ids':
                        $stmt->bindValue($identifier, $this->strank_dependency_rank_ids, PDO::PARAM_STR);
                        break;
                    case 'strank_created_by_user_id':
                        $stmt->bindValue($identifier, $this->strank_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'strank_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->strank_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'strank_created_at':
                        $stmt->bindValue($identifier, $this->strank_created_at ? $this->strank_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'strank_updated_at':
                        $stmt->bindValue($identifier, $this->strank_updated_at ? $this->strank_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RankTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getStreamId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getIsDefault();
                break;
            case 4:
                return $this->getSetIfFollowEvent();
                break;
            case 5:
                return $this->getSetIfModerator();
                break;
            case 6:
                return $this->getSetIfStreamer();
                break;
            case 7:
                return $this->getDependencyRankIds();
                break;
            case 8:
                return $this->getCreatedByUserId();
                break;
            case 9:
                return $this->getUpdatedByUserId();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Rank'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Rank'][$this->hashCode()] = true;
        $keys = RankTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getStreamId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getIsDefault(),
            $keys[4] => $this->getSetIfFollowEvent(),
            $keys[5] => $this->getSetIfModerator(),
            $keys[6] => $this->getSetIfStreamer(),
            $keys[7] => $this->getDependencyRankIds(),
            $keys[8] => $this->getCreatedByUserId(),
            $keys[9] => $this->getUpdatedByUserId(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStream) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'stream';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrm';
                        break;
                    default:
                        $key = 'Stream';
                }

                $result[$key] = $this->aStream->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collAvatarByMinRanks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'avatars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_avatar_stavtrs';
                        break;
                    default:
                        $key = 'AvatarByMinRanks';
                }

                $result[$key] = $this->collAvatarByMinRanks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAvatarByAssociatedRanks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'avatars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_avatar_stavtrs';
                        break;
                    default:
                        $key = 'AvatarByAssociatedRanks';
                }

                $result[$key] = $this->collAvatarByAssociatedRanks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserExperiences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userExperiences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_user_experience_stuexps';
                        break;
                    default:
                        $key = 'UserExperiences';
                }

                $result[$key] = $this->collUserExperiences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\StreamBundle\Model\Rank
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RankTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\StreamBundle\Model\Rank
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setStreamId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setIsDefault($value);
                break;
            case 4:
                $this->setSetIfFollowEvent($value);
                break;
            case 5:
                $this->setSetIfModerator($value);
                break;
            case 6:
                $this->setSetIfStreamer($value);
                break;
            case 7:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setDependencyRankIds($value);
                break;
            case 8:
                $this->setCreatedByUserId($value);
                break;
            case 9:
                $this->setUpdatedByUserId($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = RankTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setStreamId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIsDefault($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSetIfFollowEvent($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSetIfModerator($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSetIfStreamer($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDependencyRankIds($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedByUserId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RankTableMap::DATABASE_NAME);

        if ($this->isColumnModified(RankTableMap::COL_STRANK_ID)) {
            $criteria->add(RankTableMap::COL_STRANK_ID, $this->strank_id);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_STSTRM_ID)) {
            $criteria->add(RankTableMap::COL_STRANK_STSTRM_ID, $this->strank_ststrm_id);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_NAME)) {
            $criteria->add(RankTableMap::COL_STRANK_NAME, $this->strank_name);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_IS_DEFAULT)) {
            $criteria->add(RankTableMap::COL_STRANK_IS_DEFAULT, $this->strank_is_default);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT)) {
            $criteria->add(RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT, $this->strank_set_if_follow_event);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_MODERATOR)) {
            $criteria->add(RankTableMap::COL_STRANK_SET_IF_MODERATOR, $this->strank_set_if_moderator);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_SET_IF_STREAMER)) {
            $criteria->add(RankTableMap::COL_STRANK_SET_IF_STREAMER, $this->strank_set_if_streamer);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS)) {
            $criteria->add(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS, $this->strank_dependency_rank_ids);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_CREATED_BY_USER_ID)) {
            $criteria->add(RankTableMap::COL_STRANK_CREATED_BY_USER_ID, $this->strank_created_by_user_id);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID)) {
            $criteria->add(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, $this->strank_updated_by_user_id);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_CREATED_AT)) {
            $criteria->add(RankTableMap::COL_STRANK_CREATED_AT, $this->strank_created_at);
        }
        if ($this->isColumnModified(RankTableMap::COL_STRANK_UPDATED_AT)) {
            $criteria->add(RankTableMap::COL_STRANK_UPDATED_AT, $this->strank_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildRankQuery::create();
        $criteria->add(RankTableMap::COL_STRANK_ID, $this->strank_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (strank_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\StreamBundle\Model\Rank (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setStreamId($this->getStreamId());
        $copyObj->setName($this->getName());
        $copyObj->setIsDefault($this->getIsDefault());
        $copyObj->setSetIfFollowEvent($this->getSetIfFollowEvent());
        $copyObj->setSetIfModerator($this->getSetIfModerator());
        $copyObj->setSetIfStreamer($this->getSetIfStreamer());
        $copyObj->setDependencyRankIds($this->getDependencyRankIds());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getAvatarByMinRanks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAvatarByMinRank($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAvatarByAssociatedRanks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAvatarByAssociatedRank($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserExperiences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserExperience($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\StreamBundle\Model\Rank Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStream object.
     *
     * @param  ChildStream $v
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStream(ChildStream $v = null)
    {
        if ($v === null) {
            $this->setStreamId(NULL);
        } else {
            $this->setStreamId($v->getId());
        }

        $this->aStream = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStream object, it will not be re-added.
        if ($v !== null) {
            $v->addRank($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStream object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStream The associated ChildStream object.
     * @throws PropelException
     */
    public function getStream(ConnectionInterface $con = null)
    {
        if ($this->aStream === null && ($this->strank_ststrm_id != 0)) {
            $this->aStream = ChildStreamQuery::create()->findPk($this->strank_ststrm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStream->addRanks($this);
             */
        }

        return $this->aStream;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserStrank($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->strank_created_by_user_id != 0)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->strank_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserStranks($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserStrank($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->strank_updated_by_user_id != 0)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->strank_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserStranks($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('AvatarByMinRank' == $relationName) {
            $this->initAvatarByMinRanks();
            return;
        }
        if ('AvatarByAssociatedRank' == $relationName) {
            $this->initAvatarByAssociatedRanks();
            return;
        }
        if ('UserExperience' == $relationName) {
            $this->initUserExperiences();
            return;
        }
    }

    /**
     * Clears out the collAvatarByMinRanks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAvatarByMinRanks()
     */
    public function clearAvatarByMinRanks()
    {
        $this->collAvatarByMinRanks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAvatarByMinRanks collection loaded partially.
     */
    public function resetPartialAvatarByMinRanks($v = true)
    {
        $this->collAvatarByMinRanksPartial = $v;
    }

    /**
     * Initializes the collAvatarByMinRanks collection.
     *
     * By default this just sets the collAvatarByMinRanks collection to an empty array (like clearcollAvatarByMinRanks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAvatarByMinRanks($overrideExisting = true)
    {
        if (null !== $this->collAvatarByMinRanks && !$overrideExisting) {
            return;
        }

        $collectionClassName = AvatarTableMap::getTableMap()->getCollectionClassName();

        $this->collAvatarByMinRanks = new $collectionClassName;
        $this->collAvatarByMinRanks->setModel('\IiMedias\StreamBundle\Model\Avatar');
    }

    /**
     * Gets an array of ChildAvatar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRank is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     * @throws PropelException
     */
    public function getAvatarByMinRanks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAvatarByMinRanksPartial && !$this->isNew();
        if (null === $this->collAvatarByMinRanks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAvatarByMinRanks) {
                // return empty collection
                $this->initAvatarByMinRanks();
            } else {
                $collAvatarByMinRanks = ChildAvatarQuery::create(null, $criteria)
                    ->filterByMinRank($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAvatarByMinRanksPartial && count($collAvatarByMinRanks)) {
                        $this->initAvatarByMinRanks(false);

                        foreach ($collAvatarByMinRanks as $obj) {
                            if (false == $this->collAvatarByMinRanks->contains($obj)) {
                                $this->collAvatarByMinRanks->append($obj);
                            }
                        }

                        $this->collAvatarByMinRanksPartial = true;
                    }

                    return $collAvatarByMinRanks;
                }

                if ($partial && $this->collAvatarByMinRanks) {
                    foreach ($this->collAvatarByMinRanks as $obj) {
                        if ($obj->isNew()) {
                            $collAvatarByMinRanks[] = $obj;
                        }
                    }
                }

                $this->collAvatarByMinRanks = $collAvatarByMinRanks;
                $this->collAvatarByMinRanksPartial = false;
            }
        }

        return $this->collAvatarByMinRanks;
    }

    /**
     * Sets a collection of ChildAvatar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $avatarByMinRanks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function setAvatarByMinRanks(Collection $avatarByMinRanks, ConnectionInterface $con = null)
    {
        /** @var ChildAvatar[] $avatarByMinRanksToDelete */
        $avatarByMinRanksToDelete = $this->getAvatarByMinRanks(new Criteria(), $con)->diff($avatarByMinRanks);


        $this->avatarByMinRanksScheduledForDeletion = $avatarByMinRanksToDelete;

        foreach ($avatarByMinRanksToDelete as $avatarByMinRankRemoved) {
            $avatarByMinRankRemoved->setMinRank(null);
        }

        $this->collAvatarByMinRanks = null;
        foreach ($avatarByMinRanks as $avatarByMinRank) {
            $this->addAvatarByMinRank($avatarByMinRank);
        }

        $this->collAvatarByMinRanks = $avatarByMinRanks;
        $this->collAvatarByMinRanksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Avatar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Avatar objects.
     * @throws PropelException
     */
    public function countAvatarByMinRanks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAvatarByMinRanksPartial && !$this->isNew();
        if (null === $this->collAvatarByMinRanks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAvatarByMinRanks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAvatarByMinRanks());
            }

            $query = ChildAvatarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMinRank($this)
                ->count($con);
        }

        return count($this->collAvatarByMinRanks);
    }

    /**
     * Method called to associate a ChildAvatar object to this object
     * through the ChildAvatar foreign key attribute.
     *
     * @param  ChildAvatar $l ChildAvatar
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function addAvatarByMinRank(ChildAvatar $l)
    {
        if ($this->collAvatarByMinRanks === null) {
            $this->initAvatarByMinRanks();
            $this->collAvatarByMinRanksPartial = true;
        }

        if (!$this->collAvatarByMinRanks->contains($l)) {
            $this->doAddAvatarByMinRank($l);

            if ($this->avatarByMinRanksScheduledForDeletion and $this->avatarByMinRanksScheduledForDeletion->contains($l)) {
                $this->avatarByMinRanksScheduledForDeletion->remove($this->avatarByMinRanksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAvatar $avatarByMinRank The ChildAvatar object to add.
     */
    protected function doAddAvatarByMinRank(ChildAvatar $avatarByMinRank)
    {
        $this->collAvatarByMinRanks[]= $avatarByMinRank;
        $avatarByMinRank->setMinRank($this);
    }

    /**
     * @param  ChildAvatar $avatarByMinRank The ChildAvatar object to remove.
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function removeAvatarByMinRank(ChildAvatar $avatarByMinRank)
    {
        if ($this->getAvatarByMinRanks()->contains($avatarByMinRank)) {
            $pos = $this->collAvatarByMinRanks->search($avatarByMinRank);
            $this->collAvatarByMinRanks->remove($pos);
            if (null === $this->avatarByMinRanksScheduledForDeletion) {
                $this->avatarByMinRanksScheduledForDeletion = clone $this->collAvatarByMinRanks;
                $this->avatarByMinRanksScheduledForDeletion->clear();
            }
            $this->avatarByMinRanksScheduledForDeletion[]= $avatarByMinRank;
            $avatarByMinRank->setMinRank(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByMinRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByMinRanksJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getAvatarByMinRanks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByMinRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByMinRanksJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getAvatarByMinRanks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByMinRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByMinRanksJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getAvatarByMinRanks($query, $con);
    }

    /**
     * Clears out the collAvatarByAssociatedRanks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAvatarByAssociatedRanks()
     */
    public function clearAvatarByAssociatedRanks()
    {
        $this->collAvatarByAssociatedRanks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAvatarByAssociatedRanks collection loaded partially.
     */
    public function resetPartialAvatarByAssociatedRanks($v = true)
    {
        $this->collAvatarByAssociatedRanksPartial = $v;
    }

    /**
     * Initializes the collAvatarByAssociatedRanks collection.
     *
     * By default this just sets the collAvatarByAssociatedRanks collection to an empty array (like clearcollAvatarByAssociatedRanks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAvatarByAssociatedRanks($overrideExisting = true)
    {
        if (null !== $this->collAvatarByAssociatedRanks && !$overrideExisting) {
            return;
        }

        $collectionClassName = AvatarTableMap::getTableMap()->getCollectionClassName();

        $this->collAvatarByAssociatedRanks = new $collectionClassName;
        $this->collAvatarByAssociatedRanks->setModel('\IiMedias\StreamBundle\Model\Avatar');
    }

    /**
     * Gets an array of ChildAvatar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRank is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     * @throws PropelException
     */
    public function getAvatarByAssociatedRanks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAvatarByAssociatedRanksPartial && !$this->isNew();
        if (null === $this->collAvatarByAssociatedRanks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAvatarByAssociatedRanks) {
                // return empty collection
                $this->initAvatarByAssociatedRanks();
            } else {
                $collAvatarByAssociatedRanks = ChildAvatarQuery::create(null, $criteria)
                    ->filterByAssociatedRank($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAvatarByAssociatedRanksPartial && count($collAvatarByAssociatedRanks)) {
                        $this->initAvatarByAssociatedRanks(false);

                        foreach ($collAvatarByAssociatedRanks as $obj) {
                            if (false == $this->collAvatarByAssociatedRanks->contains($obj)) {
                                $this->collAvatarByAssociatedRanks->append($obj);
                            }
                        }

                        $this->collAvatarByAssociatedRanksPartial = true;
                    }

                    return $collAvatarByAssociatedRanks;
                }

                if ($partial && $this->collAvatarByAssociatedRanks) {
                    foreach ($this->collAvatarByAssociatedRanks as $obj) {
                        if ($obj->isNew()) {
                            $collAvatarByAssociatedRanks[] = $obj;
                        }
                    }
                }

                $this->collAvatarByAssociatedRanks = $collAvatarByAssociatedRanks;
                $this->collAvatarByAssociatedRanksPartial = false;
            }
        }

        return $this->collAvatarByAssociatedRanks;
    }

    /**
     * Sets a collection of ChildAvatar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $avatarByAssociatedRanks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function setAvatarByAssociatedRanks(Collection $avatarByAssociatedRanks, ConnectionInterface $con = null)
    {
        /** @var ChildAvatar[] $avatarByAssociatedRanksToDelete */
        $avatarByAssociatedRanksToDelete = $this->getAvatarByAssociatedRanks(new Criteria(), $con)->diff($avatarByAssociatedRanks);


        $this->avatarByAssociatedRanksScheduledForDeletion = $avatarByAssociatedRanksToDelete;

        foreach ($avatarByAssociatedRanksToDelete as $avatarByAssociatedRankRemoved) {
            $avatarByAssociatedRankRemoved->setAssociatedRank(null);
        }

        $this->collAvatarByAssociatedRanks = null;
        foreach ($avatarByAssociatedRanks as $avatarByAssociatedRank) {
            $this->addAvatarByAssociatedRank($avatarByAssociatedRank);
        }

        $this->collAvatarByAssociatedRanks = $avatarByAssociatedRanks;
        $this->collAvatarByAssociatedRanksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Avatar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Avatar objects.
     * @throws PropelException
     */
    public function countAvatarByAssociatedRanks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAvatarByAssociatedRanksPartial && !$this->isNew();
        if (null === $this->collAvatarByAssociatedRanks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAvatarByAssociatedRanks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAvatarByAssociatedRanks());
            }

            $query = ChildAvatarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByAssociatedRank($this)
                ->count($con);
        }

        return count($this->collAvatarByAssociatedRanks);
    }

    /**
     * Method called to associate a ChildAvatar object to this object
     * through the ChildAvatar foreign key attribute.
     *
     * @param  ChildAvatar $l ChildAvatar
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function addAvatarByAssociatedRank(ChildAvatar $l)
    {
        if ($this->collAvatarByAssociatedRanks === null) {
            $this->initAvatarByAssociatedRanks();
            $this->collAvatarByAssociatedRanksPartial = true;
        }

        if (!$this->collAvatarByAssociatedRanks->contains($l)) {
            $this->doAddAvatarByAssociatedRank($l);

            if ($this->avatarByAssociatedRanksScheduledForDeletion and $this->avatarByAssociatedRanksScheduledForDeletion->contains($l)) {
                $this->avatarByAssociatedRanksScheduledForDeletion->remove($this->avatarByAssociatedRanksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAvatar $avatarByAssociatedRank The ChildAvatar object to add.
     */
    protected function doAddAvatarByAssociatedRank(ChildAvatar $avatarByAssociatedRank)
    {
        $this->collAvatarByAssociatedRanks[]= $avatarByAssociatedRank;
        $avatarByAssociatedRank->setAssociatedRank($this);
    }

    /**
     * @param  ChildAvatar $avatarByAssociatedRank The ChildAvatar object to remove.
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function removeAvatarByAssociatedRank(ChildAvatar $avatarByAssociatedRank)
    {
        if ($this->getAvatarByAssociatedRanks()->contains($avatarByAssociatedRank)) {
            $pos = $this->collAvatarByAssociatedRanks->search($avatarByAssociatedRank);
            $this->collAvatarByAssociatedRanks->remove($pos);
            if (null === $this->avatarByAssociatedRanksScheduledForDeletion) {
                $this->avatarByAssociatedRanksScheduledForDeletion = clone $this->collAvatarByAssociatedRanks;
                $this->avatarByAssociatedRanksScheduledForDeletion->clear();
            }
            $this->avatarByAssociatedRanksScheduledForDeletion[]= $avatarByAssociatedRank;
            $avatarByAssociatedRank->setAssociatedRank(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByAssociatedRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByAssociatedRanksJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getAvatarByAssociatedRanks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByAssociatedRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByAssociatedRanksJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getAvatarByAssociatedRanks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related AvatarByAssociatedRanks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildAvatar[] List of ChildAvatar objects
     */
    public function getAvatarByAssociatedRanksJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildAvatarQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getAvatarByAssociatedRanks($query, $con);
    }

    /**
     * Clears out the collUserExperiences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserExperiences()
     */
    public function clearUserExperiences()
    {
        $this->collUserExperiences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserExperiences collection loaded partially.
     */
    public function resetPartialUserExperiences($v = true)
    {
        $this->collUserExperiencesPartial = $v;
    }

    /**
     * Initializes the collUserExperiences collection.
     *
     * By default this just sets the collUserExperiences collection to an empty array (like clearcollUserExperiences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserExperiences($overrideExisting = true)
    {
        if (null !== $this->collUserExperiences && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserExperienceTableMap::getTableMap()->getCollectionClassName();

        $this->collUserExperiences = new $collectionClassName;
        $this->collUserExperiences->setModel('\IiMedias\StreamBundle\Model\UserExperience');
    }

    /**
     * Gets an array of ChildUserExperience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRank is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     * @throws PropelException
     */
    public function getUserExperiences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                // return empty collection
                $this->initUserExperiences();
            } else {
                $collUserExperiences = ChildUserExperienceQuery::create(null, $criteria)
                    ->filterByRank($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserExperiencesPartial && count($collUserExperiences)) {
                        $this->initUserExperiences(false);

                        foreach ($collUserExperiences as $obj) {
                            if (false == $this->collUserExperiences->contains($obj)) {
                                $this->collUserExperiences->append($obj);
                            }
                        }

                        $this->collUserExperiencesPartial = true;
                    }

                    return $collUserExperiences;
                }

                if ($partial && $this->collUserExperiences) {
                    foreach ($this->collUserExperiences as $obj) {
                        if ($obj->isNew()) {
                            $collUserExperiences[] = $obj;
                        }
                    }
                }

                $this->collUserExperiences = $collUserExperiences;
                $this->collUserExperiencesPartial = false;
            }
        }

        return $this->collUserExperiences;
    }

    /**
     * Sets a collection of ChildUserExperience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userExperiences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function setUserExperiences(Collection $userExperiences, ConnectionInterface $con = null)
    {
        /** @var ChildUserExperience[] $userExperiencesToDelete */
        $userExperiencesToDelete = $this->getUserExperiences(new Criteria(), $con)->diff($userExperiences);


        $this->userExperiencesScheduledForDeletion = $userExperiencesToDelete;

        foreach ($userExperiencesToDelete as $userExperienceRemoved) {
            $userExperienceRemoved->setRank(null);
        }

        $this->collUserExperiences = null;
        foreach ($userExperiences as $userExperience) {
            $this->addUserExperience($userExperience);
        }

        $this->collUserExperiences = $userExperiences;
        $this->collUserExperiencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserExperience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserExperience objects.
     * @throws PropelException
     */
    public function countUserExperiences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserExperiencesPartial && !$this->isNew();
        if (null === $this->collUserExperiences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserExperiences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserExperiences());
            }

            $query = ChildUserExperienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRank($this)
                ->count($con);
        }

        return count($this->collUserExperiences);
    }

    /**
     * Method called to associate a ChildUserExperience object to this object
     * through the ChildUserExperience foreign key attribute.
     *
     * @param  ChildUserExperience $l ChildUserExperience
     * @return $this|\IiMedias\StreamBundle\Model\Rank The current object (for fluent API support)
     */
    public function addUserExperience(ChildUserExperience $l)
    {
        if ($this->collUserExperiences === null) {
            $this->initUserExperiences();
            $this->collUserExperiencesPartial = true;
        }

        if (!$this->collUserExperiences->contains($l)) {
            $this->doAddUserExperience($l);

            if ($this->userExperiencesScheduledForDeletion and $this->userExperiencesScheduledForDeletion->contains($l)) {
                $this->userExperiencesScheduledForDeletion->remove($this->userExperiencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserExperience $userExperience The ChildUserExperience object to add.
     */
    protected function doAddUserExperience(ChildUserExperience $userExperience)
    {
        $this->collUserExperiences[]= $userExperience;
        $userExperience->setRank($this);
    }

    /**
     * @param  ChildUserExperience $userExperience The ChildUserExperience object to remove.
     * @return $this|ChildRank The current object (for fluent API support)
     */
    public function removeUserExperience(ChildUserExperience $userExperience)
    {
        if ($this->getUserExperiences()->contains($userExperience)) {
            $pos = $this->collUserExperiences->search($userExperience);
            $this->collUserExperiences->remove($pos);
            if (null === $this->userExperiencesScheduledForDeletion) {
                $this->userExperiencesScheduledForDeletion = clone $this->collUserExperiences;
                $this->userExperiencesScheduledForDeletion->clear();
            }
            $this->userExperiencesScheduledForDeletion[]= $userExperience;
            $userExperience->setRank(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinChannel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Channel', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinChatUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('ChatUser', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Rank is new, it will return
     * an empty collection; or if this Rank has previously
     * been saved, it will retrieve related UserExperiences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Rank.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserExperience[] List of ChildUserExperience objects
     */
    public function getUserExperiencesJoinAvatar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserExperienceQuery::create(null, $criteria);
        $query->joinWith('Avatar', $joinBehavior);

        return $this->getUserExperiences($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStream) {
            $this->aStream->removeRank($this);
        }
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserStrank($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserStrank($this);
        }
        $this->strank_id = null;
        $this->strank_ststrm_id = null;
        $this->strank_name = null;
        $this->strank_is_default = null;
        $this->strank_set_if_follow_event = null;
        $this->strank_set_if_moderator = null;
        $this->strank_set_if_streamer = null;
        $this->strank_dependency_rank_ids = null;
        $this->strank_dependency_rank_ids_unserialized = null;
        $this->strank_created_by_user_id = null;
        $this->strank_updated_by_user_id = null;
        $this->strank_created_at = null;
        $this->strank_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collAvatarByMinRanks) {
                foreach ($this->collAvatarByMinRanks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAvatarByAssociatedRanks) {
                foreach ($this->collAvatarByAssociatedRanks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserExperiences) {
                foreach ($this->collUserExperiences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collAvatarByMinRanks = null;
        $this->collAvatarByAssociatedRanks = null;
        $this->collUserExperiences = null;
        $this->aStream = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RankTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildRank The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[RankTableMap::COL_STRANK_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\HostExperienceQuery as BaseHostExperienceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_host_experience_sthexp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class HostExperienceQuery extends BaseHostExperienceQuery
{

}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\StreamQuery as BaseStreamQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_stream_ststrm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StreamQuery extends BaseStreamQuery
{
    static public function getAllStream()
    {
        $streams = self::create()
            ->filterByType('stream')
            ->find()
        ;

        return $streams;
    }

    static public function getAllBot()
    {
        $streams = self::create()
            ->filterByType('bot')
            ->find()
        ;

        return $streams;
    }

    static function findOneByChannelNameId($channelNameId)
    {
        $stream = self::create()
            ->_if(is_numeric($channelNameId))
                ->filterById(intval($channelNameId))
            ->_else()
                ->useChannelQuery()
                    ->filterByChannel($channelNameId)
                ->endUse()
            ->_endif()
            ->findOne()
        ;
        return $stream;
    }
}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\DeepBotImportExperience as BaseDeepBotImportExperience;

/**
 * Skeleton subclass for representing a row from the 'stream_deepbot_import_experience_stdbie' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DeepBotImportExperience extends BaseDeepBotImportExperience
{

}

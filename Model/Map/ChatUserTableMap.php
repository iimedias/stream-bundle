<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_chat_user_stcusr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatUserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ChatUserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_chat_user_stcusr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\ChatUser';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.ChatUser';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the stcusr_id field
     */
    const COL_STCUSR_ID = 'stream_chat_user_stcusr.stcusr_id';

    /**
     * the column name for the stcusr_parent_id field
     */
    const COL_STCUSR_PARENT_ID = 'stream_chat_user_stcusr.stcusr_parent_id';

    /**
     * the column name for the stcusr_stsite_id field
     */
    const COL_STCUSR_STSITE_ID = 'stream_chat_user_stcusr.stcusr_stsite_id';

    /**
     * the column name for the stcusr_username field
     */
    const COL_STCUSR_USERNAME = 'stream_chat_user_stcusr.stcusr_username';

    /**
     * the column name for the stcusr_display_name field
     */
    const COL_STCUSR_DISPLAY_NAME = 'stream_chat_user_stcusr.stcusr_display_name';

    /**
     * the column name for the stcusr_logo_url field
     */
    const COL_STCUSR_LOGO_URL = 'stream_chat_user_stcusr.stcusr_logo_url';

    /**
     * the column name for the stcusr_timeout_level field
     */
    const COL_STCUSR_TIMEOUT_LEVEL = 'stream_chat_user_stcusr.stcusr_timeout_level';

    /**
     * the column name for the stcusr_last_timeout_at field
     */
    const COL_STCUSR_LAST_TIMEOUT_AT = 'stream_chat_user_stcusr.stcusr_last_timeout_at';

    /**
     * the column name for the stcusr_registered_at field
     */
    const COL_STCUSR_REGISTERED_AT = 'stream_chat_user_stcusr.stcusr_registered_at';

    /**
     * the column name for the stcusr_is_deleted_account field
     */
    const COL_STCUSR_IS_DELETED_ACCOUNT = 'stream_chat_user_stcusr.stcusr_is_deleted_account';

    /**
     * the column name for the stcusr_can_rescan field
     */
    const COL_STCUSR_CAN_RESCAN = 'stream_chat_user_stcusr.stcusr_can_rescan';

    /**
     * the column name for the stcusr_created_by_user_id field
     */
    const COL_STCUSR_CREATED_BY_USER_ID = 'stream_chat_user_stcusr.stcusr_created_by_user_id';

    /**
     * the column name for the stcusr_updated_by_user_id field
     */
    const COL_STCUSR_UPDATED_BY_USER_ID = 'stream_chat_user_stcusr.stcusr_updated_by_user_id';

    /**
     * the column name for the stcusr_created_at field
     */
    const COL_STCUSR_CREATED_AT = 'stream_chat_user_stcusr.stcusr_created_at';

    /**
     * the column name for the stcusr_updated_at field
     */
    const COL_STCUSR_UPDATED_AT = 'stream_chat_user_stcusr.stcusr_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'SiteId', 'Username', 'DisplayName', 'LogoUrl', 'TimeoutLevel', 'LastTimeoutAt', 'RegisteredAt', 'IsDeletedAccount', 'CanRescan', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'siteId', 'username', 'displayName', 'logoUrl', 'timeoutLevel', 'lastTimeoutAt', 'registeredAt', 'isDeletedAccount', 'canRescan', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ChatUserTableMap::COL_STCUSR_ID, ChatUserTableMap::COL_STCUSR_PARENT_ID, ChatUserTableMap::COL_STCUSR_STSITE_ID, ChatUserTableMap::COL_STCUSR_USERNAME, ChatUserTableMap::COL_STCUSR_DISPLAY_NAME, ChatUserTableMap::COL_STCUSR_LOGO_URL, ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL, ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT, ChatUserTableMap::COL_STCUSR_REGISTERED_AT, ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT, ChatUserTableMap::COL_STCUSR_CAN_RESCAN, ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID, ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID, ChatUserTableMap::COL_STCUSR_CREATED_AT, ChatUserTableMap::COL_STCUSR_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('stcusr_id', 'stcusr_parent_id', 'stcusr_stsite_id', 'stcusr_username', 'stcusr_display_name', 'stcusr_logo_url', 'stcusr_timeout_level', 'stcusr_last_timeout_at', 'stcusr_registered_at', 'stcusr_is_deleted_account', 'stcusr_can_rescan', 'stcusr_created_by_user_id', 'stcusr_updated_by_user_id', 'stcusr_created_at', 'stcusr_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'SiteId' => 2, 'Username' => 3, 'DisplayName' => 4, 'LogoUrl' => 5, 'TimeoutLevel' => 6, 'LastTimeoutAt' => 7, 'RegisteredAt' => 8, 'IsDeletedAccount' => 9, 'CanRescan' => 10, 'CreatedByUserId' => 11, 'UpdatedByUserId' => 12, 'CreatedAt' => 13, 'UpdatedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'siteId' => 2, 'username' => 3, 'displayName' => 4, 'logoUrl' => 5, 'timeoutLevel' => 6, 'lastTimeoutAt' => 7, 'registeredAt' => 8, 'isDeletedAccount' => 9, 'canRescan' => 10, 'createdByUserId' => 11, 'updatedByUserId' => 12, 'createdAt' => 13, 'updatedAt' => 14, ),
        self::TYPE_COLNAME       => array(ChatUserTableMap::COL_STCUSR_ID => 0, ChatUserTableMap::COL_STCUSR_PARENT_ID => 1, ChatUserTableMap::COL_STCUSR_STSITE_ID => 2, ChatUserTableMap::COL_STCUSR_USERNAME => 3, ChatUserTableMap::COL_STCUSR_DISPLAY_NAME => 4, ChatUserTableMap::COL_STCUSR_LOGO_URL => 5, ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL => 6, ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT => 7, ChatUserTableMap::COL_STCUSR_REGISTERED_AT => 8, ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT => 9, ChatUserTableMap::COL_STCUSR_CAN_RESCAN => 10, ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID => 11, ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID => 12, ChatUserTableMap::COL_STCUSR_CREATED_AT => 13, ChatUserTableMap::COL_STCUSR_UPDATED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('stcusr_id' => 0, 'stcusr_parent_id' => 1, 'stcusr_stsite_id' => 2, 'stcusr_username' => 3, 'stcusr_display_name' => 4, 'stcusr_logo_url' => 5, 'stcusr_timeout_level' => 6, 'stcusr_last_timeout_at' => 7, 'stcusr_registered_at' => 8, 'stcusr_is_deleted_account' => 9, 'stcusr_can_rescan' => 10, 'stcusr_created_by_user_id' => 11, 'stcusr_updated_by_user_id' => 12, 'stcusr_created_at' => 13, 'stcusr_updated_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_chat_user_stcusr');
        $this->setPhpName('ChatUser');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\ChatUser');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stcusr_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stcusr_parent_id', 'ParentId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', false, null, null);
        $this->addForeignKey('stcusr_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addColumn('stcusr_username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('stcusr_display_name', 'DisplayName', 'VARCHAR', false, 255, null);
        $this->addColumn('stcusr_logo_url', 'LogoUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('stcusr_timeout_level', 'TimeoutLevel', 'INTEGER', true, null, 0);
        $this->addColumn('stcusr_last_timeout_at', 'LastTimeoutAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('stcusr_registered_at', 'RegisteredAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('stcusr_is_deleted_account', 'IsDeletedAccount', 'BOOLEAN', true, 1, false);
        $this->addColumn('stcusr_can_rescan', 'CanRescan', 'BOOLEAN', true, 1, true);
        $this->addForeignKey('stcusr_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('stcusr_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('stcusr_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stcusr_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Parent', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcusr_parent_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcusr_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcusr_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcusr_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Child', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcusr_parent_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'Children', false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
        $this->addRelation('DeepBotImportExperience', '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stdbie_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'DeepBotImportExperiences', false);
        $this->addRelation('MessageExperience', '\\IiMedias\\StreamBundle\\Model\\MessageExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stmexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'MessageExperiences', false);
        $this->addRelation('ChatterExperience', '\\IiMedias\\StreamBundle\\Model\\ChatterExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatterExperiences', false);
        $this->addRelation('FollowExperience', '\\IiMedias\\StreamBundle\\Model\\FollowExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowExperiences', false);
        $this->addRelation('HostExperience', '\\IiMedias\\StreamBundle\\Model\\HostExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sthexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'HostExperiences', false);
        $this->addRelation('Experience', '\\IiMedias\\StreamBundle\\Model\\Experience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stexpr_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', 'Experiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'stcusr_created_at', 'update_column' => 'stcusr_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_chat_user_stcusr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ChatUserTableMap::clearInstancePool();
        UserExperienceTableMap::clearInstancePool();
        DeepBotImportExperienceTableMap::clearInstancePool();
        MessageExperienceTableMap::clearInstancePool();
        ChatterExperienceTableMap::clearInstancePool();
        FollowExperienceTableMap::clearInstancePool();
        HostExperienceTableMap::clearInstancePool();
        ExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatUserTableMap::CLASS_DEFAULT : ChatUserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatUser object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatUserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatUserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatUserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatUserTableMap::OM_CLASS;
            /** @var ChatUser $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatUserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatUserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatUserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatUser $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatUserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_PARENT_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_STSITE_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_USERNAME);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_DISPLAY_NAME);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_LOGO_URL);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_TIMEOUT_LEVEL);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_LAST_TIMEOUT_AT);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_REGISTERED_AT);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_IS_DELETED_ACCOUNT);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_CAN_RESCAN);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_CREATED_AT);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STCUSR_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stcusr_id');
            $criteria->addSelectColumn($alias . '.stcusr_parent_id');
            $criteria->addSelectColumn($alias . '.stcusr_stsite_id');
            $criteria->addSelectColumn($alias . '.stcusr_username');
            $criteria->addSelectColumn($alias . '.stcusr_display_name');
            $criteria->addSelectColumn($alias . '.stcusr_logo_url');
            $criteria->addSelectColumn($alias . '.stcusr_timeout_level');
            $criteria->addSelectColumn($alias . '.stcusr_last_timeout_at');
            $criteria->addSelectColumn($alias . '.stcusr_registered_at');
            $criteria->addSelectColumn($alias . '.stcusr_is_deleted_account');
            $criteria->addSelectColumn($alias . '.stcusr_can_rescan');
            $criteria->addSelectColumn($alias . '.stcusr_created_by_user_id');
            $criteria->addSelectColumn($alias . '.stcusr_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.stcusr_created_at');
            $criteria->addSelectColumn($alias . '.stcusr_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatUserTableMap::DATABASE_NAME)->getTable(ChatUserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatUserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatUserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatUserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatUser or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatUser object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\ChatUser) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatUserTableMap::DATABASE_NAME);
            $criteria->add(ChatUserTableMap::COL_STCUSR_ID, (array) $values, Criteria::IN);
        }

        $query = ChatUserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatUserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatUserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_chat_user_stcusr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatUserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatUser or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatUser object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatUser object
        }

        if ($criteria->containsKey(ChatUserTableMap::COL_STCUSR_ID) && $criteria->keyContainsValue(ChatUserTableMap::COL_STCUSR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChatUserTableMap::COL_STCUSR_ID.')');
        }


        // Set the correct dbName
        $query = ChatUserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatUserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatUserTableMap::buildTableMap();

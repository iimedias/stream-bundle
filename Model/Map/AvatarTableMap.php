<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Avatar;
use IiMedias\StreamBundle\Model\AvatarQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_avatar_stavtr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AvatarTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.AvatarTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_avatar_stavtr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Avatar';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Avatar';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the stavtr_id field
     */
    const COL_STAVTR_ID = 'stream_avatar_stavtr.stavtr_id';

    /**
     * the column name for the stavtr_ststrm_id field
     */
    const COL_STAVTR_STSTRM_ID = 'stream_avatar_stavtr.stavtr_ststrm_id';

    /**
     * the column name for the stavtr_name field
     */
    const COL_STAVTR_NAME = 'stream_avatar_stavtr.stavtr_name';

    /**
     * the column name for the stavtr_code field
     */
    const COL_STAVTR_CODE = 'stream_avatar_stavtr.stavtr_code';

    /**
     * the column name for the stavtr_is_default field
     */
    const COL_STAVTR_IS_DEFAULT = 'stream_avatar_stavtr.stavtr_is_default';

    /**
     * the column name for the stavtr_can_be_owned field
     */
    const COL_STAVTR_CAN_BE_OWNED = 'stream_avatar_stavtr.stavtr_can_be_owned';

    /**
     * the column name for the stavtr_main_color field
     */
    const COL_STAVTR_MAIN_COLOR = 'stream_avatar_stavtr.stavtr_main_color';

    /**
     * the column name for the stavtr_min_strank_id field
     */
    const COL_STAVTR_MIN_STRANK_ID = 'stream_avatar_stavtr.stavtr_min_strank_id';

    /**
     * the column name for the stavtr_associated_strank_id field
     */
    const COL_STAVTR_ASSOCIATED_STRANK_ID = 'stream_avatar_stavtr.stavtr_associated_strank_id';

    /**
     * the column name for the stavtr_created_by_user_id field
     */
    const COL_STAVTR_CREATED_BY_USER_ID = 'stream_avatar_stavtr.stavtr_created_by_user_id';

    /**
     * the column name for the stavtr_updated_by_user_id field
     */
    const COL_STAVTR_UPDATED_BY_USER_ID = 'stream_avatar_stavtr.stavtr_updated_by_user_id';

    /**
     * the column name for the stavtr_created_at field
     */
    const COL_STAVTR_CREATED_AT = 'stream_avatar_stavtr.stavtr_created_at';

    /**
     * the column name for the stavtr_updated_at field
     */
    const COL_STAVTR_UPDATED_AT = 'stream_avatar_stavtr.stavtr_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the stavtr_main_color field */
    const COL_STAVTR_MAIN_COLOR_COLORLESS = 'colorless';
    const COL_STAVTR_MAIN_COLOR_RED = 'red';
    const COL_STAVTR_MAIN_COLOR_BLUE = 'blue';
    const COL_STAVTR_MAIN_COLOR_YELLOW = 'yellow';
    const COL_STAVTR_MAIN_COLOR_GREEN = 'green';
    const COL_STAVTR_MAIN_COLOR_PINK = 'pink';
    const COL_STAVTR_MAIN_COLOR_BLACK = 'black';
    const COL_STAVTR_MAIN_COLOR_WHITE = 'white';
    const COL_STAVTR_MAIN_COLOR_GOLD = 'gold';
    const COL_STAVTR_MAIN_COLOR_SILVER = 'silver';
    const COL_STAVTR_MAIN_COLOR_ORANGE = 'orange';
    const COL_STAVTR_MAIN_COLOR_NAVY = 'navy';
    const COL_STAVTR_MAIN_COLOR_FIRE = 'fire';
    const COL_STAVTR_MAIN_COLOR_VIOLET = 'violet';
    const COL_STAVTR_MAIN_COLOR_CYAN = 'cyan';
    const COL_STAVTR_MAIN_COLOR_GREY = 'grey';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'Name', 'Code', 'IsDefault', 'CanBeOwned', 'MainColor', 'MinRankId', 'AssociatedRankId', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'name', 'code', 'isDefault', 'canBeOwned', 'mainColor', 'minRankId', 'associatedRankId', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(AvatarTableMap::COL_STAVTR_ID, AvatarTableMap::COL_STAVTR_STSTRM_ID, AvatarTableMap::COL_STAVTR_NAME, AvatarTableMap::COL_STAVTR_CODE, AvatarTableMap::COL_STAVTR_IS_DEFAULT, AvatarTableMap::COL_STAVTR_CAN_BE_OWNED, AvatarTableMap::COL_STAVTR_MAIN_COLOR, AvatarTableMap::COL_STAVTR_MIN_STRANK_ID, AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID, AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID, AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID, AvatarTableMap::COL_STAVTR_CREATED_AT, AvatarTableMap::COL_STAVTR_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('stavtr_id', 'stavtr_ststrm_id', 'stavtr_name', 'stavtr_code', 'stavtr_is_default', 'stavtr_can_be_owned', 'stavtr_main_color', 'stavtr_min_strank_id', 'stavtr_associated_strank_id', 'stavtr_created_by_user_id', 'stavtr_updated_by_user_id', 'stavtr_created_at', 'stavtr_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'Name' => 2, 'Code' => 3, 'IsDefault' => 4, 'CanBeOwned' => 5, 'MainColor' => 6, 'MinRankId' => 7, 'AssociatedRankId' => 8, 'CreatedByUserId' => 9, 'UpdatedByUserId' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'name' => 2, 'code' => 3, 'isDefault' => 4, 'canBeOwned' => 5, 'mainColor' => 6, 'minRankId' => 7, 'associatedRankId' => 8, 'createdByUserId' => 9, 'updatedByUserId' => 10, 'createdAt' => 11, 'updatedAt' => 12, ),
        self::TYPE_COLNAME       => array(AvatarTableMap::COL_STAVTR_ID => 0, AvatarTableMap::COL_STAVTR_STSTRM_ID => 1, AvatarTableMap::COL_STAVTR_NAME => 2, AvatarTableMap::COL_STAVTR_CODE => 3, AvatarTableMap::COL_STAVTR_IS_DEFAULT => 4, AvatarTableMap::COL_STAVTR_CAN_BE_OWNED => 5, AvatarTableMap::COL_STAVTR_MAIN_COLOR => 6, AvatarTableMap::COL_STAVTR_MIN_STRANK_ID => 7, AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID => 8, AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID => 9, AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID => 10, AvatarTableMap::COL_STAVTR_CREATED_AT => 11, AvatarTableMap::COL_STAVTR_UPDATED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('stavtr_id' => 0, 'stavtr_ststrm_id' => 1, 'stavtr_name' => 2, 'stavtr_code' => 3, 'stavtr_is_default' => 4, 'stavtr_can_be_owned' => 5, 'stavtr_main_color' => 6, 'stavtr_min_strank_id' => 7, 'stavtr_associated_strank_id' => 8, 'stavtr_created_by_user_id' => 9, 'stavtr_updated_by_user_id' => 10, 'stavtr_created_at' => 11, 'stavtr_updated_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                AvatarTableMap::COL_STAVTR_MAIN_COLOR => array(
                            self::COL_STAVTR_MAIN_COLOR_COLORLESS,
            self::COL_STAVTR_MAIN_COLOR_RED,
            self::COL_STAVTR_MAIN_COLOR_BLUE,
            self::COL_STAVTR_MAIN_COLOR_YELLOW,
            self::COL_STAVTR_MAIN_COLOR_GREEN,
            self::COL_STAVTR_MAIN_COLOR_PINK,
            self::COL_STAVTR_MAIN_COLOR_BLACK,
            self::COL_STAVTR_MAIN_COLOR_WHITE,
            self::COL_STAVTR_MAIN_COLOR_GOLD,
            self::COL_STAVTR_MAIN_COLOR_SILVER,
            self::COL_STAVTR_MAIN_COLOR_ORANGE,
            self::COL_STAVTR_MAIN_COLOR_NAVY,
            self::COL_STAVTR_MAIN_COLOR_FIRE,
            self::COL_STAVTR_MAIN_COLOR_VIOLET,
            self::COL_STAVTR_MAIN_COLOR_CYAN,
            self::COL_STAVTR_MAIN_COLOR_GREY,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_avatar_stavtr');
        $this->setPhpName('Avatar');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Avatar');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stavtr_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stavtr_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addColumn('stavtr_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('stavtr_code', 'Code', 'VARCHAR', false, 255, null);
        $this->addColumn('stavtr_is_default', 'IsDefault', 'BOOLEAN', true, 1, false);
        $this->addColumn('stavtr_can_be_owned', 'CanBeOwned', 'BOOLEAN', true, 1, false);
        $this->addColumn('stavtr_main_color', 'MainColor', 'ENUM', true, null, 'colorless');
        $this->getColumn('stavtr_main_color')->setValueSet(array (
  0 => 'colorless',
  1 => 'red',
  2 => 'blue',
  3 => 'yellow',
  4 => 'green',
  5 => 'pink',
  6 => 'black',
  7 => 'white',
  8 => 'gold',
  9 => 'silver',
  10 => 'orange',
  11 => 'navy',
  12 => 'fire',
  13 => 'violet',
  14 => 'cyan',
  15 => 'grey',
));
        $this->addForeignKey('stavtr_min_strank_id', 'MinRankId', 'INTEGER', 'stream_rank_strank', 'strank_id', false, null, null);
        $this->addForeignKey('stavtr_associated_strank_id', 'AssociatedRankId', 'INTEGER', 'stream_rank_strank', 'strank_id', false, null, null);
        $this->addForeignKey('stavtr_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('stavtr_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('stavtr_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stavtr_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stavtr_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('MinRank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stavtr_min_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('AssociatedRank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stavtr_associated_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stavtr_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stavtr_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_stavtr_id',
    1 => ':stavtr_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'stavtr_created_at', 'update_column' => 'stavtr_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_avatar_stavtr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        UserExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AvatarTableMap::CLASS_DEFAULT : AvatarTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Avatar object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AvatarTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AvatarTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AvatarTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AvatarTableMap::OM_CLASS;
            /** @var Avatar $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AvatarTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AvatarTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AvatarTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Avatar $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AvatarTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_STSTRM_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_NAME);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_CODE);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_IS_DEFAULT);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_CAN_BE_OWNED);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_MAIN_COLOR);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_MIN_STRANK_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_ASSOCIATED_STRANK_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_CREATED_AT);
            $criteria->addSelectColumn(AvatarTableMap::COL_STAVTR_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stavtr_id');
            $criteria->addSelectColumn($alias . '.stavtr_ststrm_id');
            $criteria->addSelectColumn($alias . '.stavtr_name');
            $criteria->addSelectColumn($alias . '.stavtr_code');
            $criteria->addSelectColumn($alias . '.stavtr_is_default');
            $criteria->addSelectColumn($alias . '.stavtr_can_be_owned');
            $criteria->addSelectColumn($alias . '.stavtr_main_color');
            $criteria->addSelectColumn($alias . '.stavtr_min_strank_id');
            $criteria->addSelectColumn($alias . '.stavtr_associated_strank_id');
            $criteria->addSelectColumn($alias . '.stavtr_created_by_user_id');
            $criteria->addSelectColumn($alias . '.stavtr_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.stavtr_created_at');
            $criteria->addSelectColumn($alias . '.stavtr_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AvatarTableMap::DATABASE_NAME)->getTable(AvatarTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AvatarTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AvatarTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AvatarTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Avatar or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Avatar object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AvatarTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Avatar) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AvatarTableMap::DATABASE_NAME);
            $criteria->add(AvatarTableMap::COL_STAVTR_ID, (array) $values, Criteria::IN);
        }

        $query = AvatarQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AvatarTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AvatarTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_avatar_stavtr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AvatarQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Avatar or Criteria object.
     *
     * @param mixed               $criteria Criteria or Avatar object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AvatarTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Avatar object
        }

        if ($criteria->containsKey(AvatarTableMap::COL_STAVTR_ID) && $criteria->keyContainsValue(AvatarTableMap::COL_STAVTR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.AvatarTableMap::COL_STAVTR_ID.')');
        }


        // Set the correct dbName
        $query = AvatarQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AvatarTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AvatarTableMap::buildTableMap();

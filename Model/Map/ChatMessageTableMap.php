<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\ChatMessage;
use IiMedias\StreamBundle\Model\ChatMessageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_chat_message_stcmsg' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatMessageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ChatMessageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_chat_message_stcmsg';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\ChatMessage';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.ChatMessage';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the stcmsg_id field
     */
    const COL_STCMSG_ID = 'stream_chat_message_stcmsg.stcmsg_id';

    /**
     * the column name for the stcmsg_ststrm_id field
     */
    const COL_STCMSG_STSTRM_ID = 'stream_chat_message_stcmsg.stcmsg_ststrm_id';

    /**
     * the column name for the stcmsg_stchan_id field
     */
    const COL_STCMSG_STCHAN_ID = 'stream_chat_message_stcmsg.stcmsg_stchan_id';

    /**
     * the column name for the stcmsg_stcusr_id field
     */
    const COL_STCMSG_STCUSR_ID = 'stream_chat_message_stcmsg.stcmsg_stcusr_id';

    /**
     * the column name for the stcmsg_type field
     */
    const COL_STCMSG_TYPE = 'stream_chat_message_stcmsg.stcmsg_type';

    /**
     * the column name for the stcusr_message field
     */
    const COL_STCUSR_MESSAGE = 'stream_chat_message_stcmsg.stcusr_message';

    /**
     * the column name for the stcusr_show field
     */
    const COL_STCUSR_SHOW = 'stream_chat_message_stcmsg.stcusr_show';

    /**
     * the column name for the send_at field
     */
    const COL_SEND_AT = 'stream_chat_message_stcmsg.send_at';

    /**
     * the column name for the hide field
     */
    const COL_HIDE = 'stream_chat_message_stcmsg.hide';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'ChatUserId', 'Type', 'Message', 'Show', 'SendAt', 'Hide', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'chatUserId', 'type', 'message', 'show', 'sendAt', 'hide', ),
        self::TYPE_COLNAME       => array(ChatMessageTableMap::COL_STCMSG_ID, ChatMessageTableMap::COL_STCMSG_STSTRM_ID, ChatMessageTableMap::COL_STCMSG_STCHAN_ID, ChatMessageTableMap::COL_STCMSG_STCUSR_ID, ChatMessageTableMap::COL_STCMSG_TYPE, ChatMessageTableMap::COL_STCUSR_MESSAGE, ChatMessageTableMap::COL_STCUSR_SHOW, ChatMessageTableMap::COL_SEND_AT, ChatMessageTableMap::COL_HIDE, ),
        self::TYPE_FIELDNAME     => array('stcmsg_id', 'stcmsg_ststrm_id', 'stcmsg_stchan_id', 'stcmsg_stcusr_id', 'stcmsg_type', 'stcusr_message', 'stcusr_show', 'send_at', 'hide', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'ChatUserId' => 3, 'Type' => 4, 'Message' => 5, 'Show' => 6, 'SendAt' => 7, 'Hide' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'chatUserId' => 3, 'type' => 4, 'message' => 5, 'show' => 6, 'sendAt' => 7, 'hide' => 8, ),
        self::TYPE_COLNAME       => array(ChatMessageTableMap::COL_STCMSG_ID => 0, ChatMessageTableMap::COL_STCMSG_STSTRM_ID => 1, ChatMessageTableMap::COL_STCMSG_STCHAN_ID => 2, ChatMessageTableMap::COL_STCMSG_STCUSR_ID => 3, ChatMessageTableMap::COL_STCMSG_TYPE => 4, ChatMessageTableMap::COL_STCUSR_MESSAGE => 5, ChatMessageTableMap::COL_STCUSR_SHOW => 6, ChatMessageTableMap::COL_SEND_AT => 7, ChatMessageTableMap::COL_HIDE => 8, ),
        self::TYPE_FIELDNAME     => array('stcmsg_id' => 0, 'stcmsg_ststrm_id' => 1, 'stcmsg_stchan_id' => 2, 'stcmsg_stcusr_id' => 3, 'stcmsg_type' => 4, 'stcusr_message' => 5, 'stcusr_show' => 6, 'send_at' => 7, 'hide' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_chat_message_stcmsg');
        $this->setPhpName('ChatMessage');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\ChatMessage');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stcmsg_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stcmsg_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stcmsg_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stcmsg_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addColumn('stcmsg_type', 'Type', 'VARCHAR', true, 255, null);
        $this->addColumn('stcusr_message', 'Message', 'LONGVARCHAR', true, null, null);
        $this->addColumn('stcusr_show', 'Show', 'BOOLEAN', true, 1, true);
        $this->addColumn('send_at', 'SendAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('hide', 'Hide', 'BOOLEAN', true, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcmsg_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcmsg_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcmsg_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatMessageTableMap::CLASS_DEFAULT : ChatMessageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatMessage object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatMessageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatMessageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatMessageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatMessageTableMap::OM_CLASS;
            /** @var ChatMessage $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatMessageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatMessageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatMessageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatMessage $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatMessageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCMSG_ID);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCMSG_STSTRM_ID);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCMSG_STCHAN_ID);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCMSG_STCUSR_ID);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCMSG_TYPE);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCUSR_MESSAGE);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_STCUSR_SHOW);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_SEND_AT);
            $criteria->addSelectColumn(ChatMessageTableMap::COL_HIDE);
        } else {
            $criteria->addSelectColumn($alias . '.stcmsg_id');
            $criteria->addSelectColumn($alias . '.stcmsg_ststrm_id');
            $criteria->addSelectColumn($alias . '.stcmsg_stchan_id');
            $criteria->addSelectColumn($alias . '.stcmsg_stcusr_id');
            $criteria->addSelectColumn($alias . '.stcmsg_type');
            $criteria->addSelectColumn($alias . '.stcusr_message');
            $criteria->addSelectColumn($alias . '.stcusr_show');
            $criteria->addSelectColumn($alias . '.send_at');
            $criteria->addSelectColumn($alias . '.hide');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatMessageTableMap::DATABASE_NAME)->getTable(ChatMessageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatMessageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatMessageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatMessageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatMessage or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatMessage object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatMessageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\ChatMessage) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatMessageTableMap::DATABASE_NAME);
            $criteria->add(ChatMessageTableMap::COL_STCMSG_ID, (array) $values, Criteria::IN);
        }

        $query = ChatMessageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatMessageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatMessageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_chat_message_stcmsg table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatMessageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatMessage or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatMessage object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatMessageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatMessage object
        }

        if ($criteria->containsKey(ChatMessageTableMap::COL_STCMSG_ID) && $criteria->keyContainsValue(ChatMessageTableMap::COL_STCMSG_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChatMessageTableMap::COL_STCMSG_ID.')');
        }


        // Set the correct dbName
        $query = ChatMessageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatMessageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatMessageTableMap::buildTableMap();

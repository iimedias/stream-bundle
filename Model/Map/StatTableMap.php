<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Stat;
use IiMedias\StreamBundle\Model\StatQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_stat_ststat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StatTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.StatTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_stat_ststat';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Stat';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Stat';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the ststat_id field
     */
    const COL_STSTAT_ID = 'stream_stat_ststat.ststat_id';

    /**
     * the column name for the ststat_ststrm_id field
     */
    const COL_STSTAT_STSTRM_ID = 'stream_stat_ststat.ststat_ststrm_id';

    /**
     * the column name for the ststat_stchan_id field
     */
    const COL_STSTAT_STCHAN_ID = 'stream_stat_ststat.ststat_stchan_id';

    /**
     * the column name for the ststat_stsite_id field
     */
    const COL_STSTAT_STSITE_ID = 'stream_stat_ststat.ststat_stsite_id';

    /**
     * the column name for the ststat_duration field
     */
    const COL_STSTAT_DURATION = 'stream_stat_ststat.ststat_duration';

    /**
     * the column name for the ststat_start_at field
     */
    const COL_STSTAT_START_AT = 'stream_stat_ststat.ststat_start_at';

    /**
     * the column name for the ststat_end_at field
     */
    const COL_STSTAT_END_AT = 'stream_stat_ststat.ststat_end_at';

    /**
     * the column name for the ststat_can_rescan field
     */
    const COL_STSTAT_CAN_RESCAN = 'stream_stat_ststat.ststat_can_rescan';

    /**
     * the column name for the ststat_stream_type field
     */
    const COL_STSTAT_STREAM_TYPE = 'stream_stat_ststat.ststat_stream_type';

    /**
     * the column name for the ststat_views field
     */
    const COL_STSTAT_VIEWS = 'stream_stat_ststat.ststat_views';

    /**
     * the column name for the ststat_maximum_viewers field
     */
    const COL_STSTAT_MAXIMUM_VIEWERS = 'stream_stat_ststat.ststat_maximum_viewers';

    /**
     * the column name for the ststat_average_viewers field
     */
    const COL_STSTAT_AVERAGE_VIEWERS = 'stream_stat_ststat.ststat_average_viewers';

    /**
     * the column name for the ststat_chatters field
     */
    const COL_STSTAT_CHATTERS = 'stream_stat_ststat.ststat_chatters';

    /**
     * the column name for the ststat_messages field
     */
    const COL_STSTAT_MESSAGES = 'stream_stat_ststat.ststat_messages';

    /**
     * the column name for the ststat_hosts field
     */
    const COL_STSTAT_HOSTS = 'stream_stat_ststat.ststat_hosts';

    /**
     * the column name for the ststat_follows field
     */
    const COL_STSTAT_FOLLOWS = 'stream_stat_ststat.ststat_follows';

    /**
     * the column name for the ststat_unfollows field
     */
    const COL_STSTAT_UNFOLLOWS = 'stream_stat_ststat.ststat_unfollows';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the ststat_duration field */
    const COL_STSTAT_DURATION_MINUTE = 'minute';
    const COL_STSTAT_DURATION_HOUR = 'hour';
    const COL_STSTAT_DURATION_DAY = 'day';
    const COL_STSTAT_DURATION_WEEK = 'week';
    const COL_STSTAT_DURATION_MONTH = 'month';
    const COL_STSTAT_DURATION_YEAR = 'year';
    const COL_STSTAT_DURATION_LIVE = 'live';
    const COL_STSTAT_DURATION_VOD = 'vod';

    /** The enumerated values for the ststat_stream_type field */
    const COL_STSTAT_STREAM_TYPE_OFFLINE = 'offline';
    const COL_STSTAT_STREAM_TYPE_LIVE = 'live';
    const COL_STSTAT_STREAM_TYPE_VOD = 'vod';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'SiteId', 'Duration', 'StartAt', 'EndAt', 'CanRescan', 'StreamType', 'Views', 'MaximumViewers', 'AverageViewers', 'Chatters', 'Messages', 'Hosts', 'Follows', 'Unfollows', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'siteId', 'duration', 'startAt', 'endAt', 'canRescan', 'streamType', 'views', 'maximumViewers', 'averageViewers', 'chatters', 'messages', 'hosts', 'follows', 'unfollows', ),
        self::TYPE_COLNAME       => array(StatTableMap::COL_STSTAT_ID, StatTableMap::COL_STSTAT_STSTRM_ID, StatTableMap::COL_STSTAT_STCHAN_ID, StatTableMap::COL_STSTAT_STSITE_ID, StatTableMap::COL_STSTAT_DURATION, StatTableMap::COL_STSTAT_START_AT, StatTableMap::COL_STSTAT_END_AT, StatTableMap::COL_STSTAT_CAN_RESCAN, StatTableMap::COL_STSTAT_STREAM_TYPE, StatTableMap::COL_STSTAT_VIEWS, StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS, StatTableMap::COL_STSTAT_AVERAGE_VIEWERS, StatTableMap::COL_STSTAT_CHATTERS, StatTableMap::COL_STSTAT_MESSAGES, StatTableMap::COL_STSTAT_HOSTS, StatTableMap::COL_STSTAT_FOLLOWS, StatTableMap::COL_STSTAT_UNFOLLOWS, ),
        self::TYPE_FIELDNAME     => array('ststat_id', 'ststat_ststrm_id', 'ststat_stchan_id', 'ststat_stsite_id', 'ststat_duration', 'ststat_start_at', 'ststat_end_at', 'ststat_can_rescan', 'ststat_stream_type', 'ststat_views', 'ststat_maximum_viewers', 'ststat_average_viewers', 'ststat_chatters', 'ststat_messages', 'ststat_hosts', 'ststat_follows', 'ststat_unfollows', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'SiteId' => 3, 'Duration' => 4, 'StartAt' => 5, 'EndAt' => 6, 'CanRescan' => 7, 'StreamType' => 8, 'Views' => 9, 'MaximumViewers' => 10, 'AverageViewers' => 11, 'Chatters' => 12, 'Messages' => 13, 'Hosts' => 14, 'Follows' => 15, 'Unfollows' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'siteId' => 3, 'duration' => 4, 'startAt' => 5, 'endAt' => 6, 'canRescan' => 7, 'streamType' => 8, 'views' => 9, 'maximumViewers' => 10, 'averageViewers' => 11, 'chatters' => 12, 'messages' => 13, 'hosts' => 14, 'follows' => 15, 'unfollows' => 16, ),
        self::TYPE_COLNAME       => array(StatTableMap::COL_STSTAT_ID => 0, StatTableMap::COL_STSTAT_STSTRM_ID => 1, StatTableMap::COL_STSTAT_STCHAN_ID => 2, StatTableMap::COL_STSTAT_STSITE_ID => 3, StatTableMap::COL_STSTAT_DURATION => 4, StatTableMap::COL_STSTAT_START_AT => 5, StatTableMap::COL_STSTAT_END_AT => 6, StatTableMap::COL_STSTAT_CAN_RESCAN => 7, StatTableMap::COL_STSTAT_STREAM_TYPE => 8, StatTableMap::COL_STSTAT_VIEWS => 9, StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS => 10, StatTableMap::COL_STSTAT_AVERAGE_VIEWERS => 11, StatTableMap::COL_STSTAT_CHATTERS => 12, StatTableMap::COL_STSTAT_MESSAGES => 13, StatTableMap::COL_STSTAT_HOSTS => 14, StatTableMap::COL_STSTAT_FOLLOWS => 15, StatTableMap::COL_STSTAT_UNFOLLOWS => 16, ),
        self::TYPE_FIELDNAME     => array('ststat_id' => 0, 'ststat_ststrm_id' => 1, 'ststat_stchan_id' => 2, 'ststat_stsite_id' => 3, 'ststat_duration' => 4, 'ststat_start_at' => 5, 'ststat_end_at' => 6, 'ststat_can_rescan' => 7, 'ststat_stream_type' => 8, 'ststat_views' => 9, 'ststat_maximum_viewers' => 10, 'ststat_average_viewers' => 11, 'ststat_chatters' => 12, 'ststat_messages' => 13, 'ststat_hosts' => 14, 'ststat_follows' => 15, 'ststat_unfollows' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                StatTableMap::COL_STSTAT_DURATION => array(
                            self::COL_STSTAT_DURATION_MINUTE,
            self::COL_STSTAT_DURATION_HOUR,
            self::COL_STSTAT_DURATION_DAY,
            self::COL_STSTAT_DURATION_WEEK,
            self::COL_STSTAT_DURATION_MONTH,
            self::COL_STSTAT_DURATION_YEAR,
            self::COL_STSTAT_DURATION_LIVE,
            self::COL_STSTAT_DURATION_VOD,
        ),
                StatTableMap::COL_STSTAT_STREAM_TYPE => array(
                            self::COL_STSTAT_STREAM_TYPE_OFFLINE,
            self::COL_STSTAT_STREAM_TYPE_LIVE,
            self::COL_STSTAT_STREAM_TYPE_VOD,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_stat_ststat');
        $this->setPhpName('Stat');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Stat');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ststat_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('ststat_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('ststat_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('ststat_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addColumn('ststat_duration', 'Duration', 'ENUM', true, null, 'minute');
        $this->getColumn('ststat_duration')->setValueSet(array (
  0 => 'minute',
  1 => 'hour',
  2 => 'day',
  3 => 'week',
  4 => 'month',
  5 => 'year',
  6 => 'live',
  7 => 'vod',
));
        $this->addColumn('ststat_start_at', 'StartAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('ststat_end_at', 'EndAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('ststat_can_rescan', 'CanRescan', 'BOOLEAN', true, 1, false);
        $this->addColumn('ststat_stream_type', 'StreamType', 'ENUM', true, null, 'offline');
        $this->getColumn('ststat_stream_type')->setValueSet(array (
  0 => 'offline',
  1 => 'live',
  2 => 'vod',
));
        $this->addColumn('ststat_views', 'Views', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_maximum_viewers', 'MaximumViewers', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_average_viewers', 'AverageViewers', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_chatters', 'Chatters', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_messages', 'Messages', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_hosts', 'Hosts', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_follows', 'Follows', 'INTEGER', true, null, 0);
        $this->addColumn('ststat_unfollows', 'Unfollows', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ststat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ststat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ststat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StatTableMap::CLASS_DEFAULT : StatTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Stat object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StatTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StatTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StatTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StatTableMap::OM_CLASS;
            /** @var Stat $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StatTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StatTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StatTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Stat $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StatTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_ID);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_STSTRM_ID);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_STCHAN_ID);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_STSITE_ID);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_DURATION);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_START_AT);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_END_AT);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_CAN_RESCAN);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_STREAM_TYPE);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_VIEWS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_MAXIMUM_VIEWERS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_AVERAGE_VIEWERS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_CHATTERS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_MESSAGES);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_HOSTS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_FOLLOWS);
            $criteria->addSelectColumn(StatTableMap::COL_STSTAT_UNFOLLOWS);
        } else {
            $criteria->addSelectColumn($alias . '.ststat_id');
            $criteria->addSelectColumn($alias . '.ststat_ststrm_id');
            $criteria->addSelectColumn($alias . '.ststat_stchan_id');
            $criteria->addSelectColumn($alias . '.ststat_stsite_id');
            $criteria->addSelectColumn($alias . '.ststat_duration');
            $criteria->addSelectColumn($alias . '.ststat_start_at');
            $criteria->addSelectColumn($alias . '.ststat_end_at');
            $criteria->addSelectColumn($alias . '.ststat_can_rescan');
            $criteria->addSelectColumn($alias . '.ststat_stream_type');
            $criteria->addSelectColumn($alias . '.ststat_views');
            $criteria->addSelectColumn($alias . '.ststat_maximum_viewers');
            $criteria->addSelectColumn($alias . '.ststat_average_viewers');
            $criteria->addSelectColumn($alias . '.ststat_chatters');
            $criteria->addSelectColumn($alias . '.ststat_messages');
            $criteria->addSelectColumn($alias . '.ststat_hosts');
            $criteria->addSelectColumn($alias . '.ststat_follows');
            $criteria->addSelectColumn($alias . '.ststat_unfollows');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StatTableMap::DATABASE_NAME)->getTable(StatTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StatTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StatTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StatTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Stat or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Stat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Stat) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StatTableMap::DATABASE_NAME);
            $criteria->add(StatTableMap::COL_STSTAT_ID, (array) $values, Criteria::IN);
        }

        $query = StatQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StatTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StatTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_stat_ststat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StatQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Stat or Criteria object.
     *
     * @param mixed               $criteria Criteria or Stat object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StatTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Stat object
        }

        if ($criteria->containsKey(StatTableMap::COL_STSTAT_ID) && $criteria->keyContainsValue(StatTableMap::COL_STSTAT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.StatTableMap::COL_STSTAT_ID.')');
        }


        // Set the correct dbName
        $query = StatQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StatTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StatTableMap::buildTableMap();

<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\DeepBotImportExperience;
use IiMedias\StreamBundle\Model\DeepBotImportExperienceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_deepbot_import_experience_stdbie' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DeepBotImportExperienceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.DeepBotImportExperienceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_deepbot_import_experience_stdbie';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.DeepBotImportExperience';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the stdbie_id field
     */
    const COL_STDBIE_ID = 'stream_deepbot_import_experience_stdbie.stdbie_id';

    /**
     * the column name for the stdbie_ststrm_id field
     */
    const COL_STDBIE_STSTRM_ID = 'stream_deepbot_import_experience_stdbie.stdbie_ststrm_id';

    /**
     * the column name for the stdbie_stchan_id field
     */
    const COL_STDBIE_STCHAN_ID = 'stream_deepbot_import_experience_stdbie.stdbie_stchan_id';

    /**
     * the column name for the stdbie_stsite_id field
     */
    const COL_STDBIE_STSITE_ID = 'stream_deepbot_import_experience_stdbie.stdbie_stsite_id';

    /**
     * the column name for the stdbie_stcusr_id field
     */
    const COL_STDBIE_STCUSR_ID = 'stream_deepbot_import_experience_stdbie.stdbie_stcusr_id';

    /**
     * the column name for the stdbie_exp field
     */
    const COL_STDBIE_EXP = 'stream_deepbot_import_experience_stdbie.stdbie_exp';

    /**
     * the column name for the stdbie_time field
     */
    const COL_STDBIE_TIME = 'stream_deepbot_import_experience_stdbie.stdbie_time';

    /**
     * the column name for the stdbie_join_at field
     */
    const COL_STDBIE_JOIN_AT = 'stream_deepbot_import_experience_stdbie.stdbie_join_at';

    /**
     * the column name for the stdbie_last_seen_at field
     */
    const COL_STDBIE_LAST_SEEN_AT = 'stream_deepbot_import_experience_stdbie.stdbie_last_seen_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'SiteId', 'ChatUserId', 'Exp', 'Time', 'JoinAt', 'LastSeenAt', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'siteId', 'chatUserId', 'exp', 'time', 'joinAt', 'lastSeenAt', ),
        self::TYPE_COLNAME       => array(DeepBotImportExperienceTableMap::COL_STDBIE_ID, DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID, DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID, DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID, DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID, DeepBotImportExperienceTableMap::COL_STDBIE_EXP, DeepBotImportExperienceTableMap::COL_STDBIE_TIME, DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT, DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT, ),
        self::TYPE_FIELDNAME     => array('stdbie_id', 'stdbie_ststrm_id', 'stdbie_stchan_id', 'stdbie_stsite_id', 'stdbie_stcusr_id', 'stdbie_exp', 'stdbie_time', 'stdbie_join_at', 'stdbie_last_seen_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'SiteId' => 3, 'ChatUserId' => 4, 'Exp' => 5, 'Time' => 6, 'JoinAt' => 7, 'LastSeenAt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'siteId' => 3, 'chatUserId' => 4, 'exp' => 5, 'time' => 6, 'joinAt' => 7, 'lastSeenAt' => 8, ),
        self::TYPE_COLNAME       => array(DeepBotImportExperienceTableMap::COL_STDBIE_ID => 0, DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID => 1, DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID => 2, DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID => 3, DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID => 4, DeepBotImportExperienceTableMap::COL_STDBIE_EXP => 5, DeepBotImportExperienceTableMap::COL_STDBIE_TIME => 6, DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT => 7, DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT => 8, ),
        self::TYPE_FIELDNAME     => array('stdbie_id' => 0, 'stdbie_ststrm_id' => 1, 'stdbie_stchan_id' => 2, 'stdbie_stsite_id' => 3, 'stdbie_stcusr_id' => 4, 'stdbie_exp' => 5, 'stdbie_time' => 6, 'stdbie_join_at' => 7, 'stdbie_last_seen_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_deepbot_import_experience_stdbie');
        $this->setPhpName('DeepBotImportExperience');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stdbie_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stdbie_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stdbie_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stdbie_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addForeignKey('stdbie_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addColumn('stdbie_exp', 'Exp', 'INTEGER', true, null, null);
        $this->addColumn('stdbie_time', 'Time', 'FLOAT', true, null, null);
        $this->addColumn('stdbie_join_at', 'JoinAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stdbie_last_seen_at', 'LastSeenAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stdbie_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stdbie_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stdbie_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stdbie_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DeepBotImportExperienceTableMap::CLASS_DEFAULT : DeepBotImportExperienceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DeepBotImportExperience object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DeepBotImportExperienceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DeepBotImportExperienceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DeepBotImportExperienceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DeepBotImportExperienceTableMap::OM_CLASS;
            /** @var DeepBotImportExperience $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DeepBotImportExperienceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DeepBotImportExperienceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DeepBotImportExperienceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DeepBotImportExperience $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DeepBotImportExperienceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_ID);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_STSTRM_ID);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_STCHAN_ID);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_STSITE_ID);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_STCUSR_ID);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_EXP);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_TIME);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_JOIN_AT);
            $criteria->addSelectColumn(DeepBotImportExperienceTableMap::COL_STDBIE_LAST_SEEN_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stdbie_id');
            $criteria->addSelectColumn($alias . '.stdbie_ststrm_id');
            $criteria->addSelectColumn($alias . '.stdbie_stchan_id');
            $criteria->addSelectColumn($alias . '.stdbie_stsite_id');
            $criteria->addSelectColumn($alias . '.stdbie_stcusr_id');
            $criteria->addSelectColumn($alias . '.stdbie_exp');
            $criteria->addSelectColumn($alias . '.stdbie_time');
            $criteria->addSelectColumn($alias . '.stdbie_join_at');
            $criteria->addSelectColumn($alias . '.stdbie_last_seen_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DeepBotImportExperienceTableMap::DATABASE_NAME)->getTable(DeepBotImportExperienceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DeepBotImportExperienceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DeepBotImportExperienceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DeepBotImportExperienceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DeepBotImportExperience or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DeepBotImportExperience object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeepBotImportExperienceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\DeepBotImportExperience) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DeepBotImportExperienceTableMap::DATABASE_NAME);
            $criteria->add(DeepBotImportExperienceTableMap::COL_STDBIE_ID, (array) $values, Criteria::IN);
        }

        $query = DeepBotImportExperienceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DeepBotImportExperienceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DeepBotImportExperienceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_deepbot_import_experience_stdbie table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DeepBotImportExperienceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DeepBotImportExperience or Criteria object.
     *
     * @param mixed               $criteria Criteria or DeepBotImportExperience object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeepBotImportExperienceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DeepBotImportExperience object
        }

        if ($criteria->containsKey(DeepBotImportExperienceTableMap::COL_STDBIE_ID) && $criteria->keyContainsValue(DeepBotImportExperienceTableMap::COL_STDBIE_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DeepBotImportExperienceTableMap::COL_STDBIE_ID.')');
        }


        // Set the correct dbName
        $query = DeepBotImportExperienceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DeepBotImportExperienceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DeepBotImportExperienceTableMap::buildTableMap();

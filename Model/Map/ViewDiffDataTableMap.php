<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\ViewDiffData;
use IiMedias\StreamBundle\Model\ViewDiffDataQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_view_diff_data_stddat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ViewDiffDataTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ViewDiffDataTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_view_diff_data_stddat';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\ViewDiffData';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.ViewDiffData';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the stddat_id field
     */
    const COL_STDDAT_ID = 'stream_view_diff_data_stddat.stddat_id';

    /**
     * the column name for the stddat_ststrm_id field
     */
    const COL_STDDAT_STSTRM_ID = 'stream_view_diff_data_stddat.stddat_ststrm_id';

    /**
     * the column name for the stddat_stchan_id field
     */
    const COL_STDDAT_STCHAN_ID = 'stream_view_diff_data_stddat.stddat_stchan_id';

    /**
     * the column name for the stddat_stsite_id field
     */
    const COL_STDDAT_STSITE_ID = 'stream_view_diff_data_stddat.stddat_stsite_id';

    /**
     * the column name for the stddat_diff field
     */
    const COL_STDDAT_DIFF = 'stream_view_diff_data_stddat.stddat_diff';

    /**
     * the column name for the stddat_at field
     */
    const COL_STDDAT_AT = 'stream_view_diff_data_stddat.stddat_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'SiteId', 'Diff', 'At', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'siteId', 'diff', 'at', ),
        self::TYPE_COLNAME       => array(ViewDiffDataTableMap::COL_STDDAT_ID, ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID, ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID, ViewDiffDataTableMap::COL_STDDAT_STSITE_ID, ViewDiffDataTableMap::COL_STDDAT_DIFF, ViewDiffDataTableMap::COL_STDDAT_AT, ),
        self::TYPE_FIELDNAME     => array('stddat_id', 'stddat_ststrm_id', 'stddat_stchan_id', 'stddat_stsite_id', 'stddat_diff', 'stddat_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'SiteId' => 3, 'Diff' => 4, 'At' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'siteId' => 3, 'diff' => 4, 'at' => 5, ),
        self::TYPE_COLNAME       => array(ViewDiffDataTableMap::COL_STDDAT_ID => 0, ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID => 1, ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID => 2, ViewDiffDataTableMap::COL_STDDAT_STSITE_ID => 3, ViewDiffDataTableMap::COL_STDDAT_DIFF => 4, ViewDiffDataTableMap::COL_STDDAT_AT => 5, ),
        self::TYPE_FIELDNAME     => array('stddat_id' => 0, 'stddat_ststrm_id' => 1, 'stddat_stchan_id' => 2, 'stddat_stsite_id' => 3, 'stddat_diff' => 4, 'stddat_at' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_view_diff_data_stddat');
        $this->setPhpName('ViewDiffData');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\ViewDiffData');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stddat_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stddat_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stddat_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stddat_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addColumn('stddat_diff', 'Diff', 'INTEGER', true, null, null);
        $this->addColumn('stddat_at', 'At', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stddat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stddat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stddat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ViewDiffDataTableMap::CLASS_DEFAULT : ViewDiffDataTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ViewDiffData object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ViewDiffDataTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ViewDiffDataTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ViewDiffDataTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ViewDiffDataTableMap::OM_CLASS;
            /** @var ViewDiffData $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ViewDiffDataTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ViewDiffDataTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ViewDiffDataTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ViewDiffData $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ViewDiffDataTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_ID);
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_STSTRM_ID);
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_STCHAN_ID);
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_STSITE_ID);
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_DIFF);
            $criteria->addSelectColumn(ViewDiffDataTableMap::COL_STDDAT_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stddat_id');
            $criteria->addSelectColumn($alias . '.stddat_ststrm_id');
            $criteria->addSelectColumn($alias . '.stddat_stchan_id');
            $criteria->addSelectColumn($alias . '.stddat_stsite_id');
            $criteria->addSelectColumn($alias . '.stddat_diff');
            $criteria->addSelectColumn($alias . '.stddat_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ViewDiffDataTableMap::DATABASE_NAME)->getTable(ViewDiffDataTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ViewDiffDataTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ViewDiffDataTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ViewDiffDataTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ViewDiffData or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ViewDiffData object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewDiffDataTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\ViewDiffData) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ViewDiffDataTableMap::DATABASE_NAME);
            $criteria->add(ViewDiffDataTableMap::COL_STDDAT_ID, (array) $values, Criteria::IN);
        }

        $query = ViewDiffDataQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ViewDiffDataTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ViewDiffDataTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_view_diff_data_stddat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ViewDiffDataQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ViewDiffData or Criteria object.
     *
     * @param mixed               $criteria Criteria or ViewDiffData object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewDiffDataTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ViewDiffData object
        }

        if ($criteria->containsKey(ViewDiffDataTableMap::COL_STDDAT_ID) && $criteria->keyContainsValue(ViewDiffDataTableMap::COL_STDDAT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ViewDiffDataTableMap::COL_STDDAT_ID.')');
        }


        // Set the correct dbName
        $query = ViewDiffDataQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ViewDiffDataTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ViewDiffDataTableMap::buildTableMap();

<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_site_stsite' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.SiteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_site_stsite';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Site';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Site';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the stsite_id field
     */
    const COL_STSITE_ID = 'stream_site_stsite.stsite_id';

    /**
     * the column name for the stsite_name field
     */
    const COL_STSITE_NAME = 'stream_site_stsite.stsite_name';

    /**
     * the column name for the stsite_code field
     */
    const COL_STSITE_CODE = 'stream_site_stsite.stsite_code';

    /**
     * the column name for the stsite_font_awesome_icon field
     */
    const COL_STSITE_FONT_AWESOME_ICON = 'stream_site_stsite.stsite_font_awesome_icon';

    /**
     * the column name for the stsite_color_hexa field
     */
    const COL_STSITE_COLOR_HEXA = 'stream_site_stsite.stsite_color_hexa';

    /**
     * the column name for the stsite_color_rgb field
     */
    const COL_STSITE_COLOR_RGB = 'stream_site_stsite.stsite_color_rgb';

    /**
     * the column name for the stsite_created_by_user_id field
     */
    const COL_STSITE_CREATED_BY_USER_ID = 'stream_site_stsite.stsite_created_by_user_id';

    /**
     * the column name for the stsite_updated_by_user_id field
     */
    const COL_STSITE_UPDATED_BY_USER_ID = 'stream_site_stsite.stsite_updated_by_user_id';

    /**
     * the column name for the stsite_created_at field
     */
    const COL_STSITE_CREATED_AT = 'stream_site_stsite.stsite_created_at';

    /**
     * the column name for the stsite_updated_at field
     */
    const COL_STSITE_UPDATED_AT = 'stream_site_stsite.stsite_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Code', 'FontAwesomeIcon', 'ColorHexa', 'ColorRgb', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'code', 'fontAwesomeIcon', 'colorHexa', 'colorRgb', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(SiteTableMap::COL_STSITE_ID, SiteTableMap::COL_STSITE_NAME, SiteTableMap::COL_STSITE_CODE, SiteTableMap::COL_STSITE_FONT_AWESOME_ICON, SiteTableMap::COL_STSITE_COLOR_HEXA, SiteTableMap::COL_STSITE_COLOR_RGB, SiteTableMap::COL_STSITE_CREATED_BY_USER_ID, SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID, SiteTableMap::COL_STSITE_CREATED_AT, SiteTableMap::COL_STSITE_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('stsite_id', 'stsite_name', 'stsite_code', 'stsite_font_awesome_icon', 'stsite_color_hexa', 'stsite_color_rgb', 'stsite_created_by_user_id', 'stsite_updated_by_user_id', 'stsite_created_at', 'stsite_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Code' => 2, 'FontAwesomeIcon' => 3, 'ColorHexa' => 4, 'ColorRgb' => 5, 'CreatedByUserId' => 6, 'UpdatedByUserId' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'code' => 2, 'fontAwesomeIcon' => 3, 'colorHexa' => 4, 'colorRgb' => 5, 'createdByUserId' => 6, 'updatedByUserId' => 7, 'createdAt' => 8, 'updatedAt' => 9, ),
        self::TYPE_COLNAME       => array(SiteTableMap::COL_STSITE_ID => 0, SiteTableMap::COL_STSITE_NAME => 1, SiteTableMap::COL_STSITE_CODE => 2, SiteTableMap::COL_STSITE_FONT_AWESOME_ICON => 3, SiteTableMap::COL_STSITE_COLOR_HEXA => 4, SiteTableMap::COL_STSITE_COLOR_RGB => 5, SiteTableMap::COL_STSITE_CREATED_BY_USER_ID => 6, SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID => 7, SiteTableMap::COL_STSITE_CREATED_AT => 8, SiteTableMap::COL_STSITE_UPDATED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('stsite_id' => 0, 'stsite_name' => 1, 'stsite_code' => 2, 'stsite_font_awesome_icon' => 3, 'stsite_color_hexa' => 4, 'stsite_color_rgb' => 5, 'stsite_created_by_user_id' => 6, 'stsite_updated_by_user_id' => 7, 'stsite_created_at' => 8, 'stsite_updated_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_site_stsite');
        $this->setPhpName('Site');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Site');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stsite_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('stsite_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('stsite_code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('stsite_font_awesome_icon', 'FontAwesomeIcon', 'VARCHAR', false, 255, null);
        $this->addColumn('stsite_color_hexa', 'ColorHexa', 'VARCHAR', true, 255, null);
        $this->addColumn('stsite_color_rgb', 'ColorRgb', 'VARCHAR', true, 255, null);
        $this->addForeignKey('stsite_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('stsite_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('stsite_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stsite_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stsite_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stsite_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stchan_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'Channels', false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcusr_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatUsers', false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
        $this->addRelation('DeepBotImportExperience', '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stdbie_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'DeepBotImportExperiences', false);
        $this->addRelation('MessageExperience', '\\IiMedias\\StreamBundle\\Model\\MessageExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stmexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'MessageExperiences', false);
        $this->addRelation('ChatterExperience', '\\IiMedias\\StreamBundle\\Model\\ChatterExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatterExperiences', false);
        $this->addRelation('FollowExperience', '\\IiMedias\\StreamBundle\\Model\\FollowExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowExperiences', false);
        $this->addRelation('HostExperience', '\\IiMedias\\StreamBundle\\Model\\HostExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sthexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'HostExperiences', false);
        $this->addRelation('ViewDiffData', '\\IiMedias\\StreamBundle\\Model\\ViewDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stddat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewDiffDatas', false);
        $this->addRelation('ViewerData', '\\IiMedias\\StreamBundle\\Model\\ViewerData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stvdat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewerDatas', false);
        $this->addRelation('FollowDiffData', '\\IiMedias\\StreamBundle\\Model\\FollowDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfdat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowDiffDatas', false);
        $this->addRelation('StatusData', '\\IiMedias\\StreamBundle\\Model\\StatusData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stsdat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'StatusDatas', false);
        $this->addRelation('TypeData', '\\IiMedias\\StreamBundle\\Model\\TypeData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sttdat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'TypeDatas', false);
        $this->addRelation('GameData', '\\IiMedias\\StreamBundle\\Model\\GameData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stgdat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'GameDatas', false);
        $this->addRelation('Stat', '\\IiMedias\\StreamBundle\\Model\\Stat', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ststat_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'Stats', false);
        $this->addRelation('Experience', '\\IiMedias\\StreamBundle\\Model\\Experience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stexpr_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', 'Experiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'stsite_created_at', 'update_column' => 'stsite_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_site_stsite     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ChannelTableMap::clearInstancePool();
        ChatUserTableMap::clearInstancePool();
        UserExperienceTableMap::clearInstancePool();
        DeepBotImportExperienceTableMap::clearInstancePool();
        MessageExperienceTableMap::clearInstancePool();
        ChatterExperienceTableMap::clearInstancePool();
        FollowExperienceTableMap::clearInstancePool();
        HostExperienceTableMap::clearInstancePool();
        ViewDiffDataTableMap::clearInstancePool();
        ViewerDataTableMap::clearInstancePool();
        FollowDiffDataTableMap::clearInstancePool();
        StatusDataTableMap::clearInstancePool();
        TypeDataTableMap::clearInstancePool();
        GameDataTableMap::clearInstancePool();
        StatTableMap::clearInstancePool();
        ExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteTableMap::CLASS_DEFAULT : SiteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Site object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteTableMap::OM_CLASS;
            /** @var Site $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Site $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_ID);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_NAME);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_CODE);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_FONT_AWESOME_ICON);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_COLOR_HEXA);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_COLOR_RGB);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_CREATED_AT);
            $criteria->addSelectColumn(SiteTableMap::COL_STSITE_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stsite_id');
            $criteria->addSelectColumn($alias . '.stsite_name');
            $criteria->addSelectColumn($alias . '.stsite_code');
            $criteria->addSelectColumn($alias . '.stsite_font_awesome_icon');
            $criteria->addSelectColumn($alias . '.stsite_color_hexa');
            $criteria->addSelectColumn($alias . '.stsite_color_rgb');
            $criteria->addSelectColumn($alias . '.stsite_created_by_user_id');
            $criteria->addSelectColumn($alias . '.stsite_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.stsite_created_at');
            $criteria->addSelectColumn($alias . '.stsite_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteTableMap::DATABASE_NAME)->getTable(SiteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Site or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Site object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Site) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteTableMap::DATABASE_NAME);
            $criteria->add(SiteTableMap::COL_STSITE_ID, (array) $values, Criteria::IN);
        }

        $query = SiteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_site_stsite table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Site or Criteria object.
     *
     * @param mixed               $criteria Criteria or Site object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Site object
        }

        if ($criteria->containsKey(SiteTableMap::COL_STSITE_ID) && $criteria->keyContainsValue(SiteTableMap::COL_STSITE_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteTableMap::COL_STSITE_ID.')');
        }


        // Set the correct dbName
        $query = SiteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteTableMap::buildTableMap();

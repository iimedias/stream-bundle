<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\ChatterExperience;
use IiMedias\StreamBundle\Model\ChatterExperienceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_chatter_experience_stcexp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatterExperienceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ChatterExperienceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_chatter_experience_stcexp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\ChatterExperience';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.ChatterExperience';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the stcexp_id field
     */
    const COL_STCEXP_ID = 'stream_chatter_experience_stcexp.stcexp_id';

    /**
     * the column name for the stcexp_ststrm_id field
     */
    const COL_STCEXP_STSTRM_ID = 'stream_chatter_experience_stcexp.stcexp_ststrm_id';

    /**
     * the column name for the stcexp_stchan_id field
     */
    const COL_STCEXP_STCHAN_ID = 'stream_chatter_experience_stcexp.stcexp_stchan_id';

    /**
     * the column name for the stcexp_stsite_id field
     */
    const COL_STCEXP_STSITE_ID = 'stream_chatter_experience_stcexp.stcexp_stsite_id';

    /**
     * the column name for the stcexp_stcusr_id field
     */
    const COL_STCEXP_STCUSR_ID = 'stream_chatter_experience_stcexp.stcexp_stcusr_id';

    /**
     * the column name for the stcexp_stuexp_id field
     */
    const COL_STCEXP_STUEXP_ID = 'stream_chatter_experience_stcexp.stcexp_stuexp_id';

    /**
     * the column name for the stcexp_chat_type field
     */
    const COL_STCEXP_CHAT_TYPE = 'stream_chatter_experience_stcexp.stcexp_chat_type';

    /**
     * the column name for the stcexp_exp field
     */
    const COL_STCEXP_EXP = 'stream_chatter_experience_stcexp.stcexp_exp';

    /**
     * the column name for the stcexp_at field
     */
    const COL_STCEXP_AT = 'stream_chatter_experience_stcexp.stcexp_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the stcexp_chat_type field */
    const COL_STCEXP_CHAT_TYPE_VIEWER = 'viewer';
    const COL_STCEXP_CHAT_TYPE_MODERATOR = 'moderator';
    const COL_STCEXP_CHAT_TYPE_GLOBAL_MOD = 'global_mod';
    const COL_STCEXP_CHAT_TYPE_ADMIN = 'admin';
    const COL_STCEXP_CHAT_TYPE_STAFF = 'staff';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'SiteId', 'ChatUserId', 'UserExperienceId', 'ChatType', 'Exp', 'At', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'siteId', 'chatUserId', 'userExperienceId', 'chatType', 'exp', 'at', ),
        self::TYPE_COLNAME       => array(ChatterExperienceTableMap::COL_STCEXP_ID, ChatterExperienceTableMap::COL_STCEXP_STSTRM_ID, ChatterExperienceTableMap::COL_STCEXP_STCHAN_ID, ChatterExperienceTableMap::COL_STCEXP_STSITE_ID, ChatterExperienceTableMap::COL_STCEXP_STCUSR_ID, ChatterExperienceTableMap::COL_STCEXP_STUEXP_ID, ChatterExperienceTableMap::COL_STCEXP_CHAT_TYPE, ChatterExperienceTableMap::COL_STCEXP_EXP, ChatterExperienceTableMap::COL_STCEXP_AT, ),
        self::TYPE_FIELDNAME     => array('stcexp_id', 'stcexp_ststrm_id', 'stcexp_stchan_id', 'stcexp_stsite_id', 'stcexp_stcusr_id', 'stcexp_stuexp_id', 'stcexp_chat_type', 'stcexp_exp', 'stcexp_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'SiteId' => 3, 'ChatUserId' => 4, 'UserExperienceId' => 5, 'ChatType' => 6, 'Exp' => 7, 'At' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'siteId' => 3, 'chatUserId' => 4, 'userExperienceId' => 5, 'chatType' => 6, 'exp' => 7, 'at' => 8, ),
        self::TYPE_COLNAME       => array(ChatterExperienceTableMap::COL_STCEXP_ID => 0, ChatterExperienceTableMap::COL_STCEXP_STSTRM_ID => 1, ChatterExperienceTableMap::COL_STCEXP_STCHAN_ID => 2, ChatterExperienceTableMap::COL_STCEXP_STSITE_ID => 3, ChatterExperienceTableMap::COL_STCEXP_STCUSR_ID => 4, ChatterExperienceTableMap::COL_STCEXP_STUEXP_ID => 5, ChatterExperienceTableMap::COL_STCEXP_CHAT_TYPE => 6, ChatterExperienceTableMap::COL_STCEXP_EXP => 7, ChatterExperienceTableMap::COL_STCEXP_AT => 8, ),
        self::TYPE_FIELDNAME     => array('stcexp_id' => 0, 'stcexp_ststrm_id' => 1, 'stcexp_stchan_id' => 2, 'stcexp_stsite_id' => 3, 'stcexp_stcusr_id' => 4, 'stcexp_stuexp_id' => 5, 'stcexp_chat_type' => 6, 'stcexp_exp' => 7, 'stcexp_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                ChatterExperienceTableMap::COL_STCEXP_CHAT_TYPE => array(
                            self::COL_STCEXP_CHAT_TYPE_VIEWER,
            self::COL_STCEXP_CHAT_TYPE_MODERATOR,
            self::COL_STCEXP_CHAT_TYPE_GLOBAL_MOD,
            self::COL_STCEXP_CHAT_TYPE_ADMIN,
            self::COL_STCEXP_CHAT_TYPE_STAFF,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_chatter_experience_stcexp');
        $this->setPhpName('ChatterExperience');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\ChatterExperience');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stcexp_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stcexp_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stcexp_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stcexp_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addForeignKey('stcexp_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addForeignKey('stcexp_stuexp_id', 'UserExperienceId', 'INTEGER', 'stream_user_experience_stuexp', 'stuexp_id', false, null, null);
        $this->addColumn('stcexp_chat_type', 'ChatType', 'ENUM', true, null, 'viewer');
        $this->getColumn('stcexp_chat_type')->setValueSet(array (
  0 => 'viewer',
  1 => 'moderator',
  2 => 'global_mod',
  3 => 'admin',
  4 => 'staff',
));
        $this->addColumn('stcexp_exp', 'Exp', 'INTEGER', true, null, 0);
        $this->addColumn('stcexp_at', 'At', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatterExperienceTableMap::CLASS_DEFAULT : ChatterExperienceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatterExperience object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatterExperienceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatterExperienceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatterExperienceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatterExperienceTableMap::OM_CLASS;
            /** @var ChatterExperience $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatterExperienceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatterExperienceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatterExperienceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatterExperience $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatterExperienceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_STSTRM_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_STCHAN_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_STSITE_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_STCUSR_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_STUEXP_ID);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_CHAT_TYPE);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_EXP);
            $criteria->addSelectColumn(ChatterExperienceTableMap::COL_STCEXP_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stcexp_id');
            $criteria->addSelectColumn($alias . '.stcexp_ststrm_id');
            $criteria->addSelectColumn($alias . '.stcexp_stchan_id');
            $criteria->addSelectColumn($alias . '.stcexp_stsite_id');
            $criteria->addSelectColumn($alias . '.stcexp_stcusr_id');
            $criteria->addSelectColumn($alias . '.stcexp_stuexp_id');
            $criteria->addSelectColumn($alias . '.stcexp_chat_type');
            $criteria->addSelectColumn($alias . '.stcexp_exp');
            $criteria->addSelectColumn($alias . '.stcexp_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatterExperienceTableMap::DATABASE_NAME)->getTable(ChatterExperienceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatterExperienceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatterExperienceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatterExperienceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatterExperience or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatterExperience object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatterExperienceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\ChatterExperience) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatterExperienceTableMap::DATABASE_NAME);
            $criteria->add(ChatterExperienceTableMap::COL_STCEXP_ID, (array) $values, Criteria::IN);
        }

        $query = ChatterExperienceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatterExperienceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatterExperienceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_chatter_experience_stcexp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatterExperienceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatterExperience or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatterExperience object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatterExperienceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatterExperience object
        }

        if ($criteria->containsKey(ChatterExperienceTableMap::COL_STCEXP_ID) && $criteria->keyContainsValue(ChatterExperienceTableMap::COL_STCEXP_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChatterExperienceTableMap::COL_STCEXP_ID.')');
        }


        // Set the correct dbName
        $query = ChatterExperienceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatterExperienceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatterExperienceTableMap::buildTableMap();

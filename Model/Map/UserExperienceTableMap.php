<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\UserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_user_experience_stuexp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserExperienceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.UserExperienceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_user_experience_stuexp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\UserExperience';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.UserExperience';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the stuexp_id field
     */
    const COL_STUEXP_ID = 'stream_user_experience_stuexp.stuexp_id';

    /**
     * the column name for the stuexp_parent_id field
     */
    const COL_STUEXP_PARENT_ID = 'stream_user_experience_stuexp.stuexp_parent_id';

    /**
     * the column name for the stuexp_ststrm_id field
     */
    const COL_STUEXP_STSTRM_ID = 'stream_user_experience_stuexp.stuexp_ststrm_id';

    /**
     * the column name for the stuexp_stchan_id field
     */
    const COL_STUEXP_STCHAN_ID = 'stream_user_experience_stuexp.stuexp_stchan_id';

    /**
     * the column name for the stuexp_stsite_id field
     */
    const COL_STUEXP_STSITE_ID = 'stream_user_experience_stuexp.stuexp_stsite_id';

    /**
     * the column name for the stuexp_stcusr_id field
     */
    const COL_STUEXP_STCUSR_ID = 'stream_user_experience_stuexp.stuexp_stcusr_id';

    /**
     * the column name for the stuexp_chat_type field
     */
    const COL_STUEXP_CHAT_TYPE = 'stream_user_experience_stuexp.stuexp_chat_type';

    /**
     * the column name for the stuexp_exp field
     */
    const COL_STUEXP_EXP = 'stream_user_experience_stuexp.stuexp_exp';

    /**
     * the column name for the stuexp_total_exp field
     */
    const COL_STUEXP_TOTAL_EXP = 'stream_user_experience_stuexp.stuexp_total_exp';

    /**
     * the column name for the stuexp_is_following field
     */
    const COL_STUEXP_IS_FOLLOWING = 'stream_user_experience_stuexp.stuexp_is_following';

    /**
     * the column name for the stuexp_messages_count field
     */
    const COL_STUEXP_MESSAGES_COUNT = 'stream_user_experience_stuexp.stuexp_messages_count';

    /**
     * the column name for the stuexp_follows_count field
     */
    const COL_STUEXP_FOLLOWS_COUNT = 'stream_user_experience_stuexp.stuexp_follows_count';

    /**
     * the column name for the stuexp_hosts_count field
     */
    const COL_STUEXP_HOSTS_COUNT = 'stream_user_experience_stuexp.stuexp_hosts_count';

    /**
     * the column name for the stuexp_chatters_count field
     */
    const COL_STUEXP_CHATTERS_COUNT = 'stream_user_experience_stuexp.stuexp_chatters_count';

    /**
     * the column name for the stuexp_strank_id field
     */
    const COL_STUEXP_STRANK_ID = 'stream_user_experience_stuexp.stuexp_strank_id';

    /**
     * the column name for the stuexp_stavtr_id field
     */
    const COL_STUEXP_STAVTR_ID = 'stream_user_experience_stuexp.stuexp_stavtr_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the stuexp_chat_type field */
    const COL_STUEXP_CHAT_TYPE_VIEWER = 'viewer';
    const COL_STUEXP_CHAT_TYPE_MODERATOR = 'moderator';
    const COL_STUEXP_CHAT_TYPE_GLOBAL_MOD = 'global_mod';
    const COL_STUEXP_CHAT_TYPE_ADMIN = 'admin';
    const COL_STUEXP_CHAT_TYPE_STAFF = 'staff';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'StreamId', 'ChannelId', 'SiteId', 'ChatUserId', 'ChatType', 'Exp', 'TotalExp', 'IsFollowing', 'MessagesCount', 'FollowsCount', 'HostsCount', 'ChattersCount', 'RankId', 'AvatarId', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'streamId', 'channelId', 'siteId', 'chatUserId', 'chatType', 'exp', 'totalExp', 'isFollowing', 'messagesCount', 'followsCount', 'hostsCount', 'chattersCount', 'rankId', 'avatarId', ),
        self::TYPE_COLNAME       => array(UserExperienceTableMap::COL_STUEXP_ID, UserExperienceTableMap::COL_STUEXP_PARENT_ID, UserExperienceTableMap::COL_STUEXP_STSTRM_ID, UserExperienceTableMap::COL_STUEXP_STCHAN_ID, UserExperienceTableMap::COL_STUEXP_STSITE_ID, UserExperienceTableMap::COL_STUEXP_STCUSR_ID, UserExperienceTableMap::COL_STUEXP_CHAT_TYPE, UserExperienceTableMap::COL_STUEXP_EXP, UserExperienceTableMap::COL_STUEXP_TOTAL_EXP, UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING, UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT, UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT, UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT, UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT, UserExperienceTableMap::COL_STUEXP_STRANK_ID, UserExperienceTableMap::COL_STUEXP_STAVTR_ID, ),
        self::TYPE_FIELDNAME     => array('stuexp_id', 'stuexp_parent_id', 'stuexp_ststrm_id', 'stuexp_stchan_id', 'stuexp_stsite_id', 'stuexp_stcusr_id', 'stuexp_chat_type', 'stuexp_exp', 'stuexp_total_exp', 'stuexp_is_following', 'stuexp_messages_count', 'stuexp_follows_count', 'stuexp_hosts_count', 'stuexp_chatters_count', 'stuexp_strank_id', 'stuexp_stavtr_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'StreamId' => 2, 'ChannelId' => 3, 'SiteId' => 4, 'ChatUserId' => 5, 'ChatType' => 6, 'Exp' => 7, 'TotalExp' => 8, 'IsFollowing' => 9, 'MessagesCount' => 10, 'FollowsCount' => 11, 'HostsCount' => 12, 'ChattersCount' => 13, 'RankId' => 14, 'AvatarId' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'streamId' => 2, 'channelId' => 3, 'siteId' => 4, 'chatUserId' => 5, 'chatType' => 6, 'exp' => 7, 'totalExp' => 8, 'isFollowing' => 9, 'messagesCount' => 10, 'followsCount' => 11, 'hostsCount' => 12, 'chattersCount' => 13, 'rankId' => 14, 'avatarId' => 15, ),
        self::TYPE_COLNAME       => array(UserExperienceTableMap::COL_STUEXP_ID => 0, UserExperienceTableMap::COL_STUEXP_PARENT_ID => 1, UserExperienceTableMap::COL_STUEXP_STSTRM_ID => 2, UserExperienceTableMap::COL_STUEXP_STCHAN_ID => 3, UserExperienceTableMap::COL_STUEXP_STSITE_ID => 4, UserExperienceTableMap::COL_STUEXP_STCUSR_ID => 5, UserExperienceTableMap::COL_STUEXP_CHAT_TYPE => 6, UserExperienceTableMap::COL_STUEXP_EXP => 7, UserExperienceTableMap::COL_STUEXP_TOTAL_EXP => 8, UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING => 9, UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT => 10, UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT => 11, UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT => 12, UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT => 13, UserExperienceTableMap::COL_STUEXP_STRANK_ID => 14, UserExperienceTableMap::COL_STUEXP_STAVTR_ID => 15, ),
        self::TYPE_FIELDNAME     => array('stuexp_id' => 0, 'stuexp_parent_id' => 1, 'stuexp_ststrm_id' => 2, 'stuexp_stchan_id' => 3, 'stuexp_stsite_id' => 4, 'stuexp_stcusr_id' => 5, 'stuexp_chat_type' => 6, 'stuexp_exp' => 7, 'stuexp_total_exp' => 8, 'stuexp_is_following' => 9, 'stuexp_messages_count' => 10, 'stuexp_follows_count' => 11, 'stuexp_hosts_count' => 12, 'stuexp_chatters_count' => 13, 'stuexp_strank_id' => 14, 'stuexp_stavtr_id' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                UserExperienceTableMap::COL_STUEXP_CHAT_TYPE => array(
                            self::COL_STUEXP_CHAT_TYPE_VIEWER,
            self::COL_STUEXP_CHAT_TYPE_MODERATOR,
            self::COL_STUEXP_CHAT_TYPE_GLOBAL_MOD,
            self::COL_STUEXP_CHAT_TYPE_ADMIN,
            self::COL_STUEXP_CHAT_TYPE_STAFF,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_user_experience_stuexp');
        $this->setPhpName('UserExperience');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\UserExperience');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stuexp_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('stuexp_parent_id', 'ParentId', 'INTEGER', false, null, null);
        $this->addForeignKey('stuexp_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stuexp_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stuexp_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addForeignKey('stuexp_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addColumn('stuexp_chat_type', 'ChatType', 'ENUM', true, null, 'viewer');
        $this->getColumn('stuexp_chat_type')->setValueSet(array (
  0 => 'viewer',
  1 => 'moderator',
  2 => 'global_mod',
  3 => 'admin',
  4 => 'staff',
));
        $this->addColumn('stuexp_exp', 'Exp', 'INTEGER', true, null, 0);
        $this->addColumn('stuexp_total_exp', 'TotalExp', 'INTEGER', true, null, 0);
        $this->addColumn('stuexp_is_following', 'IsFollowing', 'BOOLEAN', true, 1, false);
        $this->addColumn('stuexp_messages_count', 'MessagesCount', 'INTEGER', true, null, 0);
        $this->addColumn('stuexp_follows_count', 'FollowsCount', 'INTEGER', true, null, 0);
        $this->addColumn('stuexp_hosts_count', 'HostsCount', 'INTEGER', true, null, 0);
        $this->addColumn('stuexp_chatters_count', 'ChattersCount', 'INTEGER', true, null, 0);
        $this->addForeignKey('stuexp_strank_id', 'RankId', 'INTEGER', 'stream_rank_strank', 'strank_id', false, null, null);
        $this->addForeignKey('stuexp_stavtr_id', 'AvatarId', 'INTEGER', 'stream_avatar_stavtr', 'stavtr_id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Rank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Avatar', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stuexp_stavtr_id',
    1 => ':stavtr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('MessageExperience', '\\IiMedias\\StreamBundle\\Model\\MessageExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stmexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', 'MessageExperiences', false);
        $this->addRelation('ChatterExperience', '\\IiMedias\\StreamBundle\\Model\\ChatterExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatterExperiences', false);
        $this->addRelation('FollowExperience', '\\IiMedias\\StreamBundle\\Model\\FollowExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowExperiences', false);
        $this->addRelation('HostExperience', '\\IiMedias\\StreamBundle\\Model\\HostExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sthexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', 'HostExperiences', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to stream_user_experience_stuexp     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        MessageExperienceTableMap::clearInstancePool();
        ChatterExperienceTableMap::clearInstancePool();
        FollowExperienceTableMap::clearInstancePool();
        HostExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserExperienceTableMap::CLASS_DEFAULT : UserExperienceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserExperience object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserExperienceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserExperienceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserExperienceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserExperienceTableMap::OM_CLASS;
            /** @var UserExperience $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserExperienceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserExperienceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserExperienceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserExperience $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserExperienceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_PARENT_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STSTRM_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STCHAN_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STSITE_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STCUSR_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_CHAT_TYPE);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_EXP);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_TOTAL_EXP);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_IS_FOLLOWING);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_MESSAGES_COUNT);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_FOLLOWS_COUNT);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_HOSTS_COUNT);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_CHATTERS_COUNT);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STRANK_ID);
            $criteria->addSelectColumn(UserExperienceTableMap::COL_STUEXP_STAVTR_ID);
        } else {
            $criteria->addSelectColumn($alias . '.stuexp_id');
            $criteria->addSelectColumn($alias . '.stuexp_parent_id');
            $criteria->addSelectColumn($alias . '.stuexp_ststrm_id');
            $criteria->addSelectColumn($alias . '.stuexp_stchan_id');
            $criteria->addSelectColumn($alias . '.stuexp_stsite_id');
            $criteria->addSelectColumn($alias . '.stuexp_stcusr_id');
            $criteria->addSelectColumn($alias . '.stuexp_chat_type');
            $criteria->addSelectColumn($alias . '.stuexp_exp');
            $criteria->addSelectColumn($alias . '.stuexp_total_exp');
            $criteria->addSelectColumn($alias . '.stuexp_is_following');
            $criteria->addSelectColumn($alias . '.stuexp_messages_count');
            $criteria->addSelectColumn($alias . '.stuexp_follows_count');
            $criteria->addSelectColumn($alias . '.stuexp_hosts_count');
            $criteria->addSelectColumn($alias . '.stuexp_chatters_count');
            $criteria->addSelectColumn($alias . '.stuexp_strank_id');
            $criteria->addSelectColumn($alias . '.stuexp_stavtr_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserExperienceTableMap::DATABASE_NAME)->getTable(UserExperienceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserExperienceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserExperienceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserExperienceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserExperience or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserExperience object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\UserExperience) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserExperienceTableMap::DATABASE_NAME);
            $criteria->add(UserExperienceTableMap::COL_STUEXP_ID, (array) $values, Criteria::IN);
        }

        $query = UserExperienceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserExperienceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserExperienceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_user_experience_stuexp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserExperienceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserExperience or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserExperience object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserExperienceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserExperience object
        }

        if ($criteria->containsKey(UserExperienceTableMap::COL_STUEXP_ID) && $criteria->keyContainsValue(UserExperienceTableMap::COL_STUEXP_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserExperienceTableMap::COL_STUEXP_ID.')');
        }


        // Set the correct dbName
        $query = UserExperienceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserExperienceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserExperienceTableMap::buildTableMap();

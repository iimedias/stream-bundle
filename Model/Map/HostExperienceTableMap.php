<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\HostExperience;
use IiMedias\StreamBundle\Model\HostExperienceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_host_experience_sthexp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class HostExperienceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.HostExperienceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_host_experience_sthexp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\HostExperience';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.HostExperience';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the sthexp_id field
     */
    const COL_STHEXP_ID = 'stream_host_experience_sthexp.sthexp_id';

    /**
     * the column name for the sthexp_ststrm_id field
     */
    const COL_STHEXP_STSTRM_ID = 'stream_host_experience_sthexp.sthexp_ststrm_id';

    /**
     * the column name for the sthexp_stchan_id field
     */
    const COL_STHEXP_STCHAN_ID = 'stream_host_experience_sthexp.sthexp_stchan_id';

    /**
     * the column name for the sthexp_stsite_id field
     */
    const COL_STHEXP_STSITE_ID = 'stream_host_experience_sthexp.sthexp_stsite_id';

    /**
     * the column name for the sthexp_stcusr_id field
     */
    const COL_STHEXP_STCUSR_ID = 'stream_host_experience_sthexp.sthexp_stcusr_id';

    /**
     * the column name for the sthexp_stuexp_id field
     */
    const COL_STHEXP_STUEXP_ID = 'stream_host_experience_sthexp.sthexp_stuexp_id';

    /**
     * the column name for the sthexp_exp field
     */
    const COL_STHEXP_EXP = 'stream_host_experience_sthexp.sthexp_exp';

    /**
     * the column name for the sthxpr_at field
     */
    const COL_STHXPR_AT = 'stream_host_experience_sthexp.sthxpr_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'ChannelId', 'SiteId', 'ChatUserId', 'UserExperienceId', 'Exp', 'At', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'channelId', 'siteId', 'chatUserId', 'userExperienceId', 'exp', 'at', ),
        self::TYPE_COLNAME       => array(HostExperienceTableMap::COL_STHEXP_ID, HostExperienceTableMap::COL_STHEXP_STSTRM_ID, HostExperienceTableMap::COL_STHEXP_STCHAN_ID, HostExperienceTableMap::COL_STHEXP_STSITE_ID, HostExperienceTableMap::COL_STHEXP_STCUSR_ID, HostExperienceTableMap::COL_STHEXP_STUEXP_ID, HostExperienceTableMap::COL_STHEXP_EXP, HostExperienceTableMap::COL_STHXPR_AT, ),
        self::TYPE_FIELDNAME     => array('sthexp_id', 'sthexp_ststrm_id', 'sthexp_stchan_id', 'sthexp_stsite_id', 'sthexp_stcusr_id', 'sthexp_stuexp_id', 'sthexp_exp', 'sthxpr_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'ChannelId' => 2, 'SiteId' => 3, 'ChatUserId' => 4, 'UserExperienceId' => 5, 'Exp' => 6, 'At' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'channelId' => 2, 'siteId' => 3, 'chatUserId' => 4, 'userExperienceId' => 5, 'exp' => 6, 'at' => 7, ),
        self::TYPE_COLNAME       => array(HostExperienceTableMap::COL_STHEXP_ID => 0, HostExperienceTableMap::COL_STHEXP_STSTRM_ID => 1, HostExperienceTableMap::COL_STHEXP_STCHAN_ID => 2, HostExperienceTableMap::COL_STHEXP_STSITE_ID => 3, HostExperienceTableMap::COL_STHEXP_STCUSR_ID => 4, HostExperienceTableMap::COL_STHEXP_STUEXP_ID => 5, HostExperienceTableMap::COL_STHEXP_EXP => 6, HostExperienceTableMap::COL_STHXPR_AT => 7, ),
        self::TYPE_FIELDNAME     => array('sthexp_id' => 0, 'sthexp_ststrm_id' => 1, 'sthexp_stchan_id' => 2, 'sthexp_stsite_id' => 3, 'sthexp_stcusr_id' => 4, 'sthexp_stuexp_id' => 5, 'sthexp_exp' => 6, 'sthxpr_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_host_experience_sthexp');
        $this->setPhpName('HostExperience');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\HostExperience');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('sthexp_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('sthexp_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('sthexp_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('sthexp_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addForeignKey('sthexp_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addForeignKey('sthexp_stuexp_id', 'UserExperienceId', 'INTEGER', 'stream_user_experience_stuexp', 'stuexp_id', false, null, null);
        $this->addColumn('sthexp_exp', 'Exp', 'INTEGER', true, null, null);
        $this->addColumn('sthxpr_at', 'At', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sthexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sthexp_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sthexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sthexp_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sthexp_stuexp_id',
    1 => ':stuexp_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? HostExperienceTableMap::CLASS_DEFAULT : HostExperienceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (HostExperience object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = HostExperienceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = HostExperienceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + HostExperienceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = HostExperienceTableMap::OM_CLASS;
            /** @var HostExperience $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            HostExperienceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = HostExperienceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = HostExperienceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var HostExperience $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                HostExperienceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_STSTRM_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_STCHAN_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_STSITE_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_STCUSR_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_STUEXP_ID);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHEXP_EXP);
            $criteria->addSelectColumn(HostExperienceTableMap::COL_STHXPR_AT);
        } else {
            $criteria->addSelectColumn($alias . '.sthexp_id');
            $criteria->addSelectColumn($alias . '.sthexp_ststrm_id');
            $criteria->addSelectColumn($alias . '.sthexp_stchan_id');
            $criteria->addSelectColumn($alias . '.sthexp_stsite_id');
            $criteria->addSelectColumn($alias . '.sthexp_stcusr_id');
            $criteria->addSelectColumn($alias . '.sthexp_stuexp_id');
            $criteria->addSelectColumn($alias . '.sthexp_exp');
            $criteria->addSelectColumn($alias . '.sthxpr_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(HostExperienceTableMap::DATABASE_NAME)->getTable(HostExperienceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(HostExperienceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(HostExperienceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new HostExperienceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a HostExperience or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or HostExperience object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(HostExperienceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\HostExperience) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(HostExperienceTableMap::DATABASE_NAME);
            $criteria->add(HostExperienceTableMap::COL_STHEXP_ID, (array) $values, Criteria::IN);
        }

        $query = HostExperienceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            HostExperienceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                HostExperienceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_host_experience_sthexp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return HostExperienceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a HostExperience or Criteria object.
     *
     * @param mixed               $criteria Criteria or HostExperience object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(HostExperienceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from HostExperience object
        }

        if ($criteria->containsKey(HostExperienceTableMap::COL_STHEXP_ID) && $criteria->keyContainsValue(HostExperienceTableMap::COL_STHEXP_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.HostExperienceTableMap::COL_STHEXP_ID.')');
        }


        // Set the correct dbName
        $query = HostExperienceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // HostExperienceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
HostExperienceTableMap::buildTableMap();

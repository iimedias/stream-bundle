<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Rank;
use IiMedias\StreamBundle\Model\RankQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_rank_strank' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RankTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.RankTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_rank_strank';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Rank';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Rank';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the strank_id field
     */
    const COL_STRANK_ID = 'stream_rank_strank.strank_id';

    /**
     * the column name for the strank_ststrm_id field
     */
    const COL_STRANK_STSTRM_ID = 'stream_rank_strank.strank_ststrm_id';

    /**
     * the column name for the strank_name field
     */
    const COL_STRANK_NAME = 'stream_rank_strank.strank_name';

    /**
     * the column name for the strank_is_default field
     */
    const COL_STRANK_IS_DEFAULT = 'stream_rank_strank.strank_is_default';

    /**
     * the column name for the strank_set_if_follow_event field
     */
    const COL_STRANK_SET_IF_FOLLOW_EVENT = 'stream_rank_strank.strank_set_if_follow_event';

    /**
     * the column name for the strank_set_if_moderator field
     */
    const COL_STRANK_SET_IF_MODERATOR = 'stream_rank_strank.strank_set_if_moderator';

    /**
     * the column name for the strank_set_if_streamer field
     */
    const COL_STRANK_SET_IF_STREAMER = 'stream_rank_strank.strank_set_if_streamer';

    /**
     * the column name for the strank_dependency_rank_ids field
     */
    const COL_STRANK_DEPENDENCY_RANK_IDS = 'stream_rank_strank.strank_dependency_rank_ids';

    /**
     * the column name for the strank_created_by_user_id field
     */
    const COL_STRANK_CREATED_BY_USER_ID = 'stream_rank_strank.strank_created_by_user_id';

    /**
     * the column name for the strank_updated_by_user_id field
     */
    const COL_STRANK_UPDATED_BY_USER_ID = 'stream_rank_strank.strank_updated_by_user_id';

    /**
     * the column name for the strank_created_at field
     */
    const COL_STRANK_CREATED_AT = 'stream_rank_strank.strank_created_at';

    /**
     * the column name for the strank_updated_at field
     */
    const COL_STRANK_UPDATED_AT = 'stream_rank_strank.strank_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'Name', 'IsDefault', 'SetIfFollowEvent', 'SetIfModerator', 'SetIfStreamer', 'DependencyRankIds', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'name', 'isDefault', 'setIfFollowEvent', 'setIfModerator', 'setIfStreamer', 'dependencyRankIds', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(RankTableMap::COL_STRANK_ID, RankTableMap::COL_STRANK_STSTRM_ID, RankTableMap::COL_STRANK_NAME, RankTableMap::COL_STRANK_IS_DEFAULT, RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT, RankTableMap::COL_STRANK_SET_IF_MODERATOR, RankTableMap::COL_STRANK_SET_IF_STREAMER, RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS, RankTableMap::COL_STRANK_CREATED_BY_USER_ID, RankTableMap::COL_STRANK_UPDATED_BY_USER_ID, RankTableMap::COL_STRANK_CREATED_AT, RankTableMap::COL_STRANK_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('strank_id', 'strank_ststrm_id', 'strank_name', 'strank_is_default', 'strank_set_if_follow_event', 'strank_set_if_moderator', 'strank_set_if_streamer', 'strank_dependency_rank_ids', 'strank_created_by_user_id', 'strank_updated_by_user_id', 'strank_created_at', 'strank_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'Name' => 2, 'IsDefault' => 3, 'SetIfFollowEvent' => 4, 'SetIfModerator' => 5, 'SetIfStreamer' => 6, 'DependencyRankIds' => 7, 'CreatedByUserId' => 8, 'UpdatedByUserId' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'name' => 2, 'isDefault' => 3, 'setIfFollowEvent' => 4, 'setIfModerator' => 5, 'setIfStreamer' => 6, 'dependencyRankIds' => 7, 'createdByUserId' => 8, 'updatedByUserId' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(RankTableMap::COL_STRANK_ID => 0, RankTableMap::COL_STRANK_STSTRM_ID => 1, RankTableMap::COL_STRANK_NAME => 2, RankTableMap::COL_STRANK_IS_DEFAULT => 3, RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT => 4, RankTableMap::COL_STRANK_SET_IF_MODERATOR => 5, RankTableMap::COL_STRANK_SET_IF_STREAMER => 6, RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS => 7, RankTableMap::COL_STRANK_CREATED_BY_USER_ID => 8, RankTableMap::COL_STRANK_UPDATED_BY_USER_ID => 9, RankTableMap::COL_STRANK_CREATED_AT => 10, RankTableMap::COL_STRANK_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('strank_id' => 0, 'strank_ststrm_id' => 1, 'strank_name' => 2, 'strank_is_default' => 3, 'strank_set_if_follow_event' => 4, 'strank_set_if_moderator' => 5, 'strank_set_if_streamer' => 6, 'strank_dependency_rank_ids' => 7, 'strank_created_by_user_id' => 8, 'strank_updated_by_user_id' => 9, 'strank_created_at' => 10, 'strank_updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_rank_strank');
        $this->setPhpName('Rank');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Rank');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('strank_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('strank_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addColumn('strank_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('strank_is_default', 'IsDefault', 'BOOLEAN', true, 1, false);
        $this->addColumn('strank_set_if_follow_event', 'SetIfFollowEvent', 'BOOLEAN', true, 1, false);
        $this->addColumn('strank_set_if_moderator', 'SetIfModerator', 'BOOLEAN', true, 1, false);
        $this->addColumn('strank_set_if_streamer', 'SetIfStreamer', 'BOOLEAN', true, 1, false);
        $this->addColumn('strank_dependency_rank_ids', 'DependencyRankIds', 'ARRAY', false, null, null);
        $this->addForeignKey('strank_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('strank_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('strank_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('strank_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':strank_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':strank_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':strank_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('AvatarByMinRank', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stavtr_min_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', 'AvatarByMinRanks', false);
        $this->addRelation('AvatarByAssociatedRank', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stavtr_associated_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', 'AvatarByAssociatedRanks', false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'strank_created_at', 'update_column' => 'strank_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_rank_strank     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        AvatarTableMap::clearInstancePool();
        UserExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RankTableMap::CLASS_DEFAULT : RankTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Rank object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RankTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RankTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RankTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RankTableMap::OM_CLASS;
            /** @var Rank $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RankTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RankTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RankTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Rank $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RankTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_ID);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_STSTRM_ID);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_NAME);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_IS_DEFAULT);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_SET_IF_FOLLOW_EVENT);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_SET_IF_MODERATOR);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_SET_IF_STREAMER);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_DEPENDENCY_RANK_IDS);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_CREATED_AT);
            $criteria->addSelectColumn(RankTableMap::COL_STRANK_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.strank_id');
            $criteria->addSelectColumn($alias . '.strank_ststrm_id');
            $criteria->addSelectColumn($alias . '.strank_name');
            $criteria->addSelectColumn($alias . '.strank_is_default');
            $criteria->addSelectColumn($alias . '.strank_set_if_follow_event');
            $criteria->addSelectColumn($alias . '.strank_set_if_moderator');
            $criteria->addSelectColumn($alias . '.strank_set_if_streamer');
            $criteria->addSelectColumn($alias . '.strank_dependency_rank_ids');
            $criteria->addSelectColumn($alias . '.strank_created_by_user_id');
            $criteria->addSelectColumn($alias . '.strank_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.strank_created_at');
            $criteria->addSelectColumn($alias . '.strank_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RankTableMap::DATABASE_NAME)->getTable(RankTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RankTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RankTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RankTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Rank or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Rank object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Rank) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RankTableMap::DATABASE_NAME);
            $criteria->add(RankTableMap::COL_STRANK_ID, (array) $values, Criteria::IN);
        }

        $query = RankQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RankTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RankTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_rank_strank table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RankQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Rank or Criteria object.
     *
     * @param mixed               $criteria Criteria or Rank object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RankTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Rank object
        }

        if ($criteria->containsKey(RankTableMap::COL_STRANK_ID) && $criteria->keyContainsValue(RankTableMap::COL_STRANK_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RankTableMap::COL_STRANK_ID.')');
        }


        // Set the correct dbName
        $query = RankQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RankTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RankTableMap::buildTableMap();

<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Stream;
use IiMedias\StreamBundle\Model\StreamQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_stream_ststrm' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StreamTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.StreamTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_stream_ststrm';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Stream';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Stream';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the ststrm_id field
     */
    const COL_STSTRM_ID = 'stream_stream_ststrm.ststrm_id';

    /**
     * the column name for the ststrm_type field
     */
    const COL_STSTRM_TYPE = 'stream_stream_ststrm.ststrm_type';

    /**
     * the column name for the ststrm_name field
     */
    const COL_STSTRM_NAME = 'stream_stream_ststrm.ststrm_name';

    /**
     * the column name for the ststrm_can_scan field
     */
    const COL_STSTRM_CAN_SCAN = 'stream_stream_ststrm.ststrm_can_scan';

    /**
     * the column name for the ststrm_created_by_user_id field
     */
    const COL_STSTRM_CREATED_BY_USER_ID = 'stream_stream_ststrm.ststrm_created_by_user_id';

    /**
     * the column name for the ststrm_updated_by_user_id field
     */
    const COL_STSTRM_UPDATED_BY_USER_ID = 'stream_stream_ststrm.ststrm_updated_by_user_id';

    /**
     * the column name for the ststrm_created_at field
     */
    const COL_STSTRM_CREATED_AT = 'stream_stream_ststrm.ststrm_created_at';

    /**
     * the column name for the ststrm_updated_at field
     */
    const COL_STSTRM_UPDATED_AT = 'stream_stream_ststrm.ststrm_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the ststrm_type field */
    const COL_STSTRM_TYPE_STREAM = 'stream';
    const COL_STSTRM_TYPE_BOT = 'bot';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Type', 'Name', 'CanScan', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'type', 'name', 'canScan', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(StreamTableMap::COL_STSTRM_ID, StreamTableMap::COL_STSTRM_TYPE, StreamTableMap::COL_STSTRM_NAME, StreamTableMap::COL_STSTRM_CAN_SCAN, StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID, StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID, StreamTableMap::COL_STSTRM_CREATED_AT, StreamTableMap::COL_STSTRM_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('ststrm_id', 'ststrm_type', 'ststrm_name', 'ststrm_can_scan', 'ststrm_created_by_user_id', 'ststrm_updated_by_user_id', 'ststrm_created_at', 'ststrm_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Type' => 1, 'Name' => 2, 'CanScan' => 3, 'CreatedByUserId' => 4, 'UpdatedByUserId' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'type' => 1, 'name' => 2, 'canScan' => 3, 'createdByUserId' => 4, 'updatedByUserId' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(StreamTableMap::COL_STSTRM_ID => 0, StreamTableMap::COL_STSTRM_TYPE => 1, StreamTableMap::COL_STSTRM_NAME => 2, StreamTableMap::COL_STSTRM_CAN_SCAN => 3, StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID => 4, StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID => 5, StreamTableMap::COL_STSTRM_CREATED_AT => 6, StreamTableMap::COL_STSTRM_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('ststrm_id' => 0, 'ststrm_type' => 1, 'ststrm_name' => 2, 'ststrm_can_scan' => 3, 'ststrm_created_by_user_id' => 4, 'ststrm_updated_by_user_id' => 5, 'ststrm_created_at' => 6, 'ststrm_updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                StreamTableMap::COL_STSTRM_TYPE => array(
                            self::COL_STSTRM_TYPE_STREAM,
            self::COL_STSTRM_TYPE_BOT,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_stream_ststrm');
        $this->setPhpName('Stream');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Stream');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ststrm_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ststrm_type', 'Type', 'ENUM', true, null, 'stream');
        $this->getColumn('ststrm_type')->setValueSet(array (
  0 => 'stream',
  1 => 'bot',
));
        $this->addColumn('ststrm_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('ststrm_can_scan', 'CanScan', 'BOOLEAN', true, 1, false);
        $this->addForeignKey('ststrm_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('ststrm_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('ststrm_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('ststrm_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ststrm_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ststrm_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stchan_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'Channels', false);
        $this->addRelation('ChannelBot', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stchan_ststrm_bot_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'ChannelBots', false);
        $this->addRelation('Rank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':strank_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'Ranks', false);
        $this->addRelation('Avatar', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stavtr_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'Avatars', false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
        $this->addRelation('DeepBotImportExperience', '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stdbie_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'DeepBotImportExperiences', false);
        $this->addRelation('MessageExperience', '\\IiMedias\\StreamBundle\\Model\\MessageExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stmexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'MessageExperiences', false);
        $this->addRelation('ChatterExperience', '\\IiMedias\\StreamBundle\\Model\\ChatterExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatterExperiences', false);
        $this->addRelation('FollowExperience', '\\IiMedias\\StreamBundle\\Model\\FollowExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowExperiences', false);
        $this->addRelation('HostExperience', '\\IiMedias\\StreamBundle\\Model\\HostExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sthexp_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'HostExperiences', false);
        $this->addRelation('ViewDiffData', '\\IiMedias\\StreamBundle\\Model\\ViewDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stddat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewDiffDatas', false);
        $this->addRelation('ViewerData', '\\IiMedias\\StreamBundle\\Model\\ViewerData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stvdat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewerDatas', false);
        $this->addRelation('FollowDiffData', '\\IiMedias\\StreamBundle\\Model\\FollowDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfdat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowDiffDatas', false);
        $this->addRelation('StatusData', '\\IiMedias\\StreamBundle\\Model\\StatusData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stsdat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'StatusDatas', false);
        $this->addRelation('TypeData', '\\IiMedias\\StreamBundle\\Model\\TypeData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sttdat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'TypeDatas', false);
        $this->addRelation('GameData', '\\IiMedias\\StreamBundle\\Model\\GameData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stgdat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'GameDatas', false);
        $this->addRelation('Stat', '\\IiMedias\\StreamBundle\\Model\\Stat', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ststat_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'Stats', false);
        $this->addRelation('Experience', '\\IiMedias\\StreamBundle\\Model\\Experience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stexpr_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', 'Experiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'ststrm_created_at', 'update_column' => 'ststrm_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_stream_ststrm     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ChannelTableMap::clearInstancePool();
        RankTableMap::clearInstancePool();
        AvatarTableMap::clearInstancePool();
        UserExperienceTableMap::clearInstancePool();
        DeepBotImportExperienceTableMap::clearInstancePool();
        MessageExperienceTableMap::clearInstancePool();
        ChatterExperienceTableMap::clearInstancePool();
        FollowExperienceTableMap::clearInstancePool();
        HostExperienceTableMap::clearInstancePool();
        ViewDiffDataTableMap::clearInstancePool();
        ViewerDataTableMap::clearInstancePool();
        FollowDiffDataTableMap::clearInstancePool();
        StatusDataTableMap::clearInstancePool();
        TypeDataTableMap::clearInstancePool();
        GameDataTableMap::clearInstancePool();
        StatTableMap::clearInstancePool();
        ExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StreamTableMap::CLASS_DEFAULT : StreamTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Stream object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StreamTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StreamTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StreamTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StreamTableMap::OM_CLASS;
            /** @var Stream $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StreamTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StreamTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StreamTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Stream $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StreamTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_ID);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_TYPE);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_NAME);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_CAN_SCAN);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_CREATED_AT);
            $criteria->addSelectColumn(StreamTableMap::COL_STSTRM_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.ststrm_id');
            $criteria->addSelectColumn($alias . '.ststrm_type');
            $criteria->addSelectColumn($alias . '.ststrm_name');
            $criteria->addSelectColumn($alias . '.ststrm_can_scan');
            $criteria->addSelectColumn($alias . '.ststrm_created_by_user_id');
            $criteria->addSelectColumn($alias . '.ststrm_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.ststrm_created_at');
            $criteria->addSelectColumn($alias . '.ststrm_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StreamTableMap::DATABASE_NAME)->getTable(StreamTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StreamTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StreamTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StreamTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Stream or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Stream object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreamTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Stream) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StreamTableMap::DATABASE_NAME);
            $criteria->add(StreamTableMap::COL_STSTRM_ID, (array) $values, Criteria::IN);
        }

        $query = StreamQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StreamTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StreamTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_stream_ststrm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StreamQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Stream or Criteria object.
     *
     * @param mixed               $criteria Criteria or Stream object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreamTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Stream object
        }

        if ($criteria->containsKey(StreamTableMap::COL_STSTRM_ID) && $criteria->keyContainsValue(StreamTableMap::COL_STSTRM_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.StreamTableMap::COL_STSTRM_ID.')');
        }


        // Set the correct dbName
        $query = StreamQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StreamTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StreamTableMap::buildTableMap();

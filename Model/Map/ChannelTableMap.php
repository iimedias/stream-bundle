<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_channel_stchan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChannelTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ChannelTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_channel_stchan';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\Channel';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.Channel';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 31;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 31;

    /**
     * the column name for the stchan_id field
     */
    const COL_STCHAN_ID = 'stream_channel_stchan.stchan_id';

    /**
     * the column name for the stchan_ststrm_id field
     */
    const COL_STCHAN_STSTRM_ID = 'stream_channel_stchan.stchan_ststrm_id';

    /**
     * the column name for the stchan_stsite_id field
     */
    const COL_STCHAN_STSITE_ID = 'stream_channel_stchan.stchan_stsite_id';

    /**
     * the column name for the stchan_channel field
     */
    const COL_STCHAN_CHANNEL = 'stream_channel_stchan.stchan_channel';

    /**
     * the column name for the stchan_ststrm_bot_id field
     */
    const COL_STCHAN_STSTRM_BOT_ID = 'stream_channel_stchan.stchan_ststrm_bot_id';

    /**
     * the column name for the stchan_can_scan field
     */
    const COL_STCHAN_CAN_SCAN = 'stream_channel_stchan.stchan_can_scan';

    /**
     * the column name for the stchan_password_oauth field
     */
    const COL_STCHAN_PASSWORD_OAUTH = 'stream_channel_stchan.stchan_password_oauth';

    /**
     * the column name for the stchan_channel_id field
     */
    const COL_STCHAN_CHANNEL_ID = 'stream_channel_stchan.stchan_channel_id';

    /**
     * the column name for the stchan_client_id field
     */
    const COL_STCHAN_CLIENT_ID = 'stream_channel_stchan.stchan_client_id';

    /**
     * the column name for the stchan_secred_id field
     */
    const COL_STCHAN_SECRED_ID = 'stream_channel_stchan.stchan_secred_id';

    /**
     * the column name for the stchan_channel_partner field
     */
    const COL_STCHAN_CHANNEL_PARTNER = 'stream_channel_stchan.stchan_channel_partner';

    /**
     * the column name for the stchan_live_mode field
     */
    const COL_STCHAN_LIVE_MODE = 'stream_channel_stchan.stchan_live_mode';

    /**
     * the column name for the stchan_status field
     */
    const COL_STCHAN_STATUS = 'stream_channel_stchan.stchan_status';

    /**
     * the column name for the stchan_vgatga_id field
     */
    const COL_STCHAN_VGATGA_ID = 'stream_channel_stchan.stchan_vgatga_id';

    /**
     * the column name for the stchan_game field
     */
    const COL_STCHAN_GAME = 'stream_channel_stchan.stchan_game';

    /**
     * the column name for the stchan_stream_type field
     */
    const COL_STCHAN_STREAM_TYPE = 'stream_channel_stchan.stchan_stream_type';

    /**
     * the column name for the stchan_followers_count field
     */
    const COL_STCHAN_FOLLOWERS_COUNT = 'stream_channel_stchan.stchan_followers_count';

    /**
     * the column name for the stchan_followers_diff field
     */
    const COL_STCHAN_FOLLOWERS_DIFF = 'stream_channel_stchan.stchan_followers_diff';

    /**
     * the column name for the stchan_views_count field
     */
    const COL_STCHAN_VIEWS_COUNT = 'stream_channel_stchan.stchan_views_count';

    /**
     * the column name for the stchan_views_diff field
     */
    const COL_STCHAN_VIEWS_DIFF = 'stream_channel_stchan.stchan_views_diff';

    /**
     * the column name for the stchan_viewers_count field
     */
    const COL_STCHAN_VIEWERS_COUNT = 'stream_channel_stchan.stchan_viewers_count';

    /**
     * the column name for the stchan_chatters_count field
     */
    const COL_STCHAN_CHATTERS_COUNT = 'stream_channel_stchan.stchan_chatters_count';

    /**
     * the column name for the stchan_hosts_count field
     */
    const COL_STCHAN_HOSTS_COUNT = 'stream_channel_stchan.stchan_hosts_count';

    /**
     * the column name for the stchan_live_viewers_count field
     */
    const COL_STCHAN_LIVE_VIEWERS_COUNT = 'stream_channel_stchan.stchan_live_viewers_count';

    /**
     * the column name for the stchan_messages_count field
     */
    const COL_STCHAN_MESSAGES_COUNT = 'stream_channel_stchan.stchan_messages_count';

    /**
     * the column name for the stchan_lock_chatters_scan field
     */
    const COL_STCHAN_LOCK_CHATTERS_SCAN = 'stream_channel_stchan.stchan_lock_chatters_scan';

    /**
     * the column name for the stchan_lock_hosts_scan field
     */
    const COL_STCHAN_LOCK_HOSTS_SCAN = 'stream_channel_stchan.stchan_lock_hosts_scan';

    /**
     * the column name for the stchan_created_by_user_id field
     */
    const COL_STCHAN_CREATED_BY_USER_ID = 'stream_channel_stchan.stchan_created_by_user_id';

    /**
     * the column name for the stchan_updated_by_user_id field
     */
    const COL_STCHAN_UPDATED_BY_USER_ID = 'stream_channel_stchan.stchan_updated_by_user_id';

    /**
     * the column name for the stchan_created_at field
     */
    const COL_STCHAN_CREATED_AT = 'stream_channel_stchan.stchan_created_at';

    /**
     * the column name for the stchan_updated_at field
     */
    const COL_STCHAN_UPDATED_AT = 'stream_channel_stchan.stchan_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the stchan_live_mode field */
    const COL_STCHAN_LIVE_MODE_OFFLINE = 'offline';
    const COL_STCHAN_LIVE_MODE_ONLINE = 'online';
    const COL_STCHAN_LIVE_MODE_VOD = 'vod';

    /** The enumerated values for the stchan_stream_type field */
    const COL_STCHAN_STREAM_TYPE_OFFLINE = 'offline';
    const COL_STCHAN_STREAM_TYPE_LIVE = 'live';
    const COL_STCHAN_STREAM_TYPE_VOD = 'vod';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'StreamId', 'SiteId', 'Channel', 'BotId', 'CanScan', 'PasswordOauth', 'ChannelId', 'ClientId', 'SecretId', 'ChannelPartner', 'LiveMode', 'Status', 'TwitchGameId', 'Game', 'StreamType', 'FollowersCount', 'FollowersDiff', 'ViewsCount', 'ViewsDiff', 'ViewersCount', 'ChattersCount', 'HostsCount', 'LiveViewersCount', 'MessagesCount', 'LockChattersScan', 'LockHostsScan', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'streamId', 'siteId', 'channel', 'botId', 'canScan', 'passwordOauth', 'channelId', 'clientId', 'secretId', 'channelPartner', 'liveMode', 'status', 'twitchGameId', 'game', 'streamType', 'followersCount', 'followersDiff', 'viewsCount', 'viewsDiff', 'viewersCount', 'chattersCount', 'hostsCount', 'liveViewersCount', 'messagesCount', 'lockChattersScan', 'lockHostsScan', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ChannelTableMap::COL_STCHAN_ID, ChannelTableMap::COL_STCHAN_STSTRM_ID, ChannelTableMap::COL_STCHAN_STSITE_ID, ChannelTableMap::COL_STCHAN_CHANNEL, ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID, ChannelTableMap::COL_STCHAN_CAN_SCAN, ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH, ChannelTableMap::COL_STCHAN_CHANNEL_ID, ChannelTableMap::COL_STCHAN_CLIENT_ID, ChannelTableMap::COL_STCHAN_SECRED_ID, ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER, ChannelTableMap::COL_STCHAN_LIVE_MODE, ChannelTableMap::COL_STCHAN_STATUS, ChannelTableMap::COL_STCHAN_VGATGA_ID, ChannelTableMap::COL_STCHAN_GAME, ChannelTableMap::COL_STCHAN_STREAM_TYPE, ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT, ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF, ChannelTableMap::COL_STCHAN_VIEWS_COUNT, ChannelTableMap::COL_STCHAN_VIEWS_DIFF, ChannelTableMap::COL_STCHAN_VIEWERS_COUNT, ChannelTableMap::COL_STCHAN_CHATTERS_COUNT, ChannelTableMap::COL_STCHAN_HOSTS_COUNT, ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT, ChannelTableMap::COL_STCHAN_MESSAGES_COUNT, ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN, ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN, ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID, ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID, ChannelTableMap::COL_STCHAN_CREATED_AT, ChannelTableMap::COL_STCHAN_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('stchan_id', 'stchan_ststrm_id', 'stchan_stsite_id', 'stchan_channel', 'stchan_ststrm_bot_id', 'stchan_can_scan', 'stchan_password_oauth', 'stchan_channel_id', 'stchan_client_id', 'stchan_secred_id', 'stchan_channel_partner', 'stchan_live_mode', 'stchan_status', 'stchan_vgatga_id', 'stchan_game', 'stchan_stream_type', 'stchan_followers_count', 'stchan_followers_diff', 'stchan_views_count', 'stchan_views_diff', 'stchan_viewers_count', 'stchan_chatters_count', 'stchan_hosts_count', 'stchan_live_viewers_count', 'stchan_messages_count', 'stchan_lock_chatters_scan', 'stchan_lock_hosts_scan', 'stchan_created_by_user_id', 'stchan_updated_by_user_id', 'stchan_created_at', 'stchan_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'StreamId' => 1, 'SiteId' => 2, 'Channel' => 3, 'BotId' => 4, 'CanScan' => 5, 'PasswordOauth' => 6, 'ChannelId' => 7, 'ClientId' => 8, 'SecretId' => 9, 'ChannelPartner' => 10, 'LiveMode' => 11, 'Status' => 12, 'TwitchGameId' => 13, 'Game' => 14, 'StreamType' => 15, 'FollowersCount' => 16, 'FollowersDiff' => 17, 'ViewsCount' => 18, 'ViewsDiff' => 19, 'ViewersCount' => 20, 'ChattersCount' => 21, 'HostsCount' => 22, 'LiveViewersCount' => 23, 'MessagesCount' => 24, 'LockChattersScan' => 25, 'LockHostsScan' => 26, 'CreatedByUserId' => 27, 'UpdatedByUserId' => 28, 'CreatedAt' => 29, 'UpdatedAt' => 30, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'streamId' => 1, 'siteId' => 2, 'channel' => 3, 'botId' => 4, 'canScan' => 5, 'passwordOauth' => 6, 'channelId' => 7, 'clientId' => 8, 'secretId' => 9, 'channelPartner' => 10, 'liveMode' => 11, 'status' => 12, 'twitchGameId' => 13, 'game' => 14, 'streamType' => 15, 'followersCount' => 16, 'followersDiff' => 17, 'viewsCount' => 18, 'viewsDiff' => 19, 'viewersCount' => 20, 'chattersCount' => 21, 'hostsCount' => 22, 'liveViewersCount' => 23, 'messagesCount' => 24, 'lockChattersScan' => 25, 'lockHostsScan' => 26, 'createdByUserId' => 27, 'updatedByUserId' => 28, 'createdAt' => 29, 'updatedAt' => 30, ),
        self::TYPE_COLNAME       => array(ChannelTableMap::COL_STCHAN_ID => 0, ChannelTableMap::COL_STCHAN_STSTRM_ID => 1, ChannelTableMap::COL_STCHAN_STSITE_ID => 2, ChannelTableMap::COL_STCHAN_CHANNEL => 3, ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID => 4, ChannelTableMap::COL_STCHAN_CAN_SCAN => 5, ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH => 6, ChannelTableMap::COL_STCHAN_CHANNEL_ID => 7, ChannelTableMap::COL_STCHAN_CLIENT_ID => 8, ChannelTableMap::COL_STCHAN_SECRED_ID => 9, ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER => 10, ChannelTableMap::COL_STCHAN_LIVE_MODE => 11, ChannelTableMap::COL_STCHAN_STATUS => 12, ChannelTableMap::COL_STCHAN_VGATGA_ID => 13, ChannelTableMap::COL_STCHAN_GAME => 14, ChannelTableMap::COL_STCHAN_STREAM_TYPE => 15, ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT => 16, ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF => 17, ChannelTableMap::COL_STCHAN_VIEWS_COUNT => 18, ChannelTableMap::COL_STCHAN_VIEWS_DIFF => 19, ChannelTableMap::COL_STCHAN_VIEWERS_COUNT => 20, ChannelTableMap::COL_STCHAN_CHATTERS_COUNT => 21, ChannelTableMap::COL_STCHAN_HOSTS_COUNT => 22, ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT => 23, ChannelTableMap::COL_STCHAN_MESSAGES_COUNT => 24, ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN => 25, ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN => 26, ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID => 27, ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID => 28, ChannelTableMap::COL_STCHAN_CREATED_AT => 29, ChannelTableMap::COL_STCHAN_UPDATED_AT => 30, ),
        self::TYPE_FIELDNAME     => array('stchan_id' => 0, 'stchan_ststrm_id' => 1, 'stchan_stsite_id' => 2, 'stchan_channel' => 3, 'stchan_ststrm_bot_id' => 4, 'stchan_can_scan' => 5, 'stchan_password_oauth' => 6, 'stchan_channel_id' => 7, 'stchan_client_id' => 8, 'stchan_secred_id' => 9, 'stchan_channel_partner' => 10, 'stchan_live_mode' => 11, 'stchan_status' => 12, 'stchan_vgatga_id' => 13, 'stchan_game' => 14, 'stchan_stream_type' => 15, 'stchan_followers_count' => 16, 'stchan_followers_diff' => 17, 'stchan_views_count' => 18, 'stchan_views_diff' => 19, 'stchan_viewers_count' => 20, 'stchan_chatters_count' => 21, 'stchan_hosts_count' => 22, 'stchan_live_viewers_count' => 23, 'stchan_messages_count' => 24, 'stchan_lock_chatters_scan' => 25, 'stchan_lock_hosts_scan' => 26, 'stchan_created_by_user_id' => 27, 'stchan_updated_by_user_id' => 28, 'stchan_created_at' => 29, 'stchan_updated_at' => 30, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                ChannelTableMap::COL_STCHAN_LIVE_MODE => array(
                            self::COL_STCHAN_LIVE_MODE_OFFLINE,
            self::COL_STCHAN_LIVE_MODE_ONLINE,
            self::COL_STCHAN_LIVE_MODE_VOD,
        ),
                ChannelTableMap::COL_STCHAN_STREAM_TYPE => array(
                            self::COL_STCHAN_STREAM_TYPE_OFFLINE,
            self::COL_STCHAN_STREAM_TYPE_LIVE,
            self::COL_STCHAN_STREAM_TYPE_VOD,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_channel_stchan');
        $this->setPhpName('Channel');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\Channel');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stchan_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('stchan_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stchan_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addColumn('stchan_channel', 'Channel', 'VARCHAR', true, 255, null);
        $this->addForeignKey('stchan_ststrm_bot_id', 'BotId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', false, null, null);
        $this->addColumn('stchan_can_scan', 'CanScan', 'BOOLEAN', true, 1, false);
        $this->addColumn('stchan_password_oauth', 'PasswordOauth', 'VARCHAR', false, 255, null);
        $this->addColumn('stchan_channel_id', 'ChannelId', 'INTEGER', false, null, null);
        $this->addColumn('stchan_client_id', 'ClientId', 'VARCHAR', false, 255, null);
        $this->addColumn('stchan_secred_id', 'SecretId', 'VARCHAR', false, 255, null);
        $this->addColumn('stchan_channel_partner', 'ChannelPartner', 'BOOLEAN', false, 1, null);
        $this->addColumn('stchan_live_mode', 'LiveMode', 'ENUM', true, null, 'offline');
        $this->getColumn('stchan_live_mode')->setValueSet(array (
  0 => 'offline',
  1 => 'online',
  2 => 'vod',
));
        $this->addColumn('stchan_status', 'Status', 'VARCHAR', false, 255, null);
        $this->addForeignKey('stchan_vgatga_id', 'TwitchGameId', 'INTEGER', 'videogames_api_twitch_vgatga', 'vgatga_id', false, null, null);
        $this->addColumn('stchan_game', 'Game', 'VARCHAR', false, 255, null);
        $this->addColumn('stchan_stream_type', 'StreamType', 'ENUM', true, null, 'offline');
        $this->getColumn('stchan_stream_type')->setValueSet(array (
  0 => 'offline',
  1 => 'live',
  2 => 'vod',
));
        $this->addColumn('stchan_followers_count', 'FollowersCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_followers_diff', 'FollowersDiff', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_views_count', 'ViewsCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_views_diff', 'ViewsDiff', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_viewers_count', 'ViewersCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_chatters_count', 'ChattersCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_hosts_count', 'HostsCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_live_viewers_count', 'LiveViewersCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_messages_count', 'MessagesCount', 'INTEGER', true, null, 0);
        $this->addColumn('stchan_lock_chatters_scan', 'LockChattersScan', 'BOOLEAN', true, 1, false);
        $this->addColumn('stchan_lock_hosts_scan', 'LockHostsScan', 'BOOLEAN', true, 1, false);
        $this->addForeignKey('stchan_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('stchan_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('stchan_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('stchan_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Bot', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_ststrm_bot_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiTwitchGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiTwitchGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_vgatga_id',
    1 => ':vgatga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stchan_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UserExperience', '\\IiMedias\\StreamBundle\\Model\\UserExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stuexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'UserExperiences', false);
        $this->addRelation('DeepBotImportExperience', '\\IiMedias\\StreamBundle\\Model\\DeepBotImportExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stdbie_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'DeepBotImportExperiences', false);
        $this->addRelation('MessageExperience', '\\IiMedias\\StreamBundle\\Model\\MessageExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stmexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'MessageExperiences', false);
        $this->addRelation('ChatterExperience', '\\IiMedias\\StreamBundle\\Model\\ChatterExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'ChatterExperiences', false);
        $this->addRelation('FollowExperience', '\\IiMedias\\StreamBundle\\Model\\FollowExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowExperiences', false);
        $this->addRelation('HostExperience', '\\IiMedias\\StreamBundle\\Model\\HostExperience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sthexp_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'HostExperiences', false);
        $this->addRelation('ViewDiffData', '\\IiMedias\\StreamBundle\\Model\\ViewDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stddat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewDiffDatas', false);
        $this->addRelation('ViewerData', '\\IiMedias\\StreamBundle\\Model\\ViewerData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stvdat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'ViewerDatas', false);
        $this->addRelation('FollowDiffData', '\\IiMedias\\StreamBundle\\Model\\FollowDiffData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stfdat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'FollowDiffDatas', false);
        $this->addRelation('StatusData', '\\IiMedias\\StreamBundle\\Model\\StatusData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stsdat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'StatusDatas', false);
        $this->addRelation('TypeData', '\\IiMedias\\StreamBundle\\Model\\TypeData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sttdat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'TypeDatas', false);
        $this->addRelation('GameData', '\\IiMedias\\StreamBundle\\Model\\GameData', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stgdat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'GameDatas', false);
        $this->addRelation('Stat', '\\IiMedias\\StreamBundle\\Model\\Stat', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ststat_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'Stats', false);
        $this->addRelation('Experience', '\\IiMedias\\StreamBundle\\Model\\Experience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stexpr_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', 'Experiences', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'stchan_created_at', 'update_column' => 'stchan_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to stream_channel_stchan     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        UserExperienceTableMap::clearInstancePool();
        DeepBotImportExperienceTableMap::clearInstancePool();
        MessageExperienceTableMap::clearInstancePool();
        ChatterExperienceTableMap::clearInstancePool();
        FollowExperienceTableMap::clearInstancePool();
        HostExperienceTableMap::clearInstancePool();
        ViewDiffDataTableMap::clearInstancePool();
        ViewerDataTableMap::clearInstancePool();
        FollowDiffDataTableMap::clearInstancePool();
        StatusDataTableMap::clearInstancePool();
        TypeDataTableMap::clearInstancePool();
        GameDataTableMap::clearInstancePool();
        StatTableMap::clearInstancePool();
        ExperienceTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChannelTableMap::CLASS_DEFAULT : ChannelTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Channel object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChannelTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChannelTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChannelTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChannelTableMap::OM_CLASS;
            /** @var Channel $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChannelTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChannelTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChannelTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Channel $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChannelTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_STSTRM_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_STSITE_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CHANNEL);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_STSTRM_BOT_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CAN_SCAN);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_PASSWORD_OAUTH);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CHANNEL_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CLIENT_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_SECRED_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CHANNEL_PARTNER);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_LIVE_MODE);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_STATUS);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_VGATGA_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_GAME);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_STREAM_TYPE);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_FOLLOWERS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_FOLLOWERS_DIFF);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_VIEWS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_VIEWS_DIFF);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_VIEWERS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CHATTERS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_HOSTS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_LIVE_VIEWERS_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_MESSAGES_COUNT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_LOCK_CHATTERS_SCAN);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_LOCK_HOSTS_SCAN);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_CREATED_AT);
            $criteria->addSelectColumn(ChannelTableMap::COL_STCHAN_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.stchan_id');
            $criteria->addSelectColumn($alias . '.stchan_ststrm_id');
            $criteria->addSelectColumn($alias . '.stchan_stsite_id');
            $criteria->addSelectColumn($alias . '.stchan_channel');
            $criteria->addSelectColumn($alias . '.stchan_ststrm_bot_id');
            $criteria->addSelectColumn($alias . '.stchan_can_scan');
            $criteria->addSelectColumn($alias . '.stchan_password_oauth');
            $criteria->addSelectColumn($alias . '.stchan_channel_id');
            $criteria->addSelectColumn($alias . '.stchan_client_id');
            $criteria->addSelectColumn($alias . '.stchan_secred_id');
            $criteria->addSelectColumn($alias . '.stchan_channel_partner');
            $criteria->addSelectColumn($alias . '.stchan_live_mode');
            $criteria->addSelectColumn($alias . '.stchan_status');
            $criteria->addSelectColumn($alias . '.stchan_vgatga_id');
            $criteria->addSelectColumn($alias . '.stchan_game');
            $criteria->addSelectColumn($alias . '.stchan_stream_type');
            $criteria->addSelectColumn($alias . '.stchan_followers_count');
            $criteria->addSelectColumn($alias . '.stchan_followers_diff');
            $criteria->addSelectColumn($alias . '.stchan_views_count');
            $criteria->addSelectColumn($alias . '.stchan_views_diff');
            $criteria->addSelectColumn($alias . '.stchan_viewers_count');
            $criteria->addSelectColumn($alias . '.stchan_chatters_count');
            $criteria->addSelectColumn($alias . '.stchan_hosts_count');
            $criteria->addSelectColumn($alias . '.stchan_live_viewers_count');
            $criteria->addSelectColumn($alias . '.stchan_messages_count');
            $criteria->addSelectColumn($alias . '.stchan_lock_chatters_scan');
            $criteria->addSelectColumn($alias . '.stchan_lock_hosts_scan');
            $criteria->addSelectColumn($alias . '.stchan_created_by_user_id');
            $criteria->addSelectColumn($alias . '.stchan_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.stchan_created_at');
            $criteria->addSelectColumn($alias . '.stchan_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChannelTableMap::DATABASE_NAME)->getTable(ChannelTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChannelTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChannelTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChannelTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Channel or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Channel object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\Channel) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChannelTableMap::DATABASE_NAME);
            $criteria->add(ChannelTableMap::COL_STCHAN_ID, (array) $values, Criteria::IN);
        }

        $query = ChannelQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChannelTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChannelTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_channel_stchan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChannelQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Channel or Criteria object.
     *
     * @param mixed               $criteria Criteria or Channel object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChannelTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Channel object
        }

        if ($criteria->containsKey(ChannelTableMap::COL_STCHAN_ID) && $criteria->keyContainsValue(ChannelTableMap::COL_STCHAN_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChannelTableMap::COL_STCHAN_ID.')');
        }


        // Set the correct dbName
        $query = ChannelQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChannelTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChannelTableMap::buildTableMap();

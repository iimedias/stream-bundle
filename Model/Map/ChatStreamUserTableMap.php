<?php

namespace IiMedias\StreamBundle\Model\Map;

use IiMedias\StreamBundle\Model\ChatStreamUser;
use IiMedias\StreamBundle\Model\ChatStreamUserQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'stream_chat_stream_user_stcsus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatStreamUserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.StreamBundle.Model.Map.ChatStreamUserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'stream_chat_stream_user_stcsus';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\StreamBundle\\Model\\ChatStreamUser';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.StreamBundle.Model.ChatStreamUser';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the stcsus_id field
     */
    const COL_STCSUS_ID = 'stream_chat_stream_user_stcsus.stcsus_id';

    /**
     * the column name for the stcsus_parent_id field
     */
    const COL_STCSUS_PARENT_ID = 'stream_chat_stream_user_stcsus.stcsus_parent_id';

    /**
     * the column name for the stcsus_ststrm_id field
     */
    const COL_STCSUS_STSTRM_ID = 'stream_chat_stream_user_stcsus.stcsus_ststrm_id';

    /**
     * the column name for the stcsus_stchan_id field
     */
    const COL_STCSUS_STCHAN_ID = 'stream_chat_stream_user_stcsus.stcsus_stchan_id';

    /**
     * the column name for the stcsus_stsite_id field
     */
    const COL_STCSUS_STSITE_ID = 'stream_chat_stream_user_stcsus.stcsus_stsite_id';

    /**
     * the column name for the stcsus_stcusr_id field
     */
    const COL_STCSUS_STCUSR_ID = 'stream_chat_stream_user_stcsus.stcsus_stcusr_id';

    /**
     * the column name for the stcsus_exp field
     */
    const COL_STCSUS_EXP = 'stream_chat_stream_user_stcsus.stcsus_exp';

    /**
     * the column name for the stcsus_strank_id field
     */
    const COL_STCSUS_STRANK_ID = 'stream_chat_stream_user_stcsus.stcsus_strank_id';

    /**
     * the column name for the stcsus_stavtr_id field
     */
    const COL_STCSUS_STAVTR_ID = 'stream_chat_stream_user_stcsus.stcsus_stavtr_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'StreamId', 'ChannelId', 'SiteId', 'ChatUserId', 'Exp', 'RankId', 'AvatarId', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'streamId', 'channelId', 'siteId', 'chatUserId', 'exp', 'rankId', 'avatarId', ),
        self::TYPE_COLNAME       => array(ChatStreamUserTableMap::COL_STCSUS_ID, ChatStreamUserTableMap::COL_STCSUS_PARENT_ID, ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID, ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID, ChatStreamUserTableMap::COL_STCSUS_STSITE_ID, ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID, ChatStreamUserTableMap::COL_STCSUS_EXP, ChatStreamUserTableMap::COL_STCSUS_STRANK_ID, ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID, ),
        self::TYPE_FIELDNAME     => array('stcsus_id', 'stcsus_parent_id', 'stcsus_ststrm_id', 'stcsus_stchan_id', 'stcsus_stsite_id', 'stcsus_stcusr_id', 'stcsus_exp', 'stcsus_strank_id', 'stcsus_stavtr_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'StreamId' => 2, 'ChannelId' => 3, 'SiteId' => 4, 'ChatUserId' => 5, 'Exp' => 6, 'RankId' => 7, 'AvatarId' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'streamId' => 2, 'channelId' => 3, 'siteId' => 4, 'chatUserId' => 5, 'exp' => 6, 'rankId' => 7, 'avatarId' => 8, ),
        self::TYPE_COLNAME       => array(ChatStreamUserTableMap::COL_STCSUS_ID => 0, ChatStreamUserTableMap::COL_STCSUS_PARENT_ID => 1, ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID => 2, ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID => 3, ChatStreamUserTableMap::COL_STCSUS_STSITE_ID => 4, ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID => 5, ChatStreamUserTableMap::COL_STCSUS_EXP => 6, ChatStreamUserTableMap::COL_STCSUS_STRANK_ID => 7, ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID => 8, ),
        self::TYPE_FIELDNAME     => array('stcsus_id' => 0, 'stcsus_parent_id' => 1, 'stcsus_ststrm_id' => 2, 'stcsus_stchan_id' => 3, 'stcsus_stsite_id' => 4, 'stcsus_stcusr_id' => 5, 'stcsus_exp' => 6, 'stcsus_strank_id' => 7, 'stcsus_stavtr_id' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stream_chat_stream_user_stcsus');
        $this->setPhpName('ChatStreamUser');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\StreamBundle\\Model\\ChatStreamUser');
        $this->setPackage('src.IiMedias.StreamBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('stcsus_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('stcsus_parent_id', 'ParentId', 'INTEGER', false, null, null);
        $this->addForeignKey('stcsus_ststrm_id', 'StreamId', 'INTEGER', 'stream_stream_ststrm', 'ststrm_id', true, null, null);
        $this->addForeignKey('stcsus_stchan_id', 'ChannelId', 'INTEGER', 'stream_channel_stchan', 'stchan_id', true, null, null);
        $this->addForeignKey('stcsus_stsite_id', 'SiteId', 'INTEGER', 'stream_site_stsite', 'stsite_id', true, null, null);
        $this->addForeignKey('stcsus_stcusr_id', 'ChatUserId', 'INTEGER', 'stream_chat_user_stcusr', 'stcusr_id', true, null, null);
        $this->addColumn('stcsus_exp', 'Exp', 'INTEGER', true, null, 0);
        $this->addForeignKey('stcsus_strank_id', 'RankId', 'INTEGER', 'stream_rank_strank', 'strank_id', false, null, null);
        $this->addForeignKey('stcsus_stavtr_id', 'AvatarId', 'INTEGER', 'stream_avatar_stavtr', 'stavtr_id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Stream', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_ststrm_id',
    1 => ':ststrm_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Site', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_stsite_id',
    1 => ':stsite_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Channel', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_stchan_id',
    1 => ':stchan_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChatUser', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_stcusr_id',
    1 => ':stcusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Rank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_strank_id',
    1 => ':strank_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Avatar', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':stcsus_stavtr_id',
    1 => ':stavtr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatStreamUserTableMap::CLASS_DEFAULT : ChatStreamUserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatStreamUser object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatStreamUserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatStreamUserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatStreamUserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatStreamUserTableMap::OM_CLASS;
            /** @var ChatStreamUser $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatStreamUserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatStreamUserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatStreamUserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatStreamUser $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatStreamUserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_PARENT_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STSTRM_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STCHAN_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STSITE_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STCUSR_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_EXP);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STRANK_ID);
            $criteria->addSelectColumn(ChatStreamUserTableMap::COL_STCSUS_STAVTR_ID);
        } else {
            $criteria->addSelectColumn($alias . '.stcsus_id');
            $criteria->addSelectColumn($alias . '.stcsus_parent_id');
            $criteria->addSelectColumn($alias . '.stcsus_ststrm_id');
            $criteria->addSelectColumn($alias . '.stcsus_stchan_id');
            $criteria->addSelectColumn($alias . '.stcsus_stsite_id');
            $criteria->addSelectColumn($alias . '.stcsus_stcusr_id');
            $criteria->addSelectColumn($alias . '.stcsus_exp');
            $criteria->addSelectColumn($alias . '.stcsus_strank_id');
            $criteria->addSelectColumn($alias . '.stcsus_stavtr_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatStreamUserTableMap::DATABASE_NAME)->getTable(ChatStreamUserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatStreamUserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatStreamUserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatStreamUserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatStreamUser or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatStreamUser object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatStreamUserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\StreamBundle\Model\ChatStreamUser) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatStreamUserTableMap::DATABASE_NAME);
            $criteria->add(ChatStreamUserTableMap::COL_STCSUS_ID, (array) $values, Criteria::IN);
        }

        $query = ChatStreamUserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatStreamUserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatStreamUserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the stream_chat_stream_user_stcsus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatStreamUserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatStreamUser or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatStreamUser object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatStreamUserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatStreamUser object
        }

        if ($criteria->containsKey(ChatStreamUserTableMap::COL_STCSUS_ID) && $criteria->keyContainsValue(ChatStreamUserTableMap::COL_STCSUS_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChatStreamUserTableMap::COL_STCSUS_ID.')');
        }


        // Set the correct dbName
        $query = ChatStreamUserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatStreamUserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatStreamUserTableMap::buildTableMap();

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\Experience as BaseExperience;
use \DateTime;
use Propel\Runtime\ActiveQuery\Criteria;

use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ExperienceQuery;

/**
 * Skeleton subclass for representing a row from the 'stream_experience_stexpr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Experience extends BaseExperience
{
    public static function createNewView(Channel $channel, ChatUser $chatUser)
    {
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('view')
            ->setMessage(null)
            ->setExp(0)
            ->setAt(new DateTime('now'))
            ->save();
        return $experience;
    }
    
    public static function createNewStatus(Channel $channel, ChatUser $chatUser, $status)
    {
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('status')
            ->setMessage($status)
            ->setExp(0)
            ->setAt(new DateTime('now'))
            ->save();
        return $experience;
    }
    
    public static function createNewLiveStatus(Channel $channel, ChatUser $chatUser, $status)
    {
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('live')
            ->setMessage($status)
            ->setExp(0)
            ->setAt(new DateTime('now'))
            ->save();
        return $experience;
    }
    
    public static function createNewGame(Channel $channel, ChatUser $chatUser, $status)
    {
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('game')
            ->setMessage($status)
            ->setExp(0)
            ->setAt(new DateTime('now'))
            ->save();
        return $experience;
    }
    
    public static function createNewFollow(Channel $channel, ChatUser $chatUser, DateTime $at)
    {
        $follows = ExperienceQuery::create()
            ->filterByChannel($channel)
            ->filterByChatUser($chatUser)
            ->filterByType('follow')
            ->find();
        $expAmount  = $follows->count() ? 0 : 500;
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('follow')
            ->setMessage(null)
            ->setExp($expAmount)
            ->setAt($at)
            ->save();
        return $experience;
    }
    
    public static function createNewHost(Channel $channel, ChatUser $chatUser)
    {
        $at = new DateTime('now');
        $at->modify('-' . intval($at->format('s')) . ' second');
        $lastHost = ExperienceQuery::create()
            ->filterByChannel($channel)
            ->filterByChatUser($chatUser)
            ->filterByType('host')
            ->filterByAt($at)
            ->findOne()
        ;
        if (!is_null($lastHost)) {
            return null;
        }
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('host')
            ->setMessage(null)
            ->setExp(1)
            ->setAt($at)
            ->save();
        return $experience;
    }
    
    public static function createNewConnection(Channel $channel, ChatUser $chatUser)
    {
        $at = new DateTime('now');
        $at->modify('-' . intval($at->format('s')) . ' second');
        $lastConnection = ExperienceQuery::create()
            ->filterByChannel($channel)
            ->filterByChatUser($chatUser)
            ->filterByType('connection')
            ->filterByAt($at)
            ->findOne()
        ;
        if (!is_null($lastConnection)) {
            return null;
        }
        $experience = new self;
        $experience
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setType('connection')
            ->setMessage(null)
            ->setExp(1)
            ->setAt($at)
            ->save();
        return $experience;
    }
    
    public static function createNewUnfollows(Channel $channel, ChatUser $chatUser, $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $unfollow = new Experience();
            $unfollow
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setSite($channel->getSite())
                ->setChatUser($chatUser)
                ->setType('unfollow')
                ->setMessage(null)
                ->setExp(0)
                ->setAt(new DateTime('now'))
                ->save()
            ;
        }
    }
}

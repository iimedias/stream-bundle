<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\StatusDataQuery as BaseStatusDataQuery;
use IiMedias\StreamBundle\Model\StatusData;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_status_data_stsdat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StatusDataQuery extends BaseStatusDataQuery
{
    public function getLastOfChannelOrCreate(Channel $channel, $status)
    {
        $statusData = self::create()
            ->filterBySite($channel->getSite())
            ->filterByStream($channel->getStream())
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne()
        ;

        if (is_null($statusData) || $statusData->getStatus() != $status) {
            $statusData = new StatusData();
            $statusData
                ->setSite($channel->getSite())
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setStatus($status)
                ->setAt(new DateTime())
                ->save()
            ;
        }

        return $statusData;
    }
}

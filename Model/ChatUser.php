<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ChatUser as BaseChatUser;

/**
 * Skeleton subclass for representing a row from the 'stream_chat_user_stcusr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ChatUser extends BaseChatUser
{
    public function updateViaTwitchChannelJson($channelJson)
    {
        if ($this->isNew() === false) {
            $this
                ->setDisplayName($channelJson['display_name'])
                ->setLogoUrl($channelJson['logo'])
            ;
            if ($this->isModified()) {
                $this->save();
            }
        }
        return $this;
    }
}

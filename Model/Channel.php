<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\Channel as BaseChannel;
use \DateTime;

/**
 * Skeleton subclass for representing a row from the 'stream_channel_stchan' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Channel extends BaseChannel
{
    public function updateViewsCount($viewsCount)
    {
        if ($this->isNew() === false) {
            $this
                ->setViewsCount($viewsCount)
                ->save()
            ;
        }
        return $this;
    }
    
    public function updateFollowersCount($followersCount)
    {
        if ($this->isNew() === false) {
            $this
                ->setFollowersCount($followersCount)
                ->save()
            ;
        }
        return $this;
    }
    
    public function updateViaTwitchChannelJson($channelJson)
    {
        if ($this->isNew() === false) {
            $this
                ->setChannelId($channelJson['_id'])
                ->setChannelPartner($channelJson['partner'])
                ->setViewsCount($channelJson['views'])
                ->setFollowersCount($channelJson['followers'])
            ;
            if ($this->isModified()) {
                $this->save();
            }
        }
        return $this;
    }
}

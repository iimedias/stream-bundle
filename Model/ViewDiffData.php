<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ViewDiffData as BaseViewDiffData;

/**
 * Skeleton subclass for representing a row from the 'stream_view_diff_data_stddat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ViewDiffData extends BaseViewDiffData
{

}

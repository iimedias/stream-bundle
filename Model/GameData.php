<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\GameData as BaseGameData;

/**
 * Skeleton subclass for representing a row from the 'stream_game_data_stgdat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GameData extends BaseGameData
{

}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ChatterExperience as BaseChatterExperience;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChatUser;
use \DateTime;

/**
 * Skeleton subclass for representing a row from the 'stream_chatter_experience_stcexp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ChatterExperience extends BaseChatterExperience
{
    public static function createNew(Channel $channel, ChatUser $chatUser, $type, DateTime $at)
    {
        $newConnection = new self;
        $newConnection
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setChatType($type)
            ->setExp(1)
            ->setAt($at)
            ->save()
        ;
        return $newConnection;
    }
}

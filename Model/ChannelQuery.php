<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ChannelQuery as BaseChannelQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_channel_stchan' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ChannelQuery extends BaseChannelQuery
{
    public static function getOneByIdAndSiteId($id, $siteId)
    {
        $result = self::create()
            ->filterById($id)
            ->filterBySiteId($siteId)
            ->findOne();
        return $result;
    }
    
    public static function getOneByIdAndSite($id, $site)
    {
        $result = self::create()
            ->filterById($id)
            ->filterBySite($site)
            ->findOne();
        return $result;
    }
    
    public static function getOneByChannelAndSite($channel, $site)
    {
        if (substr($channel, 0, 1) == "#") {
            $channel = substr($channel, 1);
        }
        
        $result = self::create()
            ->filterByChannel($channel)
            ->filterBySite($site)
            ->findOne();
        return $result;
    }
}

<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\TypeDataQuery as BaseTypeDataQuery;
use IiMedias\StreamBundle\Model\TypeData;
use IiMedias\StreamBundle\Model\Channel;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_type_data_sttdat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TypeDataQuery extends BaseTypeDataQuery
{
    public function getLastOfChannelOrCreate(Channel $channel, $type)
    {
        $typeData = self::create()
            ->filterBySite($channel->getSite())
            ->filterByStream($channel->getStream())
            ->filterByChannel($channel)
            ->orderByAt(Criteria::DESC)
            ->findOne()
        ;

        if (is_null($typeData) || $typeData->getStreamType() != $type) {
            $typeData = new TypeData();
            $typeData
                ->setSite($channel->getSite())
                ->setStream($channel->getStream())
                ->setChannel($channel)
                ->setStreamType($type)
                ->setAt(new DateTime())
                ->save()
            ;
        }

        return $typeData;
    }
}

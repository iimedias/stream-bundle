<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\ChatUserQuery as BaseChatUserQuery;
use IiMedias\StreamBundle\Model\ChatUser;

/**
 * Skeleton subclass for performing query and update operations on the 'stream_chat_user_stcusr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ChatUserQuery extends BaseChatUserQuery
{
    public static function getOneByUsernameAndSiteId($username, $siteId)
    {
        $result = self::create()
            ->filterByUsername($username)
            ->filterBySiteId($siteId)
            ->findOne();
        return $result;
    }
    
    public static function getOneByUsernameAndSite($username, $site)
    {
        $result = self::create()
            ->filterByUsername($username)
            ->filterBySite($site)
            ->findOne();
        return $result;
    }
    
    public static function getOneOrCreateByUsernameAndSiteId($username, $siteId)
    {
        $result = self::getOneByUsernameAndSiteId($username, $siteId);
        if (is_null($result)) {
            $result = new ChatUser();
            $result
                ->setUsername($username)
                ->setSiteId($siteId)
                ->save();
        }
        return $result;
    }
    
    public static function getOneOrCreateByUsernameAndSite($username, $site)
    {
        $result = self::getOneByUsernameAndSite($username, $site);
        if (is_null($result)) {
            $result = new ChatUser();
            $result
                ->setUsername($username)
                ->setSite($site)
                ->save();
        }
        return $result;
    }
}

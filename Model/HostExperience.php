<?php

namespace IiMedias\StreamBundle\Model;

use IiMedias\StreamBundle\Model\Base\HostExperience as BaseHostExperience;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChatUser;
use \DateTime;

/**
 * Skeleton subclass for representing a row from the 'stream_host_experience_sthexp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class HostExperience extends BaseHostExperience
{
    public static function createNew(Channel $channel, ChatUser $chatUser, DateTime $at)
    {
        $newHost = new self;
        $newHost
            ->setStream($channel->getStream())
            ->setChannel($channel)
            ->setSite($channel->getSite())
            ->setChatUser($chatUser)
            ->setExp(1)
            ->setAt($at)
            ->save()
        ;
        return $newHost;
    }
}

<?php

namespace IiMedias\StreamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StreamBundle\Model\TwitchBot;
use IiMedias\StreamBundle\Model\TwitchBotQuery;
use IiMedias\StreamBundle\Form\Type\TwitchBotType;

class TwitchBotController extends Controller
{
    /**
     * Index / Liste des projets en cours
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @Route("/admin/{_locale}/stream/twitchbots", name="iimedias_stream_twitchbot_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $bots = TwitchBotQuery::create()
            ->find();
        return $this->render('IiMediasStreamBundle:TwitchBot:index.html.twig', array(
                'bots'               => $bots,
                'moduleNavActiveTab' => 'twitchbot',
        ));
    }
    
    /**
     * Génération du formulaire d'ajout et d'édition d'un projet
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $formMode Mode du formulaire
     * @param integer $projectId Id du projet
     * @Route("/admin/{_locale}/stream/twitchbot/add", name="iimedias_stream_twitchbot_add", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "botId"=0})
     * @Route("/admin/{_locale}/stream/twitchbot/{botId}/edit", name="iimedias_stream_twitchbot_edit", requirements={"_locale"="\w{2}", "botId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function botForm(Request $request, $formMode, $botId)
    {
        switch ($formMode) {
            case 'edit':
                $bot            = TwitchBotQuery::create()
                    ->filterById($botId)
                    ->findOne();
                $formActionPath = $this->generateUrl('iimedias_stream_twitchbot_edit', array('botId' => $botId));
                break;
            case 'add':
            default:
                $bot            = new TwitchBot();
                $formActionPath = $this->generateUrl('iimedias_stream_twitchbot_add');
        }
        $form = $this->createForm(TwitchBotType::class, $bot, array('action' => $formActionPath));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $bot
                        ->setCreatedByUser($this->getUser())
                    ;
                }
                $bot
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_stream_twitchbot_index'));
            }
        }
        
        return $this->render('IiMediasStreamBundle:TwitchBot:botForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'bot'                => $bot,
                'moduleNavActiveTab' => 'twitchbot',
        ));
    }
}

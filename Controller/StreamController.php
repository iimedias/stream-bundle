<?php

namespace IiMedias\StreamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Propel\Runtime\ActiveQuery\Criteria;
use \DateTime;
use \DateTimeZone;

use IiMedias\StreamBundle\Model\Stream;
use IiMedias\StreamBundle\Model\StreamQuery;
use IiMedias\StreamBundle\Form\Type\StreamType;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Form\Type\ChannelType;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;
use IiMedias\StreamBundle\Model\ExperienceQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\Rank;
use IiMedias\StreamBundle\Model\RankQuery;
use IiMedias\StreamBundle\Form\Type\RankType;
use IiMedias\StreamBundle\Model\Avatar;
use IiMedias\StreamBundle\Model\AvatarQuery;
use IiMedias\StreamBundle\Form\Type\AvatarType;
use IiMedias\StreamBundle\Model\UserExperience;
use IiMedias\StreamBundle\Model\UserExperienceQuery;
use IiMedias\StreamBundle\Form\Type\UserExperienceType;

class StreamController extends Controller
{
    /**
     * Index / Liste des streams
     *
     * @access public
     * @since 1.0.0 30/08/2016 Création -- sebii
     * @Route("/admin/{_locale}/streams", name="iimedias_stream_stream_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $streams = StreamQuery::getAllStream();
        $bots    = StreamQuery::getAllBot();
        return $this->render('IiMediasStreamBundle:Stream:index.html.twig', [
            'streams'            => $streams,
            'bots'               => $bots,
            'moduleNavActiveTab' => 'stream',
        ]);
    }

    /**
     * Vue du stream
     *
     * @access public
     * @since 1.0.0 01/09/2016 Création -- sebii
     * @Route("/admin/{_locale}/stream/{streamId}", name="iimedias_stream_stream_view", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function view(Stream $stream)
    {
        $discordBot = null;
        foreach ($stream->getChannels() as $channel) {
            if ($channel->getSite()->getCode() === 'discord') {
                foreach ($channel->getBot()->getChannels() as $channelBot) {
                    if ($channelBot->getSite()->getCode() === 'discord') {
                        $discordBot = $channelBot;
                        break 2;
                    }
                }
                break;
            }
        }
        $sites = SiteQuery::getAll();

        return $this->render('IiMediasStreamBundle:Stream:view.html.twig', [
            'stream'             => $stream,
            'discordBot'         => $discordBot,
            'sites'              => $sites,
            'moduleNavActiveTab' => 'stream',
        ]);
    }

    /**
     * JS de la vue du stream
     *
     * @access public
     * @since 1.0.0 01/09/2016 Création -- sebii
     * @param Stream $stream
     * @Route("/admin/{_locale}/stream/{streamId}/view.js", name="iimedias_stream_stream_viewjs", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function viewJs(Stream $stream)
    {
        return $this->render('IiMediasStreamBundle:Stream:view.js.twig', ['stream' => $stream]);
    }

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param Stream $stream
     * @Route("/admin/{_locale}/stream/{streamId}/ranks", name="iimedias_stream_stream_ranks", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ranksList(Stream $stream)
    {
        $ranks = RankQuery::create()
            ->filterByStream($stream)
            ->find();
        return $this->render('IiMediasStreamBundle:Stream:ranks.html.twig', [
            'stream'             => $stream,
            'ranks'              => $ranks,
            'moduleNavActiveTab' => 'stream',
        ]);
    }
    
    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param Stream $stream
     * @Route("/admin/{_locale}/stream/{streamId}/avatars", name="iimedias_stream_stream_avatars", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function avatarsList(Stream $stream)
    {
        $avatars = AvatarQuery::create()
            ->filterByStream($stream)
            ->orderById(Criteria::DESC)
            ->find();
        return $this->render('IiMediasStreamBundle:Stream:avatars.html.twig', [
            'stream'             => $stream,
            'avatars'            => $avatars,
            'moduleNavActiveTab' => 'stream',
        ]);
    }
    
    /**
     * 
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param Stream $stream
     * @Route("/admin/{_locale}/stream/{streamId}/viewers.{_format}", name="iimedias_stream_stream_viewers", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "_format"="html"})
     * @Route("/{_locale}/stream/{streamId}/viewers.json", name="iimedias_stream_stream_viewers_public", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "_format"="json"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewersList(Stream $stream, $_format)
    {
        $viewers = UserExperienceQuery::create()
            ->useChatUserQuery()
                ->orderByUsername()
            ->endUse()
            ->filterByStream($stream)
            ->find();
        if ($_format === 'html') {
            return $this->render('IiMediasStreamBundle:Stream:viewersList.' . $_format . '.twig', [
                'stream'  => $stream,
                'viewers' => $viewers,
                'moduleNavActiveTab' => 'stream',
            ]);
        } elseif ($_format === 'json') {
            foreach ($viewers as $viewer) {
                $arrayReturn[$viewer->getChatUser()->getUsername()] = [
                    'id' => $viewer->getId(),
                    'username' => $viewer->getChatUser()->getUsername(),
                    'displayName' => (!is_null($viewer->getChatUser()->getDisplayName())) ? $viewer->getChatUser()->getDisplayName() : $viewer->getChatUser()->getUsername(),
                    'siteName' => $viewer->getSite()->getName(),
                    'siteFaIcon' => (!is_null($viewer->getSite()->getFontAwesomeIcon())) ? 'fa ' . $viewer->getSite()->getFontAwesomeIcon() : null,
                    'isFollowing' => $viewer->getIsFollowing(),
                    'rankId' => (!is_null($viewer->getRank())) ? $viewer->getRank()->getId() : null,
                    'rankName' => (!is_null($viewer->getRank())) ? $viewer->getRank()->getName() : null,
                    'avatarId' => (!is_null($viewer->getAvatar())) ? $viewer->getAvatar()->getName() : null,
                    'avatarColor' => (!is_null($viewer->getAvatar())) ? $viewer->getAvatar()->getMainColor() : null,
                    'avatarUrl' => (!is_null($viewer->getAvatar())) ? 'http://beta.sebii.fr/uploads/stream/avatars/' . $viewer->getAvatar()->getId() . '.png' : null,
                    'messagesCount' => $viewer->getMessagesCount(),
                    'followsCount' => $viewer->getFollowsCount(),
                    'chattersCount' => $viewer->getChattersCount(),
                    'hostsCount' => $viewer->getHostsCount(),
                ];
            }
            return new JsonResponse($arrayReturn, 200, array('Access-Control-Allow-Origin'=> '*'));
        }
    }

    /**
     * Vue du stream
     *
     * @access public
     * @since 1.0.0 01/09/2016 Création -- sebii
     * @Route("/admin/{_locale}/stream/{streamId}/json/main", name="iimedias_stream_stream_jsonmain", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function jsonMain(Stream $stream)
    {
        $twitchChannel = ChannelQuery::create()
            ->filterByStream($stream)
            ->useSiteQuery()
                ->filterByCode('twitch')
            ->endUse()
            ->findOne()
        ;

        $viewersCount = ExperienceQuery::create()
            ->filterByStream($stream)
            ->filterByType('connection')
            ->filterByAt(new DateTime('now -1 minute'), Criteria::GREATER_THAN)
            ->count()
        ;

        $hostsCount = ExperienceQuery::create()
            ->filterByStream($stream)
            ->filterByType('host')
            ->filterByAt(new DateTime('now -1 minute'), Criteria::GREATER_THAN)
            ->count()
        ;

        $messagesCount = ExperienceQuery::create()
            ->filterByStream($stream)
            ->filterByType(array('message', 'action'), Criteria::IN)
            ->count()
        ;

        $dataJson = [
            'viewsCount'            => $twitchChannel->getViewsCount(),
            'viewersCount'          => $viewersCount,
            'hostsCount'            => $hostsCount,
            'messagesCount'         => $messagesCount,
            'followsCount'          => $twitchChannel->getFollowersCount(),
            'subscribesCount'       => 0,
            'donationsTotal'        => 0,
            'bitsTotal'             => 0,
        ];

        return new JsonResponse($dataJson);
    }

    /**
     * json charts
     *
     * @access public
     * @since 1.0.0 01/09/2016 Création -- sebii
     * @Route("/admin/{_locale}/stream/{streamId}/json/charts/{end}/{period}", name="iimedias_stream_stream_jsoncharts", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "end"="now", "period"="hour"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function jsonCharts(Stream $stream, $end, $period)
    {
        /* Initialisation du tableau de retour */
        $dataJson = [
            'labels'      => [],
            'datasets'    => [
                ['label' => 'Spectateurs', 'data' => [], 'fill' => true, 'borderColor' => '#0000FF', 'lineTension' => 0.1],
                ['label' => 'Messages', 'data' => [], 'fill' => true, 'borderColor' => '#FF0000', 'lineTension' => 0.1],
                ['label' => 'Nouvelles Vues', 'data' => [], 'fill' => true, 'borderColor' => '#FF0000', 'lineTension' => 0.1],
                ['label' => 'Nouveaux Abonnements', 'data' => [], 'fill' => true, 'borderColor' => '#FF0000', 'lineTension' => 0.1],
            ],
            'options' => [],
        ];
        $templateResults = [];

        /* Calcul de la période de fin */
        if ($end == 'now') {
            $endPeriod = new DateTime('now');
        } elseif (is_numeric($end)) {
            $endPeriod = new DateTime();
            $endPeriod->setTimestamp(intval($end));
        } else {
            $endPeriod = new DateTime($end);
        }
        $endPeriod->modify('-' . intval($endPeriod->format('s')) . ' second');

        switch ($period) {
            case 'hour':
            default:
                /* Période de début */
                $beginPeriod = new DateTime();
                $beginPeriod->setTimestamp($endPeriod->getTimestamp() - 3600);

                /* Itérations et template basique */
                for ($i = 0, $name = intval($beginPeriod->format('i')); $i < 60; $i++, $name++) {
                    if ($name == 60) {
                        $name = 0;
                    }
                    $dataJson['labels'][] = $name;
                    $templateResults[]    = 0;
                }

                /* Element de date utilisé */
                $dataKey = 'i';
        }

        /* Application des templates */
        for ($i = 0; $i <= 3; $i++) {
            $dataJson['datasets'][$i]['data'] = $templateResults;
        }

        /* Filtrage des expériences */
        $experiences = ExperienceQuery::getAllByStreamAndPeriod($stream, $beginPeriod, $endPeriod);
        foreach ($experiences as $experience) {
            switch ($experience->getType()) {
                case 'connection':
                    $dataJson['datasets'][0]['data'][array_search(intval($experience->getAt()->format($dataKey)), $dataJson['labels'])]++;
                    break;
                case 'follow':
                    $dataJson['datasets'][3]['data'][array_search(intval($experience->getAt()->format($dataKey)), $dataJson['labels'])]++;
                    break;
                case 'message':
                    $dataJson['datasets'][1]['data'][array_search(intval($experience->getAt()->format($dataKey)), $dataJson['labels'])]++;
                    break;
                case 'view':
                    $dataJson['datasets'][2]['data'][array_search(intval($experience->getAt()->format($dataKey)), $dataJson['labels'])]++;
                    break;
                default:
            }
        }

        return new JsonResponse($dataJson);
    }

    /**
     * Vue du stream
     *
     * @access public
     * @since 1.0.0 01/09/2016 Création -- sebii
     * @Route("/admin/{_locale}/stream/{streamId}/json/chatusers", name="iimedias_stream_stream_jsonchatusers", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function jsonChatUsers(Stream $stream)
    {
        $responseArray = array(
            'count'     => 0,
            'chatUsers' => array(),
        );

        $chatUsers = ChatUserQuery::create()
            ->distinct()
            ->useExperienceQuery()
                ->filterByAt(new \DateTime('now -1 hour'), Criteria::GREATER_EQUAL)
                ->filterByType('connection')
                ->filterByStream($stream)
            ->endUse()
            ->find();

        $responseArray['count'] = $chatUsers->count();

        foreach ($chatUsers as $chatUser) {
            $lastActionOnStream = ExperienceQuery::create()
                ->filterByChatUser($chatUser)
                ->filterByStream($stream)
                ->orderByAt(Criteria::DESC)
                ->findOne();

            $responseArray['chatUsers'][] = array(
                'id'           => $chatUser->getId(),
                'username'     => $chatUser->getUsername(),
                'site'         => array(
                    'id'   => $chatUser->getSite()->getId(),
                    'name' => $chatUser->getSite()->getName(),
                    'code' => $chatUser->getSite()->getCode(),
                ),
                'lastActionAt' => array(
                    'format1'   => $lastActionOnStream->getAt()->format('d/m/Y H:i:s'),
                    'format2'   => $lastActionOnStream->getAt()->format('d M Y H:i:s'),
                    'timestamp' => $lastActionOnStream->getAt()->format('U'),
                ),
            );
        }

        return new JsonResponse($responseArray);
    }

    /**
     * Génération du formulaire d'ajout et d'édition d'un stream
     *
     * @access public
     * @since 1.0.0 30/08/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $formMode Mode du formulaire
     * @param integer $projectId Id du projet
     * @Route("/admin/{_locale}/stream/{formType}/add", name="iimedias_stream_stream_add", requirements={"_locale"="\w{2}", "formType"="stream|bot"}, defaults={"_locale"="fr", "formMode"="add", "formType"="stream", "streamId"=0})
     * @Route("/admin/{_locale}/stream/{streamId}/edit", name="iimedias_stream_stream_edit", requirements={"_locale"="\w{2}", "streamId"="\d+", "formType"="stream|bot"}, defaults={"_locale"="fr", "formMode"="edit", "formType"="stream"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function streamForm(Request $request, $formMode, $formType, $streamId)
    {
        switch ($formMode) {
            case 'edit':
                $stream         = StreamQuery::create()
                    ->filterById($streamId)
                    ->findOne();
                $formType       = $stream->getType();
                $formActionPath = $this->generateUrl('iimedias_stream_stream_edit', array('streamId' => $streamId));
                break;
            case 'add':
            default:
                $stream         = new Stream();
                $formActionPath = $this->generateUrl('iimedias_stream_stream_add', array('formType' => $formType));
        }
        $form = $this->createForm(StreamType::class, $stream, array('action' => $formActionPath));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $stream
                        ->setType($formType)
                        ->setCreatedByUser($this->getUser())
                    ;
                }
                $stream
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_stream_stream_view', array('streamId' => $stream->getId())));
            }
        }

        return $this->render('IiMediasStreamBundle:Stream:streamForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'formType'           => $formType,
                'stream'             => $stream,
                'moduleNavActiveTab' => 'stream',
        ));
    }
    
    /**
     * 
     * @param Request $request
     * @param Stream $stream
     * @param string $formMode
     * @param integer $rankId
     * @Route("/admin/{_locale}/stream/{streamId}/rank/add", name="iimedias_stream_rank_add", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "rankId"=0})
     * @Route("/admin/{_locale}/stream/{streamId}/rank/{rankId}/edit", name="iimedias_stream_rank_edit", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function rankForm(Request $request, Stream $stream, $formMode, $rankId)
    {
        switch ($formMode) {
            case 'edit':
                $rank = RankQuery::create()
                    ->filterById($rankId)
                    ->findOne();
                $formActionPath = $this->generateUrl('iimedias_stream_rank_edit', array('streamId' => $stream->getId(), 'rankId' => $rankId));
                break;
            case 'add':
            default:
                $rank    = new Rank();
                $rank->setStream($stream);
                $formActionPath = $this->generateUrl('iimedias_stream_rank_add', array('streamId' => $stream->getId()));
        }
        $form = $this->createForm(RankType::class, $rank, array('action' => $formActionPath, 'stream' => $stream));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $rank->setCreatedByUser($this->getUser());
                }
                $rank
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_stream_stream_ranks', array('streamId' => $stream->getId())));
            }
        }

        return $this->render('IiMediasStreamBundle:Stream:rankForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'stream'             => $stream,
                'rank'               => $rank,
                'moduleNavActiveTab' => 'stream',
        ));
    }
    
    /**
     * 
     * @param Request $request
     * @param Stream $stream
     * @param string $formMode
     * @param integer $avatarId
     * @Route("/admin/{_locale}/stream/{streamId}/avatar/add", name="iimedias_stream_avatar_add", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "avatarId"=0})
     * @Route("/admin/{_locale}/stream/{streamId}/avatar/{avatarId}/edit", name="iimedias_stream_avatar_edit", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function avatarForm(Request $request, Stream $stream, $formMode, $avatarId)
    {
        switch ($formMode) {
            case 'edit':
                $avatar = AvatarQuery::create()
                    ->filterById($avatarId)
                    ->findOne();
                $formActionPath = $this->generateUrl('iimedias_stream_avatar_edit', array('streamId' => $stream->getId(), 'avatarId' => $avatarId));
                break;
            case 'add':
            default:
                $avatar    = new Avatar();
                $avatar->setStream($stream);
                $formActionPath = $this->generateUrl('iimedias_stream_avatar_add', array('streamId' => $stream->getId()));
        }
        $form = $this->createForm(AvatarType::class, $avatar, array('action' => $formActionPath, 'stream' => $stream));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $avatar->setCreatedByUser($this->getUser());
                }
                $avatarPath = realpath(
                                $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR
                                . 'web' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'stream'
                                . DIRECTORY_SEPARATOR . 'avatars'
                            );
                $avatar
                    ->setCode(str_replace(array(' ', '-'), '', strtolower($avatar->getCode())))
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                $avatar->getAvatar()->move($avatarPath, $avatar->getId() . '.png');
                return $this->redirect($this->generateUrl('iimedias_stream_stream_avatars', array('streamId' => $stream->getId())));
            }
        }

        return $this->render('IiMediasStreamBundle:Stream:avatarForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'stream'             => $stream,
                'avatar'             => $avatar,
                'moduleNavActiveTab' => 'stream',
        ));
    }
    
    /**
     * 
     * @param Request $request
     * @param Stream $stream
     * @param string $formMode
     * @param integer $userExperienceId
     * @Route("/admin/{_locale}/stream/{streamId}/viewer/add", name="iimedias_stream_viewer_add", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "userExperienceId"=0})
     * @Route("/admin/{_locale}/stream/{streamId}/viewer/{userExperienceId}/edit", name="iimedias_stream_viewer_edit", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function viewerForm(Request $request, Stream $stream, $userExperienceId, $formMode)
    {
        switch ($formMode) {
            case 'edit':
                $userExperience = UserExperienceQuery::create()
                    ->filterById($userExperienceId)
                    ->findOne();
                $formActionPath = $this->generateUrl('iimedias_stream_viewer_edit', array('streamId' => $stream->getId(), 'userExperienceId' => $userExperienceId));
                break;
            case 'add':
            default:
                $userExperience = new UserExperience();
                $formActionPath = $this->generateUrl('iimedias_stream_viewer_add', array('streamId' => $stream->getId()));
        }
        $form = $this->createForm(UserExperienceType::class, $userExperience, array('action' => $formActionPath, 'stream' => $stream, 'formMode' => $formMode));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($userExperience->isNew()) {
                    $userExperience
                        ->setSiteId(1)
                        ->setStreamId(2)
                        ->setChannelId(3)
                    ;
                }
                $userExperience->save();
                return $this->redirect($this->generateUrl('iimedias_stream_stream_viewers', array('streamId' => $stream->getId())));
            }
        }
        
        return $this->render('IiMediasStreamBundle:Stream:viewerForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'stream'             => $stream,
                'userExperience'     => $userExperience,
                'moduleNavActiveTab' => 'stream',
        ));
    }

    /**
     * Génération du formulaire d'ajout et d'édition d'un stream
     *
     * @access public
     * @since 1.0.0 30/08/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $formMode Mode du formulaire
     * @param integer $projectId Id du projet
     * @Route("/admin/{_locale}/stream/{streamId}/channel/{siteName}/add", name="iimedias_stream_stream_channeladd", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "channelId"=0})
     * @Route("/admin/{_locale}/stream/{streamId}/channel/{channelId}/edit", name="iimedias_stream_stream_channeledit", requirements={"_locale"="\w{2}", "streamId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit", "siteName"=""})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function channelForm(Request $request, $formMode, Stream $stream, $siteName, $channelId)
    {
        switch ($formMode) {
            case 'edit':
                $channel        = ChannelQuery::create()
                    ->filterById($channelId)
                    ->findOne();
                $formActionPath = $this->generateUrl('iimedias_stream_stream_channeledit', array('streamId' => $stream->getId(), 'channelId' => $channelId));
                break;
            case 'add':
            default:
                $site    = SiteQuery::getOneByCode($siteName);
                $channel = new Channel();
                $channel
                    ->setSite($site)
                    ->setStream($stream)
                ;
                $formActionPath = $this->generateUrl('iimedias_stream_stream_channeladd', array('streamId' => $stream->getId(), 'siteName' => $siteName));
        }
        $form = $this->createForm(ChannelType::class, $channel, array('action' => $formActionPath, 'type' => $stream->getType()/*, 'siteName' => $channel->getSite()*/));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $channel
                        ->setCreatedByUser($this->getUser())
                    ;
                }
                $channel
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_stream_stream_view', array('streamId' => $stream->getId())));
            }
        }

        return $this->render('IiMediasStreamBundle:Stream:channelForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'stream'             => $stream,
                'channel'            => $channel,
                'moduleNavActiveTab' => 'stream',
        ));
    }

    /**
     *
     * @access public
     * @since 1.0.0 10/08/2017 Création -- sebii
     * @param Request $request
     * @param Stream $stream
     * @param Site $site
     * @Route("/admin/{_locale}/stream/{streamId}/channel/{channelId}/import", name="iimedias_stream_stream_import", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("stream", class="IiMedias\StreamBundle\Model\Stream", options={"mapping"={"streamId": "id"}})
     * @ParamConverter("channel", class="IiMedias\StreamBundle\Model\Channel", options={"mapping"={"channelId": "id"}})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function import(Request $request, Stream $stream, Channel $channel)
    {
        return $this->render('IiMediasStreamBundle:Stream:import.html.twig', [
            'stream'             => $stream,
            'channel'            => $channel,
            'moduleNavActiveTab' => 'stream',
        ]);
    }
}
